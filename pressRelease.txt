Press release

2/May/2018

Today, the editors of Libre Graphics magazine officially release a new project: the Libre Graphics Magazine Archives. Containing every article and image from the magazine's five-year run, the archive makes Libre Graphics magazine browsable and linkable for the first time. The archive can be found at http://archive.libregraphicsmag.com/

In addition to eleven feature articles, 17 columns, 27 showcases, and many, many other articles and images from the original run of the magazine, the archive also contains new writing. Larisa Blazic, Ana Isabel Carvalho, ginger coons, Julien Deswaef, and Ricardo Lafuente have contributed reflections on the state of libre graphics, while Christoph Haag, Sarah Magnan, Antonio Roberts, and Femke Snelting have answered a series of questions about their experiences in the vanguard of libre graphics practice and thought. 

In the introduction to the archive, ginger coons writes:

"For five years, between 2010 and 2015, we made Libre Graphics magazine. Every issue—all eight of them—contained writing, images, ideas we found worthwhile, thought-provoking, that showed off what Free/Libre and Open Source software, design, art, and culture could do and be. We wanted to not only document what was aleady happening, but also to inform what might. It was important for it to be a print magazine because we were graphic designers who had been trained to love print, to love paper, to love the feel of a magazine and the ability to pick it up, flip through it, tear a page out, leave it on a coffee table, hand it to a friend. It was a magazine not just for the eyes, but for the hands. There's a romance to that, and we were invested in it."

The archive brings Libre Graphics magazine to life in a new medium—the web. Supplementing the complete PDFs of all eight issues which continue to be available, the archive allows easier browsing, more direct access to the sourcecode of each issue, and markdown files of articles for easier porting and modification. All content in the archive is distributed under a Creative Commons Attribution-ShareAlike 3.0 license. 


About Libre Graphics magazine
Between 2010 and 2015, Libre Graphics magazine was a print publication which showcased excellent artistic work made with Free/Libre and Open Source Software (F/LOSS). Produced using techniques borrowed from F/LOSS development, the magazine was an early adopter of version control (Git) for editorial workflows. Over five years, eight issues of Libre Graphics magazine were published by a distributed team, with a core editorial team living in two countries. Libre Graphics magazine was produced using F/LOSS graphics tools, including GIMP, Scribus, Inkscape, Git, and ImageMagick, among many others. The magazine was distributed under a Creative Commons Attribution-ShareAlike 3.0 license.

The archive can be found at http://archive.libregraphicsmag.com/


For further enquiries: enquiries@libregraphicsmag.com
