#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
import os

''' Initial Pelican setup '''

PATH = 'content'
THEME = 'theme/lgmag'
BASE_PATH = os.path.dirname(os.path.realpath(__file__))
OUTPUT_PATH = "output/"

# caching
# CACHE_CONTENT = True
# LOAD_CONTENT_CACHE = True
# CONTENT_CACHING_LAYER = 'generator'
# CHECK_MODIFIED_METHOD = 'md5'
STATIC_CHECK_IF_MODIFIED = True
STATIC_CREATE_LINKS = True

''' Content '''

# AUTHOR = 'Libre Graphics magazine team'
SITENAME = 'Libre Graphics magazine archives'
SITEURL = 'https://archive.libregraphicsmag.com'
SITE_DESCRIPTION = "The LGmag archives"
SITE_LOGO = '/theme/images/libregraphicsmag-logo.png'
TIMEZONE = 'Europe/London'
FOOTERTEXT = 'Libre Graphics magazine is licensed under a <a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike</a> license (CC BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to <a href="https://libregraphicsmag.com/">Libre Graphics magazine</a>.'
# DEFAULT_LANG = 'en'
PLUGIN_PATHS = ["plugins", THEME + "/plugins"]
PLUGINS = ["assets", "autopages", "neighbors"]
CATEGORY_PAGE_PATH = "issue-pages"

DEFAULT_DATE_FORMAT = '%d %b %Y'

'''Other internal options'''

# order by filename
ARTICLE_ORDER_BY = 'source_path'

# Tag to use for featured posts (if you change this, be sure to also alter the templates)
FEATURED_TAG = 'featured'
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
DEFAULT_PAGINATION = False
# We use this to be able to omit the Date field from articles that do not need it
DEFAULT_METADATA = {'date': '2017-11-05'}
# Set up clean URLs
ARTICLE_URL = '{category}/{slug}/'
ARTICLE_SAVE_AS = '{category}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = '{slug}/'
CATEGORY_SAVE_AS = '{slug}/index.html'
DRAFT_URL = 'private/{slug}/'
DRAFT_SAVE_AS = 'private/{slug}/index.html'
AUTHORS_URL = 'authors/index.html'
TAGS_URL = 'tags/index.html'
# Don't create author urls
AUTHOR_SAVE_AS = ''
# Static pages setup
STATIC_SAVE_AS = '{path}'
STATIC_URL = '{path}'
STATIC_PATHS = [
    'images',
    'extra/htaccess',
    'extra/robots.txt',
    'extra/favicon.ico',
    'files',
]
EXTRA_PATH_METADATA = {
    'extra/htaccess': {'path': '.htaccess'},
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'files': {'path': 'files'},
    'images': {'path': 'images'},
}

# Don't remove periods from category slugs
# we use the "issue" part to remove it so that the slugifier can work
# see https://github.com/getpelican/pelican/issues/2316 for why this is so
CATEGORY_SUBSTITUTIONS = [('issue', '', True)]

# Set up webassets plugin
ASSET_CONFIG = (
    ('sass_style', 'compact'),
    ('cache', False),
    ('manifest', False),
    ('url_expire', False),
    ('versions', False),
    ('sass_debug_info', False),
)
ASSET_SOURCE_PATHS = ['static', 'static/css']
