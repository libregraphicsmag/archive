#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Scribus Extractor
(c) 2016 Manufactura Independente
Licensed under the terms of the GPL version 3 or any later version.

This is a bespoke converter to capture text elements in Scribus files and turn
them into Markdown. This was written specifically to convert issues of Libre
Graphics magazine, and will probably need tweaking for other use cases.

Run it like this:

    python extract.py document.sla

And it will create `document.md` in the directory of the original file.

Bugs:
  * Items are parsed in the wrong order and have to be ordered manually
  * It only parses text, no images -- we found images in the document using grep

'''
import bs4

# Change the values of these variables to the style names
# used in your document
H1_STYLE = "Header 28pt Bold"
H2_STYLE = "Header 10pt Caps Bold"
SMCAPS_STYLE = "Text Small Caps"
BOLD_STYLE = "Text Bold"
ITALIC_STYLE = "Text Italic"
IGNORED_STYLES = ("Section Bold",
                  "Section Regular Caps")


def filter_itext(textbit):
    content = textbit["CH"]
    cparent = textbit.get("CPARENT")
    # parent = textbit.get("PARENT")
    if cparent:
        if cparent in IGNORED_STYLES:
            return ""
        elif cparent == H1_STYLE:
            return "# " + content
        elif cparent == H2_STYLE:
            return "## " + content
        elif cparent == SMCAPS_STYLE:
            return content.upper()
        elif cparent == BOLD_STYLE:
            return "**%s**" % content
        elif cparent == ITALIC_STYLE:
            return "_%s_" % content
    return content


def main(filename):
    output = ""
    sla_xml = open(filename, 'r').read()
    soup = bs4.BeautifulSoup(sla_xml, 'xml')
    for obj in soup.findAll("PAGEOBJECT"):
        if obj.findAll("ITEXT"):
            '''
            for item in obj.contents:
                if type(item) == bs4.Tag:
                    print item
            print
            '''
            captured_text = ""
            for textbit in obj.findAll(["ITEXT", "para", "nbhyphen"]):
                if textbit.name == "ITEXT":
                    txt = filter_itext(textbit)
                    captured_text += txt
                elif textbit.name == "para":
                    captured_text += "\n\n"
                elif textbit.name == "nbhyphen":
                    captured_text += "-"

            if captured_text.strip():
                output += captured_text
                output += "\n\n--------------\n\n"
        else:
            pass
    import codecs
    outfile = codecs.open(filename.replace(".sla", ".md"), 'w', 'utf-8')
    outfile.write(output)
    outfile.close()


if __name__ == "__main__":
    import sys
    filename = sys.argv[1]
    main(filename)
