Title: New Releases
Section: New Releases

![](/images/2.4/new-releases-illustration.png)

## Conversations

A collection of interviews and conversations focusing on the more diffuse aspects of free software and free culture. Edited by Femke Snelting in collaboration with Christoph Haag.

<http://conversations.tools>

----

## Freeze+Press

A new F/LOSS-driven small press, dedicated to the explorative use of software for printmaking.

<http://freeze.sh>

----

## html2print

A new practical proposal for using web technologies to create print layouts.

<http://osp.kitchen/tools/html2print>

----

## Synfig Studio 1.0

The up-and-coming F/LOSS animation editor marks its 1.0 release with a respectable list of improvements and fixes.

<http://synfig.org>

----

## Computed Layout

An ongoing collection of the next generation of layout paradigms, focusing on CSS Print, generative layouts and alternative publishing tools.

<http://computedlayout.tumblr.com>

----

## Chainmail Bikini: The Anthology of Women Gamers

An anthology of comics by and for female gamers. An excellent compilation of stories and viewpoints on games and gender, prominently featuring our favourite typeface, PropCourier Sans. The PDF version is available, with the print version  out in September 2015.

<http://chainmail-bikini.com>

----

## RAM: Retales Analógicos de Medialab

The first edition of Medialab-Prado’s fanzine is out, in an effort to capture the many facets of digital culture that Medialab-Prado is known to nurture and grow, this time under the subject of Habitability.

<http://medialab-prado.es/article/fanzine>

----

## MediaGoblin 0.8.0

Dubbed “A Gallery of Fine Creatures,” this new release of MediaGoblin brings our favourite media gallery closer to full federation of content, Python 3 support and Gstreamer improvements. 

<http://mediagoblin.org>

----

## Valentina 0.3.3

The F/LOSS pattern drafting software for fashion design pushes on with a new version.

<http://valentinaproject.bitbucket.org>

----

## Antimony 0.8.0

A novel approach to 3D prototyping using a node-based interface, Antimony just put out a new version with a good set of improvements.

<http://www.mattkeeter.com/projects/antimony/3>

----

## Hybrid Lecture Player

A web platform that turns lecture documentation into multi-format publications.

<https://mcluhan.consortium.io/>

