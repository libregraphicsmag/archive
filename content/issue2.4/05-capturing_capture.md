Title: Capturing capture
Author: ginger coons
Section: Editor's letter

Informally, capture can be described as the fixation of information on a persistent medium; we can frame it as a way to register temporally ephemeral and/or spatially restricted objects or events. In this sense, capture becomes archival, and the scope of capturing is widening: as photography is now ubiquitous in the hands of everyone with a phone or cheap camera, we move to denser aspirations, with 3D scanning as the next milestone.

As human actions move to the digital medium, they become targets of capture. Computers acclimatized us to the notion of “error-free” storage. From occasional capture of our moments, we now continually capture more and more aspects of our lives, actions and trails by making backups, storing logs, updating timelines, archiving e-mail messages and syncing devices. Data storage is crystal clear, with a minimal margin for glitches or noise (save the occasional hardware failure). We can take snapshots of our own computer systems so that we can roll back undesired developments—capturing the moment is part of anyone’s computing experience. Capturing their state and freezing it into a register of a specific moment in time, that we can go back to anytime—moments like carbonite.

It appears that the more we capture, the more information we realize we emit. The “quantified self” perspective amplifies this to its limit, by proposing the capture of all possible outputs, from conversation subjects to micro-behaviours in our bodies. But does this make us grow? But can yet more data enlighten us? Contemporary artistic practices address that question. From the utilitarian purposes of the Internet Archive’s Wayback Machine, which allows people to see previous versions of web pages, we can contrast the Printing Out the Internet project by Kenneth Goldsmith, which appropriately highlights the shortcomings of capture and archival in an almost absurd mission statement to capture the World Wide Web into an analog medium. Such a statement highlights the impermanence of the Internet, the medium which was supposed to become the ultimate archive by uniting the world’s storage and computing power.

The still recent NSA surveillance scandal engraved the heavily political aspects of ubiquitous capture into mainstream awareness. It’s tough to adjust to the mindset that many more actions are now subject to capture by third-parties: real-world visual or audio recording gave way to blanket logging of all possible activity; it’s telling that Google has put the brakes on its mission to catalog the world’s knowledge to focus on the monetization of captured habits. The act of capturing therefore embodies a strong, and ambivalent, aspect of morality, offering the potential to safeguard history, as well as the threat of deep surveillance.

The Capture issue plays with these topics, with the hope that the projects showcased and ideas articulated give you pause for reflection, in a world that’s increasingly recording.

_ginger coons is a member of the Libre Graphics Editorial team._

