Title: Open Color 3D Scan
Authors: Anna Carreras, Carles Domènech, Mariona Roca
Section: Interview

![](/images/2.4/c3ds-1000-11.jpg)![](/images/2.4/c3ds-1000-12.jpg)

In a few years, 3D technologies will change the way of making things. 3D printing (or additive manufacturing) already allows anyone to create any shape from scratch based on a digital file: a new method of making which is affordable, environment-friendly and, most importantly, allows for endless possibilities of personalization and customization.

This ability to customize products is what might raise the interest of individuals and the demand for 3D technologies in the home: why not design and make things oneself instead of just being a consumer and buying things?

The revolution of 3D printing is just in its first stages and now is the time for experimenting, investigating and discovering unforeseen possibilities of a technology that is called to change the traditional systems of manufacturing. In this process, homemade 3D scanners will play a crucial role, as they enable the capture of physical objects and obtain 3D models which can be stored, modified and shared. 3D scanning provides a base model which can be used by everyone as a starting point for designing and replicating objects with little or no design experience.

The team of Open Color 3D Scan have designed a low cost 3D Scanner to be assembled and used at home to provide the opportunity to capture objects and experiment with all the possibilities of 3D scanning. It sports remarkable accuracy and resolution, halfway between commercial low cost scanners and high quality ones. Our 3D scanner is designed to be built at home with consumer technology that ensures an affordable hardware cost and which allows the exchange of some of its components to personalize it. The scanner works by using a webcam to locate the points at which two laser beams bounce off an object’s surface. The points are mapped out and turned into a point cloud 3D model.

Our 3D scanner is made up of the following main parts: a mountable structure, a laser triangulation system composed of two laser modules and a USB color camera, an Arduino controller, a small stepper motor with its driver and two USB cables. The software that runs the scanner is available for free download, and it can also be developed and improved as it is open source.

The main difficulty of building the scanner and ensuring its proper operation is setting up the precise angles and distances between the lasers, the camera and the object. It is critical that the pieces that make up the structure are hard-wearing and strongly clipped together. To meet these requirements and ensure accuracy in assembling the parts of the structure, we supply manual bending aluminum pieces with precise laser cut self-referenced nodes. These are tab and hole geometrical joints that allow for a perfect assembling of the parts with a minimum error margin and using only a few screws.

Once the structure is built up, the devices and controllers assembled and the software installed in the computer, the scanner is ready to run. For further scanning precision and optimization, the calibration of the camera can be easily adjusted once with a calibration device that is also supplied.

<http://www.opencolor3dscan.com>

----

![](/images/2.4/c3ds-1000-21.jpg)![](/images/2.4/c3ds-1000-22.jpg)

**Libre Graphics magazine team:How is the 3D scan scene developing? Is there a 3D scanning scene at all?**

**Open Color 3D Scan team**: Currently, we could speak of two trends in the 3D scanning scene. On the one hand, there are some commercial products developed and oriented to professionals. These are finished products built with complex technology at high prices. Those can of course have many uses, but as the prices are still very high, they are focused on the industry. On the other hand, 3D scanning is following the path started by 3D printing in the open source 3D technology and community. With simpler and more affordable technology, it is possible to build a 3D scanner and scan physical objects at a high quality resolution. These low-cost 3D scanners are, as we might say, in a primary state. The challenge is to build an affordable high quality technology that can be adapted to the different needs of professionals from different fields, such as game developers, physicians, architects or for art and heritage conservation, as well as to approach it to a broader audience.

**Where is the project at?**

We have designed a first prototype that has already been improved to a first Open Color 3D Scanner. Our initial goal is to put our scanner on the market of the 3D community and spread the 3D scanning technology so that it can be tested and improved. To reach the community, we are planning to launch a reward based crowdfunding campaign next autumn.

**Where are you thinking of going from here?**

The idea is to be capable to create a product that could be easily adapted to different professional needs, making minor changes and at an affordable price. With the collaboration of others, we aim to improve our product as well as to discover new possibilities we might not have thought about.

**What led you to start work on this field?**

As most technological inventions, this project started as a game of experimentation and discovery. Our mechanical engineer foresaw the possibilities of consumer and affordable technologies applied to 3D scanning and built the first scanner prototype at home. As we believe in open source technology and in the capacity of individuals to create their own products and improve the technology within the community, we decided to put our DIY 3D scanner on the market and open it to the world. We also intend to be a player in the 3D technology field alongside with the growing 3D printing market and applications.

![](/images/2.4/c3ds-1000-exp1.jpg)

**What are the backgrounds, skills and motivations of the team?**

We are a team of three people: a mechanical engineer, a telecommunications engineer and a journalist. We also have the collaboration of CDEI-UPC (Center for Industrial Equipment Design), a technology innovation center at the Polytechnic University of Catalonia (UPC). Carles Domènech, our mechanical engineer, has been working at CDEI developing projects of mechanical design and is currently the director of the Center. Anna Carreras, our telecommunications engineer, holds two MSc degrees, one in Telecommunication Systems Engineering and another in Information, Communication and Audiovisual Technologies. She has a 10 years expertise in designing several interactive technologies for artistic and commercial projects. The team is completed with Mariona Roca, bachelor in Audiovisual Communication and Translation Studies, who has been working as a copywriter and on the digital marketing field. Our motivation is to develop new projects aimed at a multidisciplinar technologic community and, as doing so, helping to spread the open source philosophy.

**What technologies are you using?**

We are using simple, affordable and consumer technologies: two lasers, a webcam, an Arduino minicontroller and a stepper motor. All of them are available and affordable devices that can be easily found in the market. Besides, the design of the hardware allows adaptation to a wide range of each of them, so that users can build the scanner with technology that they might already have.

**You placed the code under a copyleft license (CC BY-SA). Can you articulate the reasoning behind your license choice?**

We chose a copyleft license because we believe in empowering the community to use technology and develop it. We think making our work accessible might create the necessary feedback to adapt it to the different needs. We want our work and authorship to be recognized and referenced, but our main goal is to approach the technology to as many people as possible so we can all win with it. 

**What would be your advice for others who want to work on diy hardware projects?** 

We think the key goal of DIY hardware products is to find simple and creative solutions and reduce the limitations of the fabrication process to a minimum. So our advice is to make things simple and clear but, at the same time, design hardware that allows for minimum error to ensure that it works properly. Probably the most important thing to take into account is to find the balance between the cost, the accessibility to materials and to promote deeper knowledge of the technology and its features.

