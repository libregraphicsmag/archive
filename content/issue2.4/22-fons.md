Title: FONS: A recipe to make fonts out of bitmap images
Author: Stéphanie Vilayphiou
Section: Dispatch
Issue: 2.4
Slug: fons

## A little bit of history

In 2011, Pierre Marchand made with his own hands, in the laboratory of OSP, a creature called Fonzie.1 Besides being cool, Fonzie was able to make a typeface out of scanned letters. In his childhood, Fonzie asked for letters to behave well and to organize themselves. They had to not hold hands and ordered themselves by the order of the Unicode. The Univers Else2 family was born. In his teenage years, in 2013, Fonzie got more and more experienced. He could decipher letters from a scanned page of a book by using OCR (Optical Character Recognition). Fonzie could then read nice stories. But Fonzie is so cool that he’s going even further. With all those a’s or all those z’s, he was drawing an average shape of all those instances of letters. Fonzie collaborated with ScanBot,3 a coarse book scanner, product of the Dr. Michael Korntheuer, which could suck the pages with a vacuum cleaner to read the next page. But ScanBot couldn’t make it through the winter, and Fonzie kind of got stuck in his sweet sixteen and didn’t follow the new trends in the libraries of code. Today, it is rare to see Fonzie around; nor Pierre Marchand whose new lazy landscape is made in wænd.4

Fons is the spiritual son of Fonzie. He does not have much ambition (like reading novels and such). He just wants to learn the alphabet, to be able to recognize the letters when we point at them. He got GlyphTracer 1.35 of Jussi Pakkanen, as a master. GlyphTracer shouts glyphs and we would point at the letters on a scanned page. When done, he autotraces in the command line limbo and generates a nicely wrapped .sfd font. GlyphTracer went a step ahead and now autotraces the font with the FontForge toolbox so that we keep the history of the scan with the glyphs.

But Fons knew this was not enough. He has a good eye! If you feed him with an unripe scan, GlyphTracer would give you marshmallows. Fons then got trained by Pierre Huyghebaert, the bitmap master, and took good note. This is the recipe that you have in your screen.

## Tools needed



    T                                    \`.    T
    |    T     .--------------.___________) \   |    T
    !    |     |//////////////|___________[ ]   !  T |
         !     `--------------'           ) (      | !
                                          '-'      !


- GIMP
- AutoTrace
- FontForge with AutoTrace
- GlyphTracer (included with this package)Preparing the imaGE

## Preparing the image

Get a bitmap image of characters (crop unnecessary white parts for a faster process, you can use GIMP’s automatic crop for that).

![](/images/2.4/scan_bitmap.jpg)

<span>A scan in bitmap</span>

![](/images/2.4/scan_gray_1200dpi.jpg)

<span>A scan in gray levels</span>

![](/images/2.4/characters-table.png)

<span>A rasterized font: to typeset it, you can use the template `characters-table.svg` in the `/input/` folder.</span>

#### Levels (have a white background, black characters but still shades of gray)

![](/images/2.4/01-bitmap_levels.png)

#### Small boost

Scale up the image to something like 254% with the “Sinc (Lanczos 3)” algorithm. We choose on purpose a non-round number of scaling to break the bitmap patterns.

![](/images/2.4/02-scale-254percent.png)

#### Sharpen with "Unsharp mask"

Put the amount to the maximum and then search for the point where the radius feels right.

![](/images/2.4/03-sharpen-ok.png)![](/images/2.4/03-sharpen-too_few.png)![](/images/2.4/03-sharpen-too_much.png)

<span>Radius too small</span><span>OK</span><span>Radius too big</span>

#### Big boost

Scale up to 403%.

![](/images/2.4/04-big_boost.png)

#### Threshold

![](/images/2.4/05-threshold-bad.png)![](/images/2.4/05-threshold-ok.png)

<span>Bad</span><span>OK</span>

#### Baseline

When importing a bitmap, FontForge scales up/down the bitmap so that it fits into the glyph box. If we use GlyphTracer 1.4, this ends up with different scales of glyphs. In order to prevent this, we can put a black rectangle of the height of the maximum ascendant and maximum descendant.

![](/images/2.4/06-baseline.png)

#### Bitmap

- Change the color mode to Indexed mode with 1 black/white bit.
- Save as a `.bmp` file.

### Vectorizing with GlyphTracer

- To launch GlyphTracer, there are two possibilities:
  - from your file manager, double-click on the file `glyphtracer` inside the `glyphtracer-1.4` folder
  - from the terminal, go to the `glyphtracer-1.4` folder and run `./glyphtracer`. This will let you know more output in case of errors.
- Feed in the `.bmp` image in 1 bit (otherwise it will complain).
- For each given glyph, click on the letter you want to use.
- Change the characters subset in the bottom left dropdown menu to select more glyphs.

![](/images/2.4/glyphtracer.png)

### Post-production

#### Merging fonts

In case you want to complete an existing font, you can use the `mergeFonts.py` script.

    python mergeFonts.py fonte1.ufo fonte2.ufo ...  fonte17.ufo fonte-out.ufo

#### Metrics and kernings

A big part of type design is managing the white space around the letters (metrics) and exceptions for specific couples of letters (kerning).

- For metrics, we make an auto-spacing while generating the `.sfd` file with GlyphTracer.
- For kerning, you can try the Kernagic6 tool.

But if you want to get back metric and kerning data from an existing font, you can use the mergeSpacing.py script.

    python mergeSpacing.py font.otf original-font.otf spaced-font.otf

----

### Troubleshooting

#### My image is too big to manipulate.

Split it into several images and generate several `.sfd` files. Then you can merge the fonts with the `mergeFonts.py` script.

#### From the terminal, I have the following error

    File "./glyphtracer", line 415
        except Exception, e:
                          ^
    SyntaxError: invalid syntax

This means your FontForge runs with Python3, whereas the majority of installs still run with Python2. I didn’t find a convenient way yet to maintain both versions at the same time. Let me know and I’ll send you the version for Python3 which I use.

#### I have several shapes overlapping on the same glyph

This is probably because you generated the font twice. I’m afraid you’d have to close and restart GlyphTracer.

<http://osp.kitchen/tools/fons>

## References

1. Fonzie: <http://git.constantvzw.org/?p=osp.tools.fonzie.git>
2. Univers Else: <http://osp.kitchen/foundry/universelse>
3. ScanBot: <https://hackerspace.be/ScanBot>
4. Wænd: <http://waend.com>
5. GlyphTracer 1.3: <https://launchpad.net/glyphtracer>
6. Kernagic: <https://github.com/hodefoting/kernagic>
