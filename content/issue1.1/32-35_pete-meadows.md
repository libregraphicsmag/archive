Title: Illustrations by Pete Meadows
Author: Pete Meadows
Section: Showcase
Tags: Illustration

Pete Meadows is a Montreal-based illustrator and musician. His illustration process heavily features GIMP, which he uses to create the angular, but organic line quality and transparency that you see on these pages. 

![](/images/1.1/Bearbot1.jpg)

<span>Bearbot 1</span>

![](/images/1.1/Bearbot2.jpg)

<span>Bearbot 2</span>

![](/images/1.1/Bishops1.jpg)

<span>Bishops 1</span>

![](/images/1.1/Kittybot2.jpg)

<span>Kitty Bot 2</span>

![](/images/1.1/MinibuddyMeetsGargantuar1.jpg)

<span>Mini buddy meets Gargantuar 1</span>


