Title: Visual literacy: knowing through images
Author: Eric Schrijver
Section: Visual feature
Slug: visual-literacy

The experience of our own time is mediated through images, but we tend to represent the past in a verbal-discursive way[^1]. That means the model of the history book, with its emphasis on words and stories. In its attempts at establishing a reliable reputation, a resource like Wikipedia exhibits an extremely conservative take on representing knowledge[^2]: an image that goes along with an article is never more than an illustration in the literal sense of the word.

This why all the images we know of child labour show unhappy children: these pictures have been selected to fit to our story of the condemnation and subsequent abolition of child labour.

But when browsing through contemporary image archives, you will find most images show smiling children. This makes perfect sense: a photographer's visit is a special and exciting occasion.

It's the shock we get when confronted with these images that suddenly makes it possible for us to relate to our past. By freeing the image from its iconic role, we can stop seeing what's depicted as nothing but logical steps in a larger story. We can identify, both with the children who are depicted and with the photographer taking the picture.

![](/images/1.1/kid1.jpg)

![](/images/1.1/kid2.jpg)

![](/images/1.1/kid3.jpg)

<span>In the pictures taken for the American National Child Labor Comittee, most depicted children look happy.</span>

--------------

![](/images/1.1/hitler.jpg)

<span>Our archives house many photographs of Hitler, and he smiles in most of them.</span>

--------------

![](/images/1.1/mark-twain1.jpg)

![](/images/1.1/mark-twain2.jpg)![](/images/1.1/mark-twain3.jpg)

<span>When you see his picture on the back of a book, Mark Twain is a long dead writer.<br>
When you see the whole series, you see he was a super star.[^3]</span>

--------------

![](/images/1.1/whimsical-grotesk.jpg)![](/images/1.1/our-father.jpg) 

<span>Grotesk typefaces like Helvetica did not start out with the ‘neutrality’ imparted on them by the Swiss design school—they were whimsical display fonts.</span><span>Forms we employ for ironic effect did not start out that way.<br>All these are examples of how ‘reading’ the images from the past can show us how our own perception and norms have changed since then, allowing us to better understand both past and present.</span>


[^1]: [Wikipedia, the deeply conservative and traditional encyclopedia](http://brianna.modernthings.org/article/147/wikipedia-the-deeply-conservative-and-traditional-encyclopedia), All The Modern Things, 2008.
[^2]: [Interactieve presentatie handschriften](http://collecties.meermanno.nl/handschriften/) Museum Meermanno-Westreenianum, 2003.
[^3]: Inspired by spreads from [Fantastic Man](http://www.fantasticmanmagazine.com)
