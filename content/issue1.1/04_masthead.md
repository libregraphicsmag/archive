Title: Masthead
Section: Masthead
Slug: masthead-11

Libre Graphics Magazine 1.1  

**First encounters -- Taking flight**

November 2010  

ISSN: 1925-1416


## Editorial Team 

* [Ana Carvalho](mailto:ana@manufacturaindependente.org)  
* [ginger coons](mailto:ginger@adaptstudio.ca)  
* [Ricardo Lafuente](mailto:ricardo@manufacturaindependente.org)


## Publisher

[Studio XX](http://www.studioxx.org)


## Community Board

* Dave Crossland  
* Louis Desjardins  
* Aymeric Mansoux  
* Alexandre Prokoudine  
* Femke Snelting


## Contributors

* Margaret Burnett
* Dave Crossland
* Laura C. Hewitt
* John LeMasney
* Ludivine Loiseau (and her class)
* Pete Meadows
* Lila Pagola
* Eric Schrijver


Printed in Montréal by [Mardigrafe](http://mardigrafe.com) on recycled paper.  

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 

Write to us at [enquiries@libregraphicsmag.com](mailto:enquiries@libregraphicsmag.com)

<http://libregraphicsmag.com>

--------------


## Cover graphics

* Silver Bird's Nest illustration created from a [picture](http://www.flickr.com/photos/perpetualplum/3598355084/#/) by Flickr user PerpetualPlum  
* Rainbow gradient found on a [post by user Erroneus](http://www.inkscapeforum.com/viewtopic.php?f=5&t=5470) on InkscapeForum.com


## Images under a CC Attribution Share-Alike license

* Photo of ginger coons by herself.
* Photo of Dave Crossland by Mary Crossland.
* Photo of Eric Schrijver by himself.
* Photos of VTF type specimens in "Notebook" section by ginger coons.
* Type specimen in "F/LOSS in the classroom" by Ludivine Loiseau.
* Illustrations in "The unicorn tutorial" by ginger coons.
* Images in "Applying F/LOSS as a final user and not dying in the attempt" by Lila Pagola.
* Photo of Ben Laenen by Dave Crossland.
* All images in the "Showcase" section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.
* [Fontmatrix screenshot](http://en.wikipedia.org/wiki/File:Screenshot_Fontmatrix.png) from Resources section by Wikipedia user Gürkan Sengün. 
* Inkscape screenshot from Resources section by Ana Carvalho.


## Images under other licenses

* Photos and images of OSP DLF/Nancy in "Notebook" section by Open Source Publishing. Free Art License.
* DejaVu specimen page adapted from a [specimen by Benjamin D. Esham](http://dejavu-fonts.org/wiki/File:DejaVu_specimen.png). Public Domain. 
* [Blender screenshot](http://en.wikipedia.org/wiki/File:Blender3D_2.4.5-screen.jpg) from Resources section by Wikipedia user DingTo. GNU General Public License. 
* FontForge screenshot from Resources section by George Williams from the tutorial [FontForge, An Outline Font Editor](http://fontforge.sourceforge.net/overview.html). Assumed to be covered by a version of the Modified BSD license. Anyone in doubt should direct themselves to <http://fontforge.sourceforge.net/license.html>.
* [GIMP screenshot](http://en.wikipedia.org/wiki/File:Gimpscreen.png) from Resources section by Wikipedia user IngerAlHaosului. GNU General Public License. 
* [Scribus screenshot](http://en.wikipedia.org/wiki/File:Scribus-1.3-Linux.png) from Resources section by Wikipedia user Kocio. GNU General Public License. 

## General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.
