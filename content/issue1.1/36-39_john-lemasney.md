Title: 365 Sketches
Author: John LeMasney
Section: Showcase
Tags: Inkscape, Illustration


John LeMasney is a designer, artist, writer, poet, technologist, open web advocate and open source evangelist.

John started his project 365Sketches in January 2010. His goal was to produce one sketch per day using only Inkscape. He would then publish it in his blog <http://365sketches.org>, set up in Wordpress, under a CC-BY-SA license. The underlying purpose of John's daily exercise was to improve his skills using Inkscape but, as he told us, the result was deeper than that. In his own words:

_I've created a daily reminder for myself and others of the power of open source. I've gathered a community of about 200 people who watch the project, about 20 real fans, and I've gotten a lot of design and consulting work. I've also made quite a few friends. I feel like I'm doing my part to help develop, advocate and advertise Inkscape._

John's plans are to go on with the project, drawing upon different tools: in 2011, GIMP would be the tool of choice, whereas Blender might be slated for 2012. In the following pages, you can see a small sample of John's work. To look through the whole project, do visit his blog.

![](/images/1.1/20101021firmstance.jpg)

![](/images/1.1/montage1.png)

![](/images/1.1/montage2.png)

![](/images/1.1/montage3.png)
