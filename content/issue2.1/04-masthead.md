Title: Masthead
Section: Masthead
Slug: masthead-21

Libre Graphics Magazine 2.1

**Localisation/Internationalization**

February 2013

ISSN: 1925-1416


## Editorial Team 

* [Ana Carvalho](mailto:ana@manufacturaindependente.org) 
* [ginger coons](mailto:ginger@adaptstudio.ca) 
* [Ricardo Lafuente](mailto:ricardo@manufacturaindependente.org)


## Copy editor

Margaret Burnett


## Publisher

ginger coons


## Community Board

* Dave Crossland
* Louis Desjardins
* Aymeric Mansoux
* Alexandre Prokoudine
* Femke Snelting


## Contributors

* Davide della Casa
* Constant
* Dave Crossland
* Denis Jacquerye
* Julian Oliver
* Nikki Pugh
* Pierros Papadeas
* Antonio Roberts
* Eric Schrijver
* Susan Spencer
* Danja Vasiliev
* Vinzenz Vietzke


Printed in Porto by [Cromotema](www.cromotema.pt) on recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 

Write to us at [enquiries@libregraphicsmag.com](mailto:enquiries@libregraphicsmag.com)

<http://libregraphicsmag.com>

Our repositories with all source material for the magazine can be found at <http://gitlab.com/libregraphicsmag>


## Images under a CC Attribution Share-Alike license

* Photo of ginger coons by herself. 
* Photo of Ricardo Lafuente by Ana Carvalho. 
* Photo of Ana Carvalho by Luís Camanho. 
* Photo of Antonio Roberts by Emily Davies. 
* Photo of Eric Schrijver by himself.
* Notebook Mozilla Festival: photos by Flickr user paul\_clarke. Image names, from top left to bottom right: `Mozfest_10Nov_011`, `Mozfest_11Nov_048`, `Mozfest_11Nov_052`, `Mozfest_11Nov_010`, `Mozfest_10Nov_134`, `Mozfest_11Nov_004`, `Mozfest_11Nov_069`.
* ["Crafting type" photos](http://www.flickr.com/photos/workturn/sets/72157631445394770) by [Rob & Lauren Lim](http://robnlauren.com).
* All the sketches from "Sketching Creative Code" can be found at http://www.flickr.com/photos/11701517@N00/sets/72157631514318200. Sketches names and authors, by order of appearance: 
  - Page 16:
    - [Suffragette](http://www.sketchpatch.net/view/9Zwh66llS8u) by Sophie McDonald. 
  - Page 17, from the top to bottom: 
    - [Stretchy Points Play v2](http://www.sketchpatch.net/view/Lqr5gWHLlg0) by DARYL.Gamma and charles\_m\_dietrich. 
    - [Tapestry 4](http://www.sketchpatch.net/view/yEeWLKQDCrM) by DARYL.Gamma. 
    - [Peaks v3](http://www.sketchpatch.net/view/ZC83eYHWq1r) by Kim Asendorf and DARYL.Gamma.
    - [Knots Weave v3](http://www.sketchpatch.net/view/oOLMrflXAK8) by DARYL.Gamma. 
  - Page 18, from the top left to the bottom right: 
    - [Kind Of Pollock](http://www.sketchpatch.net/view/GeIursTDPjj) by DARYL.Gamma and eiichiishii1. 
    - [Cushion](http://www.sketchpatch.net/view/L8arQDcCGYZ) by DARYL.Gamma and conroy. 
    - [80's mania](http://www.sketchpatch.net/view/naotkeywUPi) by Sophie McDonald. 
    - [Where Is She Going](http://www.sketchpatch.net/view/auPpktqHG9o) by Sophie McDonald and DARYL.Gamma. 
  - Page 19: 
    - [Magma Jumping Peaks v2](http://www.sketchpatch.net/view/N6fOWgdKacJ) by Kim Asendorf, DARYL.Gamma.
* Credits for the photos of the piece "La Langue Schaerbeekoise/De Schaarbeekse Taal." Photos from pages 30, 31, 33 34 and 35 by Pablo Castilla (with the exception of the first top photo in page 34). All other photos are by Constant.
* All images in the "Showcase" section can be attributed to the creators mentioned therein. All are licensed CC BY-SA with the exception of "La Langue Schaerbeekoise/De Schaarbeekse Taal" which is under the Free Art License.

## Images under other licenses

* Illustrations from the "Picture Feature" are in the Public Domain and can be found on WikiMedia Commons.
* [Denis Jacquerye's photo](http://gallery3.constantvzw.org/index.php/LGRU-Friday-Shared-Vocabularies/P2245659) by Michael Murtaugh under the Free Art License.

GeneralAdvertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

