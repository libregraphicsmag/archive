Title: Mozilla Festival
Section: Notebook

![](/images/2.1/8171657974_d966e22c78_o.jpg)

Where: Mozilla Festival, London, UK. 

![](/images/2.1/8172471074_375c0cfd46_o.jpg)

What: A three day, massively multi-track festival organized by Mozilla, promoting the Open Web and citizen involvement in networked media.

![](/images/2.1/8174842269_251ae2a10b_o.jpg)

Best vehicle: Over the weekend, a radio-controlled blimp roamed the building, taking advantage of the open spaces and connecting atria to pay visits to different corners of the festival.

![](/images/2.1/8174937591_a6964474e0_o.jpg)

Most wanted: A pre-release version of Firefox OS was on display, promising to bring some new excitement to the smartphone operating system space. Operating under the tagline “the web is the platform,” Firefox OS is based on languages already standard in web development.

![](/images/2.1/8175217205_716a1d82ab_o.jpg)

Best mash-up: Developers and journalists hacking on open data, accessible public information and citizen political action spent their weekend showing off existing open journalism projects, and building the next wave.

![](/images/2.1/8175567689_f8ae2cf3d4_o.jpg)

Biggest release: Building on the already popular Popcorn.js framework, Popcorn Maker was released to massive cheers. Promising easy development of contextually-enhanced video, Popcorn Maker is an easy, WYSIWYG interface for integrating text, web services and other assets into web video.

![](/images/2.1/8175620912_1b176a6a13_o.jpg)
