Title: Pruning branches
Authors: Ana Isabel Carvalho, Ricardo Lafuente
Section: Production colophon

![](/images/2.1/git-cleaning.jpg)

After the end of our first volume, we met in Brussels to carry out the Libre Graphics magazine camp—a week of reviewing and planning for volume 2.

One of our main tasks was housekeeping. Besides simplifying and updating our website, we spent quite some time taking care of our Git repositories. Our main problem was that all issues and other assets were together in a single, 8GB repository. A certain lack of discipline and knowledge about Git workflows ended up in a lot of cruft and leftover files that were mostly filling up space. On the other hand, we thought it would be useful to split our huge repo into smaller ones for better structure. 

In the process, we created a few tools that helped us manage and prune the existing repositories. You can find them in the new ‘productiontools’ repo. Here are the other repositories that you’ll find in our Gitorious page (see link below):

* **vol1issue1** 
* **vol1issue2**
* **vol1issue3** 
* **vol1issue4**
* **vol2issue1**
* **propcouriersans**: The official magazine typeface 
* **documentation**: Photos, presentations, event coverage and other documentation 
* **persistent**: General magazine assets (logo, columnists’ photos, editing and style guides...) 
* **productiontools**: Git and Scribus scripts written to help with a magazine workflow

In case you cloned our old repository before, you can safely trash it and clone the new ones—their file size is about half as large now.

### Wiki

We had also created a wiki, running on the popular MediaWiki software. We used it as a great resource for syncing our notes and plans, but soon ran into problems. Automated spambots are eager to dive into MediaWiki installations, and that’s what happened to our small wiki, where garbled usernames scribbled strange ads into our carefully-written notes. We spent a few hours cleaning up the mess and installing anti-spam measures, but even then we were getting seriously hit: even unable to create accounts or edit our wiki, the spambots kept fetching our pages repeatedly and non-stop. We decided to pull the plug on MediaWiki, and are now working to port the content to a smaller DokuWiki installation that will, hopefully, be easier to maintain.

### Online articles

Since the beginning of the Libre Graphics magazine project, we’ve discussed often how to combine print and web. Our priority always was to publish a physical version, but at the same time we wanted a good and clean way to provide the articles online in sensible formats. This would imply serious thought and work into selecting, installing and maintaining a CMS, as well as a set of scripts that can ensure that the online content is directly imported from the magazine repository. While an online article repository has always been on our collective TODO lists, we somehow kept it in a mid-to-low priority—print is our main focus. 

In the meantime, we received a great tip from @TheRealPomax on Twitter: use the fantastic Pdf.js library from Mozilla to display the PDF versions of the magazine in your browser, without the need of a full download. This will be our next step after sending the issue you are reading to the printer.

Note that our production repositories, where all text, layouts and assets live, are publically available on Gitorious, so feel free to clone and look into what’s inside!

<http://gitorious.org/libregraphicsmag>

--------------


