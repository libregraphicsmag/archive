Title: Masthead
Section: Masthead
Slug: masthead-14

Libre Graphics Magazine 1.4

**The physical, the digital and the designer**

March 2012

ISSN: 1925-1416


## Editorial Team 

* [Ana Carvalho](mailto:ana@manufacturaindependente.org)  
* [ginger coons](mailto:ginger@adaptstudio.ca)  
* [Ricardo Lafuente](mailto:ricardo@manufacturaindependente.org)


## Copy editor

Margaret Burnett


## Publisher

ginger coons


## Community Board

* Dave Crossland
* Louis Desjardins
* Aymeric Mansoux
* Alexandre Prokoudine
* Femke Snelting


## Contributors

* Dave Crossland
* Maria Figueiredo
* Nelson Gonçalves
* Eric de Haas
* Richard Hughes
* Jonathan Puckey
* Eric Schrijver

Printed in Porto by [Cromotema](http://cromotema.pt) and in Toronto by [Green Printer](http://www.greenprinteronline.com) in recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 

Write to us at enquiries@libregraphicsmag.com

<http://libregraphicsmag.com>

## Images under a CC Attribution Share-Alike license

* Photo of ginger coons by herself.
* Photo of Ricardo Lafuente by Ana Carvalho.
* Photo of Ana Carvalho by Luís Camanho.
* Photo of Dave Crossland by Mary Crossland.
* Photo of Eric Schrijver by himself.
* Illustrations in “Type Design” based on “1032056”, image by Flickr user fdctsevilla.
* Illustrations in “Dispatch”, page 20, by Natacha Cindy and Hugo Ferreira.
* Photos in “Notebook”, in order of appearance, by Pedro Caetano, Bruno Fernandes, ginger coons and Aloysio Araripe. All photos, with the exception of ginger coons' photo, can be found at Flickr, in the photostream of the user festivalculturadigitalbr.
* All images in the “Showcase” section can be attributed to the creators mentioned therein. All are licensed CC BY-SA with the exception of “I heart lightning” in the article “The graphic side of 3D printing” which is licensed under CC BY.
* Illustrations in “Folds, impositions and gores: an interview with Tom Lechner”, by Tom Lechner.
* Photos in “Colorhug: filling the F/LOSS colour calibration void” by Richard Hughes.
* Map images in "Best of SVG" are from OpenStreetMap contributors.


## Images under other licenses

* Photos in “Dispatch”, page 21 by Fátima Barreiros are under CC-NC-SA. They were published here with the permission of their author.
* [Compass rose image](http://commons.wikimedia.org/wiki/File:Modern_Nautical_Compass_Rose.png) used in “Best of SVG”, by Wikimedia user Themadchopper is in the public domain.


## General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.


