Title: Best of SVG
Section: Best of

We at Libre Graphics magazine have a thing for open standards. We like their transparency and their interoperability. We like that, with a well documented standard, everyone has an equal chance to play nicely together. 

That's why we like SVG so much. It's a well developed, well supported standard brought to us by the World Wide Web Consortium (W3C). It's available for implementation by anyone developing software. It shows up in modern browsers, fine vector graphics editors and any number of other places. 

One thing that's missing, though, is you: the designer, the artist, the illustrator. So put down that .ai file and check out SVG.

![](/images/1.4/spread.png)

Even blank maps can tell a history. 

Just browse the colletion of Blank World Maps offered by Wikimedia Commons and OpenStreetMap, set your eyes on the ever changing boundaries of countries and nations.

Being SVG, they are doubly useful: one, you get resolution-independent maps, allowing you to scale them without mercy. Two, SVG makes them perfect for including in a web application or visualization, creating experimental programs around geographic data, and for clever maps that embed additional cartographic data in the file itself.

It's a journey through history in SVG shapes.

—the editors
