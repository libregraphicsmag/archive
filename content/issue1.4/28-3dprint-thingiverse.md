Title: The graphic side of 3D printing
Section: Showcase
Slug: 3d-print

There's a little revolution taking place in basements, garages and hackerspaces. 3d printers—machines which turn digital meshes into physical objects—are getting cheaper, better and more popular. We've collected samples of some strikingly graphic 3d prints, designed with F/LOSS tools.

All of the projects featured in this showcase are available from Thingiverse, under permissive licenses, for printing and modification.

<http://thingiverse.com>

![](/images/1.4/emmett-betterDNA1.JPG)![](/images/1.4/emmett-thingiverse-C_display_large.jpg)

<span>DNA Playset (OpenSCAD), Emmett Lalish: <http://www.thingiverse.com/thing:17343></span>

![](/images/1.4/amyhurst-thingiverse-heart-lightning_display_large.jpg)![](/images/1.4/amyhurst-thingiverse-heart-lightning_display_primitive_large.jpg)

<span>I heart lightning (Inkscape & OpenSCAD), Amy Hurst: <http://www.thingiverse.com/thing:17495></span>

![](/images/1.4/emmett-thingiverse-airplaneCC_display_large.jpg)![](/images/1.4/emmett-thingiverse-B1_display_large.jpg)

<span>Airplane cookie cutters (Inkscape), user: emmett on Thingiverse: <http://www.thingiverse.com/thing:11098></span>

![](/images/1.4/LukeChilson-thingiverse-IMG_2901_display_large.jpg)![](/images/1.4/LukeChilson-thingiverse-watercanyon_display_large.jpg) 

<span>Lake and mountain topography (Blender), Luke Chilson: <http://www.thingiverse.com/thing:7207></span>



