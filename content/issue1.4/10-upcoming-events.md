Title: Upcoming Events
Section: Upcoming events

--------------

28-29 APR



--------------

Linux Fest Northwest

Bellingham, United States

http://linuxfestnorthwest.org

--------------

Upcoming events



--------------

We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and F/LOSS. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the F/LOSS designer at a traditional design event, or maybe the designer at a predominantly software developer event. 



--------------

2-5MAY

--------------

Libre Graphics Meeting

Vienna, Austria

http://libre-graphics-meeting.org/2012

--------------

1

JUN

--------------

Drupal Business Summit

Vancouver, Canada

https://www.drupalsummit.com

--------------

6-8 

JUN

--------------

Linux Con Japan

Yokohama, Japan

https://events.linuxfoundation.org/events/linuxcon-japan

--------------

8-10

JUN

--------------

Southeast LinuxFest

Charlotte, NC, United States

http://www.southeastlinuxfest.org

--------------

30 AUG

3 SEP

--------------

Ars Electronica Festival

Linz, Austria

http://www.aec.at/thebigpicture/en

--------------

9-11

NOV

--------------

Mozilla Festival 2012

London, United Kingdom

https://www.mozillafestival.org

--------------

18-20

MAY

--------------

SIGINT

Cologne, Germany

http://sigint.ccc.de

--------------

11-14

SEP

--------------

SVG Open & The Graphical Web

Zurich, Switzerland

http://www.svgopen.org/2012

--------------

10-18

NOV

--------------

HTMlles 10

Montreal, Canada

http://www.htmlles.net/2012

--------------

25-26

MAY

--------------

Flossie 2012

London, United Kingdom

http://www.flossie.org

--------------

25-28

JUL

--------------

Fisl 2012

Porto Alegre, Brazil

http://softwarelivre.org/fisl12

--------------

15

JUN

--------------

Ampersand 2012

Brighton, United Kingdom

http://2012.ampersandconf.com
