Title: CulturaDigital.Br Festival
Section: Notebook
slug: cultura-digital-br

### Where

CulturaDigital.Br Festival, Rio de Janeiro, Brazil. 

![](/images/1.4/6477038565_004a8b40c1_o.jpg)

### What

Brazilian free culture festival, bringing together creators and community organizations.

![](/images/1.4/6477052147_96d39e3121_o.jpg)

### Easiest How-To

Mimi Hui, a member of nyc Resistor took the opportunity during a talk about rfid technology to explain the construction of aluminum foil Faraday cages.

![](/images/1.4/6446460943_1b994097d6_o.jpg)


### Least Expected

For unadjusted ears, the sound of birds in nearby trees was a shock. Throughout, the constant chirps accompanied the voices of happy festival participants.

![](/images/1.4/20111203_006.jpg)

### Best Use of Foam

The festival's logo was rendered in giant letters, made of foam and perfect for sitting. At the end of the weekend, some participants went home with parts of this unusual type treatment.

![](/images/1.4/6477110981_aab716da80_o.jpg)

### Best Ambience

The festival took place almost entirely outside, on the grounds of the Museum of Modern Art in Rio de Janeiro. The museum itself is raised off the ground, with space below. Crowds of festival-goers wandered and worked in the shadow of a floating gallery.

