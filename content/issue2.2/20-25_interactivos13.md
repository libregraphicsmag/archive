Title: Interactivos?'13, Tools for a Read-Write World
Section: Dispatch
Slug: interactivos-13

The Libre Graphics magazine team took part as advisors in Interactivos?'13, an event hosted by Medialab Prado.

The premise of Interactivos? is to promote the development of promising ideas by joining together practitioners and learners, who spend a week hacking and building beautiful outcomes.

The theme of Interactivos?'13 was “Tools for a Read/Write World.” In this dispatch, we feature the projects that represent the 9 different takes on this theme by a diverse group of experts and enthusiasts.

--------------

## Design with Git

A visualizer of the commit history of SVG files, meant to improve collaboration, encourage the use of version control systems and push the designer's workflow into unknown territories.

<http://w.xuv.be/projects/design_with_git>

![](/images/2.2/git.png)

--------------

## Freedom of Speech Kit

An interactive and portable digital banner that displays messages sent via internet, to empower citizens by freedom of speech in public space.

<http://freedomofspeechproject.org>

![](/images/2.2/freedomkit.jpg)

--------------

## Incoma

An Open Source internet platform that serves as a space for thought and discussion, specifically designed to allow massive access and interactions, to try and answer the question "How can we think together?"

<http://incomaproject.org>

![](/images/2.2/incoma.png)

--------------

## Yes, No? Maybe

A research project on how decision-making tools might allow for and record users' ambivalence. "Yes" doesn't always mean "yes." So how can we encode "maybe?"

<http://tiny.cc/yesnomaybe>

![](/images/2.2/IMG_5637.jpg)

--------------

## El Recetario

A collaborative platform for research and experimentation on the use of waste to construct furniture and accessories, where people can share what they do and how they do it.

<http://www.el-recetario.net>

![](/images/2.2/recetario.jpg)

--------------

## Grapa

A site dedicated to promoting Libre editions and publications, focusing on the development and gathering of software, hardware and practices to rethink printed culture.

<http://grapa.ourproject.org>

![](/images/2.2/grapa.jpg)

--------------

## Colaboratorio de relatos #Hackmito

An experiment of collective creation through the remixing of collective imagination and characters, and through this reflection about open culture and the commons environment.

<http://hackmito.wordpress.com>

![](/images/2.2/hackmito.png)

--------------

## Real Time Collaboration in Fontforge

An effort to extend the new real time font collaboration feature of FontForge to the web, opening up a discussion about where Libre font collaboration should go in the future.

<http://fontforge.org>

![](/images/2.2/fontforge.jpg)

--------------

## Tau Meta Tau Physica

A tool which enables designers, patternmakers and consumers to express ideas, customize products, order individually-sized unique patterns and garments and enter into creative business relationships.

<http://taumeta.org>

![](/images/2.2/tmtp.png)



