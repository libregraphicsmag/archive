Title: Gendering Craft
Section: Showcase

Our showcase this issue focuses on gendered work in craft. The term “women's work” has been variously applied to such diverse tasks as household chores, child care, textile labour and virtually any task deemed un-manly at a given moment and in a given cultural context. Conversely, if someone is said to have “done a man's job,” it may well mean that some task was done with competence and skill. In this showcase, we seek to show work which overturns standard conceptions of what it is to do women's work or a man's job. In the following pages, images of computerized knitting, code-inspired embroidery and women infiltrating F/LOSS offer windows into sites of subversion, in which the gendered natures of crafts and tasks are blurred.
