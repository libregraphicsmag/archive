Title: Consensus is sexy? The gender of collaboration
Author: Eleanor Greenhalgh
Section: Column
Slug: consensus-is-sexy

![](/images/2.2/IMG_4806_bw.jpg)![](/images/2.2/IMG_4820-01_bw.jpg)![](/images/2.2/IMG_6996_bw.jpg)

What does it mean to reach consensus? What, in fact, does it mean to “consent?” If we take the gender/ing of collaborative practices seriously, we should ask not only _who_ participates and how they are represented, but in what ways a collaborative process may itself be gendered. 

There is a rich feminist tradition of defining and arguing for the importance of (sexual) consent. And feminist models of consent offer useful tools for evaluating collaborative practices in other contexts too, be they sexual, democratic or artistic.

I have marched in the rain many times beneath the famous slogan, “Yes Means Yes and No Means No.” (Or its more upbeat cousin, “Consent Is Sexy.”) This black-and-white approach is an important starting point, in a culture where collaboration is too often made meaningless by coercion. But starting points are just that. As artists and activists we should ask: how much better can we do? 

One reason for moving beyond a simplistic Yes/No model of consent is that it encodes a gendered power dynamic in its very grammar. “B gives consent to A, for proposal C.” In this grammar, an active (read: male) subject seeks consent for his proposal from a reactive (read: female) respondent. Moreover, we need to ask how B's "yes" is produced. The way we understand and encode consent—in both our digital tools and social protocols—makes a difference. Consider these examples: 

Using the organising tool eConsensus, there is a green icon for tagging a response to a proposal as “Consent” (but no button for refusal or veto). On Wikipedia, there is no “consent” button, but a policy that silence equals consensus.

The important point is that “consent” is not a stable or self-evident term. Its meaning and practice are variable and up for grabs. So what radical models of consent could we articulate for use in the open collaborations of sex, art or culture? How might we define consent in our own projects, and what methods of expressing it do we want to encode in our tools? These design questions are inherently gendered.

Many of the feminist proposals emerging today share a F/LOSS-like emphasis on communication and collaboration. Consent, rather than a type of gendered “permission,” has been re-defined variously as “an open dialogue,” “an active collaboration,” or “a process of affirmative participation.” Consent should not merely be the imperative, as one poster campaign put it, to “get a yes.” We must re-think consent, not as an outcome to be achieved, but as a process in and of itself. Thomas Millar argues for this model of sexual consent by re-casting sex as an artistic collaboration. Conversely, we might discover interesting models for artistic collaboration by applying practices developed in intimate relationships.

One of these (feminist) practices is a tolerance for indecision and inaction. With its unpopular insistence that “maybe also means no”, a feminist ethics of sexual collaboration demands an unusually high tolerance for the possibility that “it” might not happen. Consent is certainly _not_ always “sexy.” And when “it” is a design decision, a document, a project, this ethics is perhaps stretched to—or beyond—its limit. The term “design by committee” is reserved for those who fail to recognize this, who shrink from the necessary violence of decisive action. 

However, a F/LOSS mindset makes us well-placed to question these assumptions, and to make imaginative use of artistic ambivalence. The Libre Graphics Research Unit's _Collision_ project, for example, has showcased some excellent examples in the field of graphic decision-making. Its first work session in Brussels recently explored alternatives to the logic of conflict resolution underlying conventional graphic design practice.

Gijs de Heij ran a battleships-style game in which two teams attempt to avoid colliding on paper by sending commands to a plotter printer. In its sheer pointlessness, the game invited us to re-imagine the printed page as an open space where interactions can occur, rather than a grid where lead blocks compete for their share. Christoph Haag ran Lafkon's Forkable workshop, in which multiple collaborators contribute versions of a design to a git repository. Rejecting the imperative to chose one over another, the “conflict” is resolved by a bash script which picks versions at random and combines them in layers, producing a new iteration of possibilities each time it is run. On the final day of the meeting, digital and paper prototypes were created that attempted to realize this ethic of coexistence: using cross-hatching to indicate overlapping zones on maps, or accordion-style folding to accommodate multiple layers on the printed page. 

Just as consent needs the notion of collaboration in order to become meaningful, collaborative practices need to look closely at how they implement consent. An ability to tolerate and make skillful use of indecision, privileging process just as highly as outcome, is where F/LOSS practices and feminism might come together. And _that_ could be really sexy.

--------------

For more on the subjects and work explored in this column, see: 

* [Collision (2013) Workshop 1—Badness and Conflict](http://blogs.lgru.net/collision/?page_id=2)
* Kramer Bussel, R. (2008) “Beyond Yes or No: Consent as Sexual Process”, in Freidman, J. & Valenti, J. (eds) Yes Means Yes: Visions of Female Sexual Power and a World Without Rape (California: Seal Press).
* Millar, Thomas M. (2008) “Towards a Performance Model of Sex”, in Freidman, J. & Valenti, J. (eds) Yes Means Yes: Visions of Female Sexual Power and a World Without Rape (California: Seal Press).

_Eleanor's work on consensus and decision making can be found at <http://consentsus.org>._




