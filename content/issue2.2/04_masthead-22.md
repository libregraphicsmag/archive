Title: Masthead
Section: Masthead
Slug: masthead-22

Libre Graphics Magazine 2.2

**Gendering F/LOSS**

January 2014

ISSN: 1925-1416


## Editorial Team 

* [Ana Carvalho](mailto:ana@manufacturaindependente.org) 
* [ginger coons](mailto:ginger@adaptstudio.ca) 
* [Ricardo Lafuente](mailto:ricardo@manufacturaindependente.org)


## Copy editor

Margaret Burnett


## Publisher

ginger coons


## Community Board

* Dave Crossland
* Louis Desjardins
* Aymeric Mansoux
* Alexandre Prokoudine
* Femke Snelting


## Contributors

* Ele Carpenter
* Mar Canet
* Varvara Guljajeva
* Eleanor Greenhalgh
* Kẏra
* Aya de Leon
* Antonio Roberts
* Eric Schrijver
* Sho-Sho Smith
* Femke Snelting
* Spideralex

Printed in Porto by Cromotema (www.cromotema.pt) on recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 

Write to us at [enquiries@libregraphicsmag.com](mailto:enquiries@libregraphicsmag.com)

<http://libregraphicsmag.com>

Our repositories with all source material for the magazine can be found at <http://gitlab.com/libregraphicsmag>


--------------


## Images under a CC Attribution Share-Alike license

* Photo of ginger coons by herself. 
* Photo of Ricardo Lafuente by Ana Isabel Carvalho. 
* Photo of Ana Isabel Carvalho by Luís Camanho. 
* Photo of Antonio Roberts by Emily Davies. 
* Photo of Eric Schrijver by himself. 
* Photo of Eleanor Greenhalgh by Brendan Montague. 
* Notebook [Libre Planet 2013, photos](http://static.fsf.org/nosvn/libreplanet/LibrePlanet2013Photos) by the FSF and volunteers.
* Photo of the [Lulzbot](http://mediagoblin.com/u/cwebber/m/lulzbot) by Chris Webber.
* New Releases background is made with patterns by several authors from [Subtle Patterns](http://subtlepatterns.com). 
* All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA. 
* Best of people icons images are from [The Noun Project](http://thenounproject.com). 
* Photos from the Empowermentors Collective interview by Kýra.


## Images a CC Attribution license

* Cover photo, titled ["Knippelsbro 1947,"](http://www.flickr.com/photos/16nine/10296133066) by Flickr user Mikael Colville-Andersen.
* Showcase opening page photo, titled ["Twin Girls on Tricycles 1940,"](http://www.flickr.com/photos/12567713@N00/109419868) by Flickr user born1945.
* Illustrations on Small and Useful by Flickr user fdctsevilla from "La Terre avant le déluge" by Louis Figuier, Librairie de L. Hachette, 1863:
  - ["Craide de l'ile Moén"](http://www.flickr.com/photos/fdctsevilla/4514448588/)
  - ["Craie de meudon"](http://www.flickr.com/photos/fdctsevilla/4513849243/)
* Background photo in page 53 by Tarek Ziadé.


## Images under other licenses

* Inside cover photo, titled ["A century run or bust two photographs,"](http://commons.wikimedia.org/wiki/File:A_century_run_or_bust_two_photographs_(HS85-10-11914).jpg) by Fred L. Hacking, is under the Public Domain.
* Photos on Eleanor Greenhalgh's column by [collision](http://gallery3.constantvzw.org/Collision) under the Free Art License.
* Hackermoms logo, in page 18, is copyright and the photo, in page 20, is under CC-BY-NC.
* Photos and screenshots in Interactivos?'13 article are under the Free Art License and can be found at <http://gallery3.constantvzw.org/Tools-for-a-Read-Write-world> and <https://gitlab.com/libregraphicsmag/interactivosbook>.
* The New Releases illustration, on page 10, titled ["Image taken from page 17 of Illustrated Battles of the Nineteenth Century"](http://www.flickr.com/photos/britishlibrary/11133636554) was taken from the British Library Flickr collection and is under the Public Domain.
* Eric Schrijver's column illustration, titled ["Round Europe with the Crowd,"](http://www.flickr.com/photos/britishlibrary/11080355563) was taken from the British Library Flickr collection and is under the Public Domain.


## General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.
