Title: Knitic
Authors: Varvara Guljajeva, Mar Canet
Section: Showcase

![](/images/2.2/7177062792_fb12cb8c6a_o.jpg)![](/images/2.2/8957899686_fa966b5c41_o.jpg)

Knitic is an Open Source knitting machine, controlling a Brother electronic knitting machine via Arduino. To be more precise, Knitic is the new “brain” of a knitting machine. From the electronic part of the original machine we use only the end-of-line sensors, encoder and 16 solenoids. With Knitic, one can knit as long a pattern as desired, as well as modify the pattern on the fly. 

Why are we developing Knitic? Because we feel the knitting machine, as the first automatic domestic fabrication tool, has been totally overlooked in the age of digital fabrication.

<http://knitic.com>  
<http://varvarag.info>  
<http://mcanet.info>

![](/images/2.2/page_installationTwo-1.jpg)![](/images/2.2/page_installationTwo-2.jpg)

![](/images/2.2/8206308817_5f05f59c84_o.jpg)

![](/images/2.2/8678887412_06ecf3e80c_o.jpg)

![](/images/2.2/8757337407_ca0431f621_o.jpg)
