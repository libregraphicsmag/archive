Title: LibrePlanet 2013
Section: Notebook

![](/images/2.2/IMG_6075_BW.jpg)

### Where

LibrePlanet 2013, Cambridge, Massachusetts

![](/images/2.2/gavroche_BW.jpg)

### What

A two day conference, organized by the Free Software Foundation, exploring the diverse meanings of software freedom, as they apply to different people and contexts.

![](/images/2.2/IMG_6065_BW.jpg)

### Most wanted

A 3D printer from Lulzbot was on show, printing out the MediaGoblin mascot and characters from Blender Foundation movies. Based in the popular RepRap community, Lulzbot has produced the first 3D printer to receive the Free Software Foundation's “Respects Your Freedom” hardware certification.

![](/images/2.2/cwebber_lulzbot_BW.jpg)

### Best point

Leslie Hawthorn, channeling her years of experience as a community manager, offered an opening talk on negotiation theory. One of the key takeaways? There's a difference between being honest and being mean. 

![](/images/2.2/IMG_6077_BW.jpg)

### Best dinner

The day before the conference began, more than twenty people gathered for the Women in Free Software dinner. Coming from a diverse collection of projects, locales and roles, those present at the dinner offered proof that women in F/LOSS are both many and active.

![](/images/2.2/room110_BW.jpg)
