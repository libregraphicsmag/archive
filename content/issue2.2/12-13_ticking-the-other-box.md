Title: Ticking the other box
Author: Antonio Roberts
Section: Column

The digital world presents an opportunity for us to transcend the boundaries and limitations of the physical world. In the physical world we attempt to comprehend our day-to-day experiences with easy-to-define categories. Our lives are bounded by tick-box exercises that detail our age, race, gender, occupation and even occasionally our emotional well-being. We are bound to our location through documents and legal processes that restrict our movements. Everything that makes us unique and human is reduced to impersonal and bland form descriptions designed to make us easier to group and define.

In the digital world, however, we can choose not to participate in this box-ticking exercise. At our own will we can transform ourselves from someone human and tangible to something other than ourselves and beyond definition. We can overcome the location boundaries and connect with others across the world simultaneously and without fear of repercussions. We can be everywhere at once or occupy an imagined space. We can choose to be neither male nor female, neither black nor white, neither old nor young. We can redefine ourselves and tick the “other” box if we wish to do so.

The digital world is infinite, its inhabitants faceless. Boundaries such as location, time, speed and language break down to create a world where art—music, dance, words—and anything imaginable can exist in a chaotic harmony. Only our knowledge and imaginations limit our experiences.

Or so I'd hoped.

As the internet becomes ever more engrained within our day-to-day activities, more powers—governments, employers—attempt to restrict our movements through the digital world and remove the anonymous mask, making our digital experiences more closely tied to our physical experiences.

The digital world is becoming less of a playground for our ideas to run wild, with the same rigid, limiting boundaries we experience in our physical lives again being forced unwillingly upon us.

These limitations are being imposed on art. With the digital world should have come the end to localization of art. Many artists and institutions already practice this and allow those who aren't physically present to experience their works via recordings shared on video sites, as well as easier and more immediate access to documentation. The results of this sharing can only mean greater chances of engagement with a wider range of audiences, perhaps paving the way to collaborations and conversations not possible in the physical world.

So why do we still choose to restrict access to art based on location? What are the actual gains of denying access to a video on YouTube or an e-book on Amazon because of the user's location? How does denying access to the vast knowledge bank of Wikipedia enrich a person's life and fill them with knowledge?

These physical-world limitations also manifest themselves when it comes to abusive behaviour. Countless conflicts, quarrels, tiffs and disagreements occur daily over our physical appearances. Why, when the art published online has the potential to be completely anonymous from its author, do we still ridicule an artist based on their physical, human attributes such as gender, race or age? Why, in situations where the artist has made the conscious attempt to withhold their physical identity, do we strive to discover this instead of focusing on the art itself?

Do these politically charged actions do anything other than needlessly replicate the problems of the physical world and highlight inequalities? The physical world, with all of its problems is bad enough already. Let's not waste the potential of the digital world by bringing along our baggage.


_Antonio Roberts is a digital visual artist based in Birmingham, UK. He produces artwork that takes inspiration from glitches in software and hardware, errors and the unexpected. <http://hellocatfood.com>_
