Title: Rewriting hacker culture
Author: Eric Schrijver
Section: Column

![](/images/2.2/SchrijversColumn.jpg)

There are not many women in IT, and even less so in Free Software. A 2004 report by the National Science Foundation finds roughly 1.5% of F/LOSS community members to be female whereas in proprietary software that number is 28%.[^1]

A 2006 European Union report analyzes the reasons for this disbalance. Within “hacker culture,” a great focus lies on the willed acts of the practitioners, on what people, themselves do: Your position in the community is based on the contributions you make and is thus “meritocratic.” More structural reasons why minorities are dissuaded to make these contributions in the first place, structural reasons that go beyond the single individual, are thus out of sight.[^2]

It’s a thought process, shared by the more ruthlessly commercial side of geek culture, web 2.0 tech entrepeneurs: Tech blogger Jason Calacanis says race doesn’t exist in Silicon Valley—one gets one top through sheer will power and perseverance.[^3] This assessment is echoed by tech writer Michael Arrington who, not long afterwards stressed similar sentiments. While the tech community was rife with debates on racism, sexism, and just recovering from the suicide of Diaspora founder Ilya Zhitomirskiy, which triggered a debate on the work pressures of Silicon Valley, Arrington labels all that as “whining.” His message: Don’t whine, work hard, and the reward is yours.[^4]

Still, the debate on gender is much more present now than it was at the time of the EU’s first research into the subjet. A string of incidents surrounding sexist behaviour at conferences by male attendants (documented at the Geek Feminism wikia)[^5] brought attention to problems surrounding the attitude towards women in the communities of Free Software and Open Source. This has led to much debate, and also to actual improvement: Many conferences now feature a code of conduct, and a conference like Pycon has seen quite an uptake in both female attendants and speakers.[^6]

What is discouraging though, is that there are community leaders who are quite dismissive of such efforts. David Heinemeier Hansson, lead developer of Ruby on Rails, dismissed the critique of a female unfriendly presentation, labeling it as harmless fun, and positing that in no way this kind of attitude could be what kept women out of Open Source.[^7]

The Libre Graphics community, as I have experienced it through the LGM meetings, faces the same issues as other communities in free software, though for me it feels already rather open and diverse in comparison to other Free Software and Open Source communities I’ve partaken in.

It helps, I think, that in organizing these meetings, the organisers have not only teamed up with technical universities, polytechnichs—a traditional locus of the engineering culture from which the hacker ethic has sprung—but also with two art institutions, Constant in Brussels and Medialab Prado in Madrid. These institutions bring with them other narratives regarding masculinity, feminity and the relation to technology. And, also in purely demographic terms, a different audience as well.

I feel glad that, in our way and our time, we are busy rewriting the narrative around technology and gender. There are downsides to the fabled “hacker ethic,” and I am happy to know we do not need to keep constrained to this prototype—we are free to reinvent ourselves as people, and paraphrasing Judith Butler[^8] now: Our hostile gender roles only exist for as long as we keep playing them out.

--------------

[^1]: National Science Foundation 2004. Women, Minorities, and Persons with Disabilities in Science and Engineering, NSF04-317, Arlington, VA: National Science Foundation

[^2]: D Nafus, J Leach, B Krieger 2006. Gender: Integrated report of findings. Free/Libre and Open Source Software: Policy Support. UCAM, University of Cambridge

[^3]: <http://blog.launch.co/blog/doing-the-right-things.html>

[^4]: <http://uncrunched.com/2011/11/27/startups-are-hard-so-work-more-cry-less-and-quit-all-the-whining>

[^5]: <http://geekfeminism.wikia.com/wiki/Timeline_of_incidents>

[^6]: <https://twitter.com/jessenoller/status/315132950364708866>: “Over 20% female attendance at PyCon, from all over the world. 22 female speakers and teachers #PyCon”

[^7]: <http://david.heinemeierhansson.com/posts/40-alpha-male-programmers-arent-keeping-women-out>

[^8]: Butler, Judith (1990): Gender trouble: feminism and the subversion of identity.


_Eric Schrijver (Amsterdam, 1984) is a graphic designer and a performance artist. He is inspired by programming culture. Eric teaches Design for new media at the Royal Academy of Art in The Hague, and is a member of the design collective Open Source Publishing. <http://ericschrijver.nl>_

