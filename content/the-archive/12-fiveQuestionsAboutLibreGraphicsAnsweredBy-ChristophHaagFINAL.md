Title: Five questions about Libre Graphics, answered by Christoph Haag
Author: Christoph Haag
Section: Interview
Slug: five-questions-christoph-haag

**Who or what has been your biggest F/LOSS or Free Culture influencer?**

Florian Cramer. Martin Rumori. Femke Snelting. In chronological order. At different times, in different ways.


**What's your favourite piece of niche or off-the-beaten-track software?**

[JavE](http://www.jave.de) (Java Ascii Versatile Editor). Rather than for writing texts, it is intended for drawing texts.

**Favourite Libre typeface?**

[Autobahn](http://www.peter-wiegel.de/Autobahn.html). Still waiting for a good cause to use it.


**Finish the sentence: "Five years from now, I want..."**

\> I'll do what you want me to do  
\> I want to make a million dollars  
\> I want to live out by the sea  


**Best F/LOSS side-effect you've experienced?**

Conversations. I think that conversations are the best, biggest thing that Free Software has to offer its user. (Ed.: Along with Femke Snelting and Xavier Klein, Christoph Haag developed a book on this subject, which is available from <https://freeze.sh/_/2015/conversations/>)


**Vectors/Rasters/Others?**

Yes.

