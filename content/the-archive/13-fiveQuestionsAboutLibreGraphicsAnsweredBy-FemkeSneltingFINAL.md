Title: Five questions about Libre Graphics, answered by Femke Snelting
Author: Femke Snelting
Section: Interview
Slug: five-questions-femke-snelting

**Where do Libre Graphics/Libre Design fit into the larger F/LOSS ecosystem?**

Libre Graphics is interwoven with so many aspects and strata of the F/LOSS ecosystem. It interacts with standards and protocols (Unicode, CSS, HTML), with many different types of software, hardware (calibration), computer vision, web-technologies (html2print), collaboration platforms (etherpad, git), interface aesthetics, usability. It links to licensing politics and it questions inclusion and exclusion in F/LOSS. I think to engage in Libre Graphics means ultimately to redraw the boundaries between ‘end-user' and ‘developer'.

**Favourite license? Why?**

My favourite license would be one that could work well with collective practices (including collaborations between machinic and biological agents), that would not need to rely on the paradigm of property and its colonial heritage and one that could do without the assumption of an original author to begin with. In the mean time, my license of choice is the Free Art License. It has a preamble that reads as a manifesto instead of human-readable legalese; it allows commercial use (to exclude it does not make any sense in the age of platform capitalism) and most importantly, it allows for derivative works under the same conditions.

**Vectors/Rasters/Others?**

Vectors of course. "We find that curves are simply more interesting than pixels. They are abstracted geometric shapes that flow from intentions. They have direction and the ability to express ideas. While they may be numerically complex, they relate intimately to the inherent discreteness of computation. They take full advantage of how computers can serve imagery. Curves have the ability to be close to movements, and open up non-binary conversations between digital and analog spaces. As shareable numerical objects, they are more open to collaboration and allow actual reinterpretation." Pierre Huyghebaert, Colm O'Neill, Femke Snelting, "Pixels and lines" in Drawing Curved. Brussels: OSP, 2018. <http://drawingcurved.osp.kitchen/>
 
**Favourite lost cause?**

The Open Font Library. The arrival of web-fonts was an incredible opportunity for Libre Graphics, and very early on the project provided a prototype for a platform to distribute, learn and design Libre Fonts together. The community around the Open Font Library has been absorbed by The Alphabet companies and I still regret that we did not find the collective energy at the time to invest in a parallel practice.

**Finish the sentence: "When I started doing Libre design, I never expected…"**

When I started doing Libre design, I never expected that 12 years later I would still enjoy its aesthetics, occasionally practice it, read and write about it, and learn from it.
