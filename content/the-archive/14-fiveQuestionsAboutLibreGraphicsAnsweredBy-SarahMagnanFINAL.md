Title: Five questions about Libre Graphics, answered by Sarah Magnan
Author: Sarah Magnan
Section: Interview
Slug: five-questions-sarah-magnan

**Who or what has been your biggest F/LOSS or Free Culture influencer?**

Muriel Cooper


**Finish the sentence: "When I started doing Libre design, I never expected..."**

When I started doing Libre design, I never expected my everyday work to be that much intertwined within a collective practice and so to really never be alone.


**Favourite Libre typeface?**

I have several loved ones: yesterday it was the LALDIN, some days ago the Tsukurimashou family, some hours ago the Dauphine and maybe tomorrow the Sawadee or the Hershey family.


**Best F/LOSS side-effect you've experienced?**

Shaking from bottom up to horizontal structure work relationships and learning situations.

During a workshop in Germany, I had to install a tool with a student (sorry, I cannot remember what was it exactly) to use HTML2PRINT on her Windows computer. The installation itself was not supposed to be difficult but the student computer was in Chinese and the help I found was in German. I barely speak German and not at all Chinese. The fun part comes because we succeed having one student translating the documentation I found to the other student who translated it to the command line in Chinese. It really felt like a collective empowering moment for the three of us.


**Favourite lost cause?**

Adding some diversity on Wikipedia's content.
