Title: Five questions about Libre Graphics
Author: ginger coons
Section: Interview

We gave four practitioners of libre art and design a list of slightly facetious, but still quite serious questions about their lives with libre graphics. We gave them twelve questions to choose from, and asked for five answers. 

Any readers looking very closely at the interviews that follow will see that across the four sets of responses, ten questions are answered. Whether they were too facetious, too far down the list, or too grammatically incorrect, the two questions which did not elicit responses were "Year of the Linux desktop?" and "Free/Libre/Open/Other?"

As for the other ten questions, they were answered thoughtfully, with humour, and provide some good reminders that the libre graphics ecosystem is broad, complex, and useful. 

