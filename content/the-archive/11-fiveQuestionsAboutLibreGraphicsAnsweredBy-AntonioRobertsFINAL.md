Title: Five questions about Libre Graphics, answered by Antonio Roberts
Author: Antonio Roberts
Section: Interview
Slug: five-questions-antonio-roberts

**Who or what has been your biggest F/LOSS or Free Culture influencer?**

The FLOSS+Art book published in 2008 was a really big influence to me. Prior to reading it my only introduction to the world of free culture had been through using Firefox and Open Office. This book introduced me to what people are doing in the cultural sector and encouraged me to think critically about what free culture actually means. I've learnt that it's not about being anti-capitalist or anti-commercial and is instead about creating a fairer culture for everyone, ranging from big companies to amateur artists.


**What's your favourite piece of niche or off-the-beaten-track software?**

Although Pure Data is quite well known in the world of generative art, the scenarios in which it is used vary greatly! Sometimes it is used as the centrepiece, such as at Algoraves where the patch itself is projected to an audience, and other times it exists more as a plugin or library in games or interactive installations.


**Finish the sentence: "When I started doing Libre design, I never expected..."**

That my choice to work with free and open source software and open licences would be a central part of my work.


**Favourite Libre typeface?**

Ubuntu. I like to try and find where it has been used and really like that it has been used everywhere from transport companies to local governments. It's quite easy to spot, primarily from its very distinctive U glyph.


**Vectors/Rasters/Others?**

Vector. I initially associated vectors with the very flat vector drawing style, but as I progressed in my career I see it being used in video editors, motion graphics programs, generative art software and laser cutters and plotters. It's a very versatile format.

