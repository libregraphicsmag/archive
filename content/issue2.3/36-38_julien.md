Title: Before webfonts
Author: Julien Deswaef
Section: Feature

Fonts seem to have conquered the web. The `@font-face` CSS property is everywhere and its use has reached far beyond the display of characters.[^1] But there was a time, not so long ago, when web designers had to struggle with complex processes just to display the title of a blog article in the typography of their choice.

At its core, the web has always been text. But the glyphs to display it had to be local. When designers embraced the medium, their only options were the fonts most people had on their machines (Arial, Courier New, Times New Roman, Webdings...)[^2] The obvious solution was to replace text with images, either pre-cooked by the designer themself, or baked on the fly and cached by the server. This technique, although widely used, was not elegant. It was heavy on the server and on the bandwidth, plus those little rendered texts weren't selectable or scalable.

To circumvent these flaws, an approach called [sIFR](http://en.wikipedia.org/wiki/Scalable_Inman_Flash_Replacement) was developed. It built a small Flash file into the page, used to load the desired font. Selected pieces of text would then dynamically be replaced by Flash-rendered text, all done in the browser. Text could be scalable and selectable. But it required a proprietary plugin to be displayed correctly—and don’t even dream about printing it.

When I started to design my web portfolio, none of these solutions really appealed to me. I wanted to stick with text-based open standards and no “image tricks.” The `<canvas>` tag was then only supported by half of the browser market. And I was starting to get interested in “everything parametric.” The [font](http://xuv.be/static/xuvfont/typo.html) I was working on used a single closed polygon for each of its glyphs. Each letter was just a set of coordinate relations that would allow me to change the weight and proportions at will. To display it in the browser, I found some [obscure Javascript library](http://www.walterzorn.de/en/jsgraphics/jsgraphics_e.htm) created by Walter Zorn that exposed a set of vector drawing functions. The library worked by creating a coloured `<div>` for each pixel of the drawing using a fast algorithm to prevent too much repetition and to combine as many pixels possible into one `<div>`. It worked remarkably well across browsers and still works in recent ones because it just uses a basic element from HTML. The “pixelated” look of it also turned out to be very pleasing to me.

![](/images/2.3/xuv-test-hires.png)

<span>xuvfont</span>

After this little personal project, I kept my eyes open for unusual font drawing experiments in the browser, especially the ones involving code. I was impressed by Dave Desandro’s [Curtis CSS typeface](https://github.com/desandro/curtis-css-typeface) which is a pure HTML/CSS font using a combination of background colour, border width, border radius and a lot of `<span>` tags to draw a set of very pleasing fat characters. This is especially remarkable because it doesn’t rely on Javascript, making it a very good candidate for the “displays-everywhere” award.

![](/images/2.3/curtis-css-hires.png)

<span>The Curtis CSS typeface</span>

Another experiment to notice is Claudio Cañete’s [1k Notebook](http://www.claudiocc.com/the-1k-notebook-part-i) project to implement a full parametric font in `<canvas>`—in just under 1 kB. He first used a 3×7 grid of nodes to draw a custom, line-based font. But since this wasn’t really using much of the 1024 bytes available, he pushed further by porting a script typeface from the [Hershey font collection](https://en.wikipedia.org/wiki/Hershey_font) and tweaked it to draw the glyphs in a continuous single trace. The encoding and decoding of the font is where Claudio’s work happens, to stay under that limit he set himself. And this is where the beauty lies.

![](/images/2.3/1k-notebook-hires.png)

<span>1k Notebook</span>

With the latest HTML specifications, pretty much any typeface can be displayed in a web page, and, don’t get me wrong, this is such a relief. But creativity gets its strength from the constraints it faces. There's still room for inventiveness with font rendering in the browser. Just set yourself the limits of your canvas.

[^1]: [FF-Chartwell](https://www.fontfont.com/how-to-use-ff-chartwell)

[^2]: [Microsoft core fonts](http://en.wikipedia.org/wiki/Core_fonts_for_the_Web)
