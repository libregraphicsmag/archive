Title: Feeling design through type
Author: ginger coons
Section: Dispatch
Slug: design-through-type

A few times a year, I teach non-designers a little bit about the basics of design. Just a short workshop, a couple hours long. The people I'm teaching are young professionals, enrolled in a graduate program that aims to spit them out into the workplace with a set of up-to-date skills. The vast majority of them are good at research, writing, and even management, but visual literacy isn’t in their standard skill set.  More importantly, the idea of design as a functional activity, rather than a veneer of prettiness, is pretty foreign.

Though I teach a number of basic design principles—things like colour, balance, and pattern—a large portion of the session is taken up looking at type. Most people come in with some knowledge of type, on the popular level of "Helvetica good, Comic Sans bad." My task is to explain why different typefaces do different things, and help the participants in the workshop develop some kind of instinct for what to use, when.

We work with Libre type specimens, for the most part, though that's not readily apparent at the start. I bring a stack of pangrams with me, written in a broad collection of fonts, from Linux Libertine and OSP DIN to freeware novelty fonts that look like they’re dripping blood. We play some games with those specimens. In groups, I ask them to pick out which of their specimens look futuristic, or serious, or classic, or fun. They debate amongst themselves and have to come up with a consensus decision for each category. Inevitably, a serif or handwriting font ends up being the classic one.

But the ever-important question is "Why?" Why is one typeface more classic than another, what makes a drippy-looking font fun instead of horrifying? Why does monospace feel futuristic to a particular group? Most participants in the workshops have similar intuitive ideas about what the feelings behind particular fonts are and, by extension, what uses those fonts might be put to. By asking them to put those intuitions into words, I ask them to articulate, both to the other participants and to themselves, just what characteristics, assumptions or associations lend those fonts their particular feelings.

It seems a trivial thing, getting people to talk about how fonts make them feel, but it's a first step in a certain kind of visual literacy. Realizing that styles of type exist, that not all classic-looking serifs are Times New Roman (something I’ve often seen Linux Libertine mistaken for), and that a designed object—even something as simple as a poster for a bake sale—is an assemblage of visual elements of which type is a crucial part, all of these little things matter in instilling a basic understanding of how and why design works, and how its most basic principles can be put to use. 

