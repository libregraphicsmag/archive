Title: Masthead
Section: Masthead
Slug: masthead-23

Libre Graphics Magazine 2.3

**The Type Issue**

January 2015

ISSN: 1925-1416


## Editorial Team 

* [Ana Carvalho](mailto:ana@manufacturaindependente.org) 
* [ginger coons](mailto:ginger@adaptstudio.ca) 
* [Ricardo Lafuente](mailto:ricardo@manufacturaindependente.org)


## Copy editor

Margaret Burnett

## Publisher

ginger coons


## Community Board

* Dave Crossland
* Louis Desjardins
* Aymeric Mansoux
* Alexandre Prokoudine
* Femke Snelting


## Contributors

* Frank Adebiaye
* Raphaël Bastide
* Julien Deswaef
* Loraine Furter
* Brendan Howell
* Øyvind Kolås
* Micah Rich
* Samuel Rivers-Moore
* Antonio Roberts
* Manuel Schmalstieg
* Eric Schrijver
* Alexei Vanyashin

Printed in Brussels by [Gillis](http://gillis.be) on recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics magazine.

Write to us at [enquiries@libregraphicsmag.com](mailto:enquiries@libregraphicsmag.com)

<http://libregraphicsmag.com>

Our repositories with all source material for the magazine can be found at <http://gitlab.com/libregraphicsmag>

--------------

## Images under a CC attribution share-alike license

* Cover, back cover and inside covers by Manufactura Independente. 
* Photo of ginger coons by herself. 
* Photo of Manuel Schmalstieg by Sedat Adiyaman. 
* Photo of Antonio Roberts by Emily Davies. 
* Photo of Eric Schrijver by himself. 
* Illustration for Friendlier forks column is based on images by Flickr user fdctsevilla. 
* All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.

## Images and assets under other licenses

* [Illustration](https://commons.wikimedia.org/wiki/File:Feuillet_notation.jpg) for Moving kinetic typography column is under a CC0 1.0 Universal Public Domain Dedication license.
* Illustration for the Libre Foundry Map by Manufactura Independente, based on [this Public Domain image](http://commons.wikimedia.org/wiki/File:30_Largest_Infrared_Galaxies_with_Labels.jpg).
* Fonts used in the Libre type foundry Atlas: 
  * [Lil Grotesk](https://github.com/uplaod/LilGrotesk ) by Bastien Sozeau, A - B Foundry, under the OFL. 
  * [Amsterdam](http://www.citype.net/city/amsterdam ) by Jarrik Muller, Citype, under CC BY-SA 3.0. 
  * [Lora](http://www.cyreal.org/2012/07/lora ) by Olga Karpushina, Cyreal, under the OFL. 
  * [QumpellkaNo12](http://www.glukfonts.pl/font.php?font=QumpellkaNo12) by Grzegorz Luk, Glukfonts, under the OFL. 
  * [Playfair Display](http://www.google.com/fonts/specimen/Playfair+Display) by Claus Eggers Sørensen, Google Fonts, under the OFL. 
  * [GFS Bodoni](http://www.greekfontsociety.gr/pages/en_typefaces20th.html) by George Matthiopoulos, gfs, under the OFL. 
  * [Alegreya Sans](http://www.huertatipografica.com/fonts/alegreya-sans-ht) by Juan Pablo del Peral, Huerta Tipográfica, under the OFL. 
  * [Antykwa Półtawskiego](http://jmn.pl/en/antykwa-poltawskiego) by Janusz Marian Nowacki, under the OFL. 
  * [Kontrapunkt Bob](http://www.kontrapunkt.com/type), Kontrapunkt, under CC BY-SA 3.0. 
  * [Ostrich Sans](https://www.theleagueofmoveabletype.com/ostrich-sans) by Tyler Finck, The League of Moveable Type, under the OFL. 
  * [Unna](http://omnibus-type.com/fonts/unna.php) by Jorge de Buen, Omnibus Type, under the OFL. 
  * [Lavoir](http://openfontlibrary.org/en/font/lavoir) by Alex Chavot, under the OFL. 
  * [Reglo](http://ospublish.constantvzw.org/foundry/reglo) by Sebastien Sanfilippo, OSP Foundry, under the OFL. 
  * [Serreria Extravagante](http://openfontlibrary.org/en/font/serreria-extravagante) by the participants of the Stone to Spaceship workshop (Medialab Prado, July 2012), Oxshark Fontworks, under the OFL. 
  * [Egypt 22](http://practicefoundry.com/egypt22.html) by Ivan Kostynyk, Practice Foundry, under the OFL. 
  * [Gentium](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=Gentium) by Victor Gaultney, SIL, under the OFL.  
  * [BizMeud](http://velvetyne.fr/#bizmeud) by Quentin Bodin and Jil Daniel, Velvetyne Type Foundry, under the OFL. 


## General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.
