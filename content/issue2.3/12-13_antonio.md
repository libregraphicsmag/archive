Title: Moving kinetic typography
Author: Antonio Roberts
Section: Column

The title sequence for the 1959 Alfred Hitchcock film _North By Northwest_, made by Saul Bass, is credited as the first extensive use of kinetic typography in film. As we move through time there begin to be more examples of its use, such as the opening sequence for _Star Wars_. Although essentially scrolling text, by animating the text to disappear into a vanishing point it reemphasizes the futuristic, space-age nature of the film.

With the widespread availability of digital animation and video editing tools there are now more artists and filmmakers using kinetic typography in their work. Despite this, few tools exist specifically to create kinetic typography, and those that do have not yet matured. Quite often, those interested in kinetic typography are forced to circumnavigate and hack together programs to trick the software into doing something it wasn't actually designed for. These approaches usually fail because the software has limited capabilities to edit the many detailed typographic properties that may need to be manipulated.

After Effects is the most widely used and documented tool designers use to create kinetic typography. A search for the term "Kinetic Typography Software" usually brings up tutorials and templates available for use with After Effects. The problem with this approach is twofold—not only is After Effects proprietary, but the use of templates, whether in proprietary or open source software, results in predictable and similar results.

Outside of proprietary software a number of experimental approaches exist. Blender, kdenlive, Synfig and other video editing and animation software can be used, but these still face the same specialized problems with handling text. For those willing to delve into programming more, there are some options. Processing, especially when combined with external libraries such as Geomerative, provides many opportunities for experimenting with animated and kinetic typography. In 2002, Johnny C. Lee published _Kinetic Typography Engine_, written in Java, which goes to great lengths to provide a taxonomy for creating kinetic typography. Once the user has navigated the complexities of the program they will find that it provides opportunities to animate many attributes of text.

The problem with these programmatic solutions is that they can't easily be exported or reused outside of their applications. And that's on top of being less accessible to those without programming knowledge. This clearly points to a need for further work in F/LOSS-based kinetic typography, as well as a taxonomy for describing the changes to the appearance and movement of text over time that is software-independent and an open standard to support implementation.

When compared to animation, video editing, and filmmaking, kinetic typography is very new and, as such, a common language and set of standards have not yet been developed. Improving the medium's accessibility by developing new tools for doing kinetic typography will go some way towards maturing the standards, style and voice of moving type. 

_[Antonio Roberts](http://hellocatfood.com) is a digital visual artist based in Birmingham, UK. He produces artwork that takes inspiration from glitches in software and hardware, errors and the unexpected._
