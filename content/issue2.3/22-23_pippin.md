Title: 0xA000 font family
Author: Øyvind Kolås
Section: Showcase

![](/images/2.3/at1.png)

Design inspiration is often fuelled by constraints. In that spirit, 0xA000 started out as an experiment in trying to create a pixel font with greyscale squares for anti-aliasing. The initial ASCII set was made first as an image in GIMP and a custom C program to turn the image into a UFO file where each set pixel referenced a component.[^1] For easier editing, the program was changed to output an XPM-inspired text file description of the font, where the character mapping for different greyscale pixel values can be controlled.

By coding/creating/designing a new set of components—puzzle pieces corresponding to the different characters used to design the glyphs—new visual variations for the family can be created. When doing manual anti-aliased drawing, known as pixeling, one imagines the curvature of the shape intersecting with the pixel's geometric area. The scope and power of expression of the font creation pipeline expand when adding puzzle pieces corresponding to these imagined shapes. It is possible to create semi-legible pixel fonts with a 3px-high lower-case grid, thus the 3x3 set for drawing a lower-case "o" was a starting point.

The small set of printable ASCII characters constrained how many puzzle pieces could comfortably be managed.

Within the current constraints there's much room for refinement, and the overall project has room for deeper investigations—including both serifs and improvements to the tooling for experimenting with the design of the puzzle set.

<http://pippin.gimp.org/0xA000> 

[^1]: Many font file formats support components, reusable vector shapes. These are normally used during type design to share vector shapes, like the undecorated base glyphs "A," "E," and "O" for Ã¥Ã¢Ãªáº½Ã£ and Ã¶.

![](/images/2.3/at2.png)
![](/images/2.3/at3.png)
![](/images/2.3/at4.png)
![](/images/2.3/edit.png)
![](/images/2.3/p_comparison.png)

