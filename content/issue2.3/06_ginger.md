Title: Guesting/journeying
Author: ginger coons
Section: Editor's letter

This issue marks the second time we've invited a guest editor to take over the pages of _Libre Graphics_ magazine. The first time, issue 1.3, which we called “Collaboration, collaboratively,” brought to light artists and designers whose work we hadn't seen before, which we encountered through the eyes of our guest editors, Loredana Bontempi, Emanuele Bonetti, Morgan Fortems, and Thibaut Hofer. That was an important moment for us. It's exceptionally comfortable to travel in our own circles, and is made so much easier by having a close-knit editorial team. In considering work to include in each issue, we often find ourselves trying to break away from a reliance on the usual suspects. Our first guest editors took on that task admirably, and delivered an issue that surprised and delighted us with its variety.

That's why we've decided that, once per volume, on the third issue, we want to keep up the tradition of getting a fresh set of eyes. In this issue, 2.3, those eyes belong to Manuel Schmalstieg of Greyscale Press. This time around, Manuel brings us an inventory of new ways of thinking about type design, with a couple side excursions into publishing.

Undeniably, the history of digital type design and typography has been a trip. Moving from the high-fidelity glyphs of pre-digital type, to the aggressively pixelated letters of early computation, we now see both a glut and a renaissance in fonts for the web and other digital applications. The projects covered in this issue pick up on that trip. From generative and parametric work to approaches for curating and classifying type, the work represented in this issue presents a window into the current state of F/LOSS type design and typography. From the taxonomies and ways of organizing embodied in some of the projects, to new ways of devising and designing type, this issue also goes beyond just looking at what's happening now, and imagines some of the futures of F/LOSS type design and typography. We invite you to explore the works and ideas collected by our guest editor, and to come along as we take a look at the trip Libre type is taking.



