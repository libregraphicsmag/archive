Title: Small and Useful
Section: Small and Useful

There's an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well.

This issue, we've switched things up a little. All of our small and useful tools do one thing: they manage fonts. Whether you're trying to get previews of several thousand fonts, or interested in sorting your collection based on tags, here are a few font managers to try out.

----

## Fontlinge

It organizes your fonts, creates a database, and offers automated specimens. It's a little archaic to install, but that's part of the fun.

<http://sourceforge.net/projects/fontlinge>

----

## Font Manager

True to its name, Font Manager manages your fonts, and does so visually. It offers preview texts and lets you sort based on a variety of criteria.

<https://code.google.com/p/font-manager>

----

## Fontmatrix

One of the longest-reigning Linux font managers, Fontmatrix is still the go-to choice for many designers looking to find that perfect font in an expansive collection.

<http://fontmatrix.be>
