Title: Typographic Vagrancy
Section: Notebook

![](/images/2.3/selection/IMG_0401.jpg)

### Where

Paris

### What

A workshop in two parts, and in many senses of the word: a session introducing Libre type design tools, followed by a weekend-long marathon applying those new-found skills to develop Libre typography.

![](/images/2.3/selection/1poster.png)

### Most vernacular

The photo-walk that inspired the type created during the event—getting out, looking at signage around the venue, and taking cues from the locale.

### Best engagement

The outcome of the workshop was a Libre type pack for local use. Neighbours, associations and businesses in the surrounding area are now free to use their own, hyper-localized type.

![](/images/2.3/selection/IMG_0431.jpg)

### Best use of walls

Photos of the neighbourhood mingled with in-progress type in a glorious mess of glyphs, presiding over the event from a back wall in the communal work room.

![](/images/2.3/selection/IMG_0433.jpg)

<http://velvetyne.fr/marathongeneral>


