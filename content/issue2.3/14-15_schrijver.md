Title: Friendlier forks
Author: Eric Schrijver
Section: Column

The metaphors we live by are different in each era, and tell us about the social movements shaping the moment. In the time of Snowden and Zuckerberg, we get our metaphors from Silicon Valley. The "fork" is one such concept, originating in this case from the world of Open Source software development.

Initially considered a negative occurrence, a fork is when someone creates a new version of an existing project, taking a different direction from the original. A new style of Open Source collaboration, embodied in the popular code sharing platform Github (and Git, the system on which it is based), encourages forking. On Github, collaboration starts by creating a fork of a project and adding changes to this fork. Then one either contributes these changes back to the original repo (if the maintainer accepts it), or one goes one's own way—a fork in the traditional sense.

In the spirit of the fork, there is a lot to be said about how a pragmatic view on originality and authorship and an embracing of redundancy can make for a culturally rich ecosystem. These ideas are inspiring enough to see how they could also work outside the realm of software development. But when it comes to type design, one need not look to software development. To see how building upon existing creations makes typographic sense, one can look at type design history itself.

One of the seminal typefaces of post-war graphic design is described by its own creator as an improvement upon an existing font. Gill Sans is considered Britain's national sans serif, as seen on Penguin books and in the BBC logo. The typeface was designed by Eric Gill, who described it as an attempt to improve Edward Johnston's typeface made for the London Underground. Though one could go to great lengths enumerating the differences between the two typefaces, it is the idea that Gill Sans represents a proposition of improvement over an existing typeface that interests me.

In software, it can be confusing to have many forks of one project existing, because it is probably practical to use just one version of the package. Cultural artefacts like typefaces, however, can more easily exist in an abundance of similar guises, because they coexist. Both the inspiration and the inspired can be used by a contemporary designer. Gill Sans can exist comfortably alongside Johnston's typeface, without diminishing from its functionality. The fork is an addition, rather than a diminishment.

As more and more typefaces are becoming available under various open licenses, a type design culture of the digital fork becomes more and more feasible. Yet for this to happen, type designers might first need to give themselves permission to modify, to edit instead of re-making, and to tweak instead of reviving.

_[Eric Schrijver](http://ericschrijver.nl) (Amsterdam, 1984) is a graphic designer and a performance artist. He is inspired by programming culture. Eric teaches Design for new media at the Royal Academy of Art in The Hague, and is a member of the design collective Open Source Publishing._
