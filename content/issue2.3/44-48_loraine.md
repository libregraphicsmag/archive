Title: SPECIMEN
Author: Loraine Furter
Section: Feature

_Editor's note: This article was originally typeset by the author. While we did our best to preserve its original design choices, some of them were challenging to translate to the online medium. Be sure to take a look at the [original print version](http://libregraphicsmag.com/files/libregraphicsmag_2.3_lowquality.pdf#page=44) of the article (PDF)._

![](/images/2.3/01.png)

<span>"Phylogenetic Tree of Type History" V3.1, by Philipp H. Poll, 2011</span>


The form of type specimens has varied over time: single sheets, postcards, posters, books, and since the inception of digital typesetting, the arrival of the Internet, and the creation of web fonts, digital and web specimens. Since the first known specimen by Erhard Ratdolt in 1486, these documents have usually either displayed all the letters of the font from A to Z plus the punctuation and other glyphs; used pangrams—sentences containing all the letters of the alphabet—such as "The quick brown fox jumps over the lazy dog;" or have used "greeking," false Greek or Latin texts such as the famous lorem ipsum—derived and altered from Cicero's De finibus bonorum et malorum—to be shaped by the typefaces. 

Traditionally, collections of type specimens proposed a kind of neutral treatment for every font: the same layout, font size, text, etc. This, in addition to a sense of exhaustivity—the use of pangrams is representative of that tendency—and efficiency (one page per font or less) is supposed to help comparison of fonts. It suggests a very specific, formal approach to typography, and supposes that you already have content, naked, that needs to find the perfect shape, the "one".

The reason texts in specimens are usually uninteresting or even unreadable is because they aren't supposed to be read, but rather looked at. They are models, wearing the type until they are replaced by the "real content." Like the contestants in a beauty contest, all the typefaces are forced into the same swimming suits before it is announced: "let the most beautiful win!"

--------------

Today, since designers mostly use digitized and digital fonts, distributed through websites, specimens are usually found online. The tendency of contemporary web platforms to separate form from content (in which the content is stored in a database and pushed through a template) aligns perfectly with this aforementioned way of showing typefaces. We find this approach almost without exception in the foundries' websites and even more in the web font directories, where it is unfortunately rare to find something about the (hi)story of the font: why and how it was made, what were the references for the work, etc.  
The informative text on a font is usually after the following categories:

* Family (the name of the typeface)
* Category (Serif, Sans Serif, …)
* Designer
* License
* Full Language Support
* Description / Information

I've considered making a cut-up out of different description texts from font directories to give a sense of that kind of literature, and try to construct the most unspecific text ever written. But in fact these texts don't need to be edited to speak by themselves:

> "\*\*\* is the perfect font for body text and headlines on a website. Its modern style, suited with past characteristics of great typefaces, make it highly readable in any context. The full-circle curves on many characters make \*\*\* a great font to blend seamlessly with other fonts while still maintaining it's uniqueness.
>
> Whether to be used for body text or headlines on a web page, \*\*\* is the right font for any project."

The search engines of big font directories sort through the names of the fonts, their physical characteristics, whether they have serifs or not, width, thickness, popularity, but never through their "stories".  
And that isn't the only kind of literature that gets left out.

A digital font is made of programmatic instructions, visually interpreted and rendered by the interfaces used to create, read and print them. This dimension usually remains in the backstage of the computer.  
Funnily, an exception is when it comes to legal issues. Legally, the creative status of a font is not so clear, and in a way this is not really the main point of interest of this text. But it is interesting to note that in US law, fonts are considered as utilitarian objects and are thus exempt from copyright restrictions. And as computer programs are under copyright, many foundries consider their fonts as such in order to be subject to the same legal protections.  
This is taken very seriously by digital type foundries. One just has to look at their license agreements: in these sections, suddenly, the word font is associated with the word software in every sentence. Words that disappear once one gets back to other sections. Despite the legal importance of code, and even if it is today part of every font "body", little attention is paid to the code behind fonts.

In the context of type, using the term software means considering the fonts as a set of programmatic instructions. Basically, coordinates and bézier curve formulas.  
Like this: 

    <?xml version="1.0" encoding="UTF-8"?>
    <glyph name="A" format="1">
      <advance width="720"/>
      <unicode hex="0041"/>
      <outline>
        <contour>
          <point x="383" y="229" type="move" name="top"/>
        </contour>
        <contour>
          <point x="715" y="0" type="line"/>
          <point x="715" y="13" type="line"/>
          <point x="703" y="13" type="line" smooth="yes"/>
          <point x="645" y="13"/>
          <point x="609" y="67"/>
          <point x="588" y="121" type="curve" smooth="yes"/>
          <point x="369" y="677" type="line"/>
          <point x="366" y="677" type="line"/>
          <point x="128" y="150" type="line" smooth="yes"/>
          <point x="110" y="111"/>
          <point x="69" y="13"/>
          <point x="13" y="13" type="curve" smooth="yes"/>
          <point x="5" y="13" type="line"/>
          <point x="5" y="0" type="line"/>
          <point x="226" y="0" type="line"/>
          <point x="226" y="13" type="line"/>
          <point x="218" y="13" type="line" smooth="yes"/>
          <point x="159" y="13"/>
          <point x="141" y="56"/>
          <point x="141" y="100" type="curve" smooth="yes"/>
          <point x="141" y="127"/>
          <point x="149" y="154"/>
          <point x="157" y="173" type="curve" smooth="yes"/>
          <point x="197" y="263" type="line"/>
          <point x="430" y="263" type="line"/>
          <point x="442" y="233" type="line" smooth="yes"/>
          <point x="469" y="164"/>
          <point x="488" y="111"/>
          <point x="488" y="74" type="curve" smooth="yes"/>
          <point x="488" y="34"/>
          <point x="465" y="13"/>
          <point x="407" y="13" type="curve" smooth="yes"/>
          <point x="395" y="13" type="line"/>
          <point x="395" y="0" type="line"/>
        </contour>
        <contour>
          <point x="322" y="538" type="line"/>
          <point x="422" y="283" type="line"/>
          <point x="206" y="283" type="line"/>
        </contour>
      </outline>
    </glyph>

In case you didn't recognize it, this was the code of the "A.glif" file from the Open Baskerville font.  
Let's not be afraid of it.  
If you read it, this code describes the drawing of a classical digital font: built as outlines. The points are coordinates for the contour of the letter.

![](/images/2.3/A.png)

Technically, the code of every typeface is public, as they can all be opened in font editing software and reveal the position of their points and curves. But unlike software, the interest and originality of a font usually still remains the same: its shape. Even though there is a growing interest in programmatic fonts, fonts today are hardly ever designed by writing code, they are instead drawn in visual interfaces, and in the end it is their visual form that is read and used.  
Rather than viewing these approaches separate from fonts, wouldn't it be nice to consider all these aspects–that is the visual aspects, as well as the code and the history of a font–as a whole?  
The F/LOSS approach seems to offer a good framework for such a proposition.

In his essay _Take Care_, art critic Anthony Huberman speaks about "thankful" behaviour for institutions and curators, inviting them to perform their job in the key of the "I Care" (borrowed to Jan Vervoert's essay Exhaustion and Exuberance): paying homage to who or what they work with, taking the risk of performing both the "I Know" and "I Don't".

F/LOSS approach makes me feel something similar. There, people are invited to perform in the key of the "I Share," sharing one's work and making it possible for others to appropriate it, enrich it, enabeling a dialogue. And more importantly sharing one's sources, in the sense of recipes and instructions (code), but also sources in the sense of references and inscription in a (hi)story.  
It's about influence, as a positive dynamic of circulation and transmission.  
F/LOSS approaches emphasize the evolution, the history and narrative of projects, with frameworks and tools that facilitate the documentation of a project and show the lineage between projects.  
Soon, with versioning systems such as Git, we will be able to visualize the evolution of a typeface, from 0.0 to 1.0 and beyond.  
No type design comes completely out of nowhere. In fact, it's more the concept of originality or uniqueness that comes out of nowhere. Copy, re-interpretation of typefaces has always been part of the history of typography[^1], and still today old metal typefaces need to be translated into digital formats to be used on new mediums.  

The lines you've just read are composed in Linux Libertine, the body text font of Libre Graphics magazine, with Linux Biolinum [Fig.1] and Prop Courier Sans.  
The multilingual Linux Libertine was created in 2003 by Philipp H. Poll, and is inspired by 19th century book typefaces, such as Janson and Palatino, in turn inspired by Renaissance models. Its name is a very playful reference to the different meanings of "Open" and "Free". Not monogamist, it brings another metaphor to this whole specimen story, the family tree and its wild branches: bastards and other milkman's sons. [Fig.2]

![](/images/2.3/sade-hires.png)

It is very nice to be embrace the voyeur posture and observe the multiple relations between F/LOSS fonts. 

Friendly fork of the Not Courier Sans by OSP, Prop Courier Sans is "not here to be correct". To create this proportional version of the Not Courier Sans, Manufactura Independente developed a tool[^2] called _transpacing_, which transplants the spacing information from a font to another. With adaptations and regular tweaks, this font evolves hands in hands with Libre Graphics magazine.  
Less polite and without softening the edges, OSP took a cutting from Nimbus Mono to grow Not Courier Sans during the Libre Graphics Meeting in 2008, in Wroclaw.

Proposing another kind of tree, the colophon of the book Transparence Camouflage Opacité by Samuel Rivers-Moore highlights the lineage between the font he created for the book, Arcadia, and its "sources".   
Its graphical interpretation mixes the family tree and an indentation referring to programming languages.

![](/images/2.3/box.png)

And there again we find Libertine cooking up with OSP in a Limousine!

This article is a call for more attention to the (hi)stories in (F/LOSS) fonts projects. 

--------------

![](/images/2.3/libertinage.png)![](/images/2.3/biolinum.png)

--------------

### SPECIMEN

This article is a special version of the "about" of a web project called _SPECIMEN_ – a blog on F/LOSS fonts: <http://specimen.meteor.com>. _SPECIMEN_ is an attempt to propose a different approach to (choosing) typefaces, another way of considering specimens. Far from pretending to be neutral or exhaustive, it rather assumes its partial aspect. Each font thus has its own specific treatment, with its own content and shape, all contributing to tell its story: the (hi)story of the font is re-placed at the center of the specimen. 

This project is inspired by the _Open Source Font Specimens_, by Greyscale Press. Greyscale Press released the specimen book _L'Ève future – Spécimens de fontes libres_, collection of font specimens for usage in print, with F/LOSS fonts. Created during a workshop, this specimen book uses as sample text the whole science-fiction novel _L'Ève future_, by Auguste de Villiers de L'Isle-Adam, published in 1886 and thus in the public domain.

It's a great project that made me want to go further in the questioning of the still rigid production and use of specimens today.

--------------

### References

On appropriation:

* Ricardo Lafuente, "Appropriation and Type—Before and Today", 2008. <http://ospublish.constantvzw.org/blog/typo/appropriation-and-type-before-and-today>
* "You Need to Copy to Understand, interview with Harrisson", OSP-Blog, 2006. <http://ospublish.constantvzw.org/blog/typo/you-need-to-copy-to-understand>
* <http://i.liketightpants.net/and/no-one-starts-from-scratch-type-design-and-the-logic-of-the-fork>

On specimen:

* Nick Sherman, "Le design des spécimens typographiques", The Shelf n°1, 2012.
* _L'Ève future – Spécimens de fontes libres_, Greyscale Press, 2013.


[^1]: On this specific question, read the article "Appropriation and Type – Before and Today" by Ricardo Lafuente, 2008.

[^2]: Wouldn't it be nice to do a specimen for a font program? 

