Title: Forging foundries
Authors: Ana Isabel Carvalho, Ricardo Lafuente
Section: Dispatch

The Libre type design ecosystem has flourished in recent years. First, with hubs like Open Font Library, started in 2006, then followed by smaller, independent Libre foundries such as The League of Movable Type, OSP Foundry, and the VTF Type Foundry. In 2010, Google Fonts came onto the scene, kickstarting the rise of Libre typography by sponsoring type designers around the world to produce more Libre fonts, in a collection that's growing bigger every day.

But there hasn't been much recent activity outside the existing hubs and foundries. We think this is caused by the difficulties of publishing a font. The act of packaging and publishing is an integral part of the type design process, but it’s under-documented when compared against the act of designing a typeface. Packaging and publishing are not glamourous tasks, and we believe that the lack of a standard workflow for postprocessing and distributing a font is a major bottleneck in production.

After finishing the design of a font, as well as taking care of spacing and hinting, there are many small tasks required before a public release:

* choosing the output file formats
* picking a license
* setting and validating font metadata
* documenting the work (the README and FONTLOG files)
* ensuring compatibility with various systems
* creating a downloadable package file
* uploading everything to a server

If a designer is also running their own type foundry, there are additional tasks:

* creating new pages for the font
* updating the website text
* adding specimens and/or preview images
* adding classification or category
* uploading the package to other font distribution sites (if applicable)
* maintaining the website backend
• periodically ensuring every published version and metadata are up to date

The lack of standard ways to carry out these tasks makes packaging and distribution of fonts difficult and highly individual, with each designer and/or foundry devising their own preferred formats, methods and workflows for font postprocessing and publication. Raphaël Bastide addressed this problem with the [Unified Font Repository](https://github.com/raphaelbastide/Unified-Font-Repository), a standard proposal for Libre type font distribution.

Debian, one of the oldest and stablest Linux distributions, has provided us with clues for structuring a potential solution. Software for the Debian operating system can be found in a central repository. This repository is curated in a decentralized fashion by volunteers, organized in a clear hierarchy, who are in charge of reviewing software and packaging it for Debian. The Debian package model decouples the roles of the developer and the packager. The developer is responsible for coding the software, whereas the packager is in charge of reviewing and preparing the code to be distributed and used. Developers don't need to further burden themselves with the intricacies of packaging software for specific operating systems, and packagers are an important addition to the development flow by ensuring and certifying the integrity of software. This clear division creates a structured and standardized workflow for the release of software, minimizing the chance for errors and ensuring a smooth collaborative workflow.

We think Libre type design could adopt similar habits.  Foundry-in-a-box is an effort at providing a common way to package and distribute typefaces. We propose a flexible format for font packages and a set of tools that take advantage of the format for validating, editing, and publishing fonts, including a foundry creator.

Before proposing such a system, though, we first need to define what a font package actually is. Our definition would be the collection of elements that are essential for releasing and distributing a font or font family: the font files, metadata, and extra information. It could be extended to include specimens, extended documentation, and other miscellaneous data.

Font packages enable a simple workflow for publishing fonts.

1. Design the typeface and save it as a UFO file.
2. Add the user-specified metadata in `fontinfo.json` (author info, date, license, etc).
3. [Optional] Add a README file with an introduction and description of the font file.
4. [Optional] Add or edit the FONTLOG with the recent additions.
5. Validate the package to ensure everything is ready to publish.
6. Publish the package to the remote Git repository.

Editing and updating existing packages is also straightforward.

1. Update the typeface's UFO file.
2. Update `fontinfo.json`, README, FONTLOG if necessary.
3. Validate the package to ensure everything is ready to publish.
4. Log the changes in a Git commit message.
5. Publish the package to the remote Git repository.

So far, we've developed a proof-of-concept command line application, [fib](https://gitlab.com/manufacturaind/fib), in order to deal with all of these steps.  This forms the best base to later develop a desktop or browser-based application to create and edit font packages.

We're also developing a special mode of the _fib_ tool for a use case that will be familiar to type designers: the ability to create and maintain a simple type foundry website, which we're already using for our own foundry project.  The generator is based on a list of fonts to include, as well as miscellaneous content. The generated site is plain HTML, generated using the Jinja templating language.

All of this is a tentative first step in thinking about packaging and distribution as crucial steps in type design.  To enrich the Libre type ecosystem, we need to take some of the frustration out of making typefaces available and maintaining foundries.
