Title: About
Section: None
Category: page

### About this archive

This site puts together, in web form, the full content of all the published issues of Libre Graphics magazine.

It was built using [Pelican](http://getpelican.com) to convert all the articles in Markdown format to a static website.


### A Reader's Guide to Libre Graphics Magazine

In the articles archived here, you may find concepts, words, ideas and things that are new to you. Good. That means your horizons are expanding. The problem with that, of course, is that sometimes, things with steep learning curves are less fun than those without. 

That's why we're trying to flatten the learning curve. If, while reading Libre Graphics magazine, you encounter an unfamiliar word, project name, whatever it may be, chances are good there's an explanation. 

In this archive, as in the magazine, you'll find a glossary and resource list. The glossary aims to define words that are unique to the world of Libre Graphics. The resource list provides valuable information about tools, licenses, whatever items we may be mentioning.

Practically, this means that if, for example, you're reading an article about DejaVu (issue 1.1, pages 54 to 57), you can always check out the glossary, look up DejaVu, and become quickly informed about it. This provides some instant gratification, giving you the resources you need to understand, in a moment, just what we're talking about.

We hope you like our system.
