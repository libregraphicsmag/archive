Title: Glossary
Section: Glossary

802.11
:  Standard specification for the way wireless communication (WiFi) between computers should be processed.

Alchemy
:  A F/LOSS canvas drawing program, meant to encourage play and exploration. Available for GNU/Linux, Mac OS X AND Windows.

Arduino
:  A popular open hardware platform for rapid electronic prototyping.

Apache license
:  A permissive license, normally used for software, developed by the Apache Software Foundation.

Aptana Studio
:  An IDE for creating web applications. Available for GNU/Linux, Mac OS X and Microsoft Windows.

ArgyllCMS
:  A colour management system with icc profile compatibility for a variety of display and capture devices. Available for GNU/Linux, Mac OS X and Microsoft Windows.

ASCII
:  An early standard used for the encoding of characters. Notable for its small character set, which can be contrasted with Unicode.

Audacity
:  A F/LOSS sound editing application for GNU/Linux, Mac OS X and Microsoft Windows.

bash
:  A freely licensed Unix shell. See command line.

Bézier curve
:  A type of mathematical curve used extensively in vector graphics.

Bitstream Vera
:  A typeface released under a permissive license, allowing modification. 

Blender
:  A powerful F/LOSS 3D animation application for GNU/Linux, Mac OS X and Microsoft Windows.

Bug
:  A technical error in either software or hardware. Apocryphally believed to have been popularized by insects causing faults in early analog computers.

C
:  A popular, early programming language, still widely used, especially in high performance applications.

Cache
:  A temporary, local copy of a file or files, often used to speed up loading times for websites.

CAPTCHA
:  “Completely Automated Public Turing test to tell Computers and Humans Apart” is a clunky acronym for the familiar images which show sequences of letters and/or numbers to be typed out, with the purpose to confuse tools which automatically fill out forms, often used by spammers.
:  System for differentiating humans from computers, through the ability to recognize sets of random, distorted characters. Commonly used for SPAM prevention. 


Chiplotle
:  A Python API for pen plotters. Implements HPGL. Available as a Python library.

CMS
:  Stands for Content Management System. Software installed on a server in order to provide a simple framework for editing webpages. WordPress and Drupal are examples of content management systems.

colord
:  A tool for installing, managing and creating colour profiles. For GNU/Linux.

Command line/console
:  A text-only user interface, allowing users to input commands without the intervention of cursors and graphical user interfaces.
:  A text-based interface for controlling a computer.

Computer Vision
:  A research field that encompasses techniques for automatically analyzing and understanding images.

Constant
:  An arts organization based in Brussels, Belgium. The parent organization of Open Source Publishing.

copyleft
:  A style of licensing in which those redistributing the work are required to do so under its original (or a compatible) license.

Creative coding
:  Programming not for explicitly functional purposes, but for artistic or creative expression.

Creative Commons
:  A suite of licenses designed to allow creators and users of works flexibility beyond that offered in traditional copyright.

CSS
:  A language built to complement HTML and establish the separation between content and style: while HTML sets the document structure and hierarchy, CSS is used to define how every element will look.

Database
:  Read-write venue for storing information, applied broadly in computation. 

desktop environment
:  A collection of tools and interface elements which style the visual and functional aspects of an operating system in a certain way. 

Desktop Publishing (DTP)
:  Programs built for creating publications and other kinds of printed matter, which often offer powerful visual editing capabilities and complex typography.

Distro/Distribution
:  A specific configuration or "flavour" on GNU/Linux, often designed with a particular purpose in mind.

Digital Rights Management (DRM)
:  Technologies (of whatever sort) which prevent users from making certain uses of the larger technologies to which the DRM is applied. 

distro/distribution
:  A specific configuration of GNU/Linux, often designed with a particular purpose in mind.

Domain extension/top level domain
:  A component in every domain name. Common TLDs such as ".com" or ".net" are available to anyone, while regional TLDs such as ".ca" or ".pt" may only be available to residents of the countries they represent.

Fedora
:  A popular distribution of GNU/Linux, produced by Red Hat, Inc.

Firefox OS
:  An operating system intended for use on mobile phones. Developed by Mozilla.

firmware
:  Software designed to be permanently installed on a computer/device. Free software:A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.

flavour
:  Similar in meaning to distro/distribution, but more general. Simply means a specific version (normally of GNU/Linux).

Flickr Commons
:  A repository of photographic images, sourced from institutions around the world, with no known copyright restrictions.

FontForge
:  A F/LOSS font editor for GNU/Linux, Mac OS X and Microsoft Windows.

Fontmatrix
:  A F/LOSS font management and selection tool for GNU/Linux, Mac OS X and Microsoft Windows.

Free
:  As in freedom, or often, that which is or is of Free Software.

Free Art License
:  A copyleft license designed for creative works.

Free Culture
:  A general term for activities and artistic works which fall under a similar ideological banner to the Free Software movement.

freedesktop.org
:  A F/LOSS project which focuses on creating interoperable tools for GNU/Linux and other Unix-type systems. 

Free/Libre Open Source Software (F/LOSS)
:  Stands for Free/Libre Open Source Software. Software which has a viewable, modifiable source. It can be modified and redistributed.

Free Software
:  A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.

Front end/back end
:  A distinction in web development between code which runs on the computer of the person viewing a given site (front end) and on the server which hosts the site (back end).

Gaussian blur
:  A common image effect present in most bitmap editors, intended to reduce noise and image focus. Its name comes from the mathematical operation used as the formula to alter the image pixels.

Geomerative
:  Library for Processing, with capacity for vector graphics and typography.

GIMP
:  A raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows. (Sometimes spelled Gimp) 

Git
:  A popular version control system, originally created to manage development of the Linux kernel.

Glitch art
:  A form of art which attempts to produce or leverage bugs or mistakes in software and hardware.

GNOME
:  A popular desktop environment for GNU/Linux.

GNU General Public License (GPL)
:  A license originally intended for use with software, but now used for other applications. Made famous the principle of Copyleft, requiring those using GPL licensed work to license derivatives similarly.

GNU/Linux
:  A group of operating systems which are built on the Linux kernel and components from the GNU project, among others, which are widely distributed and freely modifiable.

hacktivism
:  A movement or school of belief based around the ideas of networked activism, aided by technical knowledge.

hexadecimal code
:  A six-character code prefaced by a hash symbol (#), used to define colour, especially for web applications. Eg. #000000 for black.

HTML
:  The language for describing web pages, enabling the use of everyday features like links, metadata and other information. Stands for Hypertext Markup Language.

Hugin
:  Panoramic photo stitching software for GNU/Linux, Mac OS X and Microsoft Windows.

IDE
:  A text editor specialized in writing code, usually including other tools useful for developers. Stands for Integrated Development Environment.

ImageMagick
:  A bitmap image editor for the command line.

implement
:  The act of integrating a feature or standard into a piece of software, rendering that software able to (for example) perform a task or use a specific file format.

Inkscape
:  A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

Internet Relay Chat (IRC)
:  A popular form of internet-based real-time chat. Has a long history of use and is still popular among groups of developers and users. 

Java
:  A programming language and platform developed by Sun Microsystems, intended for nearly universal compatibility with a variety of devices. 

JavaScript
:  A scripting language commonly used on websites. 

jQuery
:  A popular library used to write and integrate JavaScript efficiently.

KDE
:  A community project which produces various F/LOSS applications, best known as a popular desktop environment for GNU/Linux.

Kdenlive
:  F/LOSS video editor, available for GNU/Linux, Mac OS X, FreeBSD.

Kiwix
:  An offline web browser, originally developed to provide offline access to Wikipedia.

Krita
:  A drawing application supporting both vector and raster images. Available for GNU/Linux, Freebsd and Microsoft Windows.Laidout:A layout and desktop publishing application for GNU/Linux.

LAN
:  Local Area Network, a term used to refer to wired networks of many computers.

Laplacian
:  A smoothing operation for 3D objects. Similar to a Gaussian blur for points in 3D space, useful for smoothing surfaces and details.

Libgeos/Geometry Engine Open Source (GEOS)
:  A library for rendering geometry. Available for GNU/Linux and Windows.

Library
:  An encapsulated set of functions intended to be used in the development of larger programs. Often used to make common tasks easier to implement.

Libre
:  A less ambiguous adaptation of the word Free. Implies liberty of use, modification and distribution.

LibreOffice
:  A F/LOSS office suite incorporating standard productivity tools such as a word processor, spreadsheets and slideshow builder, among others. Available for GNU/Linux, Mac OS X and Windows.

Luciole
:  A stop motion animation program for GNU/Linux.

Mailing list
:  An email based forum through which subscribers may receive announcements, view or participate in discussion.

Mandrake
:  A distribution of GNU/Linux. Now known as Mandriva.

Markdown
:  A lightweight markup language, designed to describe rich text features (styles, lists, links, etc) in plain text files.

Meteor
:  A JavaScript-based platform for the development of web applications.

Movable Type
:  A F/LOSS blogging platform released under the GNU GPL.

Mozilla
:  A non-profit organization best known as the developer of the Firefox web browser. 

MyPaint
:  A digital painting application with a focus on fluid workflow. Available for GNU/Linux, Mac OS X and Microsoft Windows.

Node.js
:  A JavaScript-based system for creating internet applications.

OCR
:  Stands for Optical Character Recognition. Refers to the algorithms and techniques used by software to discern text from images.

Open Data
:  A movement defending transparency and openness of public records and their availability in open formats.

open hardware
:  Hardware which follows the same principles as F/LOSS, including publicly available, freely licensed schematics.

open license
:  A license which allows and encourages re-use and appropriation of creative works, in contrast to the all rights restricted norm provided by traditional copyright. Examples include the GNU GPL, the SIL OFL and the Creative Commons family of licenses.

Open Source
:  See Free/Libre Open Source Software.

open standards
:  A standard which is available for viewing and implementation by any party, often at no monetary cost.

Open web
:  A concept based around the combination of open standards and open licenses. Follows the ideal that development on the web should follow best practices of accessible and modifiable code and content.

OpenStreetMap
:  A permissively licensed world map project developed by a community of contributors.

OpenType
:  A vector-based font format intended to offer more advanced features than its predecessor, TrueType.

OSP (Open Source Publishing)
:  A graphic design collective which is dedicated to producing high quality work using F/LOSS tools.

Oxygen
:  A project meant to develop a coherent and attractive visual identity for kde.

PANOSE
:  A typeface classification system first developed in the 1980s.

Paper.js
:  A web-friendly JavaScript version of Scriptographer.

PCB
:  A printed circuit board, the base of most electronics, generally consisting of a number of electronic components linked together to form certain types of circuits.

Pencil
:  2D animation software with support for both vectors and rasters. Available for GNU/Linux, Mac OS X and Microsoft Windows. 

Perl
:  A popular programming language, often used for writing web applications. 

PHP
:  A popular scripting language, used for web development.

Popcorn.js
:  An interactive web framework which allows increased interactivity with video content.

Programming language
:  An artificial language with a restricted syntax, used as an intermediary between computers and human programmers. 

Processing
:  A programming language and development environment predominantly used for visually-oriented or media-rich projects. Available for GNU/Linux, Mac OS X and Microsoft Windows.
:  A minimal, designer-oriented programming environment, popular for introducing coding to visually-oriented people.

proprietary
:  A piece of software or other work which does not make available its source, which is not allowed or intended to be modified or redistributed without permission.

public domain
:  The legal status of a creative work for which the copyright (or other rights restriction) has expired. A work in the public domain can be used by anyone, for any purpose, without restriction. Licenses such as the Creative Commons CC0 license emulate public domain.

Pure Data
:  A visual programming environment designed for the production of interactive multimedia and audio works. Available for GNU/Linux, Max OS X, Microsoft Windows, iOS and Android.

Python
:  A popular interactive programming language. Available for GNU/Linux, Mac OS X and Microsoft Windows.

Qt
:  Pronounced "cute." A F/LOSS development framework which functions across platforms and provides a common set of visual elements. 

Raster
:  A method of images which makes use of pixels of colour. Treating an image like a grid of squares, pixels are laid out to form shapes.

Raspberry Pi
:  A small computer about the size of a pack of cigarettes, famous for its hackability and suitability for DIY hardware projects.

Repository
:  A stored collection of software packages, from which those packages may be dowloaded and installed. 

RGBA
:  A colour space commonly used on digital displays. 

Ruby
:  A modern scripting language commonly used in web development. 

Ruby on Rails
:  A popular framework, written in Ruby, used to make web applications.

+rwx
:  A reference of a common Unix command, chmod; the +rwx argument makes one or more files readable, editable and executable.

Scalable Vector Graphics (SVG)
:  An open standard for vector graphics, developed by the W3C.

“scratching an itch”
:  A term used in many F/LOSS communities to mean that a developer has created a program primarily to meet his/her own needs, instead of the needs of others.

Scribus
:  A desktop publishing application for GNU/Linux, Mac OS X and Windows.

script
:  A small program, often used to control a larger program or block of code.

Scriptographer
:  A scripting plugin for Adobe Illustrator which gives the user the possibility to extend Illustratorʼs functionality by the use of the JavaScript language. Scriptographer is a GPL-licensed plugin for proprietary software.

Server
:  A computer hosting data which is accessed remotely. 

SIFT
:  Scale-invariant feature transform, an algorithm used in computer vision to determine shapes and relevant features in images.

SIL Open Font License (OFL)
:  A license intended for use with fonts and font related software. Dictates terms which allow modification and redistribution of fonts.

source code
:  The human readable code on which software is based. Software distributed with its source code can be modified far more easily than software distributed without.

SSID
:  Service Set Identifier, the technical designation for a wireless network’s visible name.

STL
:  A file format for CAD files, geared towards representation of 3D objects. It has become the “de facto” format for 3D printing.

Subversion
:  A F/LOSS version control system for GNU/Linux, Mac OS X and Microsoft Windows.

Synfig
:  Timeline-based animation software supporting both vectors and rasters. Available for GNU/Linux, Mac OS X and Windows.

Template
:  In software or content management terms, a pre-made set of styles which can be applied on-demand to content.

Telnet
:  A protocol for networked communication. 

terminal
:  A program which allows users to perform actions on the command line.

Tesseract
:  A free software OCR library, known for its recognition accuracy.

TeX
:  A typesetting system devised by Donald Knuth, relevant for its lack of a graphical UI and heavy use of specialized markup.

They
:  Most commonly known as a collective pronoun, it is also used as a gender-neutral singular pronoun. 

ToonLoop
:  A stop motion animation application for GNU/Linux. A minimal version is available for Mac OS X and Microsoft Windows.

Torvalds
:  Linus Torvalds, the originator of Linux.

TrueType
:  A common vector-based font format.

Ubuntu
:  A particularly popular distribution of GNU/Linux, produced by Canonical Ltd.

UFO (Unified Font Object)
:  A cross-platform font format, based on XML. Intended to be human-readable.

UI
:  User Interface. Refers to the set of disciplines and practices related to designing and implementing the elements which allow people to interact with computers, both in hardware (keyboards, touchscreens) and software (buttons, sliders and layouts).

Unicode
:  A standard used for the encoding of characters. The term is often used to refer to the set of characters defined by the standard.

UX
:  User Experience. Comprises the spectrum of subjects related to UI, considering them under the lens of psychology, ergonomics and human behaviour.

Version control
:  A means of managing changes (and allowing reversion) to a commonly held body of work, most often a software project. 
:  Activities which have the effect or intent of distinguishing different versions of a work or body of work from one another.

Version Control System (VCS)
:  An application/collection of tools designed to facilitate version control. Tracks changes to files and allows a group of collaborators to share their changes as they are made.

vi
:  A F/LOSS text editor developed in the 1970s and still in use. Available for GNU/Linux, Mac OS X and Microsoft Windows.

W3C
:  The organization responsible for setting web standards, such as HTML5 and SVG.

Web font
:  A font housed on a server which can be dynamically called up for use on a web page.

Web-scraping
:  The use of automated tools such as scripts to extract text and other information from web pages with minimal human intervention.

Wireless beacon
:  Any device that can emit and receive wireless signals according to the 802.11 standard. Commonly used to refer to routers, but also applicable to laptops or smartphones that can be configured as a WiFi access point.

XPM (XPixMap)
:  An image file format, which uses ASCII characters and a defined palette to describe a raster image.

Zotero
:  A reference management application with extensions for browsers and word processors. Available GNU/Linux, Mac OS X and Microsoft Windows.

