Title: New Releases
Section: New releases
Tags: Tools

## Reclaim your tools, by Jakub Szypulka

Documents the slow beauty and diversity of activity to be found at even the most hectic meeting of software contributors. In this case, documenting Libre Graphics Meeting 2010. Made using Kdenlive and Audacity. 

<http://vimeo.com/18568225>


## AdaptableGIMP

A new version of GIMP, which allows users to make easy customizations. Read more about it on pages 46-50.

<http://adaptablegimp.org>



## ArtistX 1.0

A version of GNU/Linux which bills itself as able to turn a ‘common computer into a full multimedia production studio.’ Based on Ubuntu and designed for multimedia artists.

<http://www.artistx.org/site3>



## CrunchBang 10 Statler

CrunchBang is version of GNU/Linux notable for its community of users who actively share screenshots of their modifications to the desktop. They share not only screenshots of their modifications, but also instructions for replicating their results.

<http://crunchbanglinux.org>
