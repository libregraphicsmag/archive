Title: Masthead
Section: Masthead
Slug: masthead-12

Libre Graphics Magazine 1.2

**Use cases and affordances**

February 2011

ISSN: 1925-1416


## Editorial Team

* [Ana Carvalho](mailto:ana@manufacturaindependente.org) 
* [ginger coons](mailto:ginger@adaptstudio.ca) 
* [Ricardo Lafuente](mailto:ricardo@manufacturaindependente.org)

## Publisher

ginger coons


## Administrative Partner

[Studio XX](http://www.studioxx.org)


## Community Board

* Dave Crossland 
* Louis Desjardins 
* Aymeric Mansoux 
* Alexandre Prokoudine 
* Femke Snelting 


## Contributors

* Dave Crossland
* Seth Kenlon
* Pierre Marchand
* Allison Moore
* Pierros Papadeas
* Nuno Pinheiro
* Antonio Roberts
* Eric Schrijver


Printed in Montréal by [Mardigrafe](http://mardigrafe.com) on recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 

Write to us at [enquiries@libregraphicsmag.com](mailto:enquiries@libregraphicsmag.com)

<http://libregraphicsmag.com>

--------------

## Images under a CC Attribution Share-Alike license

* Cover, inside cover, index and showcase separator page illustrations by Manufactura Independente based on [images by Flickr user fdctsevilla](https://www.flickr.com/photos/fdctsevilla/albums).
* Photo of ginger coons by herself.
* Photo of Ricardo Lafuente by Ana Carvalho.
* Photo of Ana Carvalho by Luís Camanho.
* Photo of Dave Crossland by Mary Crossland.
* Photo of Eric Schrijver by himself.
* Illustrations in "Coding pictures" by Joana Estrela, Lídia Malho, Telmo Parreira, Sofia Rocha e Silva, Fábio Santos, Edgar Sprecher.
* Illustration in "Setting a book with Scribus" by Manufactura Independente based on "Book 8" by Flickr user brenda-starr.
* Signs in "Best of SVG" by Wikimedia Commons.
* Screenshots in "Desktop" by Pierros Papadeas.
* Photos in "Interview with Oxygen's Nuno Pinheiro" by Manufactura Independente.
* Icons and wallpaper in "Interview with Oxygen's Nuno Pinheiro" by the Oxygen project.
* Photos in "AdaptableGimp: user interfaces for users" by ginger coons.
* Screenshots in "AdaptableGimp: user interfaces for users" by Ben Lafreniere.
* All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.
* GIMP, Inkscape, Kdenlive, MyPaint, Scribus and Web Font Downloader screenshots in "Resources" by Manufactura Independente.

## Images under other licenses

- Signs in "Best of SVG" are all in the Public Domain. They can be found in:
 * <http://commons.wikimedia.org/wiki/File:UK_motorway_matrix_fog_nuvola.svg>
 * <http://commons.wikimedia.org/wiki/File:Ohituskielto_p%C3%A4%C3%A4ttyy_352.svg>
 * <http://commons.wikimedia.org/wiki/File:Norwegian-road-sign-406.0.svg>
 * <http://commons.wikimedia.org/wiki/File:Raitiovaunun_pys%C3%A4kki_533.svg>
 * <http://commons.wikimedia.org/wiki/File:K%C3%A4velykatu_575.svg>
 * <http://commons.wikimedia.org/wiki/File:Zeichen_238.svg>
 * <http://commons.wikimedia.org/wiki/File:Laser-symbol.svg>
 * <http://commons.wikimedia.org/wiki/File:PN_Pcha%C4%87,_aby_otworzy%C4%87.svg>
 * <http://commons.wikimedia.org/wiki/File:GHS-pictogram-explos.svg>
- [Blender screenshot](http://en.wikipedia.org/wiki/File:Blender3D_2.4.5-screen.jpg) in "Resources" by Wikipedia user DingTo. GNU General Public License. 


## General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.
