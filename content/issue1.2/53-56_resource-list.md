Title: Resource list 1.2
Section: Resource list
Tags: Tools

# Resource list 1.2

### Blender

A powerful F/LOSS 3D animation application for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

### GIMP

A raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

### ImageMagick

A raster image editing, creation and conversion suite for GNU/Linux, Mac OS X, Microsoft Windows and iPhone, among others.

```
# Create a montage from a folder containing various png images montage -geometry 400x300+0+0 *.png icon-montage.png

# Scale all jpeg images in a folder to a width of 640px for img in *.jpg ; do convert $img -scale 640 $img; done;

# Rotate a batch of jpeg images 90º and convert them to png for img in *.jpg ; do convert $img -rotate 90 ${img/jpg/png} ; done
```

--------------

### Inkscape

A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

### Kdenlive

A video editor for GNU/Linux, Mac OS X, Microsoft Windows and Freebsd.

--------------

### Scribus

A desktop publishing program for GNU/Linux, Mac OS X and Microsoft Windows. 

--------------

### Web Font Downloader

An extension for Firefox, allowing downloads of embedded web fonts.

--------------

### MyPaint

Graphics application focused on natural media simulation. Available for GNU/Linux, Mac OS X and Microsoft Windows.
