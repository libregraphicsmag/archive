Title: Upcoming events
Section: Upcoming events


We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and F/LOSS. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the F/LOSS designer at a traditional design event, or maybe the designer at a predominantly software developer event.

--------------

1-3 April

**Flourish 2011**  
Chicago  
http://flourishconf.com/2011

--------------

30 April - 1 May

**Linux Fest Northwest**  
Bellingham, Washington  
http://linuxfestnorthwest.org

--------------

1-3 May

**Pica 2011**  
Banff, Alberta  
http://picaconference.ca

--------------

2 May

**agIdeas 2011: International design research lab**  
Melbourne  
http://agideas.net/agideas-2011/design-research-lab

--------------

7-21 May

**CHI 2011**  
Vancouver  
http://chi2011.org

--------------

9-13 May

**Icograda design week**  
Vilnius  
http://icograda.org/events/events/calendar738.htm

--------------

10-13 May

**Libre Graphics Meeting**  
Montreal  
http://libregraphicsmeeting.org/2011

--------------

19-21 May

**Typo Berlin**  
Berlin  
http://typoberlin.de/2011

--------------

19-22 May

**Live Performers Meeting**  
Rome  
http://liveperformersmeeting.net
