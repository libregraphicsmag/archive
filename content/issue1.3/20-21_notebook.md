Title: Libre Graphics Meeting 2011
Section: Notebook
Tags: Events, Libre graphics
Slug: lgm-2011

## Where

Libre Graphics Meeting in Montreal.

![](/images/1.3/IMG_4063.jpg)

--------------

## What

Annual meeting of users and developers of F/LOSS graphics software and work.

--------------

## Most Fun

Toonloop, as presented by one of its developers, Alexandre Quessy, with amazing potential for creative stop-motion animation. 

![](/images/1.3/OSPp1030110.jpg)

--------------

## Most Collaborative

Birds of a Feather (BoF) meetings of developers and users of software, including a great Inkscape chat, with remote participants joining via IRC. 

--------------

## Prettiest

Old lead and wood type at the Lovell printing plant, including lots of display faces, looking beautiful all on their own.

--------------

## Most Immersive

Sewing workshop by Susan Spencer, using Inkscape and Python to customize clothing patterns.

![](/images/1.3/OSPp1030112.jpg)
