Title: Upcoming events
Section: Upcoming events

We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and F/LOSS. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the F/LOSS designer at a traditional design event, or maybe the designer at a predominantly software developer event. 


--------------

14-18 Sep

**ATypI 2011**  
Reykjavik, Iceland  
http://www.atypi.org/2011-reykjavik

--------------

16 Sep

**Brand New Conference**  
San Francisco, United States  
http://www.underconsideration.com/brandnewconference

--------------

17-25 Sep

**The London Design Festival**  
London, United Kingdom  
http://www.londondesignfestival.com

--------------

24-25 Sep

**PyCon UK 2011**  
Coventry, United Kingdom  
http://pyconuk.org

--------------

1-18 Oct

** Phoenix Design Week**  
Phoenix, United States  
http://www.phxdw.com

--------------

13-16 Oct

**Pivot: AIGA Design Conference 2011**  
Phoenix, United States  
http://designconference2011.aiga.org

--------------

13-23 Oct

**Design Philadelphia**  
Philadelphia, United States  
http://www.designphiladelphia.org

--------------

15-16 Oct

**West Coast HackMeet**  
San Francisco, United States  
http://hackmeet.org

--------------

17-20 Oct

**SVG Open**  
Cambridge, Massachusetts  
http://www.svgopen.org/2011

--------------

19-21 Oct

**LatinoWare**  
Foz do Iguaçu, Brazil  
http://www.latinoware.org

--------------

19-22 Oct

**Access 2011**  
Vancouver, Canada  
http://access2011.library.ubc.ca

--------------

20-22 Oct

**Typo London 2011**  
London, United Kingdom  
http://www.typolondon.com

--------------

31 Oct  
4 Nov

**Ubuntu Developer Summit**  
Orlando, United States  
http://uds.ubuntu.com

--------------

2-3 Nov

**DesignThinkers 2011**  
Toronto, Canada  
http://www.designthinkers.com

--------------

4-6 Nov

**Mozilla Festival 2011**  
London, United Kingdom  
http://2011.mozillafestival.org
