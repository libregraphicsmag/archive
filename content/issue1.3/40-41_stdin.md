Title: &lt;stdin&gt;
Authors: Alexandre Leray, Stéphanie Vilayphiou
Section: Showcase

There seems to a be a tendency nowadays for collaboration and cross-disciplinarity, and graphic designers are not at rest. Many designers — including ourselves — are increasivelly working together with artists, thinkers or engineers, blurring the separation between the disciplines. Why? Could it be a way to escape from the division of labour? A way to escape from the thinking of graphic design as a service industry and to start thinking of design as an embedded process? Our intuition is that graphic designers don't want to be the last element of the production line anymore.

Coming from a classical visual design education we became more and more interested in digital culture and networked media. Now, we mix a visual approach with programming to create designs for print and non-print outputs. We proudly claim the two hats of designer and programmer because for us programming is also design. Moreover, we think programs are cultural items, at least as much as they are functional. F/LOSS carries this thought, but using it doesn't necessarily imply free culture. This is why we focus our personal researches on collective platforms based on F/LOSS. Sharing is not only about giving, it's also about getting back, it's about starting a discussion.

<http://stdin.fr>

--------------

#### 2008

##### Issue magazine 

<http://www.issue-magazine.net/>

Online critical magazine on graphic and media design.  
software: Python, HTML, JQuery  
fonts: <del>Verdana</del>  
content: CC-BY-SA-NC, fair use

![](/images/1.3/issue-day-5.png)

##### Rémy Jacquier

Catalogue of French artist Rémy Jacquier.  
software: <del>Indesign</del>  
fonts: <del>CourierSans</del>  
content: <del>© Rémy Jacquier</del>, <del>© ADERA</del>

![](/images/1.3/P1050003.jpg)

##### Datateb

<http://datateb.alexandreleray.com/>

Art piece mocking the software industry through recycle bins.  
software: <del>AppleScript</del>, Python, Django  
fonts: <del>Verdana</del>  
content: Free Art License  
source code: GNU GPL
<http://github.com/aleray/datateb/>

![](/images/1.3/datateb-01.png)


##### Google Will Feed Itself

<http://googlewillfeeditself.blogspot.com/>

A Google Blogspot fed with its own Google ads.  
software: <del>BlogSpot</del>, <del>GoogleAdsense</del>  
content   ?

![](/images/1.3/3188793258_b60a943146_b.jpg)

--------------

#### 2009

##### ERBA Valence 2009—2011

Information leaflet for Valence school of Fine Art and Design.  
software: <del>Indesign</del>, GraphViz  
fonts: <del>Nobel</del>  
content: ?

![](/images/1.3/P1040956.jpg)

##### Blind Carbon Copy

<http://bcc.stdin.fr/>

Experimental design hacks to circumvent “Intellectual Property”.  
software: Python, BeautifulSoup, NLTK, <del>Indesign</del>, JavaScript   
fonts: <del>New Caledonia</del>, Vogue  
content: all wrongs reversed  
source code: GNU GPL

![](/images/1.3/bcc.jpg)

##### Cataloged

<http://cataloged.cc/>

Online portfolio, also generating a printed one, of graphic designers Coline Sunier and Charles Mazé.  
with: Coline Sunier, Charles Mazé  
software: Mercurial, Python, Django, JQuery, MySQL, reportlab  
fonts: Inconsolata  
content   ?

![](/images/1.3/cataloged-03.png)

--------------

#### 2010

##### Libre Graphics Meeting 2010

<http://libregraphicsmeeting.org/2010/>

Website for LGM 2010 in Brussels.  
with, Alessandro Rimoldi, OSP  
software: Inkscape, HTML, anwiki  
fonts: Cantarell  
content   ?

![](/images/1.3/lgm-03.png)

##### please computer | make me design

<http://git.constantvzw.org/>

Workshop on fun commandline poster generation.  
with OSP (Ludi, Ivan)  
software: Git, GNU coreutils, enscript, podofoimpose  
fonts from OSP foundry  
content: Free Art Licence  
source code: GNU GPL  

![](/images/1.3/osp_out.jpg)

##### Éditions B42

<http://editions-b42.com/>

Website of Éditions B42 publishing house.  
software: Git, Python, Django, HTML, JQuery, MySQL  
fonts: <del>Verdana</del>, <del>Hermès Sans</del>  
content: <del>© Éditions B42</del>

![](/images/1.3/b42-01.png)

##### Disappearance

A word can only live once.  
with:  Marijke Schalken  
software: Python, mplayer  
source code: Free Art Licence  

<http://git.constantvzw.org/>

![](/images/1.3/disap.jpg)

##### Constant

Flyer for Constant, association for art and media in Brussels.  
software: Scribus  
fonts: NotCourierSans, Constant Archive  
content: Free Art Licence

![](/images/1.3/constant.jpg)

--------------

#### 2011

##### ACSR

<http://acsr.be/>

Atelier de Création Sonore Radiophonique sound repository.  
with: OSP (Ludi), Jérôme Degive  
software: GIMP, Inkscape, WordPress  
fonts: Univers Else  
content: <del>© right-owners</del>

![](/images/1.3/capture-acsr.png)

##### Schaarbeekse Taal

Flyers for events organized by the Schaarbeekse Taal project.  
with: OSP (Ludi)  
software: Git, GIMP, Inkscape  
fonts: Limousine, Alfabet III  
content: <del>© right owners</del>

![](/images/1.3/P1050801.jpg)

##### Curating as Environ-mentalism

<http://environmentalism.stdin.fr/>

Online interface to comment chunks of Elke van Campenhout's essay.  
software: Git, CouchDB, HTML, JQuery  
fonts: Ume Mincho  
content: All wrongs reversed  
source code: GNU GPL

![](/images/1.3/Curating_as_Environ-mentalism.png)

--------------

#### WIP

##### Le Chant des Particules

Website of a movie about LHC-CERN.  
with deValence  
software: Git, Python, Django, HTML, JQuery
fonts: <del>ChicaGogoRGB</del>  
content: ?

![](/images/1.3/LCDP-projet110211.jpg)

##### else if

<http://else-if.net/>

Collections of critical texts on graphic design and digital media.  
software: Git, Python, Django, JQuery, SQLite, close-commenting  
fonts: <del>Helvetica</del>  
content: free licences, public domain, fair use
source code: GNU AGPL

![](/images/1.3/index.png)

##### Brainch

<http://brainch.stdin.fr/>

Collective writing application.  
software: Git, Python, git-python, Django, JQuery, SQLite  
fonts: Liberation Sans  
content: Free Art License  
source code: GNU AGPL  
http://code.dyne.org/?r=brainch

![](/images/1.3/diff.png)



