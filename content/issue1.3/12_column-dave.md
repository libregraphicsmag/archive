Title: Dave Crossland went to Argentina
Author: Dave Crossland
Section: Column
Slug: dave-argentina


![](/images/1.3/davecrosslandwenttoargentina.png)

_Dave Crossland believes anyone can learn to design great fonts. He is a type designer fascinated by the potential of software freedom for graphic design, and runs workshops on type design around the world. <http://understandingfonts.com>_

