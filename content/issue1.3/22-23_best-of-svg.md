Title: Isn't Open Clip Art Library handy?
Section: Best of
Tags: SVG, Commons
Slug: clipart-hands

The expression "many hands make light work" uses the analogy of the hand to represent participation or involvement. As we talk about collaboration, it seems appropriate, this time around, to look a little more literally at the symbol used so often to represent work.

This issue, Best of SVG scoured the revamped Open Clip Art Library, looking for the best hands on offer. As it turns out, ocal provides hands for all occasions. 

If you haven't already used or contributed to Open Clip Art Library, take a look now. All work there is in SVG format and is dedicated to the public domain. That means you can use it for just about anything. Check it out, use it and add a little work of your own. Find it at [openclipart.org](http://openclipart.org).

—the editors

![](/images/1.3/hands.png)

We at Libre Graphics magazine have a thing for open standards. We like their transparency and their interoperability. We like that, with a well documented standard, everyone has an equal chance to play nicely together. 

That's why we like SVG so much. It's a well developed, well supported standard brought to us by the World Wide Web Consortium (W3C). It's available for implementation by anyone developing software. It shows up in modern browsers, fine vector graphics editors and any number of other places. 

One thing that's missing, though, is you: the designer, the artist, the illustrator. So put down that .ai file and check out SVG.

