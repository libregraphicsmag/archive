Title: Resource list 1.3
Section: Resource list
Tags: Tools

# Resource list 1.3

## GIMP

A raster based image editor for GNU/Linux, Mac OS X AND Microsoft Windows.


## Inkscape

A vector graphics editor for GNU/Linux, Mac OS X AND Microsoft Windows.


## Toonloop

A F/LOSS stop motion animation program.



## Scribus

A desktop publishing program for GNU/Linux, Mac OS X AND Microsoft Windows. 

