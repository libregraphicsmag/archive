Title: Bash scripts for generative posters
Author: LAFKON
Section: Showcase
Tags: Generative, Scripting
Slug: lafkon

Between 2008 and 2010 LAFKON made various posters for F/LOSS-oriented festivals, conferences and conventions. Custom F/LOSS setups were described inside Bash scripts to generate editions of parametric posters. The outputs are optimized for cheap reproduction. While one print run was usually made by the festival organisation, posters were available for download and self-print.

Lafkon is a laboratory for graphic design occupied by Benjamin Stephan and Christoph Haag. Lately they have been doing practice-based research on generative processes as design tools and started to rebuild Deep Blue to work for them as a layout intern.

<http://www.lafkon.net>

![](/images/1.3/benedictegabriel_1260711416778.jpg)

![](/images/1.3/chmod+x_en--016.jpg)![](/images/1.3/chmod+x_en--018.jpg)![](/images/1.3/chmod+x_nl--009.jpg)

![](/images/1.3/compo.jpg)

![](/images/1.3/sebastienvriet_P1080706.jpg)

![](/images/1.3/dit-A2-004.jpg)![](/images/1.3/dit-A2-005.jpg)![](/images/1.3/dit-A2-032.jpg)

![](/images/1.3/dit-A2-055.jpg)![](/images/1.3/dit-A3-037.jpg)![](/images/1.3/dit-A3-065.jpg)

![](/images/1.3/IMG_7512.jpg)

![](/images/1.3/lac2008_samstag__1.jpg)

![](/images/1.3/plakatlac2008_A3_08.jpg)![](/images/1.3/plakatlac2008_A3_62.jpg)

