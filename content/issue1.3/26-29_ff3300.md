Title: FF3300
Author: FF3300
Section: Showcase

![](/images/1.3/6visual.png)![](/images/1.3/7visual.png)

![](/images/1.3/8visual.png)![](/images/1.3/9visual.png)

FF3300 is the hexadecimal code for orange. It's also the name of a magazine started in 2006 and of a visual communication studio based in Bari, founded in 2008 by Alessandro Tartaglia, Carlotta Latessa and Nicolò Loprieno.

Their website claims that they believe in the power of ideas, in design method and in culture. There is no better way to describe their approach to communication. It perfectly underlines the great responsibility they feel towards both the final communication aims and the underlying processes, giving both an important role in the project.

The experimental approach is not restricted to the conceptual area, but impacts the visual and technological spheres as well. FF3300 pushes new ways of perceiving communication, through generative graphics and cool design choices.

<http://www.ff3300.com>

![](/images/1.3/IMG_3710-bw.jpg)

![](/images/1.3/ff3300-1.jpg)

![](/images/1.3/ff3300-2.jpg)
