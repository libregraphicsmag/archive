The Libre Graphics magazine Archives
====================================

This is an effort to convert the content of the Libre Graphics magazine issues into Markdown, in order to get them together in a [Pelican](https://blog.getpelican.com/) static site.

## Directory structure

* **old-content**
  * **01-original-scribus-files**: the original `.sla` files taken from the repositories
  * **02-converted-markdown**: auto-converted Markdown versions of each issue, with pages out of order
  * **03-separated-articles**: individual files for each article, not reviewed for markup yet
  * **image-lists**: list of image files in each issue
* **issue-pages**: metadata for each issue
* **content/issueX.X**: marked up files, with metadata headers
* **content/images**: all images, full size
* **content/images-small**: all images, resized for web


## Recipes for some of our steps

### Extract text from Scribus files

See the `scripts/scribus-extract.py` script.

### Extracting image paths from Scribus files

```bash
cat 01-original-scribus-files/lgmag-1.1-p* | \
  grep -oe '"[^ ]*\(jpg\|png\)"' | \
  sort | uniq | \
  sed 's/"//g; s/\.\.\///g' > images-1.1.txt
```

### Re-downloading images from repos

After extracting the image lists from the previous recipe, we did this (example for 2.4):

```bash
mkdir -p images/2.4
cd images/2.4
while read f; do wget "https://gitlab.com/libregraphicsmag/vol2issue4/raw/master/$f"; done < ../../image-lists/images-2.4.txt
```

### Resizing images to a max width and height

We wanted max 1800px width and 1400px height, while keeping smaller images as they are.

```bash
cd images/2.4
for f in *; do echo $f; mogrify -resize 1800x1400\> $f; done
```

or, to alter all images in subdirectories:

```bash
cd images
find . -name "*" | xargs mogrify -resize 970x4000\> 
```

### Review text files with a particular string

We needed to check every instance of a lowercase acronym (e.g. "svg") and edit it manually if it needed to be made uppercase again.

```bash
grep '\bsvg\b' issue* -lr | xargs vim -p
```

### Turn all images into image links

We wanted to change all images into a thumbnail which would then link to the original image.

So this

```markdown
![](/images/2.1/picture.png)
```

should become

```markdown
[![](/images-small/2.1/picture.png)](/images/2.1/picture.png)
```

So we use the following sed script:

```bash
cd content
find . -name "*.md" | xargs sed -i 's/!\[\](\(\/images\/[^)]*\))/[![](\1)](\1)/g; s/(\/images\//(\/images-small\//'
```

The sed operation comes in two parts: first we create the "image inside link", and after that we fix the directory of the thumbnail so that it points to the `images-small` directory.
