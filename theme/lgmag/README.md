# pelican-lgmag-theme #

This theme was designed for the [Libre Graphics magazine archive](http://archive.libregraphicsmag.com/). Its structure was derived from the pelican's *notmyidea* theme.

