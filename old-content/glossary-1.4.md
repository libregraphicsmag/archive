Title: Glossary
Section: Glossary

Alchemy
:  A sketching environment designed for visual experimentation. Available for GNU/Linux, Mac OS X and Microsoft Windows.

Aptana Studio
:  An IDE for creating web applications. Available for GNU/Linux, Mac OS X and Microsoft Windows.

ArgyllCMS
:  A colour management system with icc profile compatibility for a variety of display and capture devices. Available for GNU/Linux, Mac OS X and Microsoft Windows.

Audacity
:  A sound editing application for GNU/Linux, Mac OS X and Microsoft Windows.

Blender
:  A powerful 3d animation application for GNU/Linux, Mac OS X and Microsoft Windows.

Chiplotle
:  A Python API for pen plotters. Implements HPGL. Available as a Python library.CMS:Stands for Content Management System. Software installed on a webserver in order to provide a simple framework for editing webpages. WordPress and Drupal are examples of content management systems.

command line
:  A text only user interface, allowing users to input commands without the intervention of cursors and graphical user interfaces.

colord
:  A tool for installing, managing and creating colour profiles. For GNU/Linux.

Creative Commons
:  A suite of licenses designed to allow creators and users of works flexibility beyond that offered in traditional copyright.

F/LOSS
:  Stands for Free/Libre Open Source Software. Software which has a viewable, modifiable source. It can be modified and redistributed.

firmware
:  Software designed to be permanently installed on a computer/device. Free software:A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.

GIMP
:  A raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows. (Sometimes spelled Gimp) 

Hugin
:  Panoramic photo stitching software for GNU/Linux, Mac OS X and Microsoft Windows.

Inkscape
:  A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

JavaScript
:  A scripting language commonly used on websites. 

Krita
:  A drawing application supporting both vector and raster images. Available for GNU/Linux, Freebsd and Microsoft Windows.Laidout:A layout and desktop publishing application for GNU/Linux.

Luciole
:  A stop motion animation program for GNU/Linux.

MyPaint
:  A digital painting application with a focus on fluid workflow. Available for GNU/Linux, Mac OS X and Microsoft Windows.

Node.js
:  A JavaScript-based system for creating internet applications.

open hardware
:  Hardware which follows the same principles as F/LOSS, including publicly available, freely licensed schematics.

Paper.js
:  A web-friendly JavaScript version of Scriptographer.

PCB
:  A printed circuit board, the base of most electronics, generally consisting of a number of electronic components linked together to form certain types of circuits.

Pencil
:  2D animation software with support for both vectors and rasters. Available for GNU/Linux, Mac OS X and Microsoft Windows. 

Processing
:  A programming language and development environment predominantly used for visually-oriented or media-rich projects. Available for GNU/Linux, Mac OS X and Microsoft Windows.

Python
:  A popular interactive programming language. Available for GNU/Linux, Mac OS X and Microsoft Windows.

“scratching an itch”
:  A term used in many F/LOSS communities to mean that a developer has created a program primarily to meet his/her own needs, instead of the needs of others.

Scribus
:  A desktop publishing application for GNU/Linux, Mac OS X and Windows.

Scriptographer
:  A scripting plugin for Adobe Illustrator which gives the user the possibility to extend Illustratorʼs functionality by the use of the JavaScript language. www.scriptographer.org. Scriptographer is a GPL-licensed plugin for proprietary software.SIL Open Font License (OFL):A license intended for use with fonts and font related software. Dictates terms which allow modification and redistribution of fonts.

ToonLoop
:  A stop motion animation application for GNU/Linux. A minimal version is available for Mac OS X and Microsoft Windows.

Zotero
:  A reference management application with extensions for browsers and word processors. Available GNU/Linux, Mac OS X and Microsoft Windows.

