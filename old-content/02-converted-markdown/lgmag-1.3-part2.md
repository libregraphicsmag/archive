Glossary 1.3

--------------

bash: A freely licensed Unix shell. See command line.



command line/console: A text-based interface for controlling a computer. 



copyleft: A style of licensing in which those redistributing the work are required to do so under its original (or a compatible) license.



Fedora: A popular distribution of gnu/Linux, produced by Red Hat, Inc.



Free/Libre Open Source Software (F/LOSS): Software which has a viewable, modifiable source and a permissive license (such as the gnu gpl). It can be modified and redistributed.



Git: A popular version control system, originally created to manage development of the Linux kernel.



GNOME: A popular desktop environment for gnu/Linux.



GNU General PublicLicense (GPL): A license originally intended for use with software, but now used for other applications. Made famous the principle of Copyleft, requiring those using gpl licensed work to license derivatives similarly.



GNU/Linux: A group of operating systems which are built on the Linux kernel and components from the gnu project, among others, which are widely distributed and freely modifiable.



hexadecimal code: A six-character code prefaced by a hash symbol (#), used to define colour, especially for web applications. Eg. #000000 for black.



hacktivism: A movement or school of belief based around the ideas of networked activism, aided by technical knowledge.



Internet Relay Chat (IRC): A popular form of internet-based real-time chat. Has a long history of use and is still popular among groups of developers and users. 



Movable Type: A f/loss blogging platform released under the gnu gpl.



open license: A license which allows and encourages re-use and appropriation of creative works, in contrast to the all rights restricted norm provided by traditional copyright. Examples include the gnu gpl, the sil ofl and the Creative Commons family of licenses.



Open Source: See Free/Libre Open Source Software.



open standards: A standard which is available for viewing and implementation by any party, often at no monetary cost.



Perl: A popular programming language, often used for writing web applications. 



PHP: A popular scripting language, used for web development.



proprietary: A piece of software or other work which does not make available its source, which is not allowed or intended to be modified or redistributed without permission.



public domain: The legal status of a creative work for which the copyright (or other rights restriction) has expired. A work in the public domain can be used by anyone, for any purpose, without restriction. Licenses such as the Creative Commons CC0 license emulate public domain.



Scalable VectorGraphics (SVG): An open standard for vector graphics, developed by the W3C.



script: A small program, often used to control a larger program or block of code.



SIL Open FontLicense (OFL): A license intended for use with fonts and font related software. Dictates terms which allow modification and redistribution of fonts.



Telnet: A protocol for networked communication. 



Ubuntu: A particularly popular distribution of gnu/Linux, produced by Canonical Ltd.



version control: Activities which have the effect or intent of distinguishing different versions of a work or body of work from one another.



W3C: The organization responsible for setting web standards, such as html5 and svg.

--------------

Glossary

--------------

Resource list 1.3

--------------

GIMP

A raster based image editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Inkscape

A vector graphics editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Toonloop

A f/loss stop motion animation program.

--------------

Feature

--------------

Feature

--------------

Feature

--------------

Feature

--------------

Resources

--------------

Resources

--------------

Glossary

--------------

FF3300 is the hexadecimal code for orange. It's also the name of a magazine started in 2006 and of a visual communication studio based in Bari, founded in 2008 by Alessandro Tartaglia, Carlotta Latessa and Nicolò Loprieno.

Their website claims that they believe in the power of ideas, in design method and in culture. There is no better way to describe their approach to communication. It perfectly underlines the great responsibility they feel towards both the final communication aims and the underlying processes, giving both an important role in the project.

The experimental approach is not restricted to the conceptual area, but impacts the visual and technological spheres as well. FF3300 pushes new ways of perceiving communication, through generative graphics and cool design choices.

http://www.ff3300.com

--------------

FF3300

--------------

Between 2008 and 2010 lafkon made various posters for f/loss-oriented festivals, conferences and conventions. Custom f/loss setups were described inside Bash scripts to generate editions of parametric posters. The outputs are optimized for cheap reproduction. While one print run was usually made by the festival organisation, posters were available for download and self-print.

Lafkon is a laboratory for graphic design occupied by Benjamin Stephan and Christoph Haag. Lately they have been doing practice-based research on generative processes as design tools and started to rebuild Deep Blue to work for them as a layout intern.

http://www.lafkon.net

--------------

Bash scripts for generative posters

LAFKON

--------------

Ingredients for bio ink:

strainer bowl 1/2 cup ripe berrieswooden spoon1/2 tsp. salt1/2 tsp. vinegar1 jar with tight-fitting lid





Ingredients for natural brushes:

cut potatoes (stick shapes highly recommended) A3 recycled paper nice people



Directions:

Dump the berries into a strainer and hold it over a bowl to catch all the juice.

Crush the berries against the bottom of the strainer with a wooden spoon so all the juice drips into the bowl.

Squash as much juice as possible out of the berries through the strainer and then discard the remains.

Add salt and vinegar to the berry juice and stir well. The vinegar helps to preserve the color and the salt helps to prevent mold from forming over the juice.

Transfer the mixture into a jar with a tight-fitting lid. A baby food jar or small jelly jar is ideal.

Begin your writing project dipping your potato brushes in the new ink, keeping in mind that the ink will dry quickly and that you'll probably need several iterations of your brush.

Freeze unused berry ink for later use. The berry ink will begin to smell unpleasant if left at room temperature.

Instructions for Makemake logo production:

Redraw the Makemake logo grid.

Draw the elbow position point that will be set at the same distance from the sheet for all participants.

Position the first participant elbow on the table, following the previous decision.

Participant dips cut potato in freshly-made bio ink.

Participant draws own personal Makemake typeface version, painting only with horizontal movements and following the given grid.

Change participant!



Makemake is an Italian non-profit organization for creativity and design. Based in Milan, it promotes the idea of alternative approzaches to visual communication.



--------------

Open source recipe for organic logos 

Makemake

--------------

“It is no longer necessary to deface paintings or to put a mustache on postcards of Mona Lisa, now art can be downloaded, modified  and uploaded again, with absolute delight.” Luther Blissett, Art Hacktivism 

Ddump is a digital recycling project based on sharing dumped files and providing different visual perspectives on them. It is based on a piece of software that allows users to easily share the contents of their personal computer trash cans, and encourages them to habitually share dumped files. The internet repository that collects them is specifically designed for graphic design purposes and aims to have different interfaces, in order to encourage diverse utilizations of the files. 

Contribute to the dump at: http://ddump.parcodiyellowstone.it Pickpic is a set of self-written internet applications developed to help graphic designers who want to work in an environment based on peer-to-peer collaboration. It is not meant as a replacement of traditional graphic design software but as a cross-platform layer that can be applied on top of such tools. It is available as a web platform and Firefox extension, helping teams of designers to create common spaces for sharing ideas, works and visual references. It is not another social web platform. Entirely open source, it can be fully downloaded and installed on any server running Django. All the materials are collected anonymously. Users are only aware of who is working on the project with them but no comment, image or work can be linked to its author. This helps build the idea of a collective ownership and sets all resources free from the influence of their author.

Pickpic available at http://pickpic.parcodiyellowstone.it 



--------------

Digital Dump and Pickpic

parcodiyellowstone

--------------

We so often draw a strong distinction between the physical and the digital, acting as if the one is solid, staid and reliable, while the other is born in ether, unreal and untouchable. But this is far from the case. 

The digital is merely a subset of the physical. It is a land we've come to view as different and distinct, despite its reliance on the physical. Regardless of our perceptions, the two tick along, happily co-operating and relying on one another. As the digital fails to escape the bounds of the physical, the physical comes full circle, embracing its part in the success of the digital. 

Graphic design and media arts are fields intimately acquainted with the obvious areas of overlap between the physical and the digital. From the days of air brushing and drafting by hand, to the bringing of those same metaphors into the realm of digital production, designers and media artists are at the forefront of both the conflicts and the embraces of the digital and the physical. 

Whether it manifests itself in a workflow incorporating both digital and physical methods, to different ends, or whether it is a transformation which takes place in the space between the two (not so separate as we believe) realms, the point of interaction between the digital and the physical is a special place. And it bears exploring. 

Which is why, in issue 1.4 of Libre Graphics magazine, we’re looking at the space where the digital and the physical collide. We're interested in the border cases, the role of intentionality and happy accident in the mingling of physical and digital, and any and all points of intersection. Whether it's the translation of a digital drawing to print, the scanning of an old typeface, or any other form of translation, all bets are on. 

This time around, we're interested in collisions. And we want your contributions.

Libre Graphics magazine is seeking submissions for issue 1.4, The Physical, the Digital and the Designer. We want your written or visual work, created with Free/Libre Open Source tools, methods and standards. Flip through previous issues to see what we’ve done in past, then propose to do it again, or better, or to do something else entirely.



Submissions to submissions@libregraphicsmag.com Submissions for this issue are due by:11:59PM EST, 15 October, 2011



--------------

In f/loss culture we believe in strong communities working together towards a common goal, while having the freedom to go it alone if necessary. These same philosophies of community have the potential to work really well within the art and design ecosystem. These communities allow artists to come together, collaborating or simply enjoying being in a group and seeing other great works of art.

The Ubuntu Artists community began on deviantArt, but has grown into something all its own. Started as a meeting point for like-minded artists who make their art on the Ubuntu distribution of gnu/linux, it has matured over time.

The group has attracted over 900 members who have contributed thousands of works of art to its galleries. Each of these works is presented to other members and watchers of the community. Commenting on and critiquing the works is a common practice and encouraged within the culture of the group.

One of the primary tasks of the community leader is to curate the galleries, creating spaces for different kinds of works. These galleries, through their themes and content, reflect a community dedicated to artistic production using Ubuntu. While the generic “Ubuntu Made” gallery receives the most submissions, the “About Ubuntu” gallery, for works which reference Ubuntu, such as cartoons or fan art, is also active. The community also contributes themes, wallpapers and ui design.

An ongoing job of the community leader is to approve each piece submitted. To prevent bottlenecks, the Ubuntu Artists community also has five lieutenants, empowered to approve works to certain galleries.

It's important that while the group is formed around a Free/Libre Open Source operating system, it is, at its core, a resource to help artists get together and share their ideas and creativity. This is why the group actively promotes the use of f/loss, and why an atmosphere of support is encouraged. Asking questions and presenting problems are both encouraged and supported.

Another vital function of the group and its leaders is to keep members informed about new developments in f/loss graphics. This takes the form of announcing new releases, letting users know where to find software, and providing installation instruction. This sort of support lowers barriers to entry. These sorts of announcements give members the opportunity to comment and chat about the news.

One major growth area for the Ubuntu Artists community is in encouraging more participation and collaboration. The trick is to figure out how an artistic community is similar to or different from a developer community. These similarities and differences may just prove to be the major hurdle in porting the concepts of community, well-known to the f/loss world, into an artistic context. One pressing barrier at the moment is in the platform itself. The community of deviantArt where Ubuntu Artists is hosted, are not designed to facilitate collaboration and interaction.

The Ubuntu Artists group provides a valuable service, advocating for the use of f/loss in art. One of the most powerful motivators is simply experiencing other people's works of art. Seeing the quality of work done by others who use only Free/Libre Open Source graphics applications offers the support some artists need. This may encourage them to continue with f/loss instead of switching back to proprietary, even when the learning curve may feel steep.

The togetherness of f/loss culture can be spread further into the arts. And it's important for us to do so. I'll continue to run this little corner of deviantArt, teaching more people to work with a sense of community. It's a sense of community they may never have experienced before. I believe only together can we make art truly beautiful.

--------------

Managing artist communities: the case for Ubuntu Artists

Martin Owens

--------------

Since October, I've been using SparkleShare as part of my daily workflow, almost without noticing. Using it, I've integrated sharing, collaboration and publication into the very fabric of my work. 

It started when one of our columnists suggested that we use Git to tidy up our collaboration. With editors on different continents, and with a big pile of content going into each issue, it only made sense to come up with a clever way to share things. So we set ourselves up with a repository hosted on Gitorious. Now, to make Git work, you need more than just a repository. You also need a client of some kind to sync your local version up with the central repository. I've been told that the easiest method for doing this syncing is manually, in the command line. But, well, I'm lazy. I'm happier when I have a nice, tidy program to do the work for me. That's where SparkleShare shines.

SparkleShare is a truly elegant front-end for Git. It makes syncing to and from your Git repository painless and basically invisible. It's painless because, unlike other Git clients, it syncs automatically. This automatic syncing is great, because it means that, as long as your computer is connected to the internet and SparkleShare is running, you'll never have to worry about your repository not being up to date. If one of your collaborators has committed a change to your shared repository, SparkleShare will grab it and update your files. On the flip side, if you make a change locally to one of the files in your SparkleShare directory, it gets uploaded to your shared repository. 

You may find that feature a little familiar. For those who use Dropbox, it's a habit: save your file to the appropriate directory and poof, it's there for everyone you're sharing with. While that style of interaction may be pretty standard, it's the added power of Git that makes SparkleShare a really special way of sharing. 

Because Git is a version control system, a lot of the scary parts of collaboration are eliminated. We've all been there: you're working with someone on a project, you take a look at their latest revision to a file, it's a disaster. Good thing you kept a backup, right? With the automatic version control provided by Git + SparkleShare, you don't need to keep a duplicate. Your Git repository doesn't just contain all the current versions of your files, it actually hangs onto past versions, too. And it takes note of the differences. If you're working with text or code, being able to view line-by-line differences is a pretty big deal and makes life much easier. If you're working with images, it's still great, because you can download and compare different versions, or revert if you don't like the latest changes. 

For the last nine months, I've been using SparkleShare, watching it grow, stabilise and get better. And it sure has. Now, it's your turn. If you've never used version control before, you'll be very pleasantly surprised at what it can do for you. After a couple weeks, there'll be no going back. You won't be able to remember how you coped with duplicates, ad hoc versioning and all the other little workarounds people use to get the job done. And, using SparkleShare, you'll find that it's easier than you ever imagined. It's handy, it's automatic and yes, it's version control, just as close to invisible as possible.

--------------

SparkleShare: pleasantly invisible version control

ginger coons

--------------

What the heck is version control?

Version control, while normally used by software developers who want to track changes to code, can be pretty useful for anyone doing collaborative work. We'll spare you the techincal explanations and just give you the functional basics of how it works. When using version control, a group of people are all working on the same set of files. Each collaborator has a local set of the files, in addition to a centralized, online repository of those files, which is shared by everyone. Collaborators can upload their modified versions of the files to the online repository, and download the changes made by others. To do that, a few things are necessary. You need to choose and install a version control system (we like Git, but there are lots of other options). You need to set up your repository (Gitorious is our repository host of choice). And, if you don't want to do everything in the command line, you need to get a client for your version control system. That's where programs like SparkleShare come in. They push and pull changes, making your local files match the ones in the shared repository and vice versa. 

--------------

Getting started with SparkleShare

Download it from sparkleshare.org. It's available for Linux and Mac.

Install it. On Mac, it's as easy as pointing and clicking. If you're running Fedora Linux, follow the instructions provided at sparkleshare.org/linux-downloads/. On any other version of Linux, you'll have to install from source. But don't worry! The instructions provided in the readme file are really good.

Get yourself a hosted Git repository. sparkleshare.org/help/ covers how to get set up with both Gitorious and Github, two popular Git repository hosting services. 

Set up SparkleShare and sync it to your repository. SparkleShare will walk you through the process when you run it for the first time.

Go wild!

--------------

How many of you are there?

There is no precise number. Depending on the activities, we can be two, ten, twenty... Some take part in projects on a regular basis, others prefer to stop right after a workshop. Since the beginning, about one hundred students and professionals have somehow contributed to the project. Then, some connections were made and, depending on our free time, about a dozen people keep on initiating projects now. The utopia of the Parallel School is to become a sunshade that anyone can make use of. But for now, in practice, projects are still being created by the people who set up the school. We all run the project in parallel with our occupations, so its development takes some time, and we hope that other people might inspire new ideas and projects in the future.

How was the project born? Does it draw inspiration from other models?

At the beginning, we were inspired by Jacques Rancière. In Le Maître ignorant, Rancière tells the story of Jacotot, a teacher who claimed, in the 1830s, that ignorant people could teach themselves without a master, and that masters were able to teach what they ignored themselves. To justify his position, he mentioned one of his teaching lessons, during which his students learned French thanks to a bilingual version (Flemish/French) of Télémaque, comparing the two versions.

We then put together a corpus of references that seemed to fit our concerns, which every contributor can use or not. One of the most important things—among others—we could mention is the Hidden Curriculum project by Annette Kraus, which tries to bring to life some alternative forms of education, like asking her students to reveal their tips to avoid working at school (making a screenshot of a word processing software, playing a video game and opening the image when the teacher comes, etc.) or publishing the excuses told to explain absences (“I overslept. It was raining. I was on my bike and realized that I still had my pyjamas on, and had also forgotten my bag.”). 

What are the purposes of your workshops?

Actually, there is no purpose. We want something, then we try it. It all starts with the pleasure of travelling and meeting students from other schools. Then comes the pleasure of working for yourself, without deadlines or any other pressures than the ones you put on yourself. The richness of sharing and arguing between participants is one of the most attractive points of these workshops. In the end, there is the will to store parts of our experimentations, most often as publications, so that anybody can retain a memory.

At an educational level, learning curves can be really different. What's important is to avoid hierarchy or judgements about the educational tracks suggested by the students. We may be interested in notions such as errors, bad ideas or weirdness, as we try to give priority to creative processes that don't take place in our respective schools.

How long does a workshop last? Is there a key moment or step?

Workshops last a week, more or less. This is long enough to achieve viable content and short enough to prevent it from overwhelming our everyday occupations. As in many short-term experimentations, it seems that the key moment is the end of the workshop, when the pressing necessity of designing and finishing reaches its climax. Usually, this is the moment when we finish printing the publication, too. The never-ending arguing  gives way to manual conception. In Berlin, we bound our publications until 1 a.m. with parquet boards in a wasteland, lit by the headlights of a car.

Are participants selected?

Not really, as everybody is able to suggest or collaborate. We just need to find our audience. Someone who has already contributed to an event of the Parallel School has no obligation regarding the future activities. As anyone can suggest a project, whether it is a workshop or a thought via email, readings, sharings, the instigator has to find interlocutors, announce it on the blog (anyone can ask for the codes to publish). Though it certainly is easier to organize events for people who have been involved for a long time, a Yale student recently suggested a big project that had us all interested. So everything is possible. The only necessity is to be at least two—a group—and to find an audience.

Then, the links create or change the groups, but there is no obligation to work with everybody and to reach a consensus.

Is the content of your workshops shared or spread afterwards?

Our workshops gather very few students, and we think it is actually interesting to spread these actions, whenever they have a modest scale. Firstly, because the circulation allows us to meet new people, and then because—without claiming we are defining a model—we find it interesting to show how easy and exciting it is to take care of your own learning curve. At some point, we found it necessary to legitimate our activities with a blog—though we could have kept our experimentations to ourselves—be-cause we have in mind that there is a kind of political speech hiding behind our actions. The internet allows a small workshop of fifteen people to spread, to be seen by many more. We've been greatly inspired by and sensitive to anything that can be seen on the internet. Anyway, virtual life is for now the only one that allows us to meet unknown people from everywhere in the world. We're currently thinking about publications, but we must admit that we are really content with the idea of free pdf files for now.

What do you think of open licences? Do you find them suitable for education?

They are probably the future of education. Anyway, the idea of copyleft is nearly inherent to the project of the Parallel School.

Do you allow people to download, share, maybe reprint your lectures and lessons?

We do if we can. The publication made in Moscow has been available for download on Manystuff 1, as has the one made in Berlin (and on our blog, I think). 



1. www.manystuff.org, a website gathering visual resources in graphic design on an everyday basis.





--------------

Scribus

A desktop publishing program for gnu/Linux, Mac os x and Microsoft Windows. 

--------------

The Parallel School is a virtual art school, dedicated to promoting self-education. Find it online at http://parallel-school.com

--------------

Parallel School—an interview

Thibaut Hofer



--------------

Martin Owens is the community leader of Ubuntu Artists, a community made up of painters, illustrators, modellers and others who use F/LOSS, and Ubuntu specifically, to create.

--------------

SHOWCASE

--------------

showcase

--------------

showcase

--------------

showcase

--------------

showcase

--------------

feature

--------------

<stdin>

Alexandre Leray, Stéphanie Vilayphiou



There seems to a be a tendency nowadays for collaboration and cross-disciplinarity, and graphic designers are not at rest. Many designers — including ourselves — are increasivelly working together with artists, thinkers or engineers, blurring the separation between the disciplines. Why? Could it be a way to escape from the division of labour? A way to escape from the thinking of graphic design as a service industry and to start thinking of design as an embedded process? Our intuition is that graphic designers don't want to be the last element of the production line anymore.

Coming from a classical visual design education we became more and more interested in digital culture and networked media. Now, we mix a visual approach with programming to create designs for print and non-print outputs. We proudly claim the two hats of designer and programmer because for us programming is also design. Moreover, we think programs are cultural items, at least as much as they are functional. F/loss carries this thought, but using it doesn't necessarily imply free culture. This is why we focus our personal researches on collective platforms based on f/loss. Sharing is not only about giving, it's also about getting back, it's about starting a discussion.



http://stdin.fr

--------------

Rémy Jacquier

Catalogue of French artist Rémy Jacquier.  software   Indesign 

fonts   CourierSans

content   © Rémy Jacquier   © ADERA

--------------

Éditions B42   http://editions-b42.com/

Website of Éditions B42 publishing house.

software   git   python   django   HTML

                 jquery   mysql

fonts   Verdana   Hermès Sans

content   © Éditions B42

--------------

Brainch   http://brainch.stdin.fr/

Collective writing application.

software   git   python   git-python   

                 django   jquery   sqlite

fonts   Liberation Sans

content   Free Art License

source code   GNU AGPL

http://code.dyne.org/?r=brainch

--------------

cataloged   http://cataloged.cc/

Online portfolio, also generating a printed one, of graphic designers Coline Sunier and Charles Mazé.

with   Coline Sunier   Charles Mazé

software   mercurial   python   django

                  jquery   mysql   reportlab   

fonts   Inconsolata

content   ?

--------------

Constant

Flyer for Constant, association for art and media in Brussels.

software   Scribus

fonts  NotCourierSans Constant Archive

content   Free Art Licence

--------------

Datateb

http://datateb.alexandreleray.com/

Art piece mocking the software industry through recycle bins. 

software   AppleScript   python   django

fonts   Verdana

content   Free Art License

source code   GNU GPL   http://github.com/aleray/datateb/

--------------

else if   http://else-if.net/

Collections of critical texts on graphic design and digital media. 

software   git   python   django   jquery

                 sqlite   close-commenting

fonts   Helvetica

content   free licences   public domain   

               fair use

source code   GNU AGPL

--------------

ERBA Valence 2009—2011

Information leaflet for Valence school of Fine Art and Design. 

software   Indesign   GraphViz

fonts   Nobel

content   ?

--------------

Google Will Feed Itself

http://googlewillfeeditself.blogspot.com/

A Google Blogspot fed with its own Google ads. 

software   BlogSpot   GoogleAdsense

content   ?

--------------

Libre Graphics Meeting 2010

http://libregraphicsmeeting/2010/

Website for LGM 2010 in Brussels.

with   Alessandro Rimoldi   OSP

software   Inkscape   HTML   anwiki

fonts   Cantarell

content   ?

--------------

ISSUE MAGAZINE 

http://www.issue-magazine.net/

Online critical magazine on graphic and media design. 

software   python   HTML   jquery

fonts   Verdana 

content   CC-BY-SA-NC   fair use

--------------

Disappearance

A word can only live once.

with   Marijke Schalken

software   python  mplayer

source code   Free Art Licence

http://git.constantvzw.org/

--------------

Blind Carbon Copy    http://bcc.stdin.fr/

Experimental design hacks to circumvent “Intellectual Property”.

software   python   BeautifulSoup  nltk  

                 Indesign   javascript   

fonts   New Caledonia   Vogue

content   all wrongs reversed

source code   GNU GPL

--------------

acsr   http://acsr.be/

Atelier de Création Sonore Radiophonique sound repository. 

with   OSP (Ludi)   Jérôme Degive

software   Gimp   Inkscape   WordPress

fonts   Univers Else

content © right-owners

--------------

please computer | make me design

Workshop on fun commandline poster generation. 

with   OSP (Ludi, Ivan)

software   git   GNU coreutils   enscript podofoimpose   

fonts   from OSP foundry

content   Free Art Licence

source code   GNU GPL

http://git.constantvzw.org/

--------------

Schaarbeekse Taal

Flyers for events organized by the Schaarbeekse Taal project.

with   OSP (Ludi)

software   git   Gimp   Inkscape

fonts   Limousine   Alfabet III

content   © right owners

--------------

Le Chant des Particules

Website of a movie about LHC-CERN.

with   deValence

software   git   python   django   HTML

                 jquery

fonts   ChicaGogoRGB

content   ?

--------------

2008

--------------

2009

--------------

2010

--------------

2011

--------------

Curating as Environ-mentalism

http://environmentalism.stdin.fr/

Online interface to comment chunks of Elke van Campenhout's essay.

software   git   couchdb   HTML   jquery

fonts   Ume Mincho

content  All wrongs reversed

source code   GNU GPL

--------------

WIP

--------------

Libre Graphics magazine issue 1.4: The Physical, the Digital and the Designer



--------------

showcase

--------------

Left page: The proposition letter sent to the RCA students, which initiated Parallel School, 2009.

--------------

CALL FOR ENTRIES

--------------

showcase

--------------

Feature

--------------

Feature

--------------

