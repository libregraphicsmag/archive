GIMP 2.7.5

http://www.gimpusers.com/downloads

Call it a beta of the next gimp 2.8. This devel version of gimp gives a taste of what's coming next, namely faster performance, improved tablet support and a lot of bug fixes.

--------------

Blender 2.62

http://www.blender.org/download/get-blender

The latest version of Blender includes motion tracking, an improved interface for the game engine and a number of new uv editing tools, among other additions.

--------------

VLC 2.0 Twoflower

http://www.videolan.org/vlc/releases/2.0.0.html

A major new release from the ever-popular video player. Now with faster decoding, an enlarged list of formats (which includes hd) and higher quality subtitles.

--------------

Ghostscript 9.05

http://www.ghostscript.com

With improvements to icc colour rendering and font replacement, the newest version of Ghostscript has a number of design-sensitive additions.

--------------

Gnome 3.4

http://gnome.org

The popular desktop environment has come out with its first new release in six months, incorporating new menus and search functionality, as well as a more polished look.



--------------

Index

--------------

Masthead



Editorial Team

Ana Carvalho   ana@manufacturaindependente.org

ginger coons   ginger@adaptstudio.ca

Ricardo Lafuente   ricardo@manufacturaindependente.org

Copy editor

Margaret Burnett

Publisher

ginger coons

Community Board

Dave Crossland

Louis Desjardins

Aymeric Mansoux

Alexandre Prokoudine

Femke Snelting

Contributors

Dave Crossland, Maria Figueiredo, Nelson Gonçalves, Eric de Haas, Richard Hughes, Jonathan Puckey, Eric Schrijver.



Printed in Porto by Cromotema (http://cromotema.pt) and in Toronto by Green Printer (http://www.greenprinteronline.com) in recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 



Contacts

Write to us at enquiries@libregraphicsmag.com

http://libregraphicsmag.com

--------------

We so often draw a strong distinction between the physical and the digital, acting as if the one is solid, staid and reliable, while the other is born in ether, unreal and untouchable. This is far from the case. Not only on occasion, but always, the physical and the digital are intimately intertwined.  

The digital is merely a subset of the physical. It is a land we've come to view as different and distinct, despite its reliance on the physical. Regardless of our perceptions, the two tick along, happily co-operating and relying on one another. As the digital fails to escape the bounds of the physical, the physical comes to embrace its part in the success of the digital. 

Graphic design and media arts are fields well acquainted with the obvious areas of overlap between the physical and the digital. From the days of air brushing and drafting by hand, to the bringing of those same metaphors into the realm of digital production, designers and media artists are at the forefront of both the conflicts and the embraces of the digital and the physical.

Whether it manifests itself in a workflow incorporating both digital and physical methods to different ends, or whether it is a transformation which takes place in the space between the two realms, the point of interaction between the digital and the physical is a special place. And it bears exploring. f/loss graphics and art fields are taking up that exploration in magnificent ways. In the world of f/loss, the space where the digital and the physical collide is becoming beautifully populated.

This issue of Libre Graphics magazine is about border cases, the role of intentionality and happy accident in the mingling of physical and digital, and any and all points of intersection. We're exploring the translation from the digital to the physical, the physical to the digital and the careful mapping of the two. We're looking at the place of history and the promise of the future in harmoniously melding our media.

We're looking at folds, sprays, cuts and prints. We're looking at mapping worlds onto each other. In short, in this issue, we're looking at collisions.



--------------

Will these hands never be dirty?

Eric Schrijver

--------------

Masthead

Editor's Letter

Production Colophon

New Releases

Upcoming Events

Moral rights and the sil Open Font License Dave Crossland

Will these hands never be dirty? Eric Schrijver

Notebook

Small & useful

The finished and unfinished business of OpenLab esev Nelson Gonçalves and Maria Figueiredo

Best of svg

The Interview: Natanael Gama talks techno-fonts and the benefits of Libre Dave Crossland

Showcase

    3D Printing

    Baltan Cutter Eric de Haas

    Paper.js Jonathan Puckey

Folds, impositions and gores: an interview with Tom Lechner Femke Snelting, Pierre Marchand and Ludivine Loiseau

ColorHug: filling the f/loss colour calibration void Richard Hughes

Resurrecting the noble plotter Manufactura Independente

Resource List

Glossary 1.4

--------------

4

6

7

9

10

12

14

16

19

20    

22

24    

26

28

30

34

38    

43    

46

48

49





--------------

MASTHEAD

--------------

NEW RELEASES

--------------

New releases

--------------

A Reader's Guide to Libre Graphics Magazine



In this magazine, you may find concepts, words, ideas and things which are new to you. Good. That means your horizons are expanding. The problem with that, of course, is that sometimes, things with steep learning curves are less fun than those without.

That's why we're trying to flatten the learning curve. If, while reading Libre Graphics magazine, you encounter an unfamiliar word, project name, whatever it may be, chances are good there's an explanation. 

At the back of this magazine, you'll find a glossary and resource list. The glossary aims to define words that are unique to the world of Libre Graphics. The resource list provides valuable information about tools, licenses, whatever items we may be mentioning.

Practically, this means that if, for example, you're reading an article about Paper.js (see pages 30 to 33), you can always flip to the back of the magazine, look up Paper.js in the resource list and become quickly informed about it. This provides some instant gratification, giving you the resources you need to understand, in a moment, just what we're talking about.

We hope you like our system.

--------------

Dave Crossland believes anyone can learn to design great fonts. He is a type designer fascinated by the potential of software freedom for graphic design, and runs workshops on type design around the world.http://understandingfonts.com

--------------

What's new with you? We're always eager to find out what designers, artists and others using and working with f/loss are up to. Tell us what you've done lately at enquiries@libregraphicsmag.com



--------------

Images under a CC Attribution Share-Alike license

Photo of ginger coons by herself.

Photo of Ricardo Lafuente by Ana Carvalho.

Photo of Ana Carvalho by Luís Camanho.

Photo of Dave Crossland by Mary Crossland.

Photo of Eric Schrijver by himself.

Illustrations in “Type Design” based on “1032056”, image by Flickr user fdctsevilla.

Illustrations in “Dispatch”, page 20, by Natacha Cindy and Hugo Ferreira.

Photos in “Notebook”, in order of appearance, by Pedro Caetano, Bruno Fernandes, ginger coons and Aloysio Araripe. All photos, with the exception of ginger coons' photo, can be found at Flickr, in the photostream of the user festivalculturadigitalbr.

All images in the “Showcase” section can be attributed to the creators mentioned therein. All are licensed CC BY-SA with the exception of “I heart lightning” in the article “The graphic side of 3D printing” which is licensed under CC BY.

Illustrations in “Folds, impositions and gores: an interview with Tom Lechner”, by Tom Lechner.

Photos in “Colorhug: filling the F/LOSS colour calibration void” by Richard Hughes.

Map images in "Best of SVG" are from OpenStreetMap contributors.





Images under other licenses

Photos in “Dispatch”, page 21 by Fátima Barreiros are under CC-NC-SA. They were published here with the permission of their author.

Compass rose image used in “Best of SVG”, by Wikimedia user Themadchopper is in the public domain and can be found at:

http://commons.wikimedia.org/wiki/File:Modern_Nautical_Compass_Rose.png







General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.





--------------

Eric Schrijver (Amsterdam, 1984) is a graphic designer and a performance artist. He is inspired by programming culture. Eric teaches Design for new media at the Royal Academy of Art in The Hague, and is a member of the design collective  Open Source Publishing. http://ericschrijver.nl

--------------

Editor's letter

ginger coons

--------------

Branching out:journey notes

Ana Carvalho & Ricardo Lafuente

--------------

EDITOR'S LETTER

--------------

PRODUCTION COLOPHON

--------------

Showcase

--------------

Moral rights and the SIL Open Font License

Dave Crossland

--------------

TYPE DESIGN

--------------

Designers often express caution about Libre culture. Letting other people make modifications to their designs is unusual for them. This is especially true for type designers, since the harmony of a typeface design can easily unravel if changes are made carelessly. Copyright is the major legal restriction that stops others from making modifications. In some places, though, there is another restriction: moral rights. 

Moral rights are explained on the English Wikipedia as “the right of attribution, the right to have a work published anonymously or pseudonymously, and the right to the integrity of the work.” In many places such as France, authors are not allowed to transfer, sell or give up these rights to other people or parties. 

Recently I was discussing the sil Open Font License with France's highest profile type designer, Jean Francois Porchez. He asked me if these moral rights conflict with Libre licensing. The sil Open Font License is the most popular license for Libre fonts and—I believe—it does respect moral rights. Let me walk you through the ofl's compliance measures.

Section 2 of the ofl requires that “each copy contains the above copyright notice.” As a copyright holder, your copyright notice (which can include your url and contact email) will remain with all copies and derivatives. This ensures that your attribution is made available. Additionally, there are fontlog text files commonly distributed with ofl fonts. These have a detailed description of the fonts, a timeline of releases, and details about all copyright owners and authors including those of any “parent” fonts. This is not required, but it is recommended by the ofl as a best practice and it really helps with good attribution. 

Another side to attribution is a case in which the author wishes to be misattributed, in a way: anonymously or with a pen name. In this case, designers can simply use their handle, or “Anonymous,” in their copyright notice and other fonts' metadata.

A third side of attribution, what we might call bad attribution, involves the name of the designer of a parent font being used to promote derivatives. Section 4 of the ofl provides a guard against such misuse to “promote, endorse or advertise” a derivative.

Such attribution issues are quite straightforward. The concern is really the moral right to the integrity of a work. English Wikipediaexplains that “preserving of the integrity of the work bars the work from alteration, distortion, or mutilation.” Allowing others to make alterations is fundamental to Libre culture.

Personally, I believe that saying an alteration of a typeface is a “distortion, or mutilation” is an entirely personal opinion. If I chop off the serifs from a typeface and respace it, I think it would be strange for anyone to say that I mutilated it. Others can say I did it poorly, that making a related sans is really a new design project and many other details of the typeface must be changed to re-harmonize it. But for my personal identity to be so wrapped up in my work that I could have hurt feelings in this way seems bizarre to me. Yet many artists and designers do feel strongly that the work they have made should not be altered by other people at all, even in the countries where moral rights are not institued in law, like the United States.

The identity of a designer is associated with his work. In type, there is a common association of the name of a typeface with the name of its primary designer. Making a change naturally invites altering the name to reflect differentiation. Using the original name in part could cause the association with the original designer to carry over too far. The ofl has a solution to this. It allows designers to set Reserved Font Names, which act in a similar way to trademarks.

If you declare a Reserved Font Name, the ofl ensures that when your font is modified then the name you chose for the font will not be used—unless you give additional permission.

This naming restriction encourages people who make ‘corrections,’ who adjust minor details in the font—tweaking a kerning pair here, adding some metadata there—to contribute their changes upstream, back to the original designer who maintains the font under its most well known name.

In all of these ways, among others, I believe the ofl respects moral rights, while maintaining the principles of Libre culture.



The author of this column is not a lawyer. This column is meant as a set of guidelines and opinions, not as legal advice.

--------------

Natanael Gama talks techno-fonts and the benefits of Libre

by Dave Crossland

--------------

Dave Crossland: Could you describe Exo?

Natanael Gama: Exo is a contemporary geometric sans font family with a sleek, technological look. It has a peculiar look, but it can be very versatile. It is a very complete font family, with over 700 characters supporting most Western, Central and Eastern European languages—and it can still go further!

This coverage has been reached for all 9 weights, the maximum possible on the web. Each weight has an upright style and the corresponding true italic style. Many techno fonts have only the simple slanted oblique companion styles, but for the highest typographic quality it is essential to have true italics that are drawn specifically.

The OpenType features include small caps, standard and discretionary ligatures, stylistic alternates, old-style figures, tabular figures, fractions, and more. These features will be available both as true OpenType fonts and also as simple fonts for use on the web.

Do you feel you achieved what you wanted from the design?

I feel I did, but I think in the end, only the users can say if a type designer succeeded or not.

I designed it for use in magazines and magazine-style websites, advertising. I made it for display use, and it can work to typeset small amounts of text. It could even work well as a corporate identity typeface for the right company.

It's an impressive project. What's the story behind it?

When I started my career as a freelance graphic designer I faced the problem of not having enough good fonts to do decent work. When you are starting out, you don’t have that much money to invest in good typography, so many people break the license agreements of the fonts they use. Since I wanted to be as legal as possible, I got into the world of Libre fonts—and I am so thankful to those designers who are generous enough to share their work with the world!

But along the way I have found that right now there are very few Libre fonts you can count on. This was a problem for me, and for sure it is the same for all those serious designers who avoid pirate fonts. The cheap stuff very often lacks important features to do a good job, but it can’t be changed or fixed. The amazing thing about Libre fonts is that they are both available at zero price and can also be improved, like Wikipedia!

Having this situation in mind, I decided to invest my own time into the design of a good font family to help all designers. So I made Exo for myself and designers like me—a font family for those who want to do a good job, but may lack the fonts to do it properly. I decided to make this a Libre font so the designers can do great work with a legal font family, and learn the value of high quality type.

--------------

Small & useful

--------------

SMALL & USEFUL

--------------

OpenLab started in an environment characterized by a lack of knowledge of the existing Libre alternatives and work habits exclusively built around proprietary software. The desire to promote informed choices was the motivating challenge for the founders. Today, OpenLab activities are implemented on a voluntary basis and users include a core group of five teachers, several students and former students and other collaborators. 

Since OpenLab started, we've witnessed kindergarten children playing with MyPaint and Luciole, elementary school students playing with Toonloop and Pencil, secondary school students playing with Blender and Audacity. Our own teachers and students have a place to look for support and answers. Concepts like f/loss and Creative Commons are now familiar to many students and teachers, even to those who don't use them. Several f/loss projects like Zotero, Blender, gimp, Scribus, Inkscape, Audacity, Processing and Aptana Studio have made their way into the classroom. 

In all the workshops esev has run, one of the earliest stands out and exemplifies our aims and how we want to work.

Saturday morning, May 8, 2010, the school looks empty. Not a surprise: no classes on Saturday means no students. But is it really empty? Maybe not. Two dozen students are gathered at a classroom door. We have graduate and postgraduate students from teacher education, arts & multimedia and media. Many of them don't know each other. Most of them have brought their own laptops and some have even brought graphic tablets. They've come for the “Introduction to digital painting with Free Software” workshop.

The trainer is already inside the classroom, checking to make sure the graphic tablet and the interactive whiteboard are working. Natacha Cindy, arts & multimedia student, discovered f/loss some months earlier. Armed with a passion for drawing, she immediately fell in love with Alchemy and MyPaint. She embraced the challenge of sharing her learning with other students without hesitation. It's her first experience as a trainer, the first of several.

For the next three hours, inspired by Natacha's skill and enthusiam, the students explore Alchemy and MyPaint. For most of them, it's the first time using those programs. For some, it's even the first time using a graphic tablet. For all of them, it's the first peer-guided workshop they've attended. Natacha has prepared a short tutorial for those who want to train more later or work at their own pace. However, most of the time they try to keep up, individually or in pairs, asking questions and requesting personalized support. It's tiring, mostly for Natacha, but rewarding for everyone. 

The results went far beyond the drawings produced during or after the workshop. The most important outcome was that Alchemy and MyPaint started to really exist for several students. The teacher education students took MyPaint to their kindergarten and used it in classroom activities. The students more related to artistic and creative activities installed these tools and still use them. Today, dozens of the students know about or have tried MyPaint and Alchemy. These tools found their way into the classroom and students' laptops. The best part is that this also happened with other tools, other students and other workshops.

This story can't be finished without some last words about Natacha. Today, Natacha is professionally working as an illustrator and doing her masters degree. She still uses MyPaint and Alchemy on a daily basis, although the last time we talked, she was very impressed with Krita. She's even managed to bring MyPaint to her workplace (a large book and multimedia publishing company) and integrate it in her workflow.

She's probably going to read these lines so it's only fair that we tell her: thank you and we're very proud to have you as an OpenLab esev friend and collaborator.

It's true that proprietary software is still predominant and that there's still a lot of awareness-raising and capacity building to do but we're proud of what we've accomplished and learned so far. To us, using f/loss is a statement about the world we live in and how we choose to live in it.

--------------

The finished and unfinished business of OpenLab ESEV

by Nelson Gonçalves and Maria Figueiredo

--------------

Based in the School of Education of the Polytechnic Institute of Viseu (esev), OpenLab esev aims to establish a platform to foster and support the use of f/loss, Free Culture and more flexible licenses for creative and educational purposes. Esev, located in Viseu, a city in the northern-center of Portugal, has over 1500 students and 105 teachers and offers nine undergraduate and 13 graduate programs. Programs are related to education, cultural animation, arts & multimedia, media, sports and public relations.

http://www.esev.ipv.pt/openlab

--------------

THE INTERVIEW

--------------

There's an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well.

--------------

DISPATCH

--------------

SCHRIJVER

--------------

DISPATCH

--------------

Graphic design is a nostalgic field. Even in the art schools, the students want to make books and posters. Designing for the web has little prestige. I could say that I want students to design for the screen, and to actively engage with their digital tools, but I first need to know what it is that makes it so attractive to design for the printing press.

Books and posters nowadays start their lives on a computer in proprietary software. Many of my colleagues see the software as a neutral tool, subservient to their creativity. Therefore, the software can be used as-is. For me, software is a piece of culture, an embodiment of a certain way of thinking. The software partakes in the creation. A truly rich visual culture can only come about if designers manipulate, appropriate and subvert the software technology they use. 

Hacking analogue technology is a physical affair. Cracking open software requires a different sort of interaction, with programming interfaces and computer files. It requires a new set of skills that takes time and enthusiasm to attain. It is this enthusiasm that is often lacking. In fact, my students often seem scared of digital technology.

So what are today's nascent designers scared of? The comparison with their attitude to printing technology shows it is not necessarily technology in general that designers are frightened of, nor is it geekiness. Designers actually take pride in the geeky details of their craft when they are related to the printing process, knowing about things like spot colors, paper stocks and binding methods.

Is it a matter of differing cultures? Even though design is applied mathematics, most design majors study the humanities in high school. Code seems to belong to this other world, the world of the kids who choose mathematics. The other geeks. 

If the divide is social, then a gentle introduction to the other culture, the culture of programming, itself embedded in the culture of science and mathematics, should form part of a contemporary design curriculum.

Or another strategy, can we force the students to get their hands dirty? With code? No more mockups. That's an efficient way to introduce the nature of the digital. There is always the question of whether designers should learn to code. I think they should.

As a student, I came across a printing press that worked with movable type. I spent a day setting a simple poem. It's dirty, precise, frustrating work. At the end of the day I printed my poem, and only after I had cleaned the press, I spotted the spelling error.

As tedious as the process had been, this day taught me so much about the nature of printing technology. My understanding of my profession really deepened. I know why uppercase is called uppercase (uppercase letters are stored above the lowercase ones); why leading, the space between lines, is called leading (it is strips of lead). I have an understanding of how all of the classical book layout conventions are related to the process of setting a block of movable type.

Even though I never set anything in movable type again, I understand printing technology to a further extent. The same is true for code. Going through the tedious process of writing a computer program will change your understanding of the medium you work with all the time. Your dirty hands will forever influence any interaction you have with programmers.

I say “getting your hands dirty” and it is obvious that it is a metaphor. Or is it? At any rate, anything I do, I do it with my body. My head is but a part of my body (I'm glad that html5 acknowledges this). And it is not necessarily so that my interaction with a computer is more detached than that with another device. To see what is going on, we need to move beyond tight distinctions between analogue and digital, between concrete and abstract. Like drawing, when you think and you do at the same time [sic-ed].

I don't know how physical programming actually is. What kind of software interaction is it, that is the equivalent of prying something open, or to turning it on its head? I do know that all my descriptions of what I want include this physical dimension. What I want is a relation to software that is more daring, and ultimately more intimate.

--------------

Best of SVG

--------------

BEST OF SVG

--------------

We at Libre Graphics magazine have a thing for open standards. We like their transparency and their interoperability. We like that, with a well documented standard, everyone has an equal chance to play nicely together. 

That's why we like svg so much. It's a well developed, well supported standard brought to us by the World Wide Web Consortium (w3c). It's available for implementation by anyone developing software. It shows up in modern browsers, fine vector graphics editors and any number of other places. 

One thing that's missing, though, is you: the designer, the artist, the illustrator. So put down that .ai file and check out svg.

--------------

Unpaper is a command line tool designed to correct issues in scanned book pages. It removes excess darkness around edges, rotates and de-skews. 

http://unpaper.berlios.de

--------------

Not a tool, but a resource, The Public Domain Review is a gateway to interesting works in the public domain. Saving you from slogging through  all the public domain work available, the Review presents curated collections of particularly interesting work.

http://publicdomainreview.org

--------------

Pdf Chain is a convenient addition to another small and useful tool, Pdftk. Pdf Chain provides a graphical interface for the command line power of Pdftk, allowing users to conveniently merge, split and make other modifications to multi-page pdf documents.

http://pdfchain.sourceforge.net

--------------

GimpLensfun is a handy gimp plugin using the LensFun library and database. GimpLensFun brings easy correction of lens distortion right into your raster graphics editor. 

http://lensfun.sebastiankraft.net

--------------

Gtk Vector Screenshot taking module does what it says on the tin: it takes vector-based screenshots. No more flat, un-scalable screenshots.

https://gitorious.org/gtk-vector-screenshot

--------------

Where

CulturaDigital.Br Festival, Rio de Janeiro, Brazil. 

--------------

Notebook

--------------

What

Brazilian free culture festival, bringing together creators and community organizations.

--------------

Easiest How-To

Mimi Hui, a member of nyc Resistor took the opportunity during a talk about rfid technology to explain the construction of aluminum foil Faraday cages.



--------------

Least Expected

For unadjusted ears, the sound of birds in nearby trees was a shock. Throughout, the constant chirps accompanied the voices of happy festival participants.

--------------

Best Use of Foam

The festival's logo was rendered in giant letters, made of foam and perfect for sitting. At the end of the weekend, some participants went home with parts of this unusual type treatment.

--------------

Best Ambience

The festival took place almost entirely outside, on the grounds of the Museum of Modern Art in Rio de Janeiro. The museum itself is raised off the ground, with space below. Crowds of festival-goers wandered and worked in the shadow of a floating gallery.

--------------

UPCOMING EVENTS

--------------

UPCOMING EVENTS

--------------

28-29 APR



--------------

Linux Fest Northwest

Bellingham, United States

http://linuxfestnorthwest.org

--------------

Upcoming events



--------------

We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and f/loss. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the f/loss designer at a traditional design event, or maybe the designer at a predominantly software developer event. 



--------------

2-5MAY

--------------

Libre Graphics Meeting

Vienna, Austria

http://libre-graphics-meeting.org/2012

--------------

1

JUN

--------------

Drupal Business Summit

Vancouver, Canada

https://www.drupalsummit.com

--------------

6-8 

JUN

--------------

Linux Con Japan

Yokohama, Japan

https://events.linuxfoundation.org/events/linuxcon-japan

--------------

8-10

JUN

--------------

Southeast LinuxFest

Charlotte, NC, United States

http://www.southeastlinuxfest.org

--------------

30 AUG

3 SEP

--------------

Ars Electronica Festival

Linz, Austria

http://www.aec.at/thebigpicture/en

--------------

9-11

NOV

--------------

Mozilla Festival 2012

London, United Kingdom

https://www.mozillafestival.org

--------------

18-20

MAY

--------------

SIGINT

Cologne, Germany

http://sigint.ccc.de

--------------

11-14

SEP

--------------

SVG Open & The Graphical Web

Zurich, Switzerland

http://www.svgopen.org/2012

--------------

10-18

NOV

--------------

HTMlles 10

Montreal, Canada

http://www.htmlles.net/2012

--------------

25-26

MAY

--------------

Flossie 2012

London, United Kingdom

http://www.flossie.org

--------------

25-28

JUL

--------------

Fisl 2012

Porto Alegre, Brazil

http://softwarelivre.org/fisl12

--------------

15

JUN

--------------

Ampersand 2012

Brighton, United Kingdom

http://2012.ampersandconf.com

--------------

We now re-grab the reins after last issue's experiment with collaboration. It was a thrill to work with and feature the contributions of so many great practitioners; it was also a chance to go through and become an impromptu version control system, merging the various contributions into one coherent whole.

Version control is a theme that keeps popping up when discussing free and open creative projects, and it has, for good reason, been one of the main threads of discussion at the Libre Graphics Research Unit*. Not only does it provide useful metaphors for us to (re-)think our practice, but it is an effective practical tool for the management of complex editorial projects such as a magazine (though it is becoming unwieldy to manage a repository that takes up more than 8 gigabytes).

And now that we wrap up the first year-run of production with the release of the fourth issue, it is now time to sit down and wonder where we can go from here. This publication made evident that there is an emergent kind of artistic practice based on free culture that lacks proper stages where it can show itself, and the crossings between software tools, design theory and real-world practice remain underexplored. 

There is still a lot to be said, and not just through words. The layout decisions and general graphic and artistic direction of Libre Graphics magazine are also a reflection of our unanswered questions and tentative answers. As an example, we've avoided exuberant experimentalism when conceiving the magazine's identity and graphic principles; this isn't because we're shy to experiment, but because we decided we'd be careful. Balancing reader expectations towards predictability and consistency with the designer's will to push the boundaries of graphic language is a frequent dilemma, and one that keeps popping up when we sit down to lay out a new issue: how far can we go without risking clarity and seriousness? And in what ways does our subject matter demand a new artistic approach? Our way to look for answers is to go on through slow, but steady steps.

We get closer to a satisfactory answer by taking risks and sometimes failing. We took note of our perceived failures (some of them pointed out by our readership), and are getting ready to sit down and, again, take a new risk in defining an improved identity for Libre Graphics magazine to be introduced in issue 5—the first issue of volume 2. We hope you're enjoying the journey, and we hope we can count on your curiosity and critical feedback since, in a good way, we're all lost together trying to figure out all the subtle ways that libre graphics culture is amplifying our collective possibilities and potential.



* From the lgru web site: “The Libre Graphics Research Unit is a traveling lab where new ideas for creative tools are developed. The Research Unit is an initiative of four European media-labs actively engaged in Free/Libre and Open Source Software and Free Culture. This cross-disciplinary project involves artists, designers and programmers and is developed in dialogue with the Libre Graphics community.” (http://lgru.net)



--------------

Even blank maps can tell a history. 

Just browse the colletion of Blank World Maps offered by Wikimedia Commons and OpenStreetMap, set your eyes on the ever changing boundaries of countries and nations.

Being svg, they are doubly useful: one, you get resolution-independent maps, allowing you to scale them without mercy. Two, svg makes them perfect for including in a web application or visualization, creating experimental programs around geographic data, and for clever maps that embed additional cartographic data in the file itself.

It's a journey through history in svg shapes.



—the editors

--------------

