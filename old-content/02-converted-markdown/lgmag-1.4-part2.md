Glossary 1.4

--------------

Alchemy:A sketching environment designed for visual experimentation. Available for gnu/Linux, Mac os x and Microsoft Windows.



Aptana Studio:An IDE for creating web applications. Available for gnu/Linux, Mac os x and Microsoft Windows.



ArgyllCMS:A colour management system with icc profile compatibility for a variety of display and capture devices. Available for gnu/Linux, Mac os x and Microsoft Windows.



Audacity:A sound editing application for gnu/Linux, Mac os x and Microsoft Windows.



Blender:A powerful 3d animation application for gnu/Linux, Mac os x and Microsoft Windows.



Chiplotle:A Python API for pen plotters. Implements HPGL. Available as a Python library.CMS:Stands for Content Management System. Software installed on a webserver in order to provide a simple framework for editing webpages. WordPress and Drupal are examples of content management systems.



command line:A text only user interface, allowing users to input commands without the intervention of cursors and graphical user interfaces.

colord:A tool for installing, managing and creating colour profiles. For gnu/Linux.

Creative Commons:A suite of licenses designed to allow creators and users of works flexibility beyond that offered in traditional copyright.



F/LOSS:Stands for Free/Libre Open Source Software. Software which has a viewable, modifiable source. It can be modified and redistributed.



firmware:Software designed to be permanently installed on a computer/device. Free software:A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.



GIMP:A raster based image editor for gnu/Linux, Mac os x and Microsoft Windows. (Sometimes spelled Gimp) 



Hugin:Panoramic photo stitching software for gnu/Linux, Mac os x and Microsoft Windows.



Inkscape:A vector graphics editor for gnu/Linux, Mac os x and Microsoft Windows.



JavaScript:A scripting language commonly used on websites. 



Krita:A drawing application supporting both vector and raster images. Available for gnu/Linux, Freebsd and Microsoft Windows.Laidout:A layout and desktop publishing application for gnu/Linux.



Luciole:A stop motion animation program for gnu/Linux.



MyPaint:A digital painting application with a focus on fluid workflow. Available for gnu/Linux, Mac os x and Microsoft Windows.



Node.js:A JavaScript-based system for creating internet applications.



open hardware:Hardware which follows the same principles as f/loss, including publicly available, freely licensed schematics.



Paper.js:A web-friendly JavaScript version of Scriptographer.



PCB:A printed circuit board, the base of most electronics, generally consisting of a number of electronic components linked together to form certain types of circuits.Pencil:2d animation software with support for both vectors and rasters. Available for gnu/Linux, Mac os x and Microsoft Windows. 



Processing:A programming language and development environment predominantly used for visually-oriented or media-rich projects. Available for gnu/Linux, Mac os x and Microsoft Windows.



Python:A popular interactive programming language. Available for gnu/Linux, Mac os x and Microsoft Windows.



“scratching an itch”:A term used in many f/loss communities to mean that a developer has created a program primarily to meet his/her own needs, instead of the needs of others.



Scribus:A desktop publishing application for gnu/Linux, Mac os x and Windows.



Scriptographer:A scripting plugin for Adobe Illustrator which gives the user the possibility to extend Illustratorʼs functionality by the use of the JavaScript language. www.scriptographer.org. Scriptographer is a gpl-licensed plugin for proprietary software.SIL Open Font License (OFL):A license intended for use with fonts and font related software. Dictates terms which allow modification and redistribution of fonts.



ToonLoop:A stop motion animation application for gnu/Linux. A minimal version is available for Mac os x and Microsoft Windows.



Zotero:A reference management application with extensions for browsers and word processors. Available gnu/Linux, Mac os x and Microsoft Windows.





--------------

Glossary

--------------

Resource list 1.4

--------------

GIMP

A raster based image editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Inkscape

A vector graphics editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Resources

--------------

Glossary

--------------

Scribus

A desktop publishing program for gnu/Linux, Mac os x and Microsoft Windows. 

--------------

FEATURE

--------------

There's a little revolution taking place in basements, garages and hackerspaces. 3d printers—machines which turn digital meshes into physical objects—are getting cheaper, better and more popular. We've collected samples of some strikingly graphic 3d prints, designed with f/loss tools.

All of the projects featured in this showcase are available from Thingiverse, under permissive licenses, for printing and modification.

http://thingiverse.com



--------------

The graphic side of 3D printing

--------------

Manufactura Independente: Your approach to digital design tools is rather specific, in the sense that you mostly create actual, very specific graphic tools instead of filters: user input is thus a very strong part of the outcome. How do you see the issue of control in the tools you make?

Jonathan Puckey: When I find myself doing filters, I work mainly with the programmer part of my brain. I rewrite the code, press a button, check the result, rewrite the code, press a button, check the result. When I make tools, I am able to split the process in two: first I get to design an environment to work within, then I (or others) get to push those boundaries to their limits by working within it. It becomes a collaboration once more.

What's your criteria for publishing a tool vs. keeping it for your own use?

It depends. I am happy to publish a tool when I feel that there might be other people out there who could use it in different ways than I could. So there is something missing, that needs to be filled in by the designer. This also means that the user of the tool is in the forefront, not the tool itself.

What defines whether you'll build a single-use tool or a re-usable one? Do you find yourself creating a base set of more general-purpose scripts, or do you start from scratch for a new project?

I really believe in investing time into contributing to the platforms that I use to build tools in. For graphic design tools this is Scriptographer, for web based vector graphics this is Paper.js and for backend server and automization stuff I am currently using Node.js, which has a very rich community. The code I reuse tends to be built into the libraries I work with. If something is missing in my tool belt, I am happy to contribute missing features. In the end, you need great tools to make great tools.

Would you agree there is a gap between the mindset of the tool creator and the tool user? How do you deal with it? Or does that distinction make no sense to you?

Sometimes, as a tool creator, it can be difficult to put the same amount of attention into using the tool as you put into developing it. Then it can be very enjoyable to see others working with your tool, pushing it to where it deserves to go. In the end, we are all tool creators on different meta levels. The end user of the software develops new ideas on how to apply the tool (also in combination with other tools) and therefore redefines the tool itself.

You've recently been involved in the development of the Paper.js project with Jürg Lehni, which brings the concepts of Scriptographer (a plugin to script Adobe Illustrator using JavaScript) to the web and the browser. How does the switch from a desktop environment into a browser canvas change your perceived boundaries of what you can build?

It changed more than we at first thought it would. When we did the first tests, we were only really thinking of creating generated static images. All at once we were seeing 60fps animations come out of our little framework. That was very exciting and opened up possibilities for all kinds of things we weren't thinking of in the first place. We have also plugged Paper.js into the server side using Node.js, which allows for things like video compositing and maybe in the future, pdf generating.

Would you say it makes possible new aesthetic directions? Have you found, or come up with, surprising and unexpected uses for Paper.js so far?

Currently, I am quite interested in the possibilities of developing design tools for clients. I find the idea of a client working with a design tool within a controlled environment very interesting. Balancing freedom and restriction, etc. Paper.js allows me to develop design tools for clients, without the need for them to purchase expensive and complicated professional software.

What are you busy with now?

I recently started a new graphic design studio called Moniker (http://studiomoniker.com) together with Roel Wouters and Luna Maurer. I am currently working on some meta-ball Paper.js code for a cultural website. We have also started development of a text-file based cms, which we aim to use within our studio for small portfolio websites for clients. We just received funding to work on a music video which uses some interesting backend video compositing technology. What else... Oh yes, we are currently looking for some amazing programmer/designer interns who would like to collaborate with us on these things.



--------------

Paper.js: building, designing and the browser

An interview with Jonathan Puckey by Manufactura Independente

--------------



You need great tools to make great tools.

--------------

Femke Snelting: What is Laidout? 

Tom  Lechner: Well, Laidout is software that I wrote to lay out my cartoon books in an easy fashion. Nothing else fit my needs at the time, so I just wrote it. 

FS: It does a lot more than laying out cartoons? 

TL: It works for any image, basically, and gradients. I guess it's two forms of laying out. It's laying out pieces of paper that remain whole in themselves, or you can take an image and lay it out on smaller pieces of paper. Tiling, I guess you could call it.

FS: Could you say something about your interest in moving from 2d to 3d and back again? It seems everything you do is related to that.

TL: I've been making sculpture of various kinds for quite a long time. I've always drawn. Since I was about eighteen, I started making sculptures, mainly mathematical woodwork. I don't quite have access to a full woodwork workshop anymore, so I can't make as much woodwork as I used to. It's kind of an instance of being defined by what tools you have available to you. I don't have a wood shop, but I can do other stuff. I can still make various shapes, but mainly out of paper. Since I had been doing woodwork, I picked up photography. I made a ton of panoramic images. It's kind of fun to figure out how to project these images out of the computer into something that you can physically create, for instance a T-shirt or a ball, or other paper shapes. 

FS: Is there ever any work that stays in the computer, or does it always need to become physical? 

TL: Usually, for me, it is important to make something that I can actually physically interact with. The computer I usually find quite limiting. You can do amazing things with computers, you can pan around an image—that in itself is pretty amazing—but in the end I get more out of interacting with things physically than just in the computer. 

FS: But with Laidout, you have moved folding into the computer! Do you enjoy that kind of reverse transformation? 

TL: It is a challenge to do and I enjoy figuring out how to do that. In making computer tools, I always try to make something that I can not do nearly as quickly by hand. It's just much easier to do in a computer. Or in the case of spherical images, it's practically impossible to do it outside the computer. I could paint it with airbrushes and stuff like that but that in itself would take a hundred times longer than just pressing a couple of commands and having the computer do it all automatically. 

FS: Do you think that for you, the program itself is part of the work?

TL: I think it's definitely part of the work. That's kind of the nuts and bolts that you have to enjoy to get somewhere else. But if I look back on it, I spend a huge amount of time just programming and not actually making the artwork itself. It's more just making the tools and all the programming for the tools. I think there's a lot of truth to that. When it comes time to actually make artwork, I do like to have the tool that's just right for the job, that works just the way that seems efficient.

FS: How is the material imagined in the tool? 

TL: So, far not really completely. When you fold, you introduce slight twists and things like that. And that depends on the stiffness of the paper and the thickness of the paper. I've not adequately dealt with that so much. If you just have one fold, it's pretty easy to figure out what the creep is for that. You can do tests and you can actually measure it. That's pretty easy to compensate for. But if you have many more folds than that, it becomes much more difficult. 

FS: Are you thinking about how to do that? 

TL: I am. 

FS: That would be very interesting. It is very strange to imagine the material in the digital space, to give an idea of what might come out in the end. Then you really have to work your metaphors, I think. You talked about working with physical input, having touchpads... Can you talk a bit more about why you're interested in this? 

TL: You can do a lot of things with just a mouse and a keyboard. But it's still very limiting. You have to be sitting there, and you have to just control those two things. Here's your whole body, with which you can do amazing things, but you're restricted to just moving and clicking and you only have a single point up on the screen that you have to direct very specifically. It just seems very limiting. It's largely an unexplored field, just to accept a wider variety of inputs to control things. A lot of the multitouch stuff that's been done is just gestures for little tiny phones. It's mainly for browsing, not necessarily for actual work. That's something I would like to explore quite a lot more. 

FS: Your folding simulations made me think about gestures. Do you have any fantasies about how this could work for real?

TL: There's tonnes of sci fi movies, like Minority Report, where you wear these gloves and you can do various things. Even that is still just mainly browsing. I saw one, it was a research project by this guy at Caltech. He had made this table and he wore polarized glasses so he could look down at this table and see a 3d image. And then he had gloves on, and he could sculpt things right in the air. The computer would keep track of where his hand is going. Instead of sculpting clay, you're sculpting this 3d mesh. That seemed quite impressive to me. 

FS: You're thinking about 3d printers, actually? 

TL: It's something that's on my mind. I just got something called the Eggbot. You can hold spheres in this thing and it's basically a plotter that can print on spherical surfaces or round surfaces. That's something I'd like to explore some more. I've made various balls with just my photographic panoramas glued onto them. But that could be used to trace an outline for something and then you could go in with pens or paints and add more detail. If you're trying to paint on a sphere, just paint and no photograph, laying out an outline is perhaps the hardest part. If you simplify it, it becomes much easier to make actual images on spheres. That would be fun to explore. 

Pierre Marchand: I'd like to come back to the folding. Following your existing aesthetic, the stiffness and the angles of the drawing would be very beautiful. Is it important you, preserving the aesthetic of your programs, the widgets, the lines, the arrows...?

TL: I think the specific widgets, in the end, are not really important to me at all. It's more just producing an actual effect. So if there is some better way, more efficient way, more adaptable way to produce some effect, then it's better to just completely abandon what doesn't work and make something that's new, that actually does work. Especially with multitouch stuff, a lot of old widgets make no more sense. You have to deal with a lot of other kinds of things, so you need different controls. 

PM: The way someone sets up his workshop says a lot about his work. The way you made Laidout and how you set up its screen, it's important to define a spot in the space of the possible.

Ludivine Loiseau: What is nice is that you made the visualisation so important. The windows and the rest of the interface is really simple, the attention is really focused on what's happening. It is not like shiny windows with shadows everywhere, you feel like you are not bothered by the machine. 

FS: Are there many other users of the program? 

TL: Not that I know of (laughter). I know that there is at least one other person who actually used it to produce a booklet. So I know that it is possible for someone other than myself to make things with it. I've gotten a couple of patches from people to not make it crash at various places but since Laidout is quite small, I can just not pay any attention to criticism. Partially because there isn't any, and I have particular motivations to make it work in a certain way and so it is easier to just go forward. 

FS: Can you describe again these two types of imposition, the first one being very familiar to us. What is the difference with the plan for a 3d object? A classic imposition plan is also somehow about turning a flat surface into a three dimensional object? 

TL: It is almost translatable. I'm reworking the 3d version to be able to incorporate the flat folding. It's not quite there yet, the problem is the connection between the pages. Currently, in the 3d version, you have a shape that has a definitive form and that controls how things bleed across the edges. When you have a piece of paper for a normal imposition, the pages that are next to each other in the physical form are not necessarily related to each other at all in the actual piece of paper. Right now, the piece of paper you use for the 3d model is very defined, there is no flexibility. Give me a few months! 

FS: So it is very different actually. 

TL: It is a different approach. One person wanted to do flexagons, it is sort of like origami I guess, but it is not quite as complicated. You take a piece of paper, cut out a square and another square, and then you can fold it and you end up with a square that is actually made up of four different sections. Then you can take the middle section, and you get another page and you can keep folding in strange ways and you get different pages. Now the question becomes: how do you define that page that is a collection of four different chunks of paper? I'm working on that! 

FS: We talk about the move from 2d to 3d as if these pages are empty. But you actually project images on them and I keep thinking about maps, transitional objects where physical space is projected on paper which then becomes a second real space and so on. Are you at all interested in maps? 

TL: A little bit. I don't really want to because it is such a well-explored field already. Already for many hundreds of years the problem is how do you represent a globe onto a more or less two-dimensional surface. You have to figure out a way to make globe gores or other ways to project it and then glue it on to a ball for example. There is a lot of work done with that particular sort of imagery. 

PM: Too many people in the field!

TL: Yes. One thing that might be interesting to do though is when you have a ball that is a projection surface, then you can do more things, like overlays onto a map. If you want to simulate earthquakes for example.

FS: And the panoramic images you make, do you use special equipment for this? 

TL: For the first couple, I made this 30-sided polyhedron that you could mount a camera inside and it sat on a base in a particular way so you could get thirty chunks of images from a really cheap point-and-shoot camera. You do all that, and you have your thirty images and it is extremely laborious to take all these thirty images and line them up. That is why I made the 3d portion of Laidout, it was to help me do that in an easier fashion. Since then I've got a fisheye lens which simplifies things quite considerably. Instead of spending ten hours on something, I can do it in ten minutes. I can take 6 shots, and one shot up, one shot down. In Hugin you can stitch them all together. 

FS: Panoramic images are usually spherical or circular. Do you take certain images with a specific projection surface in mind? 

TL: To an extent. I take enough images. Once I have a whole bunch of images, the task is to select a particular image that goes with a particular space. Like cubes—there are few lines and it is convenient to line them up to an actual rectangular space like a room. The tetrahedron made out of cones, I made one of Mount St. Helena, because I thought it was an interesting way to put the two cones together. One thing I would like to do is to extend the panoramic image to be more like a progression. For most panoramic images, the focal point is a single point in space. But when you walk along a trail, you might have a series of photographs all along. I think it could be an interesting work to produce, some kind of ellipsoidal shape with a panoramic image that flows along the trail.

--------------

Folds, impositions and gores: an interview with Tom Lechner

In May 2011, Femke Snelting, Pierre Marchand and Ludivine Loiseau interviewed Tom Lechner, the creator of Laidout.

--------------

What is ColorHug?

The ColorHug is a small usb device that connects to a computer and measures colours very accurately. It allows animators, designers and artists to calibrate their screens. It builds a profile that lets colour-managed applications adapt colours automatically, helping videos and photos to look as originally intended. Measuring the screen with an external device is the only way to determine the difference between what the display thinks it's showing the user and what the user is actually being shown. For instance, most Thinkpad displays are very blue, which makes photos look overly cold. This isn't even something you can measure and fix once, as when the lcd display panel gets older, it gets dimmer and more yellow due to the backlight ageing. Even the phosphors in crts and plasmas degrade at different rates over time. The ColorHug device allows you to check your screen every few months or so, and takes a few minutes to profile and correct the screen profile. The device has drivers for the colord color management framework (also written by me) and will even be supported by argyllcms in the next release. The firmware interface is available for people to play with for no charge, and of course all the firmware, bootloader and hardware design is open source and freely available.

I developed the ColorHug to really scratch an itch. In doing this, I had no intention of becoming a hardware manufacturer. I enjoy writing Free software too much. The main motivation for designing and building this hardware was that at conferences I would showcase my software framework, and surprise designers with the effect of calibrating their screen. The main sticking point was that I was telling people to go and buy some expensive hardware from a proprietary color company that doesn't care one little bit about Linux support. The proprietary hardware isn't really any good, and the profit margins must be huge on each device. So I began thinking. All I really needed to do was build a usb device that could count pulses really fast and do a few matrix calculations on a set of numbers. Luckily for the ColorHug, my university degree was in electronics and my previous job was working for a defence contractor prototyping new stuff. So really it wasn't that much work to build a functional prototype, and then build 50 units for people to play with.

ColorHug is cheap. It's £48 plus postage, and so it's really aimed at the low end of the market. I could have built something much more accurate and a lot faster, but that would mean selling a device for several hundred pounds. I'm aiming this at people who want to “fix” the colour on their screen and who want to start playing around with colour managing their workflow. If you're already a colour geek, you really want to spend about £500 on a spectrophotometer device. If you're just a Linux user who likes to look at photos but don't know what “blood red” is in xyz co-ordinates, the ColorHug might be the device for you.

The business of ColorHug

Setting up any business is difficult. Right from the start I decided to set up the business as an independent company, so my personal liability would be minimised. I injected a small amount of my own savings to be able to buy stock for 50 devices. The first 50 devices I soldered myself, not with machines, which turned out to be less fun than it sounds. The amount of profit for the first batch was tiny (less than £100) but the fact that I've got 50 users who say the device works means that I can be confident spending more money to make a larger, more profitable batch. The hardest part about actually making stuff in the uk is the sheer complexity of the tax situation and all the different registrations you have to get before you can even start. A lot of the companies I contacted to build the pcb wanted huge minimum orders, and without a few hundred thousand pounds in capital I'm forced to build up the order size organically using the profit from the last batch to buy the stock for the next, slightly larger, batch.

So, a lot of people want to know what the point is, when it all sounds like so much work for not a lot of cash: I want people to try making open hardware themselves. There are so many talented engineers in software that could make simple devices that outperform or undercut devices from proprietary companies. I've already got a fairly good-size community built around ColorHug, where people post patches for new functionality and fixes for bugs. The hard part for most people is releasing control and trusting that people will actually help the project grow.

ColorHug is only something I do late at night and at the weekends. This means the project is going to move more slowly than if I had a few full time staff. The short term prognosis is batch number 2, which is 100 units. Then a few months after that I'll probably double the batch size and build some more. I know the worldwide market of these devices is going to be small, but I'm hoping it'll kickstart other people in trying to do something like this themselves.

--------------

FEATURE

--------------

ColorHug: filling the F/LOSS colour calibration void

Richard Hughes

--------------

 

I know the worldwide market of these devices is going to be small, but I'm hoping it'll kickstart other people in trying to do something like this themselves.

--------------

The Baltan CutterOr the thin line between intuitive and generative

by Eric de Haas

--------------

In 2010, I began to work on a new visual identity for Baltan Laboratories, a lab for art, design and technological culture based in Eindhoven, the Netherlands. During this research process, more and more examples of generative graphic design started popping up, which I started to investigate as an approach that could be used for Baltan. Since Baltan is an organisation that profiles itself at the intersection of art, design and technological culture it seemed very appropriate to look into these new ways of dealing with a visual identity. Whereas a traditional graphic identity is defined by a logo, choice of colors, typeface and a set of rules for applying these elements, we are witnessing a strong shift towards more organic/random identities. This shift has been triggered by a change in available tools and mediums, and a move from print to screen.

Although fascinated by a lot of good generative design, such as the mit Media Lab identity created by TheGreenEyl (with E. Roon Kang), I found myself less interested in the generative process and more in the moment when the design is deemed to be finished. This is something that is determined, in the case of generative design, by the script. Where in generative design creativity takes place in the very beginning and middle, creating a concept and writing the code, it leaves little room for intuitive decisions at the end of the process. This endphase of a design process, where you can decide to redo a whole layout or change all the colours, is what ultimately speaks to your intuition as a designer and what can be part of a signature. A logical step from there was to design a tool which would have some of the same features as a generative tool, but still needed the human hand and eye to complete the outcome, a tool that would need the specific skill of the user.

Good examples of this type of tool can be found in many Scriptographer tools, where the hand or the operation of a mouse/pen can strongly determine the output. Inspired by this, I contacted Jonathan Puckey, a co-developer of the Scriptographer platform, and asked him to help us create a tool. The topic of generative versus intuitive triggered some interesting conversations with Jonathan, which fortunately made him decide to work with us on this project. We then entered the most difficult phase of deciding what this tool should do.

Jonathan and I started a blog that was used to document a period of correspondence between the two of us as a way to develop our thoughts around the Baltan tool. I would write a letter/post that would be answered by Jonathan in the form of a script/tool. (This correspondence can be found at http://baltanlaboratories.tumblr.com)

We started by looking at the first tool known to mankind, the bone or stick that was used for beating, the simple function of hammering, which could be used either as a weapon or as a tool to crack things. We found out that the old saying "all tools can be used as a hammer" didnʼt quite work for digital tools, but tried to simulate this by developing a version that could create but also destroy. From here we drifted off into the weird evolution of the mouse as a drawing tool. What the mouse basically did was separate the hand from the eye. After then doing some experiments where the keyboard would function as a drawing tool, and subsequently finding out that this would minimize the chance of making mistakes, we started questioning whether or not mistakes are essential in a drawing process or if this is a romantic idea triggered by the feeling that we have too much control over our toolset.

This feeling of too much control made us wonder if this could be the reason that for the last 10 or 15 years there have been a lot of projects appearing that try to have unpredictable or generative output. Could the lack of traces or textures common to analogue tools, like the inconsistent ink flow of a pen or a brush, trigger an urge for unpredictability in relation to their digital counterparts? Do we actually need to turn things around? Do we need a tool that has to learn how to complement its user, a tool with a memory, a tool that learns? But memory is also created by wielding the tool. Each outcome created using the tool shifts its potential and therefore becomes part of its memory. This can only happen when the tool provides us with a logical framework to work within, and only when the outcome can be considered to be a layered series of decisions with traceable influences.

This was why drawing with a keyboard felt unnatural: all decisions were already made by the engineer. We needed to find a way to emphasise the decisions, maybe by limiting them. Maybe the prehistoric cutting tool contained more than enough choices. Maybe it represents the perfect example of a tool where the person operating it creates a memory and learns along the way, learning to work quicker or more detailed. 

The final version of the Baltan Cutter stayed very close to one of the first sketches and can be best described as a 2d sculptor's tool. It basically has two functions: cutting and chipping. Both functions operate in a very clear way so that you are actually able to sculpt if you learn to operate the tool, but it also leaves room for intuitive cutting. Although we've just started using the Baltan Cutter for Baltanʼs identity-related items, we are finding out that the simplicity, limitations and the learning process of handling the tool create fresh outcomes and leave a lot of space for intuition and creativity. By consistently applying this output in different ways it also has a strong effect on the identity because it generates graphic imagery which have a certain level of autonomy and authenticity which makes it very recognisable in its appearance.

--------------

SHOWCASE

--------------

Resurrecting the noble plotter

Manufactura Independente & Diogo Tudela

--------------

Pen plotters are already a somewhat distant artefact of early mass-production digital devices. Designed to meet the needs of technical draftspeople by tracing cad and architectural schematics, they were made obsolete by large-format inkjet printers right before the turn of the millennium.

The appeal of a pen plotter was its precise ability to trace contours in a way that no other kind of printer could. Its transparent operation looks almost magical: after the document is sent to the device, a mechanical arm grabs one of the available pens and quickly traces a drawing, switching pens as it goes if the document requires more than one colour. Unfortunately for the plotter, breakthroughs in printing resolution made it possible to reproduce (and surpass) the quality of a plotter’s accurate lines. Thus, for technical and professional purposes, pen plotters have become obsolete and now pop up regularly at bargain prices on auction sites, with the cost of shipping the bulky device sometimes more than that of the used pen plotter itself. 

The fast-paced upgrade cycle of modern operating systems, with the inevitable consequence of dropping support for older devices, has resulted in the absence of any kind of recent drivers for most pen plotters in Windows and os x. It has also become significantly harder to find software that can work with the plotters’ page description language, hpgl.

As you would expect, given this dire scenario, f/loss comes in to save the day. Chiplotle is a small and powerful Python library which provides a command-line interface that allows sending hpgl files to a plotter. Even if it isn’t supported directly by the library, you can probably get your plotter working using Chiplotle’s generic profile. 

What about hpgl? It is indeed hard to find support in modern software, but again, as expected, our favourite vector editor provides the answer. Inkscape is able to export hpgl files, right out of the box. And since hpgl files are plaintext, there’s little to stop us from making our own scripts to fine-tune and correct the document before sending it for drawing. We’ve documented the process in some detail in our blog (http://tinyurl.com/plotterhacking).

In exploring plotter hacking, we worked with Diogo Tudela, who acquired a used Roland dpx-2200 a2 plotter and wanted to hook it up. Using Chiplotle and hacking through the hardware adapters in order to get the right configuration, the three of us managed to resurrect this 1985 device into full operation. It was a thrill to be able to experiment with things that regular printers cannot do: we can fit almost any pen in its robot hand, as well as use fancier kinds of paper that would jam a regular printer. The process of hand-tweaking the hpgl files and making pre-processing scripts brought us a lot closer to the peculiarities of a plotter’s operation principles, far from the usual black-box experience that regular inkjets give us, with their sealed print cartridges and hard-to-hack interfaces.

With f/loss developers providing the necessary tools, and with plotters available at bargain prices, there's no better time for hacking and exploration with the noble pen plotter.

--------------

SHOWCASE

--------------

SHOWCASE

--------------

FEATURE

--------------

SHOWCASE

--------------

FEATURE

--------------

Airplane cookie cutters (Inkscape), user: emmett on Thingiverse: http://www.thingiverse.com/thing:11098



DNA Playset (OpenSCAD), Emmett Lalish: http://www.thingiverse.com/thing:17343



I heart lightning (Inkscape & OpenSCAD), Amy Hurst: http://www.thingiverse.com/thing:17495



Lake and mountain topography (Blender), Luke Chilson: http://www.thingiverse.com/thing:7207

--------------

