Desktop

Pierros Papadeas

--------------

Index

--------------

Masthead



Editorial Team

Ana Carvalho   ana@manufacturaindependente.org

ginger coons   ginger@adaptstudio.ca

Ricardo Lafuente   ricardo@manufacturaindependente.org

Publisher

ginger coons

Administrative Partner

Studio XX     http://studioxx.org

Community Board

Dave Crossland

Louis Desjardins

Aymeric Mansoux

Alexandre Prokoudine

Femke Snelting

Contributors

Dave Crossland, Seth Kenlon, Pierre Marchand, Allison Moore, Pierros Papadeas, Nuno Pinheiro,  Antonio Roberts, Eric Schrijver.



Printed in Montréal by Mardigrafe on recycled paper. http://mardigrafe.com



Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 



Contacts

Write us at enquiries@libregraphicsmag.com

http://libregraphicsmag.com

--------------

Copyleft has a scary reputation among business people because they often do not understand it.

Copyright is easy — it's about what restrictions and freedoms you have to use and redistribute a work. Copyleft is a “pay it forward” feature of copyright licenses that says if you redistribute the work, you must pass it along on the same terms. You are free to take a libre work and improve it. You can take it as a part and combine it with your own parts to make a new, and hopefully better, thing. What makes copyleft powerful — and scary — is that if you choose to do this, the whole thing must be libre. You can stand on the shoulders of others but others can also stand on yours — or you can start from scratch and set your own terms. 

Copyleft has been smeared as “viral” and “a cancer” because creators of proprietary software much prefer libre licenses without this bargain. Those licenses allow people to have their cake and eat it by exercising their freedom while denying others that freedom. Including libre parts in a proprietary whole defeats the original point of setting the work free, and copyleft is a good defense against this abuse. Copyleft is central to the most popular libre licenses for programs and creative works, in the gnu gpl and the Creative Commons Attribution-ShareAlike licenses respectively. Copyleft powers the explosive, exponential growth of share-and-share-alike culture. And, as always, fonts are special. 

PostScript powered the early days of desktop publishing and it required the redistribution of complete fonts with documents. PostScript (ps) document files linked to font files. That was intensely annoying for proprietary font vendors because fonts were endlessly copied all without license fees being paid. Font Digital Rignts Management (drm) schemes were cooked up over the years and found to be more trouble than they were worth. Being unable to print documents correctly is perhaps only slightly less annoying for designers than having an application crash and taking the work with it. 

Pdf solved this by combining fonts with documents or, ideally, the minimum parts of fonts needed for that particular document to print reliably. (Scribus has had faultless pdf export as a top priority since the beginning.) But this makes the story for copyleft fonts complicated. A copyleft font may overreach into the documents that use it, unless an exception is made to the normal terms — an additional permission to allow people to combine parts of a font with a document without affecting the license of texts, photographs, illustrations and designs. Most libre fonts today have such a copyleft license — the sil ofl or gnu gpl with the Font Exception described in the gpl faq. 

Web fonts return the world to linking documents to fonts. This is extremely unfortunate for the proprietary business world because people can see a font, like it, and figure out how to download and save it without paying for a proprietary license. It is, however, extremely fortunate for those doing business with copyleft works, because copyleft distribution is a wealth creation engine for those who know how to drive it. More distribution means more money. 

The business of libre fonts is open for designers who can take a libre font and combine it with their own parts to make a custom typeface design for their clients — customers who could not afford to commission totally new typefaces, but who still desire fresh typographic identities. Since all businesses will want to use their fonts on their websites, participation in free culture is guaranteed by copyleft. If you see a great typeface on a web page and it has a libre license, you can download and save it and improve it further. 

The Web Font Downloader Firefox Add-On delivers this dream, making it easy to download libre web fonts. The next step, improving the font further, highlights the issue of font sources. OpenType has two flavours, one with PostScript-style cubic outlines and the other with TrueType-style quadratic outlines. The PostScript flavor is superior as a font format and looks great on computers using FreeType and on Mac os x, but lacks the pixel-level control of TrueType needed to look good on most Microsoft Windows computers. This means almost all web fonts are distributed in a format that is a long way from “the preferred form of the work for making modifications to it.” That is the definition of source code in the gnu gpl, and it works very well for programs. I hope one day it will be a tradition for fonts too. 

Get the Web Font Downloader Firefox Add-On now from www.webfontdownload.org

--------------

When John Whitney made his pioneering computer art films as an artist in residence for ibm in 1960, the computer screen he used did not use pixels. Rather, it was a single beam which could be instructed to move across the screen, much in the same way that postscript instructions tell a vector to move.1

The graphics in Atari’s arcade games, like Battlezone, were also drawn with vector lines on an oscilloscope.2 In the long run, a matrix of points became the preferred method to describe screen output. And it still is today. In fact, we have a more rigid matrix now that we use lcd displays: they have a “native” resolution determined by the number of pixel elements — whereas the phosphor dots in a color crt display bear no relation to pixels or subpixels displayed on them.3

So, to get your digital resources out on a computer screen, you have to describe them as a matrix of points. That’s easiest when you work with data that itself is a matrix of points. It’s even easier when you map the matrix of points directly in the data to the matrix of the points on the screen.

The easiest solution is not the best, in this case. Try to browse the internet on a 24 inch screen and, by default, it will look rather awkward: singular columns of 960 pixels, with huge swaths of background image on either side. That is because the layouts are specified in css pixels and, by default, the browser makes them correspond with “device pixels”.4 Where this used to be a predicament, now it’s just a convention. All modern browsers support zooming in on the content. They're able to make pixel-based layouts smaller or bigger. 

On the mobile phone, the rapport between the pixel of the design and the pixel of the screen has been cut entirely. The webpage is initially displayed fully, and subsequently the user can zoom, continuously, in and out on the design. 

Scalable user interfaces benefit from vector graphics. After all, vector graphics are supposed to shine in the world of scalable.5 There's even a vector format that was named after this inherent property: Scalable Vector Graphics. But does that mean we can’t use the model of the bitmap in our new designs and interfaces? Not necessarily.

When in doubt, look at your predecessors. Most of our historic design for the computer screen is bitmap-based. I like to look at early pixel-based guis for inspiration. There’s a library of icons and user interface elements for the X window system, collected by Anthony Thyssen, available online.6 Because of the limitations inherent in early systems, many of them are really simple, black and white, 16x16 bitmaps. Through tight constraints, they attain a very evocative kind of abstraction. In this they resemble Susan Kare’s icon designs for the original Macintosh, which are much better executed than current iterations.

These designs don’t deserve to stay locked to the grid of display pixels growing ever tinier. They also don’t have to: you could paint these designs with square meter pixels on your wall, with even that rendering making them look great.

But what better way to reinterpret these designs than to convert them to vectors?

Traditional tracing algorithms do no justice to these designs. Looking for the curves underlying the designs ignores that the pixel grid is constitutive of the design. We are not looking for the platonic ideal. In this case, there's nothing to do but make vector pixels: a vector square for every pixel! It doesn’t even have to be a square. After all, a bitmap is a collection of points, and points have no predefined shapes. It could be circles or any arbitrary shape. You could make the pixels come together in horizontal scanlines, or vertical ones. You could distort the grid on which they are based.

There are many possibilities in the future of rendering and the further we go in exploring them, the closer we come to keeping alive the heritage of our pixels.



1. Thanks to Joost Rekveld for his classes, introducing these works amongst others

2. Form and Code, In Design Art and Architecture: Casey Reas, Chandler McWilliams, LUST; Princeton Architectural Press 2010

3. http://en.wikipedia.org/wiki/Pixel

4. http://webkit.org/blog/55/high-dpi-web-sites

5. Actually, there are quite some pixel based scaling algorithms too: http://en.wikipedia.org/wiki/Pixel_art_scaling_algorithms

6. My reissue available at https://github.com/codingisacopingstrategy/AIcons



Eric Schrijver (Amsterdam, 1984) is a visual artist who makes installations and performances. Eric teaches Design for new media at the Royal Academy of Art in The Hague. He is inspired by open source and programming culture. http://ericschrijver.nl

--------------

Coding pictures

Ricardo Lafuente

--------------

Setting a book with Scribus

Pierre Marchand

--------------

Best of SVG



Wayfinding and warnings from Wikimedia Commons

--------------

Getting used to misuse

ginger coons

--------------

Copyleft Business

Dave Crossland

--------------

The heritage of our pixels

Eric Schrijver

--------------

Masthead

Editor's Letter

Production Colophon

New Releases

Upcoming Events

Copyleft Business Dave Crossland

The heritage of our pixels Eric Schrijver

Coding Pictures Ricardo Lafuente

Setting a book with Scribus Pierre Marchand

Best of svg

Desktop Pierros Papadeas

Interview with Oxygen's Nuno Pinheiro

Showcase

    Allison Moore Papercut

    Antonio Roberts What Revolution?

Making your workflow work for you Seth Kenlon

On being a Unicorn: the case for user-involvement in Free/Libre Open Source Software Libre Graphics Meeting Special

Talking about our tools Libre Graphics Meeting Special

AdaptableGimp: user interfaces for users ginger coons

Resource List

Glossary 1.2

--------------

4

6

7

9

10

12

14

18

22

24

26

28

35

36

38

40

43    

44

46

51

55



--------------

Versions under control

Ana Carvalho & Ricardo Lafuente

--------------

EDITOR'S LETTER

--------------

PRODUCTION COLOPHON

--------------

MASTHEAD

--------------

NEW RELEASES

--------------

TYPE DESIGN

--------------

TYPE DESIGN

--------------

SCHRIJVER

--------------

SCHRIJVER

--------------

DISPATCHES

--------------

Images under other licenses

Signs in “Best of SVG” are all in the Public Domain. They can be found in:

http://commons.wikimedia.org/wiki/File:UK_motorway_matrix_fog_nuvola.svg

http://commons.wikimedia.org/wiki/File:Ohituskielto_p%C3%A4%C3%A4ttyy_352.svg

http://commons.wikimedia.org/wiki/File:Norwegian-road-sign-406.0.svg

http://commons.wikimedia.org/wiki/File:Raitiovaunun_pys%C3%A4kki_533.svg

http://commons.wikimedia.org/wiki/File:K%C3%A4velykatu_575.svg

http://commons.wikimedia.org/wiki/File:Zeichen_238.svg

http://commons.wikimedia.org/wiki/File:Laser-symbol.svg

http://commons.wikimedia.org/wiki/File:PN_Pcha%C4%87,_aby_otworzy%C4%87.svg

http://commons.wikimedia.org/wiki/File:GHS-pictogram-explos.svg

Blender screenshot in “Resources” by Wikipedia user DingTo. GNU General Public License. http://en.wikipedia.org/wiki/File:Blender3D_2.4.5-screen.jpg



General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

--------------

FIRST TIME

--------------

FIRST TIME

--------------

Use cases, at their core, are about the way users proceed through a system in order to achieve an outcome. Normally, there are lots of diagrams and small details involved in creating a use case. But we're not here to go over technical detail. Instead, we're here to talk about that core, the idea of looking at paths of use and interaction.

Then there are affordances, the features of a thing, its possibilities, the ways in which it might come to be used.

Clearly, then, we're talking about the way things are used and, more specifically, the way things are designed to be used.

As designers, artists, makers, builders, we make things that are of use, in one way or another. At the same time, we make use of the productions of others. We do both of those things on an almost constant basis, in our lives, our vocations, our work.

A graphic designer may design a poster which serves the use of informing viewers about that which it promotes. That same designer uses a set of tools, however diverse, to fashion the poster. Thus, the builder is built for. Both the poster and the tools of the designer have affordances and potential use cases. What, after all, is the proper use of a poster? Is it to be read? Is it to be attractive? Is it to be taken off the wall and folded into a paper airplane? To be stolen, only to be hung on another, more private wall?

Our software tools, in their affordances and potential use cases, define for us, to a certain extent, what we may and may not do. Those decisions are put in place by the people who design the tools. Together, as users, developers and all areas between the two extremes, we boil in a constantly reconfiguring sea of use possibilities, material and mental affordances.

Which is why, in issue 1.2 of Libre Graphics magazine, we're looking at the interconnecting topics of use cases and affordances. We can look at it from a technical perspective but, perhaps more productively, we can also look at it philosophically. It's about the idea of the affordances of the work, who it's for, what it can do.

That applies both to the work designers do for others and also to the work of others, as it is employed by designers.

Use, misuse and happy accidents are all areas we're keen to discuss and explore in this issue. We look, this time around, at glitch art, smart workflows, the history of the pixel and its adoption, user interfaces designed to work for instead of against you and any number of other exciting topics. 

We hope you'll stick with us as we wander through the diverse meanings of what it is to use and be used.



ginger coons is a member of the Libre Graphics Magazine editorial team.

--------------

New releases

--------------

Reclaim your tools

by Jakub Szypulka

http://vimeo.com/18568225

--------------

Documents the slow beauty and diversity of activity to be found at even the most hectic meeting of software contributors. In this case, documenting Libre Graphics Meeting 2010. Made using Kdenlive and Audacity. 

--------------

ArtistX 1.0

http://www.artistx.org/site3



--------------

A version of gnu/Linux which bills itself as able to turn a ‘common computer into a full multimedia production studio.’ Based on Ubuntu and designed for multimedia artists.

--------------

CrunchBang is version of gnu/Linux notable for its community of users who actively share screenshots of their modifications to the desktop. They share not only screenshots of their modifications, but also instructions for replicating their results.

--------------

CrunchBang 10 Statler

http://crunchbanglinux.org

--------------

A new version of gimp, which allows users to make easy customizations. Read more about it on pages 46-50.

--------------

AdaptableGIMP

http://adaptablegimp.org

--------------

Upcoming events



--------------

UPCOMING EVENTS

--------------

UPCOMING EVENTS

--------------

A Reader's Guide to Libre Graphics Magazine



In this magazine, you may find concepts, words, ideas and things which are new to you. Good. That means your horizons are expanding. The problem with that, of course, is that sometimes, things with steep learning curves are less fun than those without.

That's why we're trying to flatten the learning curve. If, while reading Libre Graphics magazine, you encounter an unfamiliar word, project name, whatever it may be, chances are good there's an explanation. 

At the back of this magazine, you'll find a glossary and resource list. The glossary aims to define words that are unique to the world of Libre Graphics. The resource list provides valuable information about tools, licenses, whatever items we may be mentioning.

Practically, this means that if, for example, you're reading an article about Scribus (see pages 22 to 23), you can always flip to the back of the magazine, look up Scribus in the resource list and become quickly informed about it. This provides some instant gratification, giving you the resources you need to understand, in a moment, just what we're talking about.

We hope you like our system.

--------------



The Web Font Downloader Firefox Add-On delivers the dream, making it easy to download libre web fonts.

--------------

Dave Crossland believes anyone can learn to design great fonts. He is a type designer fascinated by the potential of software freedom for graphic design, and runs workshops on type design around the world.

http://understandingfonts.com

--------------

BEST OF SVG

--------------

Warning signs, street signs, all kinds of signs: they blend into our environments, letting us know what we need to know with minimal fuss. Rather, that's what they do when well designed. When badly designed, they confuse and jar us. 

This time around, Best of svg has collected some of the finest examples of signage Wikimedia Commons has to offer. From warnings of lasers and potential explosions, to incredibly pleasing no passing signs, there's a nice assortment on offer.

We've found that warning and traffic signs are one of the strong points of the Wikimedia Commons collection of svg graphics. Signs and heraldry. But that's a collection for another day. If you don't yet know about Wikimedia Commons, it's well worth checking out. Not only do its graphics feature in the Wikipedia articles we know and love, but it has a pretty nice collection of other media, all under permissive licenses, for your appreciation and re-use. Find it at commons.wikimedia.org. 

—the editors

--------------

We at Libre Graphics magazine have a thing for open standards. We like their transparency and their interoperability. We like that, with a well documented standard, everyone has an equal chance to play nicely together. 

That's why we like SVG so much. It's a well developed, well supported standard brought to us by the World Wide Web Consortium (W3C). It's available for implementation by anyone developing software. It shows up in modern browsers, fine vector graphics editors and any number of other places. 

One thing that's missing, though, is you: the designer, the artist, the illustrator. So put down that .ai file and check out SVG.

--------------

I remember my own first time, the first serious one, was a bookletization of a famous, amongst afficionados, little parody by Pierre Louys under the title of Manuel de civilité pour les petites filles à l'usage des maisons d'éducation. With its typical late 19th century French style, it was natural to associate it to a didone font. I ended up using the Didot shipped with the Mac os i owned at this time. 

Here is the crux of this story: at this point I hadn't yet read the paper by René Ponot1 convincingly establishing that it was not a good idea to use ligatures with the Didot typeface. I wanted to use them! But in this instance of the typeface, the ligatures and old numerals were outside the charmap, intended to be accessed only by means of OpenType substitution or glyph index. That, in itself, was an adventure.

This time, though, was also my first time going deeply into font technologies and Scribus code. Along my journey in these fields I came to read Theotiste Lefevre. His amazing Guide pratique du compositeur d'imprimerie helped me realize how much, even if still non-trivial, the making of a book has become within everyone's reach with desktop publishing and personal printers. For now, forget Louys and Didot and go for a book in a minute!

The recipe is as follows. First be modest and grab some text fallen into the public domain at gutenberg.org. If you attempt to write your own material, you will definitely not be able to do a book in a minute. Next, run a bit of Perl magic powder onto it like perl -n -e 's/(\S)\r\n/\1 /ms; print $_;'  original.txt > withoutlinebreak.txt to let Scribus do its work at line breaking. (See below for more detailed instructions.)

Now you can create a new double-sided Scribus document with a bunch of pages and automatic text frames turned on. Import the text into the first text frame. Set the default paragraph style to something that looks like a book, serif typeface at 10 points, justified, etc. Et voilá! 

Well, it isn't exactly ready to serve to your friends, but you get enough of the taste of an actual book to open the door and start to work. If you think not, click on the eye at the right bottom of the Scribus window to turn on Preview mode.

While writing these few lines, I'm doing the same as I did years ago and am still amazed by what Scribus brought to us — by what it allows us to do and the opportunity it gives to learn about desktop publishing. We have the opportunity to do publishing work as Scribus exposes its internal representation of graphic objects through an opened source code and file format.



1. Le Didot a-t-il besoin de ligatures ?

Cahiers Gutenberg no. 22 (1995), p. 17-41

http://cahiers.gutenberg.eu.org/cg-bin/article/CG_1995___22_17_0.pdf

--------------

No matter what operating system you're using, you've got a command-line interface at your disposal. If you're of a certain age, you may remember fiddling around a little with ms-dos. Even if you never did, don't worry about it. The command line is friendlier than you may think.

Now, because we're all designers here, chances are good you're using a Mac. Or, if you're like us, Linux. The tips and commands listed below work just fine for both. If you try them in ms-dos (under Windows), your computer may explode. We're not quite sure, really.

Open a terminal: On a Mac: Crack open your Applications folder and go to Utilities. There's a program there called Terminal. Open it. 

On Linux: Normally under your Accessories or System menu, you'll find something called Terminal. Open it.

Get to the right place: If you've downloaded a book from Project Gutenberg, hopefully as a plain text file (something ending with .txt), great. If not, go back and do that. But make sure to take note of where you've saved it. 

When you opened Terminal, it should have started you up in your home directory. To make it easier to find where you're going, open up your file browser (Finder on a Mac, or on most kind of Linux, just double click on the icon for your home directory). Navigate to where you put your file. Now, take a look at the path leading up to that. For example, if you left it in your Downloads directory, chances are good that it'll only be one directory past home. 

Once you have an idea of where you've put your file, go back to the Terminal. To change directories (because that's what you're about to do, unless you've left the file in your home directory), you're going to use the cd command. It allows you to (yes!) change directories. Let's say you've left your file in the Downloads directory. In your terminal, you'd type "cd Downloads" (without the quotation marks). That would take you to your Downloads directory. If, in the Downloads directory, you happened to have another directory, this one called books, for example, you'd then go "cd books" (note that it's case sensitive). 

Looking around: Now, we're in our fictional home/Downloads/books directory. Let's take a look at what's there. To get a list of the contents of a directory, just type "ls" while you're in the directory you want to look at. It'll turn up a list of all the files and directories contained within that directory. If you've gotten to the right place, ls should show you the book you've downloaded. 

Running the script: Now you can run the script mentioned in the article. Just copy it and paste it into your terminal. Or, if you're reading this in print, type it. Heck, type it in regardless, just for practice!

perl -n -e 's/(\S)\r\n/\1 /ms; print $_;'  original.txt > withoutlinebreak.txt

Of course, you'll want to change "original.txt" to reflect the actual file name of the book you downloaded. Then, hit Enter. If all goes well, the next time you do an ls, you'll find a new file, called "withoutlinebreak.txt" which will be the book you downloaded, without linebreaks and ready to be conveniently typeset in Scribus.

While this may seem like a lot of complicated steps, once you get used to it, you'll find that it's easy, convenient and fast. And it's just the beginning of what you can do with the command line.

—the editors

--------------



When in doubt, look at your predecessors. Most of our historic design for the computer screen is bitmap-based.

--------------

A cityBeatiful abstractions in Anthony’s icons

--------------

Above: A calendar.

Left: A tornado (from Nethack).

--------------

Executing commands in the command line

--------------

#!/usr/bin/env python

""" Generates vectorpixels based on 2-bitmaps (2 color pictures).



    TODO: use element tree for XML; implement Floyd-Steinberg

    dithering for color and greyscale images; implement vertical

    and horiontal scanlines """



import Image



SOURCEIMAGE = 'city.tiff'



class vectorpixel:

    def __init__(self, image):

        self.i = Image.open(image)

        self.px = self.i.load()

        self.constructed = False



    def construct(self, grid=24, line=1, rounded=4, test=(lambda x: x == 0)):

        self.grid = grid

        self.line = line

        self.rounded = rounded

        self.width = self.height = self.grid - 2 * self.line

        self.test = test

        self.fill = '#000000'

        self.constructed = True



    def _yieldlocations(self):

        for x in range(self.i.size[0]):

            for y in range(self.i.size[1]):

                if self.test(self.px[x,y]):

                    yield (x,y)



    def _mkelements(self):

        for l in self._yieldlocations():

            yield "<rect x='%s' y='%s' width='%s' height='%s' rx='%s' fill='%s'/>" % (

    self.grid * l[0] + self.line, self.grid * l[1] + self.line, self.width, self.height, self.rounded, self.fill)



    def _format(self):

        output = '<svg xmlns="http://www.w3.org/2000/svg" width="%s" height="%s">\n' % (self.i.size[0] * self.grid, self.i.size[1] * self.grid)

        for e in self._mkelements():

            output += e

            output += '\n'

        output += '</svg>'

        return output



    def generate(self):

        if not self.constructed:

            self.construct()

        return self._format()

    

if __name__ == "__main__":

    v = vectorpixel(SOURCEIMAGE)

    print v.generate()



--------------

At the Fine Arts Faculty of Porto University, we built up an introdutory class focusing on procedural strategies inside a graphic design context. In less stuffy terms, the purpose was to introduce design students to code. However, this required some thought on what subjects to teach (and which to leave out), which pitfalls to avoid, and which approach would work best to introduce an alien, cold and logical subject such as programming to creative people.

Designers are inevitably involved with computers, which are present in most stages of a graphic designer's workflow, from initial sketches to printing finished pieces. Yet there's a dearth of formal education on the technical and social workings of computers and digital media in general.

Nevertheless, attention has been paid to this field during the last decade, which saw the birth and growth of creative-oriented applications, spearheaded by its most popular example, Processing. Among other creative coding tools, we find Pure Data, Context Free, Drawbot, Nodebox, Shoebot, Supercollider and Fluxus. The overwhelming majority of these tools are f/loss.

Learning to code is becoming more and more of an obvious choice for designers. The rising popularity of the web has created a huge demand for skilled coders, but designers are also a key part of any serious venture. A designer who can implement his own ideas, instead of just handing over mockups to a web developer, ends up with a big advantage. Becoming acquainted with digital logic, the workings of computers and their bells and whistles is also a way to liberate oneself from being a software operator, and be able to think for and with the machine.



Tools and strategies

In our semester-long class, we focused solely on still, static output, meaning that animation and interactivity were left out. This gave room to explore the basic commands and features, as well as combining them with creative strategies that the digital medium enables, such as repetition and randomness.

Processing was considered as the application for this class, but Nodebox/Shoebot were picked because they work natively with vector graphics, which was a crucial factor when considering that the created designs should be meant for print. The fact that they're based on Python, whereas Processing is based on Java, also played a part. Python is one of the most appropriate languages for introducing people to programming, due to its clear, readable syntax, which almost resembles the pseudocode used to explain abstract programming concepts. It also hides away (or puts sugar on) much of the complexity behind programming concepts, allowing us to focus on properly wording our orders to the computer.

Nodebox and Shoebot provide a sketchpad for writing small Python scripts. When running them, the program will create graphical output—an image. This instant visual feedback is a big plus for teaching code to creative-minded people, since it allows for swift tinkering and borrowing from the provided example scripts, and was crucial in easing design students into the coder way of thinking.



Practical example: character generator

One major assignment in this class was to design an identicon generator. Identicons are icons commonly used in blogs, especially inside comment sections, which identify the commenter through a graphical representation of their ip address. This is done by combining different possible parts into one final image. Monsterid and Wavatar provide icons in the shape of quirky monsters, whereas Identicon generates abstract, geometric shapes. The goal of this assignment was to think up the design for an identicon, freely choosing the subject, and create a program that could generate different outputs randomly.

The size constraints of a blog icon are a big limitation, one that wasn't forced on the students in order for them to focus on the more relevant creative questions. Many of the students went through the character-design route, though others attempted more daring approaches, such as cake and bug identicons.

The challenge of this assignment was not the coding itself. Most students were already rather comfortable with using randomness, drawing with code and importing external images. The focus was on creating consistent designs which could work with different compositions and still end up as a complete final result, not giving away the fact that it was generated by a program. 

The illustrations running alongside this article are some of the results of this assignment.



Image Credits: 

Page 18: Fábio Santos; Edgar Sprecher; Joana Estrela.

Page 21: Sofia Rocha e Silva; Telmo Parreira; Lídia Malho.



Nodebox: http://nodebox.net

Shoebot: http://shoebot.net

Pure Data: http://puredata.info

Drawbot: http://drawbot.com

Context Free: http://contextfreeart.org

Fluxus: http://pawfal.org/fluxus



--------------

Icograda design week

Vilnius

--------------

9-13

May

--------------

10-13

May

--------------

Libre Graphics Meeting

Montreal

--------------

19-21

May

--------------

Typo Berlin

Berlin

--------------

19-22

May

--------------

Live Performers Meeting

Rome

--------------

1-3

APRIL

--------------

Flourish 2011

Chicago

--------------

30 APRIL

1 May

--------------

Linux Fest Northwest

Bellingham, Washington

--------------

Pica 2011

Banff, Alberta

--------------

1-3

May

--------------

agIdeas 2011: International design research lab

Melbourne

--------------

2

May

--------------

7-21

May

--------------

CHI 2011

Vancouver

--------------

We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and F/LOSS. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the F/LOSS designer at a traditional design event, or maybe the designer at a predominantly software developer event. 



--------------

What's new with you? We're always eager to find out what designers, artists and others using and working with F/LOSS are up to. Tell us what you've done lately at enquiries@libregraphicsmag.com



--------------

DESKTOP

--------------

DESKTOP

--------------

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWWWW#W#W#WBWWWWQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQW#ZXdZZZZUZZZZXZXZZZZ####BBWWWQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQW#XXXon2SXXXXXXSXXXXXZZZXXXZXXXXXXXXZXUUUWQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWZS22nnn2wXndSnnX2onnnnnonoXXXXXXXXXXXXXSqQWWQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ#ZSn1n1oo2ono2nnd2onvnnoSlnoXo21oX21vnXXXmQWWQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQBXS21vnnnoond1nnoXvvnvvn1ovuXn2nnd13uXXXXmQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWX2n1v1vvvunn1v1noSvvnvv2v1nXno1vdnIuXXXe{|ilvYS??$QQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQWWWWWWWWWWQQQQQQQQQ@o2S1v%ivvzvn11vnuvvsvva1IlX2uXuvXlnXXX2^;=||++-__ajQQQQQQQQ

QQQQQQQQQQQQQQQQQQmQ#mmmZX###ZZZZZZXX#H&oo2ll%ilIzivlIl1zvl%viuszvd1uei{2IdSXXr::=>+<vXWQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQWQW8T!"-     "??Y##ZXXXqqXXXonuuassil%%Ii%i%vi%islueli2oXXXS}.=+     .-:=<vmQQQQQQQ

QQQQQQQQQQQQQQW8VY*=-:.              "??X#U#Z#mXwXoSopoouu%s{i%%i}IenuoXXXXSr=/`. - .:)?TTVHQWQQQQQQ

QQQQQQQQQQQQF-^^+_=::-.                   "!!ZXZ#U#Y?!!?!"~    --+vno22S2SSe=/.:,+^` ----  --WQQQQQQ

QQQQQQQQQQQF      .--:::..    .                                    +no22S2S|>_%iiiiiii|||||=+]WQQQQQ

QQQQQQQQQQP  ... ... .  .-- ...                          ..._;====_.=vo2S2c%=vvv>~:....::..::;3QQQQQ

QQQQQQQQQP ... . .. .. .    . .-.:....              ..:::==+|+|iiiv%;+n222vinn>==|%i|||||||+|==$QQQQ

QQQQQQQQ@`... . ...... .      .  . --.::::......:::::::===+|+|||iiiIs|)n2e%vn+|Ivi|+|==__;====;-$@WQ

QQQQQQQ@`....... . . . . . . .       .. -:;;;;;=::::===++||+||||ivllII>{o1voi%Ili||||+==-=+=:- .  ]Q

QQQQQQ@`.:::.....:..... . . .  . . ..  .. . ---::::;==+|+||||||lIvvvvvI%n1o}%Ii|+|+||||+||--     _mQ

QQQQQ@' ....::::-...... .     .      .. . . . ::::;===|=||+=i|i|iivvnlll{nniv>==---~---      _awWQQQ

QQQQF` .... ....::::... ......    . .  . .   .::::==+|++|+|i||+=|IIvvlilI|iIi=          _saymWWWQQQQ

QP" -. ... .................. ...  .  .      ;:::======>+<||=|=+||iilIv+-   -       _awmWmWBWQWQWQQQ

QL     ...:.:..... .....:.....    .. . . . .:=;:;;==||+|+=|=+- . --|i>:        _sammQWmWWWWWQWQQQQQQ

QQma,   .-::.--:... . ....--...... . .. . .;=;:;:+=|=++==|-~       ::      _awmWWBWWWWWQWQQQQQQQQQQQ

QQQQQma.    --.: ... .. . ...... ..... . .;;=;===+=====:-              <ayWQQWQQWQQQQQQQQQQQQQQQQQQQ

QQQQQQQQma,     -..........       . . ...+===;:;=:;;;-  .         _awmQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQa,      -.... .. .         .===;::;=::  .        ._wwQQQWWQQQQQQQQQQQQQQQQQQQQQQQWQQQQQQ

QQQQQQQQQQQWQQQwa      -.-.           .;:::: - :         _aamWQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQWWWga.       .         --            ._wwmQWWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWQQQQQQQQ

QQQQQQQQQQQQQQQQQQQWQma,        .               _awmWQWWWQWQQQQQQQQQQQQQQQQQQQQQQWQQQWQQQWQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQa,                 sayWWWBWQWQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQWWQQwc         _aamQWBWWQWQQQQQQQQQQQQQQQQQQQWQQQQQQQQQQWQQQQQQQQQQQQQQQQQWQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQWWQga,_aayQQWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWQQQQQQQQQQQQQQQQQQQQQQQQ

QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ



--------------

When working on a project, it helps to have a proper workflow set up, one which can help us do away with boring and repetitive tasks through as much automation as possible. One of the crucial parts in such a workflow is version control.

The most popular proprietary design software tools haven't yet incorporated the latest improvements in version control — in many cases it's totally absent from software suites and is usually provided by third-party commercial plug-ins. The consequence of this is that regular users are forced to adopt very crude ways of managing the versions of their work, usually with awkward numbering and notes on the filenames themselves. Things like “illustration7-final_version3-print-final-srsly_this_is_it-final2.jpg” should be familiar to more than a few designers.

On the other hand, the Free/Libre and Open Source software (f/loss) world is very much in touch with version control and other project management strategies. These strategies quite often come from the domain of software development. Thus, the thought of using a version control system (vcs) for the production of this magazine came up early. There is a wide array of choice for this purpose. The most popular options are Subversion, Git, Darcs, Bazaar and Mercurial. It should be said that there's some heavy argument about which vcs is the best and this discussion is nearing the status of a holy war. We decided not to waste too much time weighing choices and instead run with one and see how it fared — and Git was what we stuck with.

Cliché as it might sound, version control is one of the things that once you pick up, you can't figure out how you ever managed to do without. Not only do we get a full log of every change that has been made, using version control makes collaborative work much more straightforward, giving us the ability to always know what the status of our project is and, if necessary, revert to older versions of any file without hassle.

Nevertheless, version control systems require some learning and hand-holding to get comfortable with. Among all the technical jargon — learning the meanings and effects of committing, reverting, staging, branching, merging, rebasing, pruning, annotating — we are slowly becoming familiar with this way of working, and are definitely seeing the advantages.



PropCourier 1.2

The ever-evolving typeface for this magazine, PropCourier Sans, benefitted from some tweaks for issue 1.2. Most of the work dealt with punctuation, softening the weight of the most used punctuation glyphs. We also began working on kerning, the headache of choice for type designers. In order to preserve our neurons, we decided to kern as we go: after typesetting 1.2, we looked at printed proofs for the most glaring kerning problems (as in “f/loss”) and fixed them. Each issue, we'll be adding more kerning pairs as we find the need for them.



--------------

http://liveperformersmeeting.net



--------------

http://typoberlin.de/2011

--------------

http://icograda.org/events/events/calendar738.htm

--------------

http://libregraphicsmeeting.org/2011

--------------

http://flourishconf.com/2011

--------------

http://linuxfestnorthwest.org

--------------

http://picaconference.ca

--------------

http://agideas.net/agideas-2011/design-research-lab

--------------

http://chi2011.org

--------------

SCHRIJVER

--------------

Want to make your own vector pixels? Follow these (relatively easy) steps to generate your own vector pixel icons. 

The following instructions should work just fine on either Linux or Mac. 

Grab the code: Either type it in by hand, copying the code [on the right] or go to the assets sections of our website (http://libregraphicsmag.com/assets) and download the vector pixel pack we've prepared for you. 

If you're copying out the code manually, enter it into a text editor and call the file vectorpixel.py. 

Find an image: If you're doing it on your own (instead of using the assets we've provided), find a simple image. Make sure it has very few colours (you're going to have to strip all the colour out of it). Simple logos, warning signs and similar types of images work well. Open it up in your favourite raster image editor (we used gimp). 

Strip out the colour by doing things like increasing the contrast as much as possible and posterizing. You're aiming to have an image with only black and white. While you're at it, resize the image to a very small size. 50px by 50px works well.

Warning! We're serious about the small image size. If it's too big, the resulting svg will be very, very big and may just crash your image viewer. 

Save your image (as something like a png, jpg or other basic raster format). Make sure to flatten while you're at it. Layers will only cause trouble in this case. Make sure you save it in the same directory as your vectorpixel.py file. 

Point the script: Take another look at vectorpixel.py. On the 8th line, you'll find something that looks like this: SOURCEIMAGE = 'city.png'. If you've made an image of your own, you'll want to change city.png to whatever the name of your file is. Then save vectorpixel.py again. Now, when you run it, it'll be looking for the right image.

Convert it: Open up your terminal (for more on using the terminal, check out the detailed instructions and explanation on pages 22-23). Navigate to the directory containing vectorpixel.py and your image. 

At the prompt, type: python vectorpixel.py > city.svg

If you've provided your own image, you can change that last bit. For example, if your source file is called attention.png, you can sub in attention.svg. All this does is set up a destination file. 

Hit enter. It'll look a little like nothing has happened. However, if you go and take a look in your folder, you'll find a new file, called city.svg (or whatever you've named it). Take a look at it. It should be made up of lots of little vector pixels. 

You've just made a vector pixel icon! 



--------------

Images under a CC Attribution Share-Alike license

Cover, inside cover, index and showcase separator page illustrations by Manufactura Independente based on images by Flickr user fdctsevilla.

Photo of ginger coons by herself.

Photo of Ricardo Lafuente by Ana Carvalho.

Photo of Ana Carvalho by Luís Camanho.

Photo of Dave Crossland by Mary Crossland.

Photo of Eric Schrijver by himself.

Illustrations in “Coding pictures” by Joana Estrela, Lídia Malho, Telmo Parreira, Sofia Rocha e Silva, Fábio Santos, Edgar Sprecher.

Illustration in “Setting a book with Scribus” by Manufactura Independente based on “Book 8” by Flickr user brenda-starr.

Signs in “Best of SVG” by Wikimedia Commons.

Screenshots in “Desktop” by Pierros Papadeas.

Photos in “Interview with Oxygen's Nuno Pinheiro” by Manufactura Independente.

Icons and wallpaper in “Interview with Oxygen's Nuno Pinheiro” by the Oxygen project.

Photos in “AdaptableGimp: user interfaces for users” by ginger coons.

Screenshots in “AdaptableGimp: user interfaces for users” by Ben Lafreniere.

All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.

GIMP, Inkscape, Kdenlive, MyPaint, Scribus and Web Font Downloader screenshots in “Resources” by Manufactura Independente.

--------------

DISPATCHES

--------------

SCHRIJVER

--------------

Desktop features clever hacks, workarounds and customizations used by designers and artists. We look at the way the best and brightest of our peers work. This issue, Desktop features customizations used by Pierros Papadeas, a Fedora Ambassador and Maintainer of the Fedora Design Suite Spin.





Transparent Windows

A common drawing problem is layering info from different apps. gnome with Desktop Effects gives you the ability to make a window transparent. It means being able to trace without having to go back and forth, saving, exporting and importing. This effect can also be achieved with other desktop environments.



Expose Windows

It's gnome and Desktop Effects again. Just point your mouse at the upper right corner and you get an “Expose” of all windows. It allows you to easily change between windows and get a quick view of the huge amount of awesomeness you're working on.



In this instalment of Desktop, Pierros is using Fedora Linux, GNOME desktop environment with Desktop Effects, Inkscape and Alchemy.

--------------

