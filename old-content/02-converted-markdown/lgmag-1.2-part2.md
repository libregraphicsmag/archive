Let's talk about tools for a moment. We, as humans, distinguish ourselves from other animals by talking about our ability to make and use tools. We make tools, we use tools, we are tools, all at different times and in different amounts. 

Tools can be physical things used to manipulate equally physical things. At the same time, they can be digital things, used to shift bits. We can love them or hate them. The one thing we can't manage is to escape them. 

As we define what they do, so too do they define what we do. In the shape of a paint brush, the kink of a bezier curve, the change a gaussian filter exerts over an image, they make our work what it is. We are our tools and our tools are us. So let's talk about tools, in the best way we know how, graphically.

Libre Graphics Meeting, Libre Graphics magazine and Mardigrafe are co-sponsoring a juried exhibition of f/loss graphics work on the subject of tools. Break out your own f/loss graphics tools and design a poster (24”x34”) detailing your perception or ideas about tools. 

All submissions will be included in an online gallery, presented in conjunction with Libre Graphics meeting. In addition, a jury of designers, thinkers and doers will meet in May. They'll pick 15 posters to be printed by Mardigrafe and displayed during Libre Graphics Meeting in Montreal. The editors of Libre Graphics magazine will pick a further eight to be featured in the showcase section of an upcoming issue. 

So get thinking about your tools, what they mean to you and what you mean to them. Then, get designing.

More details and how to submit at http://libregraphicsmag.com/tools



--------------

Talking about our tools

Call for submissions

--------------

Glossary 1.2

--------------

Alchemy:A f/loss canvas drawing program, meant to encourage play and exploration. Available for gnu/Linux, Mac os x and Windows.



Audacity: A f/loss sound editing application for gnu/Linux, Mac os x and Microsoft Windows.



Blender: A powerful 3d animation application for gnu/Linux, Mac os x and Microsoft Windows.



command line: A text-based interface for controlling a computer. 



desktop environment: A collection of tools and interface elements which style the visual and functional aspects of an operating system in a certain way. 



Digital Rights Management (DRM): Technologies (of whatever sort) which prevent users from making certain uses of the larger technologies to which the dmr is applied.

distro/distribution: A specific configuration of gnu/Linux, often designed with a particular purpose in mind.



Fedora: A popular distribution of gnu/Linux, produced by Red Hat, Inc.



flavour: Similar in meaning to distro/distribution, but more general. Simply means a specific version (normally of gnu/Linux).



Free: As in freedom, or often, that which is or is of Free Software.



Free Culture: A general term for activities and artistic works which fall under a similar ideological banner to the Free Software movement.



freedesktop.org: A f/loss project which focuses on creating interoperable tools for gnu/Linux and other Unix-type systems. 



Free/Libre Open Source Software (F/LOSS): Software which has a viewable, modifiable source and a permissive license (such as the gnu gpl). It can be modified and redistributed.



GIMP: A raster based image editor for gnu/Linux, Mac os x and Microsoft Windows.



Git: A popular version control system, originally created to manage development of the Linux kernel.



GNOME: A popular desktop environment for gnu/Linux.



GNU General PublicLicense (GPL): A license originally intended for use with software, but now used for other applications. Made famous the principle of Copyleft, requiring those using gpl licensed work to license derivatives similarly.



implement: The act of integrating a feature or standard into a piece of software, rendering that software able to (for example) perform a task or use a specific file format.



Internet Relay Chat (IRC): A popular form of internet-based real-time chat. Has a long history of use and is still popular among groups of developers and users. 



KDE: A community project which produces various f/loss applications, best known as a popular desktop environment for gnu/Linux.



Libre: A less ambiguous adaptation of the word Free. Implies liberty of use, modification and distribution.



mailing list: An email-based forum through which subscribers may receive announcements, view or participate in discussion.



open standards: A standard which is available for viewing and implementation by any party, often at no monetary cost.



Oxygen: A project meant to develop a coherent and attractive visual identity for kde.

proprietary: A piece of software or other work which does not make available its source, which is not allowed or intended to be modified or redistributed without permission.



Scalable VectorGraphics (SVG): An open standard for vector graphics, developed by the W3C.



SIL Open FontLicense (OFL): A license intended for use with fonts and font related software. Dictates terms which allow modification and redistribution of fonts.



source code: The human readable code on which software is based. Software distributed with its source code can be modified far more easily than software distributed without.



terminal: A program which allows users to perform actions on the command line.



Ubuntu: A particularly popular distribution of gnu/Linux, produced by Canonical Ltd.



version control: Activities which have the effect or intent of distinguishing different versions of a work or body of work from one another.

Version ControlSystem (VCS): An application/collection of tools designed to facilitate version control. Tracks changes to files and allows a group of collaborators to share their changes as they are made.



W3C: The organization responsible for setting web standards, such as html5 and svg.

--------------

Everyone works differently, regardless of the task. Every artist has an individual style for getting things done quickly, efficiently, and in such a way that the effort required doesn't ruin the inspiration driving the work in the first place. Whether the motivation is a client or a personal passion, the process that an artist uses to finish the job is generally known by the term "workflow."

Even though everyone tends to be unique in the way they work, much proprietary software enforces a very specific workflow. In fact, deviation from that workflow is discouraged. The nature of the business demands that a proprietary software vendor ensures its product is all an artist needs. In other words, proprietary software, in order to make the greatest sales, seeks to be a monopoly.

Many artists take this for granted because those proprietary software packages are what they learned in school or at work. Some literally do not realize there is any other option. However, on almost any platform there are a host of f/loss tools which can enable artists to take control of how they want to work, and what works best for them.

How do you know if your workflow needs refinement?  There are a few good indications:

—If you find yourself using applications to do things that they (technically) can do but were clearly not designed to do, you might find it far more efficient to seek out the right tool for the right job.

A characteristic of Free/Libre Open Source Software applications is that very few attempt to be everything to everyone. In fact, a basic tenet of f/loss, handed down from Unix, an historically easy operating system for which to create custom applications, is that of modularity. This idea is commonly expressed in the mantra "do one thing and do it well."

This means that f/loss tends to focus on individual tasks that can then be strung together. Does this sound like the great beginning of a formidable personalized workflow? It is.

Proprietary graphics applications lull users into believing they can do everything, but in reality they do one general set of tasks well and offer heavily pared-down tools for everything else. For instance, a bitmap graphic manipulation software might offer some basic page layout and vector drawing features. The theory, presumably, is that if a user only needs a few basic vector illustration or page layout tools, then those tools will be available. In practise, however, artists become so familiar with this monolithic application that they start using it for everything, cobbling and hacking together entire pieces with one wrong tool. While this does get work produced (a result that is always difficult to argue with), it often does so after far too much unnecessary pain, too many workarounds and speed bumps. 

F/LOSS software encourages people to use the tools that are designed for the job. In so doing, the artist is freed to use anything he wants to use. Whatever application an artist finds easiest and most suitable for his art, he is free to use, from the most complex vector drawing program to the most basic paint program. Since f/loss is dedicated to interoperability, there aren't as many format problems; the work done in one application can be imported and modified in another. No separate, fancy, confusing bridge application necessary.

In a way, this means an artist might need to learn more applications. Most people find that while learning f/loss applications, there is enough internal logic to that application that the learning curve is modest. And certainly the fact that the application is designed to do the task being done helps a lot. There's no hacking around the fact that an application doesn't do the normal things it should do.

—If you find yourself doing repetitious tasks by hand, again and again throughout a project, then there may be something designed to take that burden from you.

This idea springs up in many different places within the f/loss world. Since none of the code in f/loss applications is hidden, scripting these applications is quite simple if you have even modest scripting skills. However, some people have no scripting skills and don't want them - and for them, there is the Internet. Simple searches uncover myriad scripts to do repetitious tasks with command line applications.

The Image Magick suite, for example, which itself consists of a number of command line tools is one of those applications that no graphic artist should ever be without -- regardless of preferred os.

Now, it often puzzles people to think of graphic work being done from a command line, but it is amazingly useful and flexible. Graphic artists using propriety software might spend an afternoon opening a graphic in a big bulky graphics application just to convert its colourspace. Artists using Image Magick, on the other hand, can issue a simple line command:



bash$ convert file.tif -colorspace cmyk fileCMYK.tif

and have the job finished in moments. Script that and hundreds of files can be done while you're onto the next task.

—If you find yourself consistently being stopped or drastically slowed by the same set of small "quirky" problems on every project you do, then you may need a specialized tool to avert that issue.

Proprietary software typically has two answers to your problems: don't do it, or spend more money to be able to do it. This might apply to a specific file format you want to use, or an effect you want to achieve, or a way of working.

The f/loss world is set up differently, because there's no agenda to up-sell you on improved versions of the software and no need to limit what you can do. New tools are being developed every day to meet the demands of artists, and these tools are all free to download and use. All Free/Libre Open Source Software, by the very nature of having free source code, is extensible and expandable. As new tools are released, they can be integrated into the applications you use.



DESIGNING F/LOSS WORKFLOWS

Whether or not you have an existing workflow based on proprietary software, working on f/loss for multimedia is most efficient with a little planning. Without stepping back and looking at the whole project, it's quite likely that you'll reach a critical point and realize you're not prepared for the next step - or even aware of what your next step should be.

The first step in designing your workflow is to identify what raw materials you'll need for production. If you're doing a digital painting, you might want to go out and find brushes and establish a custom color palette. If your work is graphic manipulation, then you might want to find useful textures, patterns, brushes, fonts, stock images, and so on. If your work is a magazine then you'll need articles, images, and fonts.

Having this kind of kit before starting will make the project flow more smoothly during the creation phase. Some proprietary software comes pre-packaged with gigabytes and gigabytes of royalty-free stock content which, among other things, takes up quite a bit of room on your hard drive, mostly will never be used by you, and is stylistically quite identifiable as the corporate, royalty-free stock content that it is. f/loss does not ship with this, so you'll have to find your own, but with Creative Commons being the force that it is, this is a trivial matter and one that, in the end, produces a more unique work than the alternative.

A good place to start is the so-called "Great Multimedia Sprint" from http://slackermedia.info/sprints. This is a nearly 2gb collection of Creative Commons licensed content meant to be used as raw materials. More sprints are scheduled for the future, so more content will be available soon.

The next step is to determine what software tasks and compatibility your project requires. If you're working on a magazine, for instance, then you're sure to need both bitmap and vector manipulation programs, a host of fonts and some way to organize and track them, as well as a good layout program. If you're not already familiar with the tools that f/loss has to offer for these tasks, investigate and try some of them to determine which one you prefer and which one will actually do the tasks you want to accomplish.

Since you'll potentially be able to break up tasks into smaller applications, you might also want to consider how multiple computers might be put to work for your project. In the studio where I work, an old g4 running Debian Linux has been re-purposed with the solitary job of converting music files from one format to another while a g5 converts still frames to video. They aren't the fastest computers, they don't have so much as a monitor connected to them, but they can run these dedicated tasks all day and all night, so that the materials are available when needed.

In the end you should be able to trace in a flow-chart how the work will get done. A graphic might first be converted and scaled with one application, manipulated and customized in another, and laid out in the final work in yet another. Exporting should, as often as possible, be done at maximum quality to result in a "gold master," which can then be modified and compressed into easily-distributed versions.  Again, this can easily be done with dedicated line commands that specialize in compressing (Image Magick for graphics including pdfs, pdftk for pdf modification, ffmpeg for video, and so on).





THE WAY FREEDOM WORKS

The bottom line is that the workflow in f/loss is not pre-determined for the artist. While this places the burden of designing a workflow upon the artist, it also frees the artist from a locked-down, inefficent art creation process, and opens a world of possibilities and creativity. And that's something worth working for.

--------------

Making your workflow work for you

Seth Kenlon

--------------

Resource list 1.2

--------------

Blender

A powerful f/loss 3d animation application for gnu/Linux, Mac os x and Microsoft Windows.

--------------

GIMP

A raster based image editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

ImageMagick

A raster image editing, creation and conversion suite for gnu/Linux, Mac os x, Microsoft Windows and iPhone, among others.

--------------

Inkscape

A vector graphics editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Kdenlive

A video editor for gnu/Linux, Mac os x, Microsoft Windows and Freebsd.

--------------

Scribus

A desktop publishing program for gnu/Linux, Mac os x and Microsoft Windows. 

--------------

Web Font Downloader

An extension for Firefox, allowing downloads of embedded web fonts.

--------------

MyPaint

Graphics application focused on natural media simulation. Available for gnu/Linux, Mac os x and Microsoft Windows.

--------------



Do one thing and do it well.

--------------

In 2007, Michael Terry and other members of the University of Waterloo hci lab set out to learn just what gimp users actually do. To achieve that lofty goal, they created something called ingimp, a variation of gimp which tracked feature use. Four years later, they have an answer, in a way. 

The answer, broadly, is what you might expect. It turns out that different users of gimp do different things. Ben Lafreniere, a doctoral candidate in Terry's hci lab, has combed through the data and come up with a more nuanced answer. Usage tends to be focused on small sets of tools, using only a tiny percentage of the actual capabilities of the program. The members of the lab refer to these groups as "corners." 

According to Lafreniere, "not only do people use just a small corner of the functionality in the system, but they tend to use fairly distinct corners." Which means that there's no one-size-fits-all answer. With different users making use of small, distinct sets of tools, no one easy interface tweak will suit everyone and make gimp universally more usable. 

But never fear. There's another, far more exciting option. That option comes in the form of AdaptableGimp. The premise of AdaptableGimp, another project from the hci lab, is that not only should users be able to customise the interface of their software, they should be able to share those customisations with others. Or, as Lafreniere puts it, crowdsourcing "the creation of customisations to the entire user community." 

To do this, AdaptableGimp relies on a modified version of MediaWiki. Task sets—customised collections of gimp commands tailored to a specific use—are stored in a central repository, tied to wiki pages which are capable of both describing and controlling the mix of features in each set. 

The beauty of this, according to Lefreniere, is that “when anybody creates a customisation to the interface, it's immediately there, available to all the users of the application.” This provides all users with a collection of available task sets, just waiting to be used. Says Lafreniere, the intention is that a user “can sit down at the interface, type a few keywords describing what they want, searching things made by the community, select one, and then immediately have it.”

And who will build those task sets? According to Terry, there's already tangible evidence that some users are more than willing to create documentation, tutorials and other resources. “What we're doing,” he says, “is bringing that practice more directly into the interface.” 

This community approach to building and documenting task sets has an added benefit: it makes the efforts of one person useful and valuable to all other users of the software. This means that different types of users can work to their own strengths and preferences, while benefiting from the preferences of others. 

“People are hesitant to stop the current task that they're working on to create a customisation” says Lafreniere. To Filip Krynicki, one of the hci Lab's co-op students, this is one of the major benefits of the AdaptableGimp approach. According to Krynicki, “in most interfaces where someone can make a customisation,that's where it stops.” But in the case of AdaptableGimp, if even one percent of users actually create customisations, all users benefit.

Users creating customisations may see some added incentive, too. Terry suggests that, given AdaptableGimp's ability to collect usage data, task sets could well come along with information about how many users they've been installed by, how active their development is and even how recently they've been used. To Terry, this gives creators of task sets “some sense of feedback of the utility of the task set.”



A new approach to interface design

Members of the hci Lab see current interface design as something hierarchical and designed more to contain functionality than to help users accomplish their tasks. According to Terry, one of the goals of AdaptableGimp is to help users define their own workflows. This approach contrasts strongly with hierarchical interfaces, which he says are “designed in reaction to the large number of commands that are available and not designed around how people actually sit down and want to use the tool for a particular task.” 

This does not mean changing the entire functioning of the program or reinventing the wheel. To Terry, it's a case of “grafting onto the existing interface paradigm,” adding in a “task-centric view of computing where you say ‘This is what I want to do' and the interface modifies itself to accomodate that particular task.”

Krynicki puts it into contrast with existing tactics: “It's like an infinite set of overlapping Microsoft ribbons. They try to do the same thing, they're trying to group functionality. But we're saying that it doesn't need to be the six that are defined by the people making the application, there can be a million. You can't only have the paintbrush in one. The paintbrush can be in 500,000 of them.”

The future of AdaptableGimp looks, at very least, exciting. Lafreniere suggests the possibilities presented by a built-in recommendation system, offering complementary task sets based on use patterns or even suggesting task sets which frame commands the user already knows, but to different ends. As Lafreniere puts it, “you know all these commands, you could do this task.”

Of course, it's not just gimp standing to benefit from this work. Terry hopes to offer a core set of AdaptableGimp components which would help developers of other software in implementing crowdsourced customisation themselves. Says Terry, “we hope that we can provide a tool set for them that they can plug in and start to use in their own application.”

AdaptableGimp is available now, for users who don't mind compiling from source. Get it at http://adaptablegimp.org.

--------------

AdaptableGIMP: user interfaces for users

ginger coons

--------------

"It's like an infinite set of overlapping Microsoft ribbons. They try to do the same thing, they're trying to group functionality. But we're saying that it doesn't need to be the six that are defined by the people making the application, there can be a million. You can't only have the paintbrush in one. The paintbrush can be in 500,000 of them."—Filip Krynicki

--------------

What Revolution?

Antonio Roberts



What Revolution? is the first in a series of images challenging the ideas of celebrity and idols. The 1960 photograph of Che Guevara by Alberto Korda has been endlessly mutated, transformed, and morphed. It can be found advertising anything from belts and "hip and cool" t-shirts to health insurance. It is tacked onto political movements without much consideration of the history behind it. One has to ask if his image is still the symbol for change and revolution that it was fifty years ago, when it was furiously distributed throughout Europe by Jim Fitzpatrick in protest of the conditions of Guevera's murder.

The vector image of Che was glitched using a C script written by Garry Bulmer. The script randomises the position and other values of the nodes in the file. The background is a random image found on the Internet tagged with "Revolution," which was then glitched many times using ucnv's Glitchpng Ruby script. To get the sharp colours, I reduced the image from 8 bits to 1 bit using ImageMagick. All of the separate elements were then recomposed in Inkscape.



http://www.hellocatfood.com

--------------

Papercut

Allison Moore



Papercut is a Blender-based video game in the style of traditional side-scroller roleplaying games. There is a central character and a landscape to traverse. You are a lumberjack. You must cut down trees with a chainsaw. The game world is designed combining hand drawn illustrations with cut-out scanned textures.   

There are two characters to choose from: a Lumber Man and a Lumber Lady. The main character must deforest the landscape. Your only tool is a chainsaw. As you cut down trees you collect points. There is a time limit to each level, and if you meet your tally, you advance to the next level.  

Papercut creates a main character with a questionable morality. In traditional gameplay, the main character is definitively good whilst any character blocking the path is definitively bad. Geographical obstacles, woodland creatures and hippies block your path.

The virtual world combines exaggerated representations of the existing world with elements interpreted from my imagination. 

I played a lot of games in the 80s and early 90s, so I like vintage/retro games and this is the aesthetic that influences me most. It was hard to wrap my mind around a 3d world, so I decided to make it 2 ½d. I use 2d references of vintage games incorporated in the 3d landscape. The final result is like a paper puppet set, my 2d characters like puppets navigating through a diorama-style set built in 3d. Trees fall like leaves of paper.



http://www.looper.ca



--------------

# Create a montage from a folder containing various png images montage -geometry 400x300+0+0 *.png icon-montage.png



# Scale all jpeg images in a folder to a width of 640px for img in *.jpg ; do convert $img -scale 640 $img; done;



# Rotate a batch of jpeg images 90º and convert them to png for img in *.jpg ; do convert $img -rotate 90 ${img/jpg/png} ; done

--------------

As an artist or designer (or both), you use a range of tools in your everyday work. Even though it's not something you think about, you may be contributing to the growth of these tools without realising. Every time an application crashes and you hit the button, giving permission for it to report, you're contributing a little something. But, if you're interested, there's more. And there's more you can get out of it than just reliable software. 

Let's assume that you think of yourself exclusively as a user of design tools. In the same way you don't offer suggestions to the company manufacturing your pencils, you don't consider letting the people making your software know what you think. 

And you know what? You're not alone. Not many designers let the people behind their favourite tools know what they think. It's not common for designers and artists to make their voices heard, but it is useful. 

Because, you see, it works this way: if you use f/loss graphics software, standards and methods in your art or design practice, chances are good you have something interesting to talk to developers about. What you have to talk to them about is the way you use their software. And they want to hear it. They want the gory details about which specific tools and commands you use, what problems you have, why you use the things you use in your workflow. 

There are lots of different opportunities to have these conversations. The one we're going to suggest right now is Libre Graphics Meeting, an annual meet-up of developers and users. The one thing tying everyone together is an interest in f/loss graphics. We want to let you know, as a little service to you, the designer or artist using extensively or even just dabbling with f/loss graphics software, standards and tools, that it's coming up.

We want to let you know because, as a designer or artist using f/loss, you're a bit of a unicorn. By which we mean that you're something kind of rare and beautiful, not often seen by f/loss developers, and perhaps even misunderstood. And as something a little out of the ordinary, you're interesting. You've got lots to contribute, so consider joining in with the spirit of the community a little and bringing your own expertise to the table. 



The sixth annual Libre Graphics Meeting is taking place May 10-13 2011 in Montreal. More information is available at libregraphicsmeeting.org





--------------

On being a Unicorn:

the case for user-involvement in Free/Libre Open Source Software

--------------

Libre Graphics Meeting special

--------------

The Interview

--------------

The Interview

--------------

Feature

--------------

Feature

--------------

Feature

--------------

Interview with Oxygen's Nuno Pinheiro

Manufactura Independente interviews Nuno Pinheiro

--------------

Nuno Pinheiro coordinates the Oxygen project, initially a set of icons for KDE which evolved into a design platform comprising 2000+ icons, wallpapers, sound effects and window styles. He's employed as a UI designer at Klarälvdalens Datakonsult AB. Ana Carvalho and Ricardo Lafuente went to ask him about project management, art direction and the history of Oxygen.



Manufactura Independente: Tell us about Oxygen. What is it? How is it related to the KDE project?

Nuno Pinheiro: Well, Oxygen is considered one of the pillars of kde. It's a design platform.

Initially, it was created by three people—I wasn't one of them—at this get-together called Appeal Meeting, which took place right after the kde 3.4 release. Many important kde people were present in order to discuss and decide where to go from there. kde had reached a fairly mature state and it was appropriate to find out which next steps to take.

Two people involved in the meeting were Kenneth Wimer and David Vignoni. David is the author of the Nuvola theme which was, back then, one of the most popular alternative themes for kde. Actually, it was the most used theme. At this meeting, we decided to begin work on a new icon theme, which was to be called Oxygen.

Ken and David then invited me to join the effort of building a completely new icon theme. We had the sponsorship of Novell, which was a nice and cool company back in the day. That's the story behind the creation of Oxygen, which was set to become the icon theme for the fourth version of kde. We started work on the icons. Novell ended up changing their minds and left the project. We carried on nevertheless.

As we progressed on Oxygen, it became clear that icons were a single aspect of the user desktop experience. The desktop has many things, and it became clear that the user interface (ui) toolkit (or window decorations) was a significant part of the experience. kde used Qt, which had its own window decorations. We thought it was appropriate to do our own ui theme. So I started work on that as a sub-project of Oxygen, drawing many mock-ups using Inkscape. I made a full mock-up of the theme, without any code underneath. Then, I approached some developers and they ended up supporting my work. We worked together and did some iterations until we got to the current version.

Now, if you can make an icon theme and a ui theme, you can also make a window theme. So we did that next. If you can make a window theme, you can also make a sound theme. So I talked to Nuno Póvoa, who made it. If you can make a sound theme, you can also make a mouse pointer theme. If you make a mouse pointer theme, you can also make wallpapers. If you can make wallpapers, you can make websites. This way, Oxygen ended up becoming a design platform. Everything in kde that is design-related is taken care of inside Oxygen (except type).

In the meantime, while making Oxygen, we decided to adopt the Freedesktop standard naming spec. Thanks to this icon naming scheme, you can get a kde icon theme and use it within gnome, and vice-versa. This means that Oxygen, though closely connected to the kde project, can be used in many other contexts—in fact, we encourage that other projects use Oxygen. The license is free and the process is open. I get really happy when Oxygen is used in other projects, other places and for other purposes.

Going from this idea of Oxygen being the design hub for KDE, there's a question we're interested in: what's the decision-making process for aesthetic criteria? In the end, how does it come together? Is it a top-down process, or can anyone propose new directions? Is there any other kind of control?

It's bidirectional. Actually, I coordinate the project, and I'm the guy who says “Okay, we're going that way” or “We're not going that way.” I've got the role of drawing the line when it comes to final design choices.

However, Oxygen is not a young project. There's quite some years behind it. It's sported some different visual tendencies through time, graphically and formally. Every two versions, we try to slightly change the general concept and message of the theme. This message is not defined by us, but rather by the community. For instance, the message we're working on for 4.6 and 4.7 is about elegance, in its broadest definition. Code can be elegant, user experience can be elegant. So, we took this message and tried to convey it through the theme design, aiming for an elegant experience: elegant wallpapers, elegant sound pieces, and so on. This is the centerpiece of the experience we want to pass on to the user—a global message that Oxygen helps get across.

And this is the most complicated part inside a design project: achieving consistency when we have several people with very different styles and ideas contributing to the same project. It's the challenge of creating a bundle that is smooth and continuous, has an even pace, and speaks the same language. Managing all of this is my task: talking to people and trying to have their work flow into something that's consistent and dynamic, something that goes along with the rest and, at the same time, addresses the core message.

Regarding your tools of choice, we know you use Inkscape...

I do use Inkscape. I also work with Blender, Gimp, Krita, scanner, pencil, pen and my imagination.

Have these been your tools all along?

When I started, my first tool was Sodipodi, the predecessor of Inkscape. Inkscape is definitely my main design tool.

Have you ever approached the Inkscape developers to ask for a specific feature?

To be honest, I'm not close to the Inkscape guys. On the other hand, I do frequent exchanges with the Scribus people. We get along rather well. I'm almost done with their icons! Scribus requires a lot of icons, around three hundred.

How many icons are there in Oxygen?

Two thousand and something. It's the largest part of kde in terms of file size—two hundred or so megabytes. As far as I know, it's the world's most complete icon theme. I'm not aware of any other theme with such an amount of icons. Tango had almost as much, but we're bigger. To give you a point of comparison, Apple only has around eighty base icons, and then each application brings their own set.

Are there any style guidelines that you set out before starting work on a new theme? Setting a formal style direction is a mainstay of traditional graphic design, usually through corporate identity manuals or interface guides. Our question is, do you follow this tendency, or is the Oxygen style defined through a less formal, more organic way?

It is organic. To be blunt, I don't believe in those things. I've read several identity and interface guideline manuals, particularly icon style guides. I could get the style guidelines for Windows Vista and create Mac icons following them, and vice versa. This while strictly following their rules.

And you could end up with something consistent.

I could! Any designer worth his name can do that. It's very easy for a designer to follow every single rule, and still end up with something that doesn't fit. There's some intangible aspects, a kind of feeling, which you can't turn into logical rules and crystalise on guidelines. Having 42 bullet points that you have to go through in order to achieve X is not something that works in this case. I've heard many dissenting opinions, but I seriously don't agree with this way of doing things. It's my personal opinion. I've started writing basic icon guidelines to help newcomers.

Oxygen could have better documentation, but it's more about having good designers. Every time I have a designer asking for the rules, I tell them to look at the icons. If, after analyzing the icons from any theme, you still have doubts about their graphical and aesthetic rules, you probably shouldn't be working on this. Honestly, it's a language. If it's well written, one should be able to clearly interpret and identify the meaning just by reading it. Something along the lines of “Oh, they're using references to this and that. And I think I get where they're trying to go here.” If you need a manual for a language in order to be able to write it, then something failed during the process, I'd say.

Now, we might be basing this on a historical inaccuracy here, but we're led to believe that KDE pioneered the glossy interface look, with polished looks, clean lines and shiny surfaces. The same approach that has now been made popular by Apple on its recent user interfaces.

Yes—a great designer, Everaldo Coelho, is to blame for the glossy style. He worked on this theme, Crystal, which is very well known and heavily used on a number of web sites worldwide. It was one of the first Free themes made by a designer, with a rather high quality standard considering the tools available at the time.

The Crystal theme was of very high quality, indeed. But it's the result of what Everaldo is as a designer, a specific kind of stubborn designer with a distinct style. It is glossy, playful, colorful, fun. Its visual style eventually became associated with kde.

What we were trying to get at with the previous question was, does the Oxygen team see themselves as trendsetters? Do you think that you're creating a norm, a set of unwritten rules of taste that would motivate others?

Honestly, with Oxygen I'm aiming to influence my community to get better. I think that there are much more interesting things out there than Oxygen, and I'm not just talking about the desktop. I think that there's incredibly interesting stuff made for the web—which by the way, I don't approve of as a platform, we're sacrificing our future freedom by moving everything to the cloud—but design-wise, there's very exciting things being done for the web today. I try to translate some of those new principles to the desktop, and in doing so try to influence design perception inside the community.

Design is learned, an acquired taste. Just like enjoying wine, or cooking in general. You might go your whole life just eating french fries and burgers; one day, you try a fish course, and the next day you come back. Maybe you'll go on to like fish. Then you try a good wine, and you gradually become able to appreciate wine. I try to influence the community to become aware of these little things.

Icons are pieces created with the purpose of being used and re-used in different contexts. In the case of Oxygen this becomes more evident because of the Free licensing you employ. Have you ever been surprised by a particular use of the Oxygen icons?

Oh, many surprises. “A Bola” [a top-selling daily Portuguese sports newspaper] used my icons. Any mention to the license or even attribution is nowhere to be seen. I've stopped worrying about licensing issues.

The license we use is the lgpl. It's not the perfect license for icons. We could have used Creative Commons, but the most permissive cc license is very similar to lgpl. With it, you only have to make sure proper attribution is made; other than that, the icons belong to whoever wants to use them.

In corporate settings, one can usually find a schism between designers and developers or engineers. In Oxygen's case, does this kind of tension occur?

Such tensions are nowhere to be found. I'm actually lucky—I'm an engineer. I studied engineering. And I find this is one of the reasons why Oxygen solved many of the issues that can plague other open source projects. I can speak both languages.

I come from a specific background: I'm a civil engineer. Civil engineering implies a crossover between architecture and engineering. My sister is an architect, so I know the battlefield well when it comes to the problems of both theorizing and implementing. The person who theorizes—often the designer—should be comfortable with implementation details, but very often that's not the case. A good designer should incorporate the engineer and the artist, but most of the time the artist wins. I keep trying to make sure I'm in touch with both perspectives.

There was a particular issue in Oxygen regarding the styling of window shadows. Only someone well aware of design and implementation issues could tackle the problem of how to properly anti-alias window corners and make it look good. I knew the implementation pitfalls, knew how the tech worked, went to the developers and presented a different solution that could elegantly solve the problem. It is very important for the designer to be aware of the technical limitations. However, it's very important for the designer to not be aware of the technical limitations.

That's why coordination is important. I like having designers in the Oxygen project who work with absolute freedom, pure artists. The kind of people who come to me with completely nonsensical ideas and make me say “You're an idiot, this is impossible.” But it's very important that they keep pushing me in that direction so that I can go “This is impossible but hey, maybe we can do half of it.” Then I go to the developer and he'll say “This is impossible because of this, this and that,” and I can suggest “But this and this could be done in this particular way,” to which he'll reply “Maybe we can do half of it.” This way, things progress according to the artist's vision and the developer's understanding.

--------------

Code can be elegant, user experience can be elegant.

--------------

A good designer should incorporate the engineer and the artist, but most of the time the artist wins.

--------------

"What we're grafting onto the existing interface paradigm, is this task-centric view of computing where you say 'This is what I want to do' and the interface modifies itself to accomodate that particular task."—Michael Terry

--------------

Proprietary software typically has two answers to your problems: don't do it, or spend more money to be able to do it. This might apply to a specific file format you want to use, or an effect you want to achieve, or a way of working.

--------------

Libre Graphics Meeting special

--------------

Showcase

--------------

Showcase

--------------

Feature

--------------

Resources

--------------

Resources

--------------

Resources

--------------

Glossary

--------------

Resources

--------------

Glossary

--------------

Libre Graphics Magazine 1.2

February 2011

--------------

ISSN: 1925-1416

--------------

