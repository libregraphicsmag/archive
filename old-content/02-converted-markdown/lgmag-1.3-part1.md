As co-workers, we needed to gather ideas in a straightforward way, beyond just notes and emails. Pickpic was good for this, because it allowed us to upload pictures while chatting. We met this way on April 21 and 27, then again on May 3. Here are some excerpts, according to the tracks we followed, from conceptual to technical questions.

10:09 mib_x9vil1 we could do this way: visual research - discussion - first draft - discussion - second draft - adjustments.

10:11 loredana ah monsters with many eyes! cool!

10:13 thibaut Because on the one hand, we have multiple subjects, and on the other we have one entity with somehow multiple personalities

10:17 thibaut A blob, says morgan

10:20 thibaut This is kind of horrible but collaboration could be a “blob” in the sense that when collaborating, 1 and 1 are 3, not 2

10:41 mib_x9vil1 another track might be, on a more general level, the idea of multiple things being something else when all together...

15:20 lori let's divide the mitosis example in two concepts: mitosis and multiple visualization

15:22 mib_f3yalx yes, that's what I was still thinking about... maybe eyes could work as a module for the mitosis pattern...

15:28 mm collaboration is a more complex and free process than a simple addition

15:30 lori could we say we restricted to: eyes, mitosis, blob?

16:01 mib_f3yalx randomly constructed/complex/organic pattern.

16:03 mib_f3yalx I would say that the "module" of the pattern could be either geometric or organic

10:02 morgan maybe, but the final result is more consensual than the two others

10:03 thibaut I definitely love the eyes made with Python, Lori too, both of you too. Does anyone has something esle to say?

10:14 sprog yes... and also you can play a bit with the script if you feel like... changing stuff is quite easy if you don't want to change the shape

10:16 thibaut I think the proportions are good. From far, it really looks like a cell pattern, and close, it appears that the idea of multi viz is pregnant

10:26 thibaut Then, do we need type on our cover?

10:27 sprog do we? :)

--------------

April 21

Keywords: Creation process, many eyes, blob, multiple visualiza-tion, 1 and 1 are 3, copy-paste

--------------

April 27

Keywords: Cell mitosis, many eyes, pattern, Bremen musicians

--------------

may 3

Keywords: Cell pattern, many eyes, relevancy > consensus, multiple visualization, script, type

--------------

14-18 SEP

--------------

Index

--------------

Masthead



Editorial Team

Ana Carvalho   ana@manufacturaindependente.org

ginger coons   ginger@adaptstudio.ca

Ricardo Lafuente   ricardo@manufacturaindependente.org

Publisher

ginger coons

Community Board

Dave Crossland

Louis Desjardins

Aymeric Mansoux

Alexandre Prokoudine

Femke Snelting

Contributors

Emanuele Bonetti, Loredana Bontempi, Dave Crossland, Morgan Fortems, Thibaut Hofer, Martin Owens, Eric Schrijver.



Printed in Porto by Cromotema in recycled paper. http://cromotema.pt



Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 



Contacts

Write to us at enquiries@libregraphicsmag.com

http://libregraphicsmag.com

--------------

Notebook

--------------

Isn't Open Clip Art Library handy?

--------------

EDITOR'S LETTER

--------------

Giving up the reins

ginger coons

--------------

In issue 1.1 of Libre Graphics magazine, Eric Schrijver very wisely wrote that "sharing means giving up control." We liked his sentiment so much, we made it a pull-quote. And we've been repeating it ever since, every chance we get and every time we talk to people about Libre Graphics magazine. 

Here we are, then, at issue 1.3: Collaboration, collaboratively, where we're coming to realise that Eric's words couldn't be truer. As a small editorial group, we're used to a high degree of control and mutual trust. We've certainly stepped outside of our comfort zone this time around. We've handed the vision over to a very talented group of outsiders. 

After all, it only makes sense to approach the idea of collaboration in a more collaborative way. So we're pushing the boundaries of what we think when we think of collaboration. Beyond the collaboration of the usual suspects, we're concerned, this time around, with all sorts of methods. We're concerned, in fact, with taking some of the control out of our own hands and handing it off to some trustworthy and talented others. 

From here, we're passing it over, for the most part, to Loredana Bontempi, Emanuele Bonetti, Morgan Fortems and Thibaut Hofer. But don't worry. You're in good hands.



--------------

The voice of the shell—in collaboration with my computer

Eric Schrijver

--------------

Masthead

Editor's Letter 1

Editor's Letter 2

Production Colophon

New Releases

Upcoming Events

Dave Crossland went to Argentina Dave Crossland

The voice of the shell—in collaboration with my computer Eric Schrijver

In practise RestructWeb

Breaking into f/loss Morgan Fortems

Notebook

Isn't Open Clip Art Library handy?

Showcase

    FF3300

    Lafkon Bash scripts for generative posters

    Makemake Open source recipe for organic logos

    Parcodiyellowstone Digital Dump and Pickpic

    Stdin

Libre Graphics magazine issue 1.4: The Physical, the Digital and the Designer

Managing artist communities: the casefor Ubuntu Artists Martin Owens

SparkleShare: pleasantly invisible version control ginger coons

Parallel School—an interview Thibaut Hofer

Resource List

Glossary 1.3

--------------

4

5

6

7

9

10

12

13    

15

19

20

22

24

26

30

34

36

40

42    

44    

48

51

55

57    



--------------

MASTHEAD

--------------

NEW RELEASES

--------------

TYPE DESIGN

--------------

SCHRIJVER

--------------

My associates and I have always been vigilant—without being activists or pioneers—to the alternative software tools we can use to oppose the dictates of manufacturers and habits in our fields (graphics, video, photography). Our first moves were primarily motivated by economic choices (we could not afford to buy all the necessary software) or technical problems (to meet needs that proprietary software does not cover or to solve incompatibilities). 

It seems that today the practice of alternative solutions has became more accessible, as it has left the realm of developers and has risen to meet the needs of professional workflows.

So far, we have easily implemented simple f/loss applications or plug-ins in the daily routine of the studio without impairing our efficiency or our relationship with customers and suppliers. 

For now, we don't see f/loss as a major threat to the status quo but, for ourselves, it is time to consider new methods. We decided to train with small internal projects and master these new methods and f/loss tools before switching over completely. For example, and among other things, we are currently developing the layout of our new fanzine, Mmagazine, in Scribus and we are using Libertinage, a font licensed under the Open Font License, for the identity of a concert hall.

I had some apprehensions about leaving behind a precious bit of technical know-how, built over almost a decade, but my very first steps with Scribus and my meetings with specialists convinced me. We are so far on the way of atonement that Damien, one of my associates, plans to “liberate” his Mac during a Pizza/Beer/Ubuntu party!



Morgan Fortems is a freelance graphic designer. He is a member of Please Let Me Design studio and heads My.monkey, a non-profit art gallery in France.





--------------

Breaking into F/LOSS

Morgan Fortems

--------------

FIRST TIME

--------------

“A camel is a horse designed by a committee.”



When this sentence was first published in 1958 in Vogue magazine, it was meant to emphasize the inconsistency and lack of a unified vision in products created without one strong leader. More than fifty years later, this still seems to be a common assumption in creative fields such as graphic design. 

Most professionals are still convinced that the only way of producing an effective result is to have a visionary leader who dictates design guidelines to his followers from atop his ivory tower. The great majority of graphic design studios are still based on this top-down approach.

The effectiveness of collaboration has been widely demonstrated in recent years by the Free/Libre Open Source Software communities. Why can't we apply the same principles to a more traditionally creative field? Is collaboration good only in order to solve problems demanding an objective solution?

If we look at the great majority of graphic design studios we quickly notice that most of them tend to get stuck within a certain style. This is natural. Individuals have their own personal taste and background which will inevitably be reflected in their work. However, if their personal taste and background are the only inputs, the designer will always end up producing the same output. In other words, trying to communicate different ideas with the same language will result in communicating the same idea again and again.

As designers, we think that graphic design shouldn't be about giving a personal interpretation of an issue, but about finding the most effective solution to communicate a message. Which is one of the reasons we believe that collaboration means, more than anything else, mixing together different backgrounds, tastes, skills and knowledge in order to build a richer group knowledge. This group knowledge provides a much wider range of alternatives to every project.

In this issue of Libre Graphics magazine, we've tried to collect some of the most effective examples of collaboration in graphic design, looking at the experiences of people who have chosen to work as collectives not only to divide tasks, but to really improve their individual creativity. This issue also covers some nice examples of tools that aim to improve the way people collaborate with one another.

We hope you'll enjoy it and end up as convinced as we are that eight eyes see better than two. And four brains dream better than one.



--------------

New releases

--------------

Pinpoint 0.1.2

http://live.gnome.org/Pinpoint

A tool for simply creating beautiful presentations.

--------------

Mediagoblin

http://mediagoblin.org

A newly-announced, in-progress decentralised media hosting platform. Alpha release currently slated for October 2011.

--------------

Vips 7.24

http://www.vips.ecs.soton.ac.uk

A library and interface for quickly and efficiently manipulating large images.

--------------

Open Font Library and Open Clip Art Library

http://www.openfontlibrary.org • http://www.openclipart.org

Relaunched, with lots of improved functionality.

--------------

Upcoming events



--------------

UPCOMING EVENTS

--------------

UPCOMING EVENTS

--------------

A Reader's Guide to Libre Graphics Magazine



In this magazine, you may find concepts, words, ideas and things which are new to you. Good. That means your horizons are expanding. The problem with that, of course, is that sometimes, things with steep learning curves are less fun than those without.

That's why we're trying to flatten the learning curve. If, while reading Libre Graphics magazine, you encounter an unfamiliar word, project name, whatever it may be, chances are good there's an explanation. 

At the back of this magazine, you'll find a glossary and resource list. The glossary aims to define words that are unique to the world of Libre Graphics. The resource list provides valuable information about tools, licenses, whatever items we may be mentioning.

Practically, this means that if, for example, you're reading an article about Scribus (see pages 22 to 23), you can always flip to the back of the magazine, look up Scribus in the resource list and become quickly informed about it. This provides some instant gratification, giving you the resources you need to understand, in a moment, just what we're talking about.

We hope you like our system.

--------------

Dave Crossland believes anyone can learn to design great fonts. He is a type designer fascinated by the potential of software freedom for graphic design, and runs workshops on type design around the world.

http://understandingfonts.com

--------------

BEST OF SVG

--------------

The expression “many hands make light work” uses the analogy of the hand to represent participation or involvement. As we talk about collaboration, it seems appropriate, this time around, to look a little more literally at the symbol used so often to represent work.

This issue, Best of svg scoured the revamped Open Clip Art Library, looking for the best hands on offer. As it turns out, ocal provides hands for all occasions. 

If you haven't already used or contributed to Open Clip Art Library, take a look now. All work there is in svg format and is dedicated to the public domain. That means you can use it for just about anything. Check it out, use it and add a little work of your own. Find it at openclipart.org

—the editors



--------------

We at Libre Graphics magazine have a thing for open standards. We like their transparency and their interoperability. We like that, with a well documented standard, everyone has an equal chance to play nicely together. 

That's why we like SVG so much. It's a well developed, well supported standard brought to us by the World Wide Web Consortium (W3C). It's available for implementation by anyone developing software. It shows up in modern browsers, fine vector graphics editors and any number of other places. 

One thing that's missing, though, is you: the designer, the artist, the illustrator. So put down that .ai file and check out SVG.

--------------

What is RestructWeb? 

Restruct web is a three-person web design & development agency from Rotterdam, creating user-friendly websites and (web) applications. We are a full service commercial web/software agency, with a passion for f/loss. Services we provide include: online strategy, web design, web development, mobile applications and custom/special software projects. 



Why Collaboration?

We have a background in graphic design/video, and met at the Piet Zwart Institute. After our graduation we each ran our own separate studios for a while. During these years we found it hard to do everything on our own. One has to stay up to date with the latest technologies, design developments, handle business, act on malfunctioning websites/servers and try to find time to actually work as well. Our conclusion was that while all this work could be done by one person, it would be tough to also be good at it.

The logical solution was to merge our three studios. Naturally, each one of us also had individual reasons to collaborate. They ranged from being taken more seriously as a business, being able to handle bigger projects and sharing the process of creation to sharing the rent, dividing tasks and being able to specialize. Of course, wanting to work with each other helped, too. 

We make each other laugh. A lot.

Who's the boss?

We all are. When we started, we clearly outlined the direction we wanted to take as a company and who would focus on what. While we try to stick to this plan, it doesn't always work. Depending on the projects at a specific moment, we may all, for instance, be doing some coding. That's one of our strengths as a group: we each have our specialties but we can also cover for each other.

Whenever we make bigger decisions, we naturally discuss them first. But we also explicitly trust each other in smaller decisions so we don't have to discuss everything. As a company, we are a bit like a mesh network; we each have an equal say in decisions and each do the part of the work we're most interested in. We also have a built-in veto-clause: any one of us can decide that the group should not take on a specific project or client. In those cases it's be up to the other members if they want to do the project outside of RestructWeb.

Our collaboration, by having a non-hierarchical structure, causes each one to be responsible for keeping the company and the others up to date. We adapted the workflow for user-centered design as described by J.J. Garrett to our projects. This is working well for us so far. Being able to divide an assignment into specific chunks like strategy, wireframing, design and development, etc. allows each of us to focus on these tasks and develop our abilities in a specific direction. We've noticed this clear workflow is also appreciated in communication with our clients. It helps them focus and decide on the right choices at the right time.

We fill gaps in the workflow with in-company projects, like the wireframe-library we're currently working on, or by doing research. These are tougher undertakings when working alone.

In all our projects we have a strong focus on f/loss, and especially open standards. 

Programming, being able to script and being able to understand the inner workings of a technology, are important aspects of f/loss for us. We may script a part of an svg and finish it in Inkscape (or the other way around). Recently, we've made a generative identity which reacts to rhythms found in nature. We wrote the algorhythm in php/Python and generated some 20.000 different logos within a few hours. We then scripted Inkscape to convert a few hundred of them into business cards (pdf), and used pdftk to combine them into one printable document. Try that by apple-scripting Illustrator!

Another reason we love f/loss is cost. We occasionally set up machines for exhibitions. Cheap, after-market Linux boxes usually suffice instead of the expected, super expensive Macintoshes. While this same pragmatic financial reason is gaining support in the business world, the art schools in the Netherlands seem to be lagging behind. They still tend to mainly teach the Adobe toolchain on high-end, brand new Apple machines.

After graduation in the Netherlands, when you start your own studio without much of a budget, you have roughly two options; start out with pirated software or try to get a “startstipendium” (funding for auspicious graphic designers by the Dutch government) and use that to buy official hardware and software. It's a bit sad to realize that a big chunk of this public tax money gets poured straight into the bank accounts of Apple and Adobe. If students were educated to consider f/loss as a viable alternative, it could save them a lot of money. Money we think would be better spent on the acquisition of knowledge and self-initiated projects to develop the profession further.



--------------

ATypI 2011

Reykjavik, Iceland

http://www.atypi.org/2011-reykjavik

--------------

We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and F/LOSS. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the F/LOSS designer at a traditional design event, or maybe the designer at a predominantly software developer event. 



--------------

What's new with you? We're always eager to find out what designers, artists and others using and working with F/LOSS are up to. Tell us what you've done lately at enquiries@libregraphicsmag.com



--------------

Images under a CC Attribution Share-Alike license

Cover by Loredana Bontempi, Emanuele Bonetti, Morgan Fortems and Thibaut Hofer, generated by a Python script by Parcodiyellowstone.

Photo of ginger coons by herself.

Photo of Ricardo Lafuente by Ana Carvalho.

Photo of Ana Carvalho by Luís Camanho.

Photo of Dave Crossland by Mary Crossland.

Photo of Eric Schrijver by himself.

Illustration in “Type Design” by Dave Crossland.

Photos in “In Practice” by RestructWeb.

Illustration in “First Time” based on  “191008”, a photo by Flickr user fdctsevilla.

All images in the “Showcase” section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.

Illustrations in “Managing artist communities: the case for Ubuntu Artists”, by order of appearance: Limon y Mora (http://peritoproducciones.deviantart.com/art/Limon-y-Mora-209872950) by peritoproducciones; Dream Colours (http://pr09studio.deviantart.com/art/Dream-colours-209077953) and Hidden Universe by pr09studio (http://pr09studio.deviantart.com/art/Hidden-universe-212391023).

Screenshots in “SparkleShare: pleasantly invisible version control” by ginger coons.

Photos in “Parallel School—an interview” by Parallel School.

GIMP, Inkscape, Scribus screenshots in “Resources” by Manufactura Independente.

Toonloop screenshot in “Resources” by Alexandre Quessy.

Images under other licenses

Photos in “Notebook” by Open Source Publishing are under the Free Art License.

Illustrations in “Best of SVG” by Open Clip Art Library user cybergedeon are all in the public domain. They can be found at:

http://www.openclipart.org/user-detail/cybergedeon

Photo in  “Showcase” opening page, “Tropical Cyclone Imani”, by Flickr user NASA Goddard Photo and Video is under CC-BY.

Photo used in the illustration of “Call for entries 1.4”, by Flickr user petercat.harris, is under CC-BY.



General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

--------------

IN PRACTICE

--------------

Look at my camel.My camel is amazing.

Loredana Bontempi, Emanuele Bonetti, Morgan Fortems, Thibaut Hofer

--------------

Cover process

Thibaut Hofer & Morgan Fortems sum up the process that ended up with the cover.

--------------

EDITOR'S LETTER

--------------

PRODUCTION COLOPHON

--------------

LightTwist lt-align

http://vision3d.iro.umontreal.ca/en/blog/2011/05/11/easy-multi-projector-desktop

The debut of convenient and handy tool for turning multiple projectors into one giant projector, an accessible addition to the larger LightTwist project.

--------------

Toonloop 2.0.0

http://toonloop.com

Fun and powerful stop motion animation software. Excellent for performance.

--------------

Linux Mint 11

http://www.linuxmint.com

The latest from the second most popular distribution of gnu/Linux. Version 11 makes software management and installation of extras easier than ever.

--------------

Fedora 15

http://fedoraproject.org

Version 15 makes Fedora (our favourite) the first distribution of gnu/Linux to include gnome 3 as its default desktop environment.

--------------

GNOME 3

http://gnome3.org

An attractive, new take on that old favourite: the gnome desktop environment.

--------------

16 SEP

--------------

Brand New Conference

San Francisco, United States

http://www.underconsideration.com/brandnewconference

--------------

17-25

SEP

--------------

The London Design Festival

London, United Kingdom

http://www.londondesignfestival.com

--------------

24-25 

SEP

--------------

PyCon UK 2011

Coventry, United Kingdom

http://pyconuk.org

--------------

1-18

OCT

--------------

Phoenix Design Week

Phoenix, United States

http://www.phxdw.com

--------------

13-16

OCT

--------------

Pivot: AIGA Design Conference 2011

Phoenix, United States

http://designconference2011.aiga.org

--------------

15-16

OCT

--------------

West Coast HackMeet

San Francisco, United States

http://hackmeet.org

--------------

17-20

OCT

--------------

SVG Open

Cambridge, Massachusetts

http://www.svgopen.org/2011

--------------

19-21 OCT

--------------

LatinoWare 

Foz do Iguaçu, Brazil

http://www.latinoware.org

--------------

19-22 OCT

--------------

Access 2011 

Vancouver, Canada

http://access2011.library.ubc.ca

--------------

20-22 OCT

--------------

Typo London 2011

London, United Kingdom

http://www.typolondon.com

--------------

31 OCT

4 NOV

--------------

Ubuntu Developer Summit

Orlando, United States

http://uds.ubuntu.com

--------------

2-3 NOV

--------------

DesignThinkers 2011

Toronto, Canada

http://www.designthinkers.com

--------------

4-6 NOV

--------------

Mozilla Festival 2011

London, United Kingdom

http://www.svgopen.org/2011

--------------

RestructWeb

 

Annemieke van der Hoek, Timo Klok, Michael van Schaik

--------------

IN PRACTICE

--------------

NOTEBOOK

--------------

I tell my students that the command line is the way to go. I am not, however, the kind of person who thinks the command-line is somehow a more true experience—it’s another modality, another way of accessing and manipulating the data on your machine.

The query-response format is wonderful. I amaze students with the whoami command—the computer knows the answer! This gives the impression, to some, that it is possible, through the console, to have a conversation with the computer.

I do often feel like I am talking to the computer. But when the computer talks back to me, from time to time the voice of the computer gives way to the voice of the programmer; or at least, to my image of this programmer. This is the output of a 7zip command:

7-Zip  4.44 beta  Copyright © 1999-2007 Igor Pavlov  2007-01-20 p7zip Version 4.44 (locale=nl_NL.UTF-8,Utf16=on,HugeFiles=on,2 CPUs)

Processing archive: Fedora 9.7z Extracting  Fedora 9/Fedora 9.vmkd Extracting  Fedora 9/Fedora 9.nvram Extracting  Fedora 9/users.txt Extracting  Fedora 9/Fedora 9.vmx.lck/M00232.lck Extracting  Fedora 9/Fedora 9.vmx Extracting  Fedora 9/Fedora 9.vmxf  Extracting  Fedora 9/Fedora 9.vmsd Extracting  Fedora 9/Fedora 9.vmx.lck Extracting  Fedora 9

Everything is Ok

In any case, it’s endearing: I imagine the programmer as a pretty, dark-haired boy. He is shy and his eyes, hiding already behind large glasses, avoid your gaze. Igor is not so handy with words, but he means well towards the world. 

The encounter is not always so nice. The reason I install Fedora is because I have to compile some Perl modules and put them on my shared hosting service to make Movable Type work. The interactive mode of cpan has the most condescending error messages yet.

But nothing beats Telnet. When typing in “rcpt:” instead of the required “rcpt to:,” I get:

Error: I can break rules, too. Goodbye.

I project in my mind this monster: crouched behind his computer, staring intently at the screen, a system administrator, overweight and with unkempt hair, staring maniacally at the screen, laughing out loud about this error message that is going to upset the people using his code. But I have been awake too long. I made him in my own image. Look at me: I’m also alone, staring maniacally at the screen, making up these stories about computer programmers.

With minds so volatile and temperaments so inflammable, it’s important we stay nice to each other. If the only way we talk to each other is through error messages, let these error messages be exceptionally kind.



Eric Schrijver (Amsterdam, 1984) is a graphic designer and a performance artist. He is inspired by programming culture. Eric teaches Design for new media at the Royal Academy of Art in The Hague, and is a member of the design collective Open Source Publishing. http://ericschrijver.nl

--------------

Where

Libre Graphics Meeting in Montreal.



--------------

Prettiest

Old lead and wood type at the Lovell printing plant, including lots of display faces, looking beautiful all on their own.

--------------

Most Collaborative

Birds of a Feather (BoF) meetings of developers and users of software, including a great Inkscape chat, with remote participants joining via irc. 

--------------

What

Annual meeting of users and developers of f/loss graphics software and work.

--------------

Most Fun

Toonloop, as presented by one of its developers, Alexandre Quessy, with amazing potential for creative stop-motion animation. 

--------------

Most Immersive

Sewing workshop by Susan Spencer, using Inkscape and Python to customize clothing patterns.

--------------

13-23

OCT

--------------

Design Philadelphia

Philadelphia, United States

http://www.designphiladelphia.org

--------------

