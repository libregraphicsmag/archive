Index

--------------

Masthead

--------------

Editorial Team

Ana Carvalho   ana@manufacturaindependente.org

ginger coons   ginger@adaptstudio.ca

Ricardo Lafuente   ricardo@manufacturaindependente.org





Publisher

Studio XX      http://www.studioxx.org



Community Board

Dave Crossland

Louis Desjardins

Aymeric Mansoux

Alexandre Prokoudine

Femke Snelting



ContributorsMargaret Burnett, Dave Crossland, Laura C. Hewitt, John LeMasney, Ludivine Loiseau (and her class), Pete Meadows, Lila Pagola, Eric Schrijver.



Printed in Montréal by Mardigrafe on recycled paper.



Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 



Write us at enquiries@libregraphicsmag.com



http://libregraphicsmag.com

--------------





First times are seen as momentous events in our lives. The first time away from home, first job, first kiss, all are seen as seminal events, meant to be packed up and remembered, fondly or not. The first, narratively, carries a heavier weight in our memories than so many subsequent events. After the first time, you start getting used to it, you become proficient, the magic and mystery start to wear off.

This issue, we're talking about firsts. We're talking about first experiences, first efforts, first anythings, about the liberation of letting go and trying something new, and the terror that sometimes comes with it. Firsts are about taking flight, about leaving behind the things you know and embracing something else.

That's where our second theme, Taking Flight, comes in. Taking flight, leaving behind, in a physical and a metaphorical sense, carries with it that sense of liberation and trepidation, that change of perspective. Lifting off of our metaphorical ground and looking down from above packs the power to change our perspective, to let us a little bit out of ourselves. In the same way that seeing your own city from above can completely change your conception of it, seeing your own possibility and creative process from above has the power to make you stop and think again about the elements of process, style or workflow that you take for granted.

Taken together, the ideas of First Encounters and Taking Flight are all about discovering the new, the momentous, the big and the unexpected that you never even thought you possessed. These things, in your own head, in your own possibility, unlocked if only you give them a chance and break from the norm.

That's why, this month, we're talking about firsts and flights, about the magic, the mystery, the revelation of doing something new and discovering what you never even suspected you possessed. We've got articles about first experiences in design, with tools and even more theoretical firsts, too.

We hope that you, picking up Libre Graphics Magazine for the first time, see it that way, too. We hope you'll join us on a journey which promises to be an interesting, hopefully long, and maybe just a little bumpy.

All of this, we think, may just encourage you to think about collecting a few more of those all-important firsts for yourself.

ginger coons is a member of the Libre Graphics Magazine editorial team.

--------------

New Releases

--------------

Fonts are essential to the design of every web page, and true web fonts are about to bring the typographic sophistication of print to the web in force.

It’s taken a while. In 1998, Internet Explorer 4 implemented the feature with a DRM font format, Embedded OpenType (EOT), because it was part of CSS2. Despite that fact, it was ignored for years by other browsers until CSS co-founder Håkon Wium Lie championed it 10 years later. Slowly all the other big browsers began to support regular TrueType fonts, and resisted pressure to implement a standardised DRM format. Today SVG Fonts are used in iPhones, Internet Explorer 9 supports TrueType fonts, and Libre tools do EOT conversions. Web fonts work everywhere.

In May, Google launched the first font service providing only Libre fonts. The blog for the service (http://googlewebfonts.blogspot.com) recently revealed some details about its growth – nearly 500,000 domains showing web fonts to 17 million people a day.

In 2011, CSS3 will take web fonts beyond the simple font linking feature of 1998. Designers will finally be able to use the full power of OpenType fonts directly in webpages. This can be tested today with Firefox 4 beta releases. 

The web is about to become as typographically powerful as the latest proprietary desktop publishing applications. This means that web designers should now take time to learn about how typefaces vary and what those variations mean for readers – there will be a lot of libre fonts to choose from, and we should choose fonts wisely.

Three kinds of font classification schemes are useful to guide designers in their choices. 

1. Simple Categories. These are are deceptively simple. The deception is their “Other" categories. CSS itself has a very simple scheme (Serif, Sans-Serif, Fantasy, Mono) and this is useful because everyone can remember its groups. Contrast that with the ‘IBM Family’ scheme which uses a long list of historical genres and many “Miscellaneous" caverns.

2. Parametric systems. Common forms of the elements of the Latin alphabet are described and related. The result allows users to drill down into a font collection interactively: “Letters that wide, serifs this thick, an axis of contrast at that angle." PANOSE’s second version was a sophisticated example that has sadly disappeared thanks to an acquisition by and merger with Hewlett Packard. 

3. Tagging. Just as we tag photographs in centralised proprietary photo storage services, font can be tagged. I imagine that eventually a de-centralised federated font classification network service will be made, perhaps building on the Libre-licensed Typedia.org project. While the first two have their place in learning how fonts are different, tagging is for me the most useful classification technique when deciding on a font.

The words people tag with are often not the words used by other schemes. Instead, people relate to fonts with what Ovink described in 1938 as atmosphere values: "Those properties by which [a font] excites feelings within the reader." 

Curiously, Ovink found in his research that general readers can't tell the difference in atmosphere values of two typefaces which are very similar. I might suggest that this means designing new type is, for practical purposes, entirely pointless. 

Most importantly, he concluded that picking the "wrong" font is a "missed... opportunity to intensify the force of impression of the text in a considerable degree."

To web designers choosing web fonts, watch out. 

Dave Crossland believes anyone can learn to design great fonts. He is a type designer fascinated by the potential of software freedom for graphic design, and runs workshops on type design around the world. 

http://understandingfonts.com

--------------

The web is about to become as typographically powerful as the latest proprietary desktop publishing applications.



--------------

The Dutch computer scientist Edgar Dijkstra believed in first times. He thought it a contemporary charade to give programs version numbers. Since it's possible for a program to be correct, better to get them right the first time around. I don’t agree with Edgar Dijkstra. I believe I will achieve my first proper column after five or six tries.

There is a second first time, when you finally manage to rise above the preconceptions and ideas you started your project with. Then your project can start to work for you.

The rumour that Bill Joy wrote the vi editor in one night is persistent, even though he denies it himself. Anything awesome has a gestation period. But we just happen to love believing in spontaneous creation.

As to Libre Graphics, it is at the nexus of many exciting developments. Libre Graphics lies at the intersection of art and technology, science and the humanities. And I am quite sure we will see a number of wonderful projects take flight.

Applying F/LOSS principles to art and design might help us improve visual literacy, just as F/LOSS improves computer literacy. Applying F/LOSS principles to art and design might help us better understand the knowledge present in the creative process.

Not just that, I think it might help us understand what it means to share. Getting artists to share will be difficult, initially. Scientists have built an economy where giving things away will increase their reputation, as well as the chances of getting and keeping jobs. Artists, on the other hand, traditionally try to earn money by selling their work.

Even if the production methods of art have changed radically, the art market is still built on scarcity. Galleries will produce a limited number of copies of a video or photograph even if this medium potentially allows for unlimited copying and redistribution.

With all the talk of sharing that comes from the world of F/LOSS, I have never heard much about just how freaking scary it can be to share. Sharing means giving up control. Letting go of control can be very, very difficult.

Of course, it is potentially beautiful, too. Recognizing that my understanding is limited, allowing someone else to find something in my work that I had never seen before can be beautiful and fulfilling. Yet, having someone reinterpret my artwork is not the same as someone coming up with a clever new use for a sorting algorithm. My art deals with people and emotions. That is why there is the possibility for such a reinterpretation to hurt me.

(There’s always the option to keep something for myself. Assuming current copyright law, good health and some cooperation from my progeny, it will subsequently take about 130 years before it lapses into the public domain.)

Anyway.

Whenever something wonderful takes off, it feels like a beginning. But I don’t want to take the idea of beginning too literally. It’s more like the experience opens up new possibilities. At the same time, it seems to make up for the hardship experienced up to this point.

Madonna also treats the first time as a metaphor. She references the prototypical first time in "Like a Virgin." Yet it’s not about the first time, it’s about when it’s like the first time. Open source developers work like Madonna. Projects are well underway before they reach 1.00. This is not the first release, this is the first release that works like the developer wanted it to initially. What the first attempt should have been like but necessarily couldn’t.

In fact, many projects never even reach 1.00. Most things never happen. When David Bowie shook the scene with his Ziggy Stardust character, he'd already had a trial run with a band called Arnold Corns, for which he styled a fashion designer to be the lead singer.

Release early release often is a Torvalds maxim. In F/LOSS, next to the projects that take flight, we get to see all the other projects as well, the entire primordial soup. We don’t just get Ziggy Stardust, we get Arnold Corns too.

It’s a mess, frankly. This might not work for the Dijkstras of this world. F/LOSS encourages a mindset of bringing together disparate sources to make something new. This is why artists could potentially feel at home. There’s never a clean slate when you make a work of art or design. We are informed by our personal history, we are informed by all the other works we know. Bowie, on Ziggy, said “it just seemed perfectly natural for me at the time to put together all these odds and ends of art and culture that I really adore.” Never mind that everything always comes from somewhere, when it starts to work together, it feels like something new. It feels like you’ve just begun.

--------------

Anything awesome has a gestation period. But we just happen to love believing in spontaneous creation.

--------------

Today, web fonts are most useful at large sizes where the rendering problems associated with hinting do not occur, where images of text are common. Since the beginning of the web, a lot of large-size text on webpages has not been typeset in overly-familiar system fonts. Instead, headlines are frozen into static images. This can be only just accessible at best. Today, the accessibility of the web impacts everyone because it defines our ability to machine-translate the webs of other language cultures.

--------------

I have never heard much about just how freaking scary it can be to share. Sharing means giving up control.

--------------

Session 1 - September 24 according to Jérémy and Jiacinto

The goal of this session was to explain the principles behind F/LOSS software (GIMP and others). These programs are conceived by programmers who have decided to develop, on Linux (to start). Freeware: These softwares are free to use and let the possibility open, for users capable of programming, to modify the code of the software and, by connection, its functions. Knowing that the license for a similar Adobe program might be 700€ and that it takes more than one program to make a work flow, freeware is something of obvious interest.

! freeware ≠ open source → libre and open source do not mean monetarily free

The goal of our first project is to create an alphabet made up of photos. Choose a theme, take photos of objects which fit the theme. Each object has to start with a specific letter of the alphabet.

The photos are then modified using GIMP, followed by Inkscape to turn a bitmap-based image ( JPG, tiff, PSD in GIMP ) into a vector-based image (the drawing is written by the computer in the form of coordinates and points, making it infinitely scalable, allowing scale changes without quality loss). Once in a vector format ( .ai in Adobe Illustrator ) the image can now be used as a font after some manipulation with FontForge.

Onto the job, kids!



Session 2 - October 1according to Edwin and Flore

In this session, we familiarized ourselves with pixels in order to better understand that digital image matrix (acquiring, creating, processing or storing in binary form). We also learned about the existence of a multitude of image formats like .tiff (for high quality, lossless images) or .jpg (images compressed with an algorithm which simplifies pixels of similar colours). We also deepened our use of GIMP, working directly on our laptops. Not only that, but we had an introduction to vectorising images which allowed us to visualize the next step in our alphabet-building process.



Session 3 - October 15 according to Viola and Gwenael

Some notions on ASCII art: images are replaced by text → 128 characters monospace, which means that all letters have the same pitch. If the image is crowded, all it takes is to change the typeface.

Vectorisation with Inkscape: vector-based formats are made up of mathematical coordinates, not pixels. It's possible to have access to the source code of vectors in an image and also to change, at will, the source code.



Session 4 - October 22 according to Adrien

We took a look at the command line - which provides a space for executing actions by typing text in a window provided for that purpose. This allows us to carry out actions in batches, like reducing the size of multiple images by inputting a single line, modifying a whole text, executing actions in multiple programs at once... Once we have that figured out, it could prove to be very useful and advantageous.

Next, we had an intro to FontForge, a program for creating fonts. We learned how to import images to make a font, modify them...

http://www.erg.be/erg http://www.ludi.be/erg/doku.php?id=notes_de_cours



--------------

F/LOSS in the classroom

Ludivine Loiseau

--------------

The École de Recherche Graphique in Brussels has a history of introducing F/LOSS tools to its students. But in the autumn, 2010 semester, it went all out. Ludi OSP taught the first instance of a class solely devoted to F/LOSS-based design tools and tactics. Collected below are reactions and recollections from the class, translated into English.

--------------

The unicorn tutorial

ginger coons

--------------

On vectors, nodes and the power of simple examples

--------------

I remember my first introduction to nodes and vector-based illustration. When I was about seven years old, my father, who was a high school tech teacher at the time, sat me down in front of Corel Draw 3. Up until that day, I had seen the program as a repository of clip art, not knowing what I could actually do with it. He loaded a clip art horse. Everything changed when he showed me the node selection tool. The previously clean line drawing suddenly had a mass of dots all along its outline. He explained that these were nodes, the points defining the shape of the horse.

And then the magical bit: he had me select the node at the apex of the horse's ear. When I clicked and dragged that node, the horse changed. The ear elongated, following my mouse. He instructed me to move the node a little distance and then drop it. The horse was no longer a horse. Elongating that ear had turned it into a unicorn.

Since then, I've learned more about how nodes really work and what can be done with them. But that lesson still sticks in my head. It was an incredibly powerful introduction. It started a (so far) life-long love of vectors. A love of all their extensibility, elegance and possibility.

So I present to you a horse. More accurately, it's just an outline of a horse, no shading, nothing fancy. It's a horse with two pointy ears, one of which has a little node at the apex. You can find the SVG file on our website. If you want, you can download it, open it up with Inkscape or whatever vector manipulation program you use, and turn it into a unicorn. To me, it's the most powerful, understandable first introduction to nodes and their possibilities.

--------------

Pierre Marchand is the creator of Fontmatrix, the leading F/LOSS font management program.  



Dave Crossland: You were working as a graphic designer [when you started working on Fontmatrix].

Pierre Marchand: I pretended to be a graphic designer. But I wasn't really a graphic designer. I started because I had this little, little, little agency and no clients. So I started to play a bit with programming. There was this program to compose text by hand. And there was, in this program, a font chooser. You could choose a font file and it was not enough for me. So I was looking for a font manager, didn't find one. Because I was working a lot with FreeType it was quite trivial for me to just have something to display fonts. All in all, it was really for my own needs.

What happened next? I posted on the Scribus mailing list. I wrote a little font viewer manager. And what happened next? Still no clients. Still time to program. At this point, nobody but regular Scribus users were following the list and you start to do something and Wow! people are interested in the project and come with some user feedback. And there's a lot of raising your self esteem. It works well for that. And so I did continue, because in my regular work, it was completely the reverse. No clients at all and no client, no client. So I had just enough money to live and with coding, you need nothing more than a computer. Coming from being nobody to being someone. So it was very interesting for me... And when you're doing something interesting, people come to you, they listen to you and you can discuss things you have no occasion to do face to face. So it was the first push to work on Fontmatrix. My own needs and after that, the good reception by people.

There were people contributing in terms of ideas...

A lot. From being very reactive. If someone tells you that he would like something like a new feature and one hour later, there's the feature in the Subversion repository, they're happy. And they come back again and again and say "I would like that and that..." It's very useful for the developer because you can't have all the ideas. You're just alone and you have your own ideas, but that's just a subset of what's possible. So it's really interesting to have a lot of user input. And because you are very reactive and you're ready to just stop doing your stuff and work on what people want, they are happy and they come back again. So it's a...

Cycle.

You were working on it, kind of in your spare time, while you were doing freelance graphic design work.

It was not exactly my spare time. It was my main time. 

Because you were filling out the extra hours in the day that weren't being filled by clients. And you were building the tools that you needed for yourself as a designer. 

It wasn't even exactly that. When I finished school, after that, I spent ten years working on my art work, etc. And I stopped because of things, life. But I was very frustrated about stopping. And when I started working on Free Software, writing Free Software, for me, it was a means to come back. To come back to something creative. The more I work on Free Software, the more I write code, the more I think that there is really creative work there. 

In programming?

Yeah. Not like code as art. I don't like that. It's not exactly that. But the practice of writing code can give you something like the practice of doing art work. But the result is not the same. You have to go a bit further to make it a real art work. And it's what I can do with OSP nowadays. And Constant. I take my handcraft of writing code and I turn it into something artistic. But it's another work.

You need to start somewhere. When you start by writing Free Software, you are writing the place to do this work. Free Software gives you access to the machine, the culture, the coding. You can't do real art work with computers without going Free Software. Because if you try to do that by using Microsoft APIs to write, you're still a user. You don't own the machine. The machines own you. It's the same for Fontmatrix. Even if now, I'm more productive at working on Fontmatrix, really focused on user integration and interactions, still, the way I do it is looking for new ideas and finding something like a compromise between my way of coding, which is a bit weird sometimes, and the way the users can be part of the project. To form a community. Because I need this community to continue to work...

To motivate you.

Yeah. And to make it meaningful. I mean, if there's nobody to use your software, there's no point in publishing it.

How do you think the Free Software community, the Free Software programming community, can engage people who work professionally on those kinds of jobs? There are user interface designers, interaction designers, who do that kind of work professionally.

Hmm. The huge work was really to change my mind. About the software I was writing. To try to think as a user. In the state it was, I heard users complaining "I can't do this, I can't do that with the software." And I was just answering, each time, "Yes, you can do that. You have to go to this menu and this sub-menu and click there and it's possible. It's possible this way or this way." To make it usable by these people, you have to just change that into when you hear something like that, to think "Okay, so the menu is not in the right place. They can't find it. So I will change my software to make it accessible for users." So I started like that.



Because it's a Qt application, it's cross-platform, right? So it's going to be useful for users on Windows and Mac OS. 

Useful not as a font manager. They have font managers. But it will be useful to make them Free. 

What do you mean?

I mean that they have enough font managers on Mac OS X and Windows platforms. They don't need FontMatrix as badly as on Linux, where there's nothing at all.

But the value of FontMatrix as a font manager comes from its Free Software touch. I mean that if you want to run Free Software on your platform, whatever it is, you need Free Software. So if you're on Mac and you have a feeling it will be great because you've tried Inkscape and it was cool and you've tried FontForge and it was cool and you want to continue to use Free Software, maybe you need a font manager. So you're seeking a Free font manager and there's nothing. But hopefully, there will be FontMatrix in a state of development ready for daily use.

But as a font manager, what it will bring for these users is really the freedom and the community, the direct access to developers. But as a font manager itself, it won't be more than Suitcase or something. In the first case. After that, as people can come with new ideas, we can imagine that it will bring something really new, because it's really fitted to their needs. But in the first place, it'll just be a font manager, as a font manager for all those users. And if they're used to using font managers like Linotype Font Explorer, etc., they won't be amazed by it. It will be just another font manager. And the real value will come from the freedom of the software.

--------------

Pierre Marchand talks Fontmatrix, responsiveness and user engagement

Dave Crossland interviews Pierre Marchand

--------------

Eric Schrijver (Amsterdam, 1984) is a visual artist who makes installations and performances. Eric teaches Design for new media at the Royal Academy of Art in The Hague. He is inspired by open source and programming culture.

http://ericschrijver.nl

--------------

A Reader's Guide to Libre Graphics Magazine



In this magazine, you may find concepts, words, ideas and things that are new to you. Good. That means your horizons are expanding. The problem with that, of course, is that sometimes, things with steep learning curves are less fun than those without. 

That's why we're trying to flatten the learning curve. If, while reading Libre Graphics Magazine, you encounter an unfamiliar word, project name, whatever it may be, chances are good there's an explanation. 

At the back of this magazine, you'll find a glossary and resource list. The glossary aims to define words that are unique to the world of Libre Graphics. The resource list provides valuable information about tools, licenses, whatever items we may be mentioning.

Practically, this means that if, for example, you're reading an article about DejaVu (see pages 54 to 57), you can always flip to the back of the magazine, look up DejaVu in the resource list and become quickly informed about it. This provides some instant gratification, giving you the resources you need to understand, in a moment, just what we're talking about.

We hope you like our system.



--------------

Hugin, our favourite panorma stitching program, has a pack of new features, including masking and a mosaic mode.





--------------

An SVG based presentation program, allowing users to create on a movable canvas. Say goodbye to slides.

--------------

A major new release of Blender. Road tested on production of Sintel.

--------------

An overview of Inkscape 0.48, geared specifically towards the needs of web designers. 

--------------

Another year, another Blender Foundation project. Sintel is a short film showing Blender at its best and most beautiful.

--------------

Makes version control painless. Allows for folder based workflow, but with the added power of full-on version control.

--------------

[I]f you... us[e] Microsoft APIs to write, you're still a user. You don't own the machine. The machines own you.

--------------

Software

In the process of bringing this first issue into being, our work was made easier by the excellent F/LOSS design tools available.

Scribus 1.3.8 was used to typeset and arrange all the magazine content into a final layout for print.

Inkscape 0.48 was the tool of choice to create the magazine cover, as well as the "Have your say", calendar and UpStage ads.

GIMP 2.7.2 helped us to retouch and edit columnist photos and most of the bitmap illustrations, as well as converting some PDF files into bitmap for inclusion in the layout.

Phatch 0.2.7 saved us from the tedious work of converting images to grayscale for printing.

Pdftk 1.41, a tool for merging several PDFs into one (among many, many other uses), helped us streamline our workflow and produce the layout using smaller Scribus files instead of a big, heavy one.

Git 1.7.0.4 was the version control system that we used to share work between ourselves, as well as providing it for anyone interested in taking a peek into the process. Gitorious was our service of choice when it came to publishing the magazine repository. We also used gitg and git-cola to provide a graphical interface for the Git repository.

A spreadsheet for sorting through submissions was created in OpenOffice.org Calc.

Finally, all plain-text editing was done inside Gedit and vim.



Fonts

Linux Libertine body text 

Linux Biolinum footnotes and captions

PropCourier Sans titles

Linux Libertine and Linux Biolinum are two beautiful and well-crafted typefaces by the Libertine Open Fonts Project.

PropCourier Sans is the official custom typeface for Libre Graphics Magazine. It is a friendly fork of Open Source Publishing's NotCourier Sans, a monospaced typeface which sports the statement "We are not here to be polite." PropCourier Sans is the proportional version of NotCourier; it is an ongoing effort that will be worked on and updated on each issue of Libre Graphics Magazine, following the "Release early, release often" principle. You'll find some inaccuracies and inconsistencies in the current version, but we'd rather just use what we can have now instead of going for a finished and polished typeface -- whatever "finished and polished" might mean. We decided that we would prefer being honest than being right, hence PropCourier's tagline: "We are not here to be correct."



Finds

If you flip over to page 20, you'll find a text illustration based on a photo of Pierre Marchand. This effect was achieved with a Python script written by Alexandre Truppel who is 14 years old, and is attending tenth grade of school in Portugal.

Alex popped up in Hacklaviva (a hackerspace in Porto, Portugal) during a Python meet-up. It didn't take long for everyone to be amazed by his coding-fu, complete with his own full-fledged GUIs. When we asked Alex if we could use his programs to create illustrations for the magazine, we got a positive response, along with some heartening remarks: "I never thought my programs could be used for anything else besides random fun. I did the program as a challenge to learn more Python and to work with images. Then I used it to make some gifts for my family."

Ana Carvalho and Ricardo Lafuente make up Manufactura Independente, a design research studio based in Porto, Portugal. Their design practice orbits around the principles of free software and free culture.

http://manufacturaindependente.org

--------------

If, at first...

ginger coons

--------------

Freed Fonts, Strong Web

Dave Crossland

--------------

This is the first day of my life

Eric Schrijver

--------------

Sozi 

http://sozi.baierouge.fr/wiki/doku.php

--------------

Sintel

http://www.sintel.org

--------------

Blender 2.55b 

http://www.blender.org

--------------

SparkleShare0.2b1

http://sparkleshare.org

--------------

Hugin 2010.2.0

http://hugin.sourceforge.net

--------------

Inkscape 0.48 Essentials for Web Designers (book)

https://www.packtpub.com/inkscape-0-48-essentials-for-web-designers/book

--------------

Silver Bird's Nest illustration created from a picture by Flickr user PerpetualPlum

http://www.flickr.com/photos/perpetualplum/3598355084/#/

Rainbow gradient found on a post by user Erroneus on InkscapeForum.com

http://www.inkscapeforum.com/viewtopic.php?f=5&t=5470



--------------

Masthead

Editor's Letter

Production Colophon

Notebook

New Releases

Freed Fonts, Strong Web Dave Crossland

This is the first day of my life Eric Schrijver

F/LOSS in the classroom Ludivine Loiseau

The unicorn tutorial ginger coons

Pierre Marchand talks Fontmatrix, responsiveness and user engagement

Showcase

    Laura C. Hewitt

    Pete Meadows

    John LeMasney

Applying F/LOSS as a final user and                                        not dying in the attempt Lila Pagola

Visual literacy: knowing through images Eric Schrijver

Interview with Ben Laenen of DejaVu

Resource List

Glossary and resources

--------------

4

6

7

8

11

12

14

16

18

20    

25

26

32

36

41    

47

54

59

62

--------------

<!-- cover nest excerpt --><g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1"> <g id="g38378"> <path inkscape:connector-curvature="0" id="path16611" d="m 225.82431,132.46721 17.4409,2.28159" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20472" d="M 219.05596,168.22787 206.77063,154.72065" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16607" d="m 200.15959,153.04307 6.61104,1.67758" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20475" d="m 200.53421,145.30828 -4.18015,8.67534" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16603" d="m 187.09434,155.9977 9.25972,-2.01408" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20478" d="m 179.79965,172.78437 10.93431,9.37073" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16599" d="m 185.55904,188.1525 5.17492,-5.9974" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20481" d="m 178.84708,190.69835 11.21845,-1.49125" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16595" d="m 188.4914,197.19743 1.57413,-7.99033" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20484" d="M 177.24272,206.10799 188.6485,194.60558" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16591" d="m 193.52753,194.47025 -4.87903,0.13533" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20487" d="m 185.82537,210.62216 4.50234,-10.46706" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16587" d="M 203.96617,209.97213 190.32771,200.1551" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20490" d="m 189.94005,222.13775 15.43965,7.54394" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16583" d="m 210.98227,239.21531 -5.60257,-9.53362" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20493" d="m 220.62756,234.14166 3.26974,-5.35131" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16579" d="m 220.13361,238.53825 3.76369,-9.7479" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20496" d="m 211.04891,253.12733 14.02267,4.51152" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16575" d="m 217.84395,258.81557 7.22763,-1.17672" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20499" d="M 235.52707,275.36791 221.32259,264.14945" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16571" d="m 213.72293,257.05891 7.59966,7.09054" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20502" d="m 208.0218,253.35236 5.57663,11.79035" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16567" d="m 209.27331,260.58973 4.32512,4.55298" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20505" d="m 199.60985,236.51039 3.44245,19.358" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16563" d="M 203.92834,267.76898 203.0523,255.86839" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20508" d="m 232.37264,281.58421 0.0181,-10.11156" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16559" d="m 238.70086,275.93809 -6.31014,-4.46544" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20511" d="M 255.65977,285.20723 242.85724,271.62589" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16555" d="m 237.26082,264.76584 5.59642,6.86005" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20514" d="m 217.80045,251.12069 4.8807,-5.01021" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16551" d="m 229.06569,251.69164 -6.38454,-5.58116" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20517" d="m 244.23402,265.17763 1.87905,-1.65287" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16547" d="m 241.72371,256.46134 4.38936,7.06342" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path20520" d="m 249.24923,257.98813 -10.87798,-3.25914" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> <path inkscape:connector-curvature="0" id="path16543" d="m 228.131,254.39223 10.24025,0.33676" style="opacity:0.86138611;fill:none;stroke:#c2c2f5;stroke-width:1.29999995;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:0.8627451;stroke-dasharray:none;marker-start:url(#EmptyTriangleOutS);marker-mid:none;marker-end:url(#EmptyDiamondS);display:inline" /> [...] </g></g>



--------------

Production Colophon

Ana Carvalho & Ricardo Lafuente

--------------

Where

Make Art 2010 in Poitiers, France.

--------------

Notebook

--------------

What

Art and design festival focusing on exciting uses of Free/Libre Open Source Software.

--------------

Who

Presented by GOTO10, who are also well known for producing the Puredyne distro of GNU/LINUX.

--------------

Prettiest

Type specimens from VTF foundry. They're funny, they're well designed and there's a truly exhaustive selection of dingbats and stars. Without a doubt, the most exciting bits of paper I've seen in a while.

--------------

Most Collaborative

Nancy, the dingbats font from Open Source Publishing. Visitors to the exhibition space were invited to create their own interpretation of hundreds of characters, from squares to arrows to numbers in circles. Resulted in an impressive array of different styles and levels of interpretation. 

--------------

Least Expected

One visitor who, upon seeing the gender breakdown of artists in the room, promptly asked why there were so few women. And then wanted to talk about it. For an hour. Good debate all 'round.

--------------

Most Immersive

Electroacoustic concerts held in the planetarium. Relaxing end to a long day, with some truly neat visuals.

--------------

EDITOR'S LETTER

--------------

PRODUCTION COLOPHON

--------------

MASTHEAD

--------------

NEW RELEASES

--------------

TYPE DESIGN

--------------

TYPE DESIGN

--------------

SCHRIJVER

--------------

SCHRIJVER

--------------

DISPATCHES

--------------

DISPATCHES

--------------

FIRST TIME

--------------

FIRST TIME

--------------

THE INTERVIEW

--------------

THE INTERVIEW

--------------

THE INTERVIEW

--------------

Images under a CC Attribution Share-Alike license

Photo of ginger coons by herself.

Photo of Dave Crossland by Mary Crossland.

Photo of Eric Schrijver by himself.

Photos of VTF type specimens in "Notebook" section by ginger coons.

Type specimen in "F/LOSS in the classroom" by Ludivine Loiseau.

Illustrations in "The unicorn tutorial" by ginger coons.

Images in "Applying F/LOSS as a final user and not dying in the attempt" by Lila Pagola.

Photo of Ben Laenen by Dave Crossland.

All images in the "Showcase" section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.

Fontmatrix screenshot from Resources section by Wikipedia user Gürkan Sengün. http://en.wikipedia.org/wiki/File:Screenshot_Fontmatrix.png

Inkscape screenshot from Resources section by Ana Carvalho.



Images under other licenses

Photos and images of OSP DLF/Nancy in "Notebook" section by Open Source Publishing. Free Art License.

DejaVu specimen page adapted from a specimen by Benjamin D. Esham. Public Domain. http://dejavu-fonts.org/wiki/File:DejaVu_specimen.png

Blender screenshot from Resources section by Wikipedia user DingTo. GNU General Public License. http://en.wikipedia.org/wiki/File:Blender3D_2.4.5-screen.jpg

FontForge screenshot from Resources section by George Williams from the tutorial 'FontForge, An Outline Font Editor'. http://fontforge.sourceforge.net/overview.html

Assumed to be covered by a version of the Modified BSD license. Anyone in doubt should direct themselves to: http://fontforge.sourceforge.net/license.html

GIMP screenshot from Resources section by Wikipedia user IngerAlHaosului. GNU General Public License. http://en.wikipedia.org/wiki/File:Gimpscreen.png

Scribus screenshot from Resources section by Wikipedia user Kocio. GNU General Public License. http://en.wikipedia.org/wiki/File:Scribus-1.3-Linux.png



General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

--------------

