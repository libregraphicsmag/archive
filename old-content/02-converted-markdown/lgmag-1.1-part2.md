Camouflage and Mimicry Illuminated Drawing



Statement

When I was a little kid and heard adults mention "illuminated manuscripts", I visualized an enormous dark archive, reminiscent of cathedrals and the basement vaults of libraries, full of rich images that were softly glowing. I was disappointed to discover that it just meant books with weird little pictures in the margins, however intriguing those books might be. Still, decades later, when someone mentions illuminated manuscripts, the first image that comes to mind is my childhood vision and I have to consciously remember that it's illustrated texts, always with a sense of disappointment.

Flight and Pursuit is the adult creation of my childhood vision. I have combined work from my adult artistic practice and interests with the fairy tales, myths and dreams of childhood to create the imagery. It is an adult narrative of my flight from and pursuit of technology; my love/hate relationship with computers, motorcycles, microwaves, compound bows, my hearing aids, clocks...the entire paraphenalia of technological apparatus. Combined with pre-Internet childhood imagery, it becomes an expression of my flight through, and pursuit of, time and memory.

--------------

42 x 18". Watercolor, aquarelle pencil and pastel on paper stretched over what more or less amounts to a fancy lightbox. From a series of illuminated drawings I call Flight and Pursuit.

--------------

Laura C. Hewitt

--------------

Warfly 1



There is an Asian story that butterflies carry the souls of the dead to heaven. Early Native Americans believed that part of the human soul was captured in photographs. Combining these two ideas, I used media war images to create butterflies after having a dream in which human created patterns became so pervasive that butterflies started mimicking them. Could war images be turned into something beautiful? Dangerous thought. This is one of many butterflies I have designed using computer graphics then hand painting and drawing on the computer print. The butterfly's pattern is a digitally enhanced war image.





--------------

12 x 14". Watercolor and computer print of war image.

--------------

Pete Meadows is a Montreal-based illustrator and musician. His illustration process heavily features GIMP, which he uses to create the angular, but organic line quality and transparancy that you see on these pages. 

--------------

Pete Meadows

--------------

Bearbot 1Bearbot 2Bishops 1Mini buddy meets Gargantuar 1Kitty Bot 2

--------------

Visual literacy:knowing through images

Eric Schrijver

--------------

The experience of our own time is mediated through images, but we tend to represent the past in a verbal-discursive way1. That means the model of the history book, with its emphasis on words and stories. In its attempts at establishing a reliable reputation, a resource like Wikipedia exhibits an extremely conservative take on representing knowledge2: an image that goes along with an article is never more than an illustration in the literal sense of the word.

This why all the images we know of child labour show unhappy children: these pictures have been selected to fit to our story of the condemnation and subsequent abolition of child labour.

But when browsing through contemporary image archives, you will find most images show smiling children. This makes perfect sense: a photographer's visit is a special and exciting occasion.

It's the shock we get when confronted with these images that suddenly makes it possible for us to relate to our past. By freeing the image from its iconic role, we can stop seeing what's depicted as nothing but logical steps in a larger story. We can identify, both with the children who are depicted and with the photographer taking the picture.

In the pictures taken for the American National Child Labor Comittee, most depicted children look happy.



Our archives house many photographs of Hitler, and he smiles in most of them.



When you see his picture on the back of a book, Mark Twain is a long dead writer. When you see the whole series, you see he was a super star.3

Grotesk typefaces like Helvetica did not start out with the ‘neutrality’ imparted on them by the Swiss design school—they were whimsical display fonts.

Forms we employ for ironic effect did not start out that way.

All these are examples of how ‘reading’ the images from the past can show us how our own perception and norms have changed since then, allowing us to better understand both past and present.



--------------

The artistic community of Córdoba first noticed F/LOSS in August 2003. There was an introductory talk about the philosophy of free software, given by Grulic, the local F/LOSS group. The talk was part of an art and technology event, the main topic of which was local initiatives merging technology and artistic practices.

That talk was a breath of fresh air for our little world which was exploring how net.art initiatives were going mainstream. For most people, the information in the talk remained just some ideas, with no effect on daily software tool use.

In 2004 many of us began some touristic trips to GNU/Linux. We discovered there were barriers of knowledge and practice preventing us from adopting it permanently. In March 2005, after an inspirational workshop given by Constant and Studio XX in Buenos Aires, the idea of creating a special project to help artists test and migrate to free software emerged.

The first step was a dedicated install party in May 2005. On that occasion, we provided and helped to install Mandrake GNU/Linux with some customised software packages specially selected by us and added to the distribution by Grulic. We also hosted some short demos by artists with software such as Blender, Audacity, and GIMP in order to present the general characteristics, possibilities, limitations and defining traits of these programs.

Knowing the importance of further support in the first experience, this step was continued and documented in a shared wiki, a mailing list and later, a blog. From the original 20 participants, less than half continued the regular use of free software. Some had serious problems with specific hardware support, such as video capture cards and professional sound cards. Others never got used to new interfaces, with the loss of expertise that comes with them. The commitment shown by Grulic, which created a kind of  direct line to us, in order to solve problems and discuss doubts, was remarkable.

The Nómade project's other line of work was producing complete graphic design pieces with free software. Most of them were completed thanks to the good faith of the Vía Libre foundation, an NGO devoted to spreading free software philosophy, as well as other local F/LOSS and free culture organisations in Argentina. So far, we've designed several books, posters, booklets and other pieces.



The Context

The use of software in graphic design has a short history. It feels as if software appeared in the late eighties and began to be massively used in the early nineties. Of course, there were only one or two very well known brands.

Later, when the first successful graphical user interfaces appeared for the PC, they came with lots of new software for drawing, post-production images, painting (maybe you remember the amazing /Painter/). We furiously installed and tested these programs. Artists and designers were living in heroic times, installing from 10 or more floppies, over the course of almost an hour. And we were so proud of our machines with 65,000 colours.

Those were times of intense experimentation. We experienced daily discoveries and improvements, as well as heavy battles for standards. In the late 90s, some stability was achieved. That stability meant the end of some experimentation, the definition of right ways of doing things, and also the standardisation of some options - those related to apps and file formats, for instance. These became not just the preferable or the better formats, but the only usable ones.

This was the case until F/LOSS arrived on the design horizon. For many designers, F/LOSS was not much more than a curiosity, meant for amateurs, or even a solution for young and poor beginners.

This context has had, until now, two main contributors: education and the market. In Argentina, education is the determinant for further professional practice. As in other fields, the first pedagogical approaches to digital tools were very instrumental. Whole lessons -and even syllabi- were built around a specific software, with absurdly detailed “step by step” guides for a tool in constant change. Of course, the tool chosen was “the only one,” well known and preferred in the actual market.

Why choose F/LOSS in that context?

For the Nómade project, the option for free software is about aligning poetics and politics, form and content. It is a way of  being consistent with the topics communicated by the pieces we design. We use free software and culture as actions against monopolies, patents, DRM, etc.

At the same time, the experience became a live example of viable alternatives to prohibitive and/or illegal software for graphic design. We began the discussion around which software we use in professional practice and which software we teach in design schools.

Choosing F/LOSS was also a way to escape the pressure of productivity and market determining times, regimes and “right” ways of doing. It represents a possibility of reinstalling experimentation as procedure and criticism on the politics behind tools. Of course, the F/LOSS model promotes the debate about circulation of culture and authorship.



One Work-flow

By the end of  2006, when we began the first big project -the book Artificial monopolies on intangible goods. We made a bet. None of us actually knew if we would be able to finish the book using only F/LOSS, or how acceptable the results would be.

There were some critical known aspects to explore, such as 4-colour output and colour management and others to test, like the actual compatibility of the generated output files in a common production process. With the first book, we gained experience about how to manage a better work-flow for preventing mistakes and countless reviews. 

The first problem appeared around fonts and their installation. Because we were newbies in Ubuntu, but used to installing and uninstalling fonts visually, it was tough to venture into the command line.

In my personal case, as a former QuarkXpress user, I began to work with Scribus. After using it for some hours, the work-flows became clear in their rough aspects. Maybe the most critical was the problem of footnotes. Having started from original texts in OpenOffice, with styles and notes at the bottom of the page, we quickly discovered incompatibilities between the two programs. Styles were imported by Scribus in a very messy way and when we saved the original .odt files as plain text, we lost parts of sentences. In order to solve this, the original texts were formatted again, setting notes at the end of the text, as a way to easily separate them from the main text. After that, footnotes were designed manually.

The next challenge was dealing with the Random Access Memory (RAM) that Scribus consumes with files bigger than 30 pages. The book had 130 pages. Our plan was to include all content in only one file for ease in using functions such as the dynamic table of contents, the same master pages, etc. But we quickly discovered, after passing 40 or 50 pages, that the text editor began to work very slowly. So, we decided to divide the book into five sections and put it all together, for the web version of the book, with pdftk.

For the third book, the editors decided to use a wiki (https://wiki.vialibre.org.ar/moinmoin/vialibre/Publicaciones/VotoElectronico) instead of originals in .odt. It was a better solution with regard to text styles and footnote management, but it generated a new problem: while on a wiki, changes were made directly there during a much longer period.

Printing process issues

We considered it a great advantage to have a friend in charge of the printing process, as we were interested in experimenting with new ways of doing this work. His patience and help were invaluable: he was a kind of bridge with people in pre-press and a backup for problems that, of course, occurred.

The first book was also an ambitious design. Pages were printed in two colours, and the originals for each colour were made directly by the printer from the Scribus file, opened in a Microsoft Windows system (although the book was designed under Ubuntu). Then we discovered the first problem: some fonts were interpreted by MS Windows with a slight difference in metrics, and some parts of the design were misplaced. So, we had to check and manually correct every page again.

The cover was done with Inkscape, GIMP and converted to CMYK mode with Krita. The first challenge here was to create a file with vectorial quality for drawings and typos and without colour differences between the raster images. Here, we faced some problems around the PDF output made from Inkscape, including a raster image with an alpha channel. We had to change the strategy, integrating all the vectorial background, except illustrations with raster images, eliminating the transparency.

The next question to solve was to find out which output would be best accepted by the pre-press company. We exported EPS, PDF and even an Adobe Illustrator files in order to deal with any incompatibility. The first time, the EPS was the “winner” because the people in pre-press preferred to open our original and manually set the resolution of vectors.

From this first experience, we have repeated the process with a few variations. The second book was designed in Córdoba, but printed in Costa Rica by someone who didn't even know what software we were using. We decided not to tell the printer because of distance, and because we didn't talk directly to him, but only to our client.



From doing to teaching

This practical experience, using free software for design under normal conditions of production, has been a useful point for introducing the same software in design and photography classrooms. Actual designs done are a kind of backup for overcoming the first reaction of students and colleagues confronting the possibility of using free software in the actual field of visual communication.

However,there are some barriers that are important to consider when discussing the option of free software in learning design.  In Argentina, pirated software is very easy and cheap to get and install, even - or especially - in public institutions. Faced with this reality, it is difficult to focus the discussion only on ethics or ideological positions. These theoretical debates tend to have no effect on the actual practices of students, teachers or professionals.

In the case of the photography school at which I work, free software was installed in 2005 in a very forced way, due to licensing problems. When this happened, the minimal discussion teachers were able to have around the tools we use (and teach) led us to this unacceptable analogy: using just one brand and version of software in digital photography classes is like teaching about exposure techniques with just one brand and model of camera. For the camera, this would be considered clearly unacceptable and non-ethical for both teachers and students. However, using just one brand and version of software is a common practice and even one preferred by many.

So, ideological positions plus proofs of efficiency are needed for opening the debate, but are not enough. Tools are not neutral, and probably will be always a field of political negotiation in terms of distribution, possibilities of improvement and customisation.

On the one hand, we need to go back to the attitude of the early times of intense experimentation, when designers explored, tested and rated software without any external pressures (such as using certain software because it is a synonym for “professional”).

And, on the other hand, scholar communities should deeply analyse their practices on software teaching and learning. Both teachers and students should promote critical approaches and capabilities to adaptation and flexibility in who learns, instead of creating captive users with no social awareness.

How can final users collaborate with developers?

Perhaps the most critical point is how to build bridges between users (students and designers) and developers so we can  balance and improve the tools for everyone. Most people don't know that communities around applications exist, and that it's possible to participate. This is the very first task necessary among teachers and students.

But I believe designers and students should be involved in a more direct way. They should be stimulated to test, compare, discuss, and modify developments, and probably the best person to do that is the teacher. In many cases, it's just a matter of time and dissemination of information: as in other great initiatives of free culture, often the point is not disagreement or lack of interest, it's just ignorance on why and how to do it. Concrete environments and ways of participation where the added skills of everyone can make the difference.



http://www.nomade.org.ar/sitio

--------------

Applying F/LOSS as a final user and not dying in the attempt

Lila Pagola

--------------

about this text

This text is an adaptation of a talk presented at LGM 2010 in Brussels, Belgium. It shares the experience of some pieces of design (books, booklets, and brochures) completely made with F/LOSS by a team working in Córdoba, Argentina from 2005 to present (with many changes), called Nómade project.

The project emerged in a moment of growing interest in the Free Software philosophy among digital artists. Its main purposes are being an “interface between artists and [F]ree [S]oftware” and building actual ways to close the gap between theory and practice while providing support to “newbies” from the creative field. In other words, a proposal for evolving from affinity and theoretical discussions, to becoming users.

The project developed this basic idea through two lines of action: the first one was creating a platform for helping a special group of guest digital artists from different fields (musicians, animators, graphic designers, web designers, etc.) to encounter, test and migrate to F/LOSS alternatives while doing their routine tasks. The second was producing graphic design completely with free software.

--------------

Ben Laenen is a maintainer and developer of DejaVu, the widely used but not often mentioned interface font.





Ben Laenen: My first action on DejaVu was drawing Greek glyphs which was actually a huge project in retrospect. I was naive at the time. I thought "oh, let's draw a lot of glyphs quickly and it'll work out." But actually, [the DejaVu community] weren't that harsh on me so the first glyphs immediately got in.

ginger coons: For all of this, you use FontForge, right? 

Yes.

Your first experience with FontForge: how was it?

I know what you want me to say: It's ugly! That's basically the first thing everyone thinks when they open FontForge. It's not really appealing to many people. Afterwards, we learned from George Williams [developer of FontForge] that he doesn't really care about the looks of FontForge. I think the bad looks are [meant] to scare away people who aren't really serious about it. So you have to be serious about it or you won't go on with it. 

Had you been using DejaVu beforehand? 

I can't remember about that, actually, if I used it. I think I was using Bitstream Vera at the time. And I wanted to have Greek glyphs in that style, but there weren't any. So I was Googling around, and then came upon DejaVu. I mean, at the time, the default font for most Linux distributions was Bitstream Vera, because DejaVu wasn't big yet. So, Googling around, I found DejaVu, that they were a project making new glyphs for Bitstream Vera, so I entered into it and... 

So wait. Why did you want to do Greek in the first place? What did you need the Greek for?

I've always loved Greek. I did Greek at school, old Greek, but I wanted to learn a little bit of new Greek. I just find it a nice alphabet. And I was trying to learn it a bit by moving the interface of my computer. Instead of English, which I'm used to, I wanted to have it all in Greek one day. Just try it out, see if it could work. 

Have you managed that yet? 

I actually managed to do that for a few months, then I said "Let's go back to English again."

Were you using Deja Vu for your system font when you did that? 

Not at the start. I had to first draw the glyphs, of course. But after a few months, then I switched my interface to Greek. 

Your interface was in Greek...

Using my own font. Actually, drawing a font was something that goes many years before that. But nothing came from it, really. 

What was that?

I was just thinking about it today. Actually, a font should be quite easy. You just draw one glyph and then you can use it in the entire text, the same glyph. That's my first idea of making a font. But I wasn't really serious the first years. 

So how long ago was that?

I think ten years. 

Ten years before you started on Deja Vu?

Yeah. 

So you've been thinking about it for a long time. 

Yeah. It used to be at a time when you had bitmap fonts. That was pretty easy, you just click some pixels and then you have a font, pretty easy.

Did you ever do one of those, or no?

Not really. If you really go back in time, I was using QBasic. I think that's more than ten years. I actually made a program which drew its own glyphs and I had to, in QBasic, really make arrays of all the letters to put on the screen. 

That's dedication.

I even had sort of vectorised fonts by drawing the lines, but just by coordinates.

Like choppy vectors. 

Basically, just glyphs were drawn as polygons, not filled in but just straight lines. And that made a letter. 

Fifteen years ago, or more, that's not bad. 

Yeah, and a very old computer. It wasn't very fast, either.

Early 90s, right?

I think yeah. But I mean that's nostalgia. I think it's a recurring theme, actually. If you really go back to it. And now... When Deja Vu came in, the real fonts business really started for me. 

What's the uptake been on Deja Vu since five years ago, when you started on it?

Well, the first years were pretty intense because lots of glyphs were added at the time. We became default font in a lot of distributions [of GNU/Linux]. So the first years were pretty busy. Had a lot of contact with the maintainers from distributions, who really pushed it. So that made DejaVu the default font. The last few years, it's slowed down a bit. We are stabilising. 

How long had Deja Vu been going before you started on it?

I think a year or so. It started with Štěpán Roh... He's from [the Czech Republic] so he needed a few more glyphs that weren't in Bitstream Vera. So he started with that. It was pretty slow before [Denis Jacquerye and I] entered the project but I came with Greek, Denis came with a lot of glyphs needed for African languages which included the International Phonetic Alphabet, for example. [The International Phonetic Alphabet is] not really all needed, but it came with it, almost. Afterwards, Cyrillic,I think Cyrillic came into it. Later, Arabic and Armenian... We have Georgian, which was made by a former Prime Minister of Georgia. 

What?! Really? 

Who lives in exile in Finland. 

And he made the Georgian set for DejaVu?

Yeah.

That's incredible.

That's one of our weirdest contributors. Besarion Gugushvili. 

If the previous system font in most distros was Bitstream Vera, or Bitstream Vera Sans...

Bitstream Vera Sans was the font used in the interface.

So, if it didn't have all these international glyphs, have you guys made localisation an easier thing? 

This is why people from distributions were pushing to get DejaVu instead of Bitstream Vera. Because if you couldn't even use Bitstream Vera in most eastern European countries, then you have a big problem if you want to see Linux taking off over there. And there weren't really a lot of alternatives. We were the first real font that could be used on screen as an interface font which had those glyphs. So there wasn't really much choice. 

Have any competitors to DejaVu sprung up?

Not really. There were a lot of Vera derivatives which were trying to do the same. Štěpán Roh was actually quite clever because he basically ported all those glyphs from the other fonts and tried to get them in DejaVu. So we ended up with the most complete set of glyphs. And we became a little community around the font, which also helped. The other derivatives were missing the community. It was basically one person doing the work.

Have your work habits, your toolchain, your workflow, your process, changed at all in the last five years of working on Deja Vu? 

We're still using FontForge and the build scripts have changed a little, but that's not really important. I mean, it just made it a little bit easier for us. Instead of using a Python script you could just use a makefile. Doesn't really make it a lot easier. It's a little bit easier for maintenance. 

So you're basically doing the same thing that you did, work-wise, five years ago, when you started? 

Making the font itself... Basically, you become better at it, of course. But the tools are the same. 

From your first faux-vector type in Basic up until now, you've come a very long way. 

Yeah. I didn't know anything about fonts at the time. I didn't look into trying to guess what font formats there were at the time. I just said "Hey, I want letters" and decided to make something. I had a lot of time to program all day. But that doesn't work if you have to do it properly. No one will use that font. 

So are there any secrets hiding in Deja Vu?

No. Unfortunately not. There's one nice glyph which tells you the point size of the rendering. Say it's displaying the number eight and you make the font bigger, it'll display ten or fifteen. Hinting magic. It helps a lot in debugging. Everyone defines their own point size. But their point size isn't the same as our point size. So just pick that glyph and it tells us what number it is. Then we know what we have to debug.

Most people don't notice the fonts. The font is good if no one notices it. When I drew the Arabic glyphs, for example, suddenly everyone noticed. So we knew it wasn't really that good. 

If someone wants to contribute or make an improvement, what do they do?

They'd usually ask about it on the mailing list. But most people enter the project by just making a few glyphs beforehand and then just showing it in one go to us on the mailing list. A few weeks ago, someone came with ancient Italic glyphs. Italic, used 2000 years ago in Italy. 

So have you incorporated Ancient Italic?

Not yet. He first made his alphabet and then put a patch on the mailing list, so we have to review that patch. Then we say "Oh, this can be improved" and he'll just work on it again.

--------------

Interview with Ben Laenen of DejaVu

ginger coons interviews Ben Laenen 

--------------

I just said 'Hey, I want letters' and decided to make something.

--------------

By virtue of being called a showcase, we assume that you already know quite a bit about this section. Many design and art publications prominently feature work in their excitingly glossy pages. 



The difference, of course, between our showcase and those others is that we do care about the process and tool chain behind the works we show. For that reason, every work featured in this section has been created using F/LOSS graphics programs. Sometimes, those F/LOSS tools are among other techniques. Sometimes, they are the only tool in use. 



Our goal, in the showcase, is to show you just how stunning F/LOSS graphics can be. We hope you'll agree that the works on these pages are exciting, well crafted and very much worth pulling out and sticking to your walls.



--------------

Resource List

--------------

Blender

A powerful F/LOSS 3D animation application for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

FontForge

A F/LOSS font editor for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

Fontmatrix

A F/LOSS font management and selection tool for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

GIMP

A raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows..

--------------

Inkscape

A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

Audacity: A F/LOSS sound editing application for GNU/Linux, Mac OS X and Microsoft Windows.

Bitstream Vera: A typeface released under a permissive license, allowing modification. 

Blender: A powerful F/LOSS 3D animation application for GNU/Linux, Mac OS X and Microsoft Windows.

Command line: A text only user interface, allowing users to input commands without the intervention of cursors and graphical user interfaces.

Constant: An arts organization based in Brussels, Belgium. The parent organization of Open Source Publishing.

Distro/Distribution: A specific configuration or "flavour" on GNU/Linux, often designed with a particular purpose in mind.

DRM (Digital Rights Management): Technologies (of whatever sort) which prevent users from making certain uses of the larger technologies to which the DRM is applied. 

FontForge: A F/LOSS font editor for GNU/Linux, Mac OS X and Microsoft Windows.

Fontmatrix: A F/LOSS font management and selection tool for GNU/Linux, Mac OS X and Microsoft Windows.

Free: As in freedom, or often, that which is or is of Free Software.

Free Software: A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.

GIMP: A raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows.

Inkscape: A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

Mailing list: An email based forum through which subscribers may receive announcements, view or participate in discussion.

Mandrake: A distribution of GNU/Linux. Now known as Mandriva.

OSP (Open Source Publishing): A graphic design collective which is dedicated to producing high quality work using F/LOSS tools.

PANOSE: A typeface classification system first developed in the 1980s.

Qt: Pronounced "cute." A F/LOSS development framework which functions across platforms and provides a common set of visual elements. 

Repository: A stored collection of software packages, from which those packages may be dowloaded and installed. 

Subversion: A F/LOSS version control system for GNU/Linux, Mac OS X and Microsoft Windows.

Torvalds: Linus Torvalds, the originator of Linux.

Ubuntu: A particularly popular distribution of GNU/Linux, produced by Canonical Ltd.

Version control: A means of managing changes (and allowing reversion) to a commonly held body of work, most often a software project. 

vi: A F/LOSS text editor developed in the 1970s and still in use. Available for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

Glossary and resources 1.1

--------------

John LeMasney is a designer, artist, writer, poet, technologist, open web advocate and open source evangelist.

John started his project 365Sketches in January 2010. His goal was to produce one sketch per day using only Inkscape. He would then publish it in his blog http://365sketches.org, set up in Wordpress, under a CC-BY-SA license. The underlying purpose of John's daily exercise was to improve his skills using Inkscape but, as he told us, the result was deeper than that. In his own words:



I've created a daily reminder for myself and others of the power of open source. I've gathered a community of about 200 people who watch the project, about 20 real fans, and I've gotten a lot of design and consulting work. I've also made quite a few friends. I feel like I'm doing my part to help develop, advocate and advertise Inkscape.

John's plans are to go on with the project, drawing upon different tools: in 2011, GIMP would be the tool of choice, whereas Blender might be slated for 2012. In the following pages, you can see a small sample of John's work. To look through the whole project, do visit his blog.

--------------

John LeMasney

--------------

SCRIBUS

A desktop publishing software for GNU/Linux, Mac OS X and Microsoft Windows.

--------------

1. Wikipedia, the deeply conservative and traditional encyclopedia,All The Modern Things, 2008 http://brianna.modernthings.org/article/147/wikipedia-the-deeply-conservative-and-traditional-encyclopedia



2. Interactieve presentatie handschriftenMuseum Meermanno-Westreenianum, 2003 http://collecties.meermanno.nl/handschriften/.

--------------

3. Inspired by spreads from Fantastic Man http://www.fantasticmanmagazine.com

--------------

The DejaVu font family

http://dejavu-fonts.org

--------------

SHOWCASE

--------------

SHOWCASE

--------------

SHOWCASE

--------------

FEATURE

--------------

FEATURE

--------------

FEATURE

--------------

FEATURE

--------------

VISUAL FEATURE

--------------

VISUAL FEATURE

--------------

VISUAL FEATURE

--------------

VISUAL FEATURE

--------------

VISUAL FEATURE

--------------

VISUAL FEATURE

--------------

VISUAL FEATURE

--------------

PRODUCTION COLOPHON

--------------

FEATURE

--------------

FEATURE

--------------

FEATURE

--------------

RESOURCE LIST

--------------

RESOURCE LIST

--------------

RESOURCE LIST

--------------

GLOSSARY AND RESOURCES

--------------

Libre Graphics Magazine 1.1

November 2010

--------------

ISSN: 1925-1416

--------------

