An image is more than the sum of its pixels.



In the context of the traditional art school, we are taught to distrust the “effects” of photo editing software. Why use a digital simulation when you can work with the “true materials” of paint? What are the “true materials” of software, and hadn't we better use those when considering what painting means on a digital canvas? How can digital tools embrace the actual material of the algorithmic rather than merely simulating the analog?

The more we investigated “computer vision” techniques, however, the more we realized computer scientists are using the same techniques, and even employing an approach where techniques are composed almost as the cutups of a visual collage. No time to compute an actual Laplacian of gaussian? No problem, the textbooks offer, just gaussian blur at multiples of the square root of 2 and subtract the results to get a pretty good approximation.

A scanned image is more than a matrix of color values destined only to be displayed as pixels. Viewed through the lens of algorithms, an image is multiple layers of potential interpretations. Each layer tells different story, revealing some aspects, obscuring others. Algorithmic glitches are revealing: is this a letter, or the edge of a roof tile? What are the visual features of text when treated again as an image? The layers also speak to each other; how do SIFT features related to edges, and edges to the automatic detection of text in an OCR software?

http://sicv.activearchives.org

--------------

Active Archives

Scandinavian Institute for Computational Vandalism

--------------

**802.11** Standard specification for the way wireless communication (WiFi) between computers should be processed.

**C**APTCHA “Completely Automated Public Turing test to tell Computers and Humans Apart” is a clunky acronym for the familiar images which show sequences of letters and/or numbers to be typed out, with the purpose to confuse tools which automatically fill out forms, often used by spammers.

**Computer Vision** A research field that encompasses techniques for automatically analyzing and understanding images.

**C**SS A language built to complement HTML and establish the separation between content and style: while HTML sets the document structure and hierarchy, CSS is used to define how every element will look.

**Desktop Publishing (**DTP**)** Programs built for creating publications and other kinds of printed matter, which often offer powerful visual editing capabilities and complex typography.

**Gaussian blur** A common image effect present in most bitmap editors, intended to reduce noise and image focus. Its name comes from the mathematical operation used as the formula to alter the image pixels.

**I**DE A text editor specialized in writing code, usually including other tools useful for developers. Stands for Integrated Development Environment.

**ImageMagick**A bitmap image editor for the command line.

**Kiwix**An offline web browser, originally developed to provide offline access to Wikipedia.

**H**TML The language for describing web pages, enabling the use of everyday features like links, metadata and other information. Stands for Hypertext Markup Language.

**L**AN Local Area Network, a term used to refer to wired networks of many computers.

**Laplacian** A smoothing operation for 3D objects. Similar to a Gaussian blur for points in 3D space, useful for smoothing surfaces and details.

**Markdown** A lightweight markup language, designed to describe rich text features (styles, lists, links, etc) in plain text files.

**O**CR Stands for Optical Character Recognition. Refers to the algorithms and techniques used by software to discern text from images.

**Open Data** A movement defending transparency and openness  of public records and their availability in open formats.

**Processing** A minimal, designer-oriented programming environment, popular for introducing coding to visually-oriented people.

**Raspberry Pi** A small computer about the size of a pack of cigarettes, famous for its hackability and suitability for DIY hardware projects.

SIFT Scale-invariant feature transform, an algorithm used in computer vision to determine shapes and relevant features in images.

**S**SID Service Set Identifier, the technical designation for a wireless network’s visible name.

**Stl** A file format for CAD files, geared towards representation of 3D objects. It has become the “de facto” format for 3D printing.

**+rwx** A reference of a common Unix command, chmod; the +rwx argument makes one or more files readable, editable and executable.

**Tesseract** A free software OCR library, known for its recognition accuracy.

**TeX** A typesetting system devised by Donald Knuth, relevant for its lack of a graphical UI and heavy use of specialized markup.

**U**I User Interface. Refers to the set of disciplines and practices related to designing and implementing the elements which allow people to interact with computers, both in hardware (keyboards, touchscreens) and software (buttons, sliders and layouts).

**Ux** User Experience. Comprises the spectrum of subjects related to UI, considering them under the lens of psychology, ergonomics and human behaviour.

**Wireless beacon** Any device that can emit and receive wireless signals according to the 802.11 standard. Commonly used to refer to routers, but also applicable to laptops or smartphones that can be configured as a WiFi access point.

--------------

# Resources/Glossary

--------------

Search by Image is a series of algorithmic and experimental videos analyzing Google’s image search function of the same name.

Search by Image (Recursively, Transparent PNG, #1) begins with an empty image. This image—a transparent PNG—served as the starting point for an image search whose result acted as the basis of yet another search. This recursive process was repeated 2951 times and then compiled into a video.

The process was made physical through a series of 3 Artist Blankets and one artist book (edition of 250) developed with Thomas Spallek and Florian Kuhlmann, using the same method to gather and montage image sequences.

http://sebastianschmieg.com/searchbyimage

--------------

Printing Out The Internet

Kenneth Goldsmith

--------------

http://printingtheinternet.tumblr.com

--------------

“Bring a thing!” Such a simple and innocuous prompt to incite fear and apprehension in your humble free software developer. A thing? But I write software!

I  had been exploring the idea that careful reading could be a creative process—that the traces from active navigation of media could themselves be media. And I had been writing software around that concept. Perhaps I had achieved at least a virtual tangibility for digital video collections that would otherwise float through the ether-net, leaving traces exclusively for the advertising and spy agencies with an interest in one form of targeting or another. With InterLace, the playback rectangle is always shown in context, and every attempt is made to treat the viewer as a full participant in determining their points of focus.

While my timelines gave graphic form to the passage of time—at least a shadow of the moment—I still didn’t think my software qualified for membership in the world of things. So was born Hyperopia Thing. Building on the Kiwix standalone Wikipedia server, I wrote an experimental wiki interface that would open links inline and implicitly build an “associative trail” in the right-hand sidebar with all of the links, selections, and images clicked on while browsing. To make it a “thing,” I looked to the past and appropriated the book-form of yesteryears’s reference collection: I loaded all of Wikipedia onto a microSD card in a Raspberry Pi, and carved it into an old Dutch encyclopedia. A new research lab should have a copy of Wikipedia for their shelves, I reasoned, even if the physical form was symbolic.

The thermal printer was a joke, but like many good jokes it turned out to be more “true” than expected. Housed inside the physical encyclopedia, I wired up a receipt printer to create an instant record of the reading process, emanating out from the soul of the book. At first I saw the receipts as a gimmicky echo of the digital sidebar, but as I spend more time with them and they outlive the server and database and disk migrations of their digital counterparts, I begin to wonder if there’s something to having a physical trace (a receipt, if you will) that’s worth, well, holding on to.

What traces of us will be left when machines have “learned” from our literature and seem capable of reading and writing and translating our languages with relative fluency? Paradoxically, the word for the digital data inputs fed into computers, the “corpus,” comes from the Latin word for “body.” I was thinking about the assumptions and impenetrable insides of machine learning with a study, PDF To Cognition, that trained a word-image based model of written text. Is a corpus strung together with characters isomorphic to ASCII character codes, or is a corpus compiled of typographic shape, reaching our eyes as image? And does the model ever transcend its inputs, or do its traces—us—forever lurk within?

--------------

Bring a thing!

Robert M OcHshorn

--------------

InterLace (2012-)

The first use of InterLace was in collaboration Eyal Sivan on his web documentary, Montage Interdit.

As a data-based film, continuity is not a linear narrative, but is rather achieved at “runtime” by the viewer, who can navigate by tag, source, or timeline, re-sorting to make new continuities and montage. The act of authorship, then, is in the creation of focus and metadata rather than narrative.

http://montageinterdit.nethttp://interlace.videovortex9.net

Hyperopia Thing (2014)

Custom offline Wikipedia interface and thermal printer, embedded into an old encyclopedia. Offers wireless network with captive portal collaborative encyclopedia; tracks browsing and prints receipts of drifts.

Hyperopia is a device based on the transitive principle that “Reading is Writing” and that if whole is comprised of parts then the parts should stay connected to the whole: look at something closely enough and the truth of the universe is manifest in its every detail. The name “hyperopia” refers to a (supposed) defect of vision commonly known as farsightedness.

PDF To Cognition (2014)

Word-shape representation of text, trained on the Neuroscience literature. By taking PDF journal articles and splitting them into words—without breaking them down into letter—I saved each word as an image, and then built up a basis set of word-shapes. The distance between words is entirely a function of their visual similarity.

To encode a concept of difference and distance into these word-forms, I have used a statistical technique known as Principal Component Analysis (PCA), which is a mainstay of any modern compression and data analysis workflow. PCA takes data as input with any number of characteristics, and forms a basis set of abstract components in the same form as the input data, much like Gilbert Ryle’s Average Taxpayer. In this case, the components are statistically relevant word-forms that represent correlations between different parts of the shape, and PCA functions by enabling projection from any word-form into a combination of its basis set. Moving from a complex word form with, say, 1,500 pixels, and reducing it to a specific blending of 50 basis words is a dramatic dimensional reduction, and it is this reduction that allows for spatialization and comparison of words as shapes. If I take the liberty of calling this compression a “reading” of the word, then PCA is significant because its reading is bidirectional—to read implies an ability to write. Analysis and synthesis are deeply entwined.

http://readmes.numm.org/pdftocognition



--------------

In a few years, 3D technologies will change the way of making things. 3D printing (or additive manufacturing) already allows anyone to create any shape from scratch based on a digital file: a new method of making which is affordable, environment-friendly and, most importantly, allows for endless possibilities of personalization and customization.

This ability to customize products is what might raise the interest of individuals and the demand for 3D technologies in the home: why not design and make things oneself instead of just being a consumer and buying things?

The revolution of 3D printing is just in its first stages and now is the time for experimenting, investigating and discovering unforeseen possibilities of a technology that is called to change the traditional systems of manufacturing. In this process, homemade 3D scanners will play a crucial role, as they enable the capture of physical objects and obtain 3D models which can be stored, modified and shared.  3D scanning provides a base model which can be used by everyone as a starting point for designing and replicating objects with little or no design experience.

The team of Open Color 3D Scan have designed a low cost 3D Scanner to be assembled and used at home to provide the opportunity to capture objects and experiment with all the possibilities of 3D scanning. It sports remarkable accuracy and resolution, halfway between commercial low cost scanners and high quality ones. Our 3D scanner is designed to be built at home with consumer technology that ensures an affordable hardware cost and which allows the exchange of some of its components to personalize it. The scanner works by using a webcam to locate the points at which two laser beams bounce off an object’s surface. The points are mapped out and turned into a point cloud 3D model.

Our 3D scanner is made up of the following main parts: a mountable structure, a laser triangulation system composed of two laser modules and a USB color camera, an Arduino controller, a small stepper motor with its driver and two USB cables. The software that runs the scanner is available for free download, and it can also be developed and improved as it is open source.

The main difficulty of building the scanner and ensuring its proper operation is setting up the precise angles and distances between the lasers, the camera and the object. It is critical that the pieces that make up the structure are hard-wearing and strongly clipped together. To meet these requirements and ensure accuracy in assembling the parts of the structure, we supply manual bending aluminum pieces with precise laser cut self-referenced nodes. These are tab and hole geometrical joints that allow for a perfect assembling of the parts with a minimum error margin and using only a few screws.

Once the structure is built up, the devices and controllers assembled and the software installed in the computer, the scanner is ready to run. For further scanning precision and optimization, the calibration of the camera can be easily adjusted once with a calibration device that is also supplied.

http://www.opencolor3dscan.com

--------------

**Libre Graphics magazine team****:** **How is the 3**D** scan scene developing? Is there a 3**D** scanning scene at all?**

**Open Color 3**D** Scan team****: **Currently, we could speak of two trends in the 3D scanning scene. On the one hand, there are some commercial products developed and oriented to professionals. These are finished products built with complex technology at high prices. Those can of course have many uses, but as the prices are still very high, they are focused on the industry. On the other hand, 3D scanning is following the path started by 3D printing in the open source 3D technology and community. With simpler and more affordable technology, it is possible to build a 3D scanner and scan physical objects at a high quality resolution. These low-cost 3D scanners are, as we might say, in a primary state. The challenge is to build an affordable high quality technology that can be adapted to the different needs of professionals from different fields, such as game developers, physicians, architects or for art and heritage conservation, as well as to approach it to a broader audience.

**Where is the project at?**

We have designed a first prototype that has already been improved to a first Open Color 3D Scanner. Our initial goal is to put our scanner on the market of the 3D community and spread the 3D scanning technology so that it can be tested and improved. To reach the community, we are planning to launch a reward based crowdfunding campaign next autumn.

**Where are you thinking of going from here?**

The idea is to be capable to create a product that could be easily adapted to different professional needs, making minor changes and at an affordable price. With the collaboration of others, we aim to improve our product as well as to discover new possibilities we might not have thought about.

**What led you to start work on this field?**

As most technological inventions, this project started as a game of experimentation and discovery. Our mechanical engineer foresaw the possibilities of consumer and affordable technologies applied to 3D scanning and built the first scanner prototype at home. As we believe in open source technology and in the capacity of individuals to create their own products and improve the technology within the community, we decided to put our DIY 3D scanner on the market and open it to the world. We also intend to be a player in the 3D technology field alongside with the growing 3D printing market and applications.

**What are the backgrounds, skills and motivations ****of the team?**

We are a team of three people: a mechanical engineer, a telecommunications engineer and a journalist. We also have the collaboration of CDEI-UPC (Center for Industrial Equipment Design), a technology innovation center at the Polytechnic University of Catalonia (UPC). Carles Domènech, our mechanical engineer, has been working at CDEI developing projects of mechanical design and is currently the director of the Center. Anna Carreras, our telecommunications engineer, holds two MSc degrees, one in Telecommunication Systems Engineering and another in Information, Communication and Audiovisual Technologies. She has a 10 years expertise in designing several interactive technologies for artistic and commercial projects. The team is completed with Mariona Roca, bachelor in Audiovisual Communication and Translation Studies, who has been working as a copywriter and on the digital marketing field. Our motivation is to develop new projects aimed at a multidisciplinar technologic community and, as doing so, helping to spread the open source philosophy.

**What technologies are you using?**

We are using simple, affordable and consumer technologies: two lasers, a webcam, an Arduino minicontroller and a stepper motor. All of them are available and affordable devices that can be easily found in the market. Besides, the design of the hardware allows adaptation to a wide range of each of them, so that users can build the scanner with technology that they might already have.



**You placed the code under a copyleft license ****(**CC BY-SA**). Can you articulate the reasoning ****behind your license choice?**

We chose a copyleft license because we believe in empowering the community to use technology and develop it. We think making our work accessible might create the necessary feedback to adapt it to the different needs. We want our work and authorship to be recognized and referenced, but our main goal is to approach the technology to as many people as possible so we can all win with it. 

**What would be your advice for others who want to work on diy hardware projects?** 

We think the key goal of DIY hardware products is to find simple and creative solutions and reduce the limitations of the fabrication process to a minimum. So our advice is to make things simple and clear but, at the same time, design hardware that allows for minimum error to ensure that it works properly. Probably the most important thing to take into account is to find the balance between the cost, the accessibility to materials and to promote deeper knowledge of the technology and its features.

--------------

There’s an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well. This issue, we highlight some tools for capturing the world around you.

--------------

# Practical # & Useful

--------------

↑ Contours: shows the contours detected on each image. In addition to tracing the edges, the algorithm connects the lines into a series of distinct segments represented by a different color.

--------------

↑ Red, green, blue:What is significant color information? Contrary to human intuition, for a computer, a white image is an image saturated with red, blue and green. To find the images that look the most blueish, reddish or greenish, we counted only the color values that were superior to the others by a certain threshold.

--------------

↑↗ Lexicality and Texture: These layers are produced using Tesseract, a software for Optical Character Recognition (ocr). An ocr program operates at different levels of granularity. It can detect lines, words, symbols. 

Lexicality shows the words detected in an image, while Texture shows the symbols detected. Texture is configured to be rather tolerant in its understanding of what a character is. It therefore tends to see characters in very unexpected places.

--------------

← Scale-invariant feature transform (sift):sift features are “interesting” points of an image that can be extracted to provide its “feature description.” This description can then be used to identify (parts of) an image even when rotated or changed.

--------------

Earlier this week, an artist asked me what software he should use to manage his business contacts. If he uses free software, would he be acting as a professional?

NAMING

What’s in the picture? Eyes capture grey, black, white. Eyes reformat and transmit information as signals relayed along the nervous system to the brain. The brain, the house of re-cognition assembles the image. The brain remembers, assigns the name: gun.

Our bodies capture information through automatic, unconscious perceptions. Naming comes so quickly we forget that naming is a separate act.

In his 1962 book _The Ticket that Exploded_, William S. Burroughs wrote, “From symbiosis to parasitism is a short step. The word is now a virus.” Words inwardly attach to a host’s nervous system, an inward Other. Burroughs’ language becomes a disease, passed from person to person; words are caught, used, shared.

Experience remains cloistered from language until the person brings experience to language for publication to Others. Or, perhaps this—the internal Other packs the containers for experience [ words ] in imperfect translations.

Who owns the containers [ words ]? Who lays claim to our shared alphabets? Other transactions are taxed, in this world of ours. The words we share—paid when we bundle them into our data plans. 

TROPHY

Thieves stole Rembrandt’s _Storm on the Sea of Galilee_ from Boston’s Isabella Stuart Gardener Museum in 1990. Two thieves pretended to be Boston police officers, tricked the museum guards, tied them up in the basement, stole thirteen works of art. 

We can’t see the canvas now. The canvas became an open question when it disappeared. But I can show you the image of the painting, the sign the painting became when it disappeared. 

The canvas was captured. The sign remains. 

HOW I STOLE THE IMAGE OF THE GUN

I image-searched “handgun.” I chose a picture. I took it—right-click “save to downloads folder.” No, wait—click-dragged from the browser to the desktop. Theirs. Mine. Have I captured it?

How can I steal it, if it is still there, in the browser window, and on my computer. I can take it five, ten, a hundred, a thousand times. 

Theft has changed. Rather than disappearance, multiplication.

Computers work together to create the internet by copying information from one volume to another. My computer copies information to me from an Other computer. Republication, again, again, again. 

TROPHY

Here, remember, the sign, a reproduction of the Rembrandt painting. Here, I stole a picture of it. No, I didn’t dress up as a police officer and steal it. I mean, I dragged a picture from the Wikipedia page about the Gardner Museum theft.

Have I captured it? Is this re-presentation of the stolen painting accurate? How do we know? Where is the true picture of the picture?

Let’s ask Google.

**trophy** |ˈtrōfē|noun (pl. trophies)

1. a cup or other decorative object awarded as a prize for a victory or success.

• a souvenir of an achievement, especially a part of an animal taken when hunting.

2. (in ancient Greece or Rome) the weapons and other spoils of a defeated army set up as a memorial of victory.

• a representation of a memorial of victory; an ornamental group of symbolic objects arranged for display.

This Google image search result captured via “screenshot,” or, “screencap” for screen capture. A single gesture, pressing shift-command-4 [ on a Mac ] turns cursor to crosshairs. Clicking and dragging across visible information allows you to shoot it. 

OWNERSHIP

Our phony policeman holds Rembrandt’s painting, but not the legal right to own it. The canvas remains unseen. Some real policeman might take it back. 

The painting has a price on its head. The museum has legal rights to the painting, rights created by language.

False policemen assumed the appearance of power, handcuffed the guards in the basement overnight. By force, they assumed ownership of thirteen works of art. 

A copy of a policeman, wearing the signs associated with the office of policeman. The double tricked the guard at the door. 

MEMORY

Its theft precludes my visit to the museum to re-member the Rembrandt by re-seeing it. Since a thief has the canvas, I ask Google to re-present the sign. I remember the painting by looking at pictures of its sign. I ask this magazine to publish pictures that I stole-by-copying, pictures that tell you to remember the sign of a stolen painting.

When I look at the picture of the sign of the stolen Rembrandt painting, I create an inward experience. I create a memory, an inner sign of the painting. 

BLADE

A friend of mine makes knives. If he makes a knife for me, it will fit my hand. I can watch him make the knives, he posts photos from his studio on social media. I know the knife will be balanced for my stature, and for how I want to use it. 

A pair of scissors cuts, blade against blade. A knife cuts. Why must we have a steak knife and a fillet knife? A paring knife?

I can use Photoshop to resize images. That’s expensive. I have to pay a monthly fee for that knife, to cut images down to size. I could pay once for Pixelmator or Acorn. But here’s the thing—I like using tools my friends have made. So I take out the GIMP, and do the work, thinking of the hundreds of people who have called me friend and made this knife for me. 

COP

Which cop is real? The policeman-cum-thief, who locked up the security guards? He was real to the guards. The cop who arrived in the morning, after the morning shift discovered the night shift locked in the basement? 

Where is the power—in the sign, or in the access granted by using the sign? The cop-by-uniform accesses the museum. The trick works by manipulating an Other’s obedience to the symbol-set, “police.” 

There is a real power over there. I will access it, over here, with this symbol, this uniform. You will obey me because I enforce your rules. My taxes pay the police to (ostensibly) protect me. I pay the power outside of me to take care of me? Society is so weird.

If I use free software, will it be professional? The semiotics of free, libre, open source reveal the real power is not sitting on the desk plugged into the wall. Instead, we remember, the computer only ever does exactly what we tell it to—unless it doesn’t. The knife cuts, unless it slips.

I keep looping back to our remoteness from power, and people being tricked. The experience of a human body, like that of a computer—everything through one viewport. Commerce, sanctity, vulgarity, much depends on the aims of the user. 

BRAND

If I use the free software, will it be professional? If I sign this urinal and put it into an art show, will it be art? Is my signature a brand? What context demands “branding”? 

Brands, burned into the skin of animals, indicate ownership. Sign becomes scar. The scar on the animal’s body tells the Other to whom the animal belongs. 

If I lease Illustrator from Adobe, who does the tool obey? If the client knew what vector design was, would they be hiring me? They know this name though. Adobe’s tether of a financial transaction runs in the background. 

This is professional work, right? If the client questions the quality of the work, is the problem in the tool—or between the keyboard and the floor? Give me a moment while Inkscape launches.

PAPER CLIP

The American television show _The X_-_Files_ had, in the opening of the third season, a plot that turned on possession of stolen data encrypted in Navajo and written to digital tape cassette. There was only one cassette. The cassette passed from hand to hand, from bad guys to good guys and back.

Prior episodes referred to the content of the tape as documentation of government cooperation with aliens but by episode 2 of season 3 we have forgotten what the data says. We are more concerned with who has it.

The data represents power over the Conspiracy. Its existence signifies power.

The data was decrypted by Albert, a Navajo. The agent overseeing the X-Files instructed Albert and twenty other Navajo to memorize the data. Multiple living copies rendered the power of the single copies moot. 

Though three episodes were devoted to this fugitive recording, the Conspiracy was not exposed. _The X_-_Files_ played for six more seasons, and returns to television for a miniseries later this year. You can watch copies of _The X_-_Files_ on Netflix or other online streaming services.

--------------

On capture [ a semiotic meditation ]

Jessica Fenlon

--------------

←↓ Rendering, Pdf To Cognition. Words are digitally represented from an image-form basis (here trained on academic neuroscience papers) instead of character codes. The pca basis forms a continuous space of text-image.

--------------

→ Screenshot, Pdf To Cognition. The training data (from critical theory reading lists) can reappear in unlikely arrangements when parsing new data.

--------------

Online interface: http://hyperopia.meta4.orgSnapshots: http://rmozone.com/snapshots/2014/02/hyperopia



--------------

WayBack Machine

The Internet Archive maintains this tremendously useful resource, which allows anyone to step back in time to see previous versions of a web site.

https://archive.org/web





Textfiles.com

A comprehensive repository of ASCII art and all sorts of text files found in the wild in the 80's and 90's during the golden age of the BBS.

http://textfiles.com





Hybrid Publishing Resources

Part of the Digital Publishing Toolkit, this wiki gathers many hacks, shortcuts and advice for creating publications using contemporary practices for digital and print outputs.

https://gitlab.com/DigitalPublishingToolkit/Hybrid-Publishing-Resources/wikis/home





sK1 Palette Collection

A collection of color compilations, ranging from Android UI to browser-safe colors, released to the public domain by the sK1 Project team.

http://sk1project.org/palettes.php



LOLcommits

Get a snapshot of yourself at every time you commit code, in true LOLcat style. “Git blame has never been so much fun!”

https://github.com/mroth/lolcommits





OpenStreetMap Traces

A repository of GPS tracks and subjective paths, open for uploading your own.

https://www.openstreetmap.org/traces







Setting up a hidden volume

Part of the Hide Your Data book from Floss Manuals, this is a quick guide to easily set up encrypted partitions and keep your hard drive safe from prying eyes.

https://flossmanuals.net/hide-your-data-workbook/setting-up-a-hidden-volume

--------------

The challenge is to build an affordable high quality technology that can be adapted to the different needs of professionals from different fields, such as game developers, physicians, architects or for art and heritage conservation, as well as to approach it to a broader audience.

--------------

**Capture**

ISSUE 2.4, October 2015ISSN: 1925-1416WWW.LIBREGRAPHICSMAG.COM

--------------

Open Color 3D Scan

Anna Carreras, Carles Domènech and Mariona Roca

--------------

Search by Image

Sebastian Schmieg

--------------

