# Colophon

**Capture**

ISSUE 2.4, October 2015

ISSN: 1925-1416

www.libregraphicsmag.com

## Editorial Team

* Ana Isabel Carvalho
* ginger coons
* Ricardo Lafuente

## Copy editor

* Margaret Burnett

## Publisher

* ginger coons

## Community Board

* Dave Crossland
* Louis Desjardins
* Aymeric Mansoux
* Alexandre Prokoudine
* Femke Snelting

## Contributors 

* Birgit Bachler
* Anna Carreras
* Carles Domènech
* Jessica Fenlon
* Kenneth Goldsmith
* Walter Langelaar
* Robert M Ochshorn
* Antonio Roberts
* Mariona Roca
* Sebastian Schmieg
* Eric Schrijver
* Stéphanie Vilayphiou
* Scandinavian Institute for Computational Vandalism

Printed in Porto by [Cromotema](http://www.cromotema.pt) on recycled paper. 

Licensed under a Creative Commons Attribution-ShareAlike license (CC BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to _Libre Graphics_ magazine.

## Contact

Write to us at enquiries@libregraphicsmag.com

Our repositories with all source material for the magazine can be found on [GitLab](http://gitlab.com/libregraphicsmag).

## General

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

## Images under a CC Attribution-ShareAlike license

* Cover and showcase separator by Manufactura Independente.
* Photo of ginger coons by herself.
* Photo of Antonio Roberts by Emily Davies.
* Photo of Eric Schrijver by himself.
* All the images in the Dispatch are by Stéphanie Vilayphiou.
* All the images in In Practice are by Birgit Bachler and Walter Langelaar.
* All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA. 
* Photos in the article Search by Image, in pages 30-32, are by Thomas Spallek. The image in page 33 is by Sebastien Schmieg.
* Photos in the article Printing Out the Internet are by Marisol Rodríguez, courtesy of LABOR.
* Image of a gun in the essay On Capture is by Wikipedia user Nukes4Tots. [Original](https://commons.wikimedia.org/wiki/File:Glock_17C_cropped.jpg)


## Images and assets under other licenses

* Graphics in Index and Practical & Useful are based in an image from page 29 of the book Practical Wire Rope Information and Useful Information on the Drag-line Cableway Excavators, under the Public Domain. [Original](https://www.flickr.com/photos/internetarchivebookimages/14592415089)
* Illustration for Antonio Roberts’s column is a modified version of Closed Eye by Yaroslav Samoilov, under CC BY. [Original](https://thenounproject.com/search/?q=eyes&i=98222)
* Illustration in New Releases is based in Haloir, ou séchoir, pour la fabrication du fromage de Camembert, from the book Les Merveilles de l’Industrie ou, Description des Principales Industries Moderne, by Louis Figuier, under CC BY. [Original](https://www.flickr.com/photos/fdctsevilla/4305560559)
* Images for Active Archives, by Scandinavian Institute for Computational Vandalism (SICV) are under the Free Art License. The original images are: Leaf, from Schedel’s Weltchronik, printed by Anton Koberger in Nuremberg (1493), from the collection of Guttorm Guttormsgaard; scan of the first page of the essay Digital Textuality: The Implicit (Re-)Turn of the Gutenberg Galaxy, by Wolfgang Ernst, available [here](http://www.obs-osv.com/bilder/Report%20from%20the%20Gutenberg%20Galaxy_1.pdf).
* Image of Storm on the Sea of Galilee, by Rembrandt, in the essay On Capture, is under the Public Domain. [Original](https://commons.wikimedia.org/wiki/File:Rembrandt_Christ_in_the_Storm_on_the_Lake_of_Galilee.jpg)




