* Editor's Letter -- **Capturing Capture**, ginger coons
* Notebook -- **Outils Libres Alternatifs**
* Production Colophon -- **Dry layout**, Manufactura Independente 
* New Releases
* Column -- **Evasive Maneuvers**, Antonio Roberts
* Column -- **Tonight we’re making web sites like it’s 1999**, Eric Schrijver
* In Practice: **+rwx data and pipes: Better design for otherwise non-writable infrastructure and unreadable data**, Birgit Bachler and Walter Langelaar
* Dispatch: **FONS, A recipe to make fonts out of bitmap images**, Stéphanie Vilayphiou
* Showcase
  * **Active Archives**, Scandinavian Institute for Computational Vandalism
  * **Search by Image**, Sebastian Schmieg
  * **Printing Out The Internet**, Kenneth Goldsmith
  * **Bring a thing!**, Robert M Ochshorn
* Essay -- **On capture [ a semiotic meditation ]**, Jessica Fenlon
* Interview -- **Open Color 3D Scan**, Anna Carreras, Carles Domènech & Mariona Roca
* Resources/Glossary

