# Dry Layout

## Manufactura Independente

Layout programs, in general, lack automation and modularity. After another issue of the Libre Graphics magazine, where most of our time was spent in front of Scribus, we decided to address the topic. We’ll focus on Scribus since it’s the tool we mostly use for layout.

Design workflows contain a great deal of repetition, and layout suffers particularly from this. In layout work, mundane tasks—such as importing images, creating text boxes, importing text, applying text styles, setting up text flows, distributing content—take up significant time and offer little space for creativity or enjoyment. It is a particularly mechanical work that we wish could be automated whenever possible.

In coding circles, there’s a common best principle that’s usually summarized as an acronym, DRY—meaning “Don’t Repeat Yourself.” It refers precisely to the best practice of automating common tasks and minimising repetition or redundancies in code. It makes sense that repetitive tasks are scripted or automated to make layout a less robotic experience.

Besides this, layout software presents a bubble-like way to work. Scribus follows an application-centric model, where most elements are managed from inside the tool. When text is imported, it is copied over to the Scribus document. This means that, from that point on, any change to the text needs to be made within the layout software—changing the original text file will have no effect. Contrast this to the way DTP software treats images, which are usually linked to their source files. The approach of linking text to its source file is a feature we’ve been missing for years in layout editors.

By now we haveve pretty much covered the two things we miss the most in our layout tool—automation and modularity—and we can dive into what we see as the ideal scenario: having the ability to map text boxes to text files, written in Markdown or even HTML.

This would go beyond importing text as we have it now: the text box would be linked to a text file, and reflect any file changes. Each time the file is modified, the layout would update itself to reflect the new state (including semantic notations from the editor such as bold, italic, quotes, titles and subtitles).

With this magazine as a use case, it would mean any last edits, additions and fixes to the text could flow directly into the layout. Since there are always last minute edits to be made, which happen in tight timeframes where work is rushed, it’s common that the final corrections are done in the layout document and not in the source text files. The effect is that the source files are not in sync with the layout document, making it much more difficult to re-use the magazine content without needing to open Scribus to get the latest version of the text. 

If there is a subsequent change in the text file, we have to be attentive to the commit logs to manually check the changes and repeat them on the Scribus document.  A proper way to link text files could allow external contributors to participate in the editing process without the bottleneck of firing up Scribus to integrate edits into the layout document.

All of this can probably sound like a nitpick, but the underlying issue is one of the core dilemmas of digital publishing: the relationship between content and styling. In DTP software, content and styling are tightly integrated into each other. 

Scribus does offer a powerful internal text editor, but it struggles to represent paragraph and character styles, and thus is not a proper replacement for a link-based model. (While Scribus does have a HTML importer, what it does is to copy the HTML file’s content to a text box—not exactly the functionality we outlined. Nevertheless, it’s a rather handy feature if preserving markup and styling is important.)

----

An opposite paradigm has been developed for the web: HTML and CSS embody the paradigm of total separation. 

In its early days, HTML would be used to store not only content, but also all the graphical rules to render that content (such as fonts, colors or visual spacing). CSS was brought in to deal with the inevitable confusion, and effectively propose a separation between the actual content and the directives it should be styled with. 

There is also a sense of modularity in how content is written and rendered, with the editing tool (text editor or web IDE) being distinct from the viewing tool (the browser). The content/style distinction became important and useful: editors can focus on the content, whereas designers can work on the style, all without stepping on each others’ toes.

This workflow becomes tricky to implement in a single-application model.  With DTP applications, the text is contained inside the layout file, as well as the style settings.  Editors are forced to edit text directly inside the DTP application, even though a simple text editor would suffice for most edits.  Even designers might not need to launch the full application to make a small color tweak and export a PDF file (another feature request: export Scribus files to PDF from the command line).

In an ideal scenario, the layout file would be laid out in a format that could be opened by other, smaller applications.  However, the complexity of DTP makes for file formats that are complex, hard to understand and often opaque. 

This is unfortunate, since it would make sense that DTP would also benefit from the Unix way of doing things: having small tools that can be chained together. 

We’d like to do with Scribus what we already do with ImageMagick: use and make small tools for jobs that can be easily automated.

The TEX model also springs to mind. It avoids the “huge GUI-centric application” problem by doing all its rendering on-demand, but has a steep learning curve in order to get to grips with its automated layout settings and the ways to fine-tune or hack them. This balance highlights the challenges behind creating a powerful, DRY-oriented layout tool that is also approachable by non-specialists, a holy grail of sorts.

However, there’s promising developments in tools that propose alternative models for desktop publishing. 

- [Flat/Even](http://xxyxyz.org/flat) by Juraj Sukop proposes a code-based solution to create layouts, using Python scripts that generate PDF files. The code aspect immediately enables generative experiments with layout.
- On another front, [Html2Print](http://osp.kitchen/tools/html2print) aims to create complete print layouts using only HTML, CSS, JavaScript and a web browser. 

All that said, Scribus is still our main tool for everything layout-related: it produces flawless PDF files, takes good care of complex text and handles dozens of pages without hitches. Version 1.5 is just out, and we’re rooting for the Scribus team to keep rocking with a fantastic tool.


