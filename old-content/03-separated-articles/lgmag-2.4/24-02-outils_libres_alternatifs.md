# Outils Libres Alternatifs

## Sarah Garcin, Bachir Soussi Chiadmi & Raphaël Bastide

[Outils Libres Alternatifs](http://outilslibresalternatifs.org) means “Alternative Libre Tools” in French. This non-profit initiative gathers Sarah Garcin, Bachir Soussi Chiadmi and Raphaël Bastide around a shared goal: to promote and broadcast free/libre and open source tools for creativity. Workshops are the perfect way to focus on a specific creative practice.

The first OLA workshop (OLA #0) took place in Paris, in May of 2015. Each workshop is organised according to this protocol:

1. The team of Outils Libres Alternatifs chooses a date and a place to organise a 3-day workshop.
2. A creative practice is chosen, from illustration to publishing, from 3D editing to sound making or VJing. For OLA #0, this practice was “Publishing.”
3. We contact a master, someone who has a technical and creative approach to the chosen practice, someone who wants to share this knowledge. OLA #0 invited Aurélie Delafon, sub-editor of Le Tigre, a beautiful French magazine laid out in Scribus.
4. The workshop is ready to start, with all kinds of creative participants with assorted skills, status or age, ready to experiment during 3 days. Speed talks are planned to allow everyone to share their own practices or hand crafted tools!
5. Three days of experimentations with Scribus, Python scripting, discovering SVG and libre typography in the spacious premises offered by La Générale.
6. Documents begin to appear, generative layouts and illustrations, articles, counterfeit newspaper, textual bug reports...
7. During the last hours of the workshop, a publication is printed and assembled, gathering all the participations. During an “apéritif” (an informal public presentation with drinks), the workshop and its subjects are introduced to the visitors, neighbours and guests. 
8. Time to organise a new event in October. This time the practice will be about 3D editing, and a new master will be invited!

