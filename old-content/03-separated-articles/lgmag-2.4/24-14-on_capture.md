# On capture [ a semiotic meditation ]

## Jessica Fenlon


Earlier this week, an artist asked me what software he should use to manage his business contacts. If he uses free software, would he be acting as a professional?

### NAMING

What’s in the picture? Eyes capture grey, black, white. Eyes reformat and transmit information as signals relayed along the nervous system to the brain. The brain, the house of re-cognition assembles the image. The brain remembers, assigns the name: gun.

Our bodies capture information through automatic, unconscious perceptions. Naming comes so quickly we forget that naming is a separate act.

In his 1962 book _The Ticket that Exploded_, William S. Burroughs wrote, “From symbiosis to parasitism is a short step. The word is now a virus.” Words inwardly attach to a host’s nervous system, an inward Other. Burroughs’ language becomes a disease, passed from person to person; words are caught, used, shared.

Experience remains cloistered from language until the person brings experience to language for publication to Others. Or, perhaps this—the internal Other packs the containers for experience [ words ] in imperfect translations.

Who owns the containers [ words ]? Who lays claim to our shared alphabets? Other transactions are taxed, in this world of ours. The words we share—paid when we bundle them into our data plans. 

### TROPHY

Thieves stole Rembrandt’s _Storm on the Sea of Galilee_ from Boston’s Isabella Stuart Gardener Museum in 1990. Two thieves pretended to be Boston police officers, tricked the museum guards, tied them up in the basement, stole thirteen works of art. 

We can’t see the canvas now. The canvas became an open question when it disappeared. But I can show you the image of the painting, the sign the painting became when it disappeared. 

The canvas was captured. The sign remains. 

### HOW I STOLE THE IMAGE OF THE GUN

I image-searched “handgun.” I chose a picture. I took it—right-click “save to downloads folder.” No, wait—click-dragged from the browser to the desktop. Theirs. Mine. Have I captured it?

How can I steal it, if it is still there, in the browser window, and on my computer. I can take it five, ten, a hundred, a thousand times. 

Theft has changed. Rather than disappearance, multiplication.

Computers work together to create the internet by copying information from one volume to another. My computer copies information to me from an Other computer. Republication, again, again, again. 

### TROPHY

Here, remember, the sign, a reproduction of the Rembrandt painting. Here, I stole a picture of it. No, I didn’t dress up as a police officer and steal it. I mean, I dragged a picture from the Wikipedia page about the Gardner Museum theft.

Have I captured it? Is this re-presentation of the stolen painting accurate? How do we know? Where is the true picture of the picture?

Let’s ask Google.

**trophy** |ˈtrōfē|noun (pl. trophies)

1. a cup or other decorative object awarded as a prize for a victory or success.
  * a souvenir of an achievement, especially a part of an animal taken when hunting.

2. (in ancient Greece or Rome) the weapons and other spoils of a defeated army set up as a memorial of victory.
  * a representation of a memorial of victory; an ornamental group of symbolic objects arranged for display.

This Google image search result captured via “screenshot,” or, “screencap” for screen capture. A single gesture, pressing shift-command-4 [ on a Mac ] turns cursor to crosshairs. Clicking and dragging across visible information allows you to shoot it. 

### OWNERSHIP

Our phony policeman holds Rembrandt’s painting, but not the legal right to own it. The canvas remains unseen. Some real policeman might take it back. 

The painting has a price on its head. The museum has legal rights to the painting, rights created by language.

False policemen assumed the appearance of power, handcuffed the guards in the basement overnight. By force, they assumed ownership of thirteen works of art. 

A copy of a policeman, wearing the signs associated with the office of policeman. The double tricked the guard at the door. 

### MEMORY

Its theft precludes my visit to the museum to re-member the Rembrandt by re-seeing it. Since a thief has the canvas, I ask Google to re-present the sign. I remember the painting by looking at pictures of its sign. I ask this magazine to publish pictures that I stole-by-copying, pictures that tell you to remember the sign of a stolen painting.

When I look at the picture of the sign of the stolen Rembrandt painting, I create an inward experience. I create a memory, an inner sign of the painting. 

### BLADE

A friend of mine makes knives. If he makes a knife for me, it will fit my hand. I can watch him make the knives, he posts photos from his studio on social media. I know the knife will be balanced for my stature, and for how I want to use it. 

A pair of scissors cuts, blade against blade. A knife cuts. Why must we have a steak knife and a fillet knife? A paring knife?

I can use Photoshop to resize images. That’s expensive. I have to pay a monthly fee for that knife, to cut images down to size. I could pay once for Pixelmator or Acorn. But here’s the thing—I like using tools my friends have made. So I take out the GIMP, and do the work, thinking of the hundreds of people who have called me friend and made this knife for me. 

### COP

Which cop is real? The policeman-cum-thief, who locked up the security guards? He was real to the guards. The cop who arrived in the morning, after the morning shift discovered the night shift locked in the basement? 

Where is the power—in the sign, or in the access granted by using the sign? The cop-by-uniform accesses the museum. The trick works by manipulating an Other’s obedience to the symbol-set, “police.” 

There is a real power over there. I will access it, over here, with this symbol, this uniform. You will obey me because I enforce your rules. My taxes pay the police to (ostensibly) protect me. I pay the power outside of me to take care of me? Society is so weird.

If I use free software, will it be professional? The semiotics of free, libre, open source reveal the real power is not sitting on the desk plugged into the wall. Instead, we remember, the computer only ever does exactly what we tell it to—unless it doesn’t. The knife cuts, unless it slips.

I keep looping back to our remoteness from power, and people being tricked. The experience of a human body, like that of a computer—everything through one viewport. Commerce, sanctity, vulgarity, much depends on the aims of the user. 

### BRAND

If I use the free software, will it be professional? If I sign this urinal and put it into an art show, will it be art? Is my signature a brand? What context demands “branding”? 

Brands, burned into the skin of animals, indicate ownership. Sign becomes scar. The scar on the animal’s body tells the Other to whom the animal belongs. 

If I lease Illustrator from Adobe, who does the tool obey? If the client knew what vector design was, would they be hiring me? They know this name though. Adobe’s tether of a financial transaction runs in the background. 

This is professional work, right? If the client questions the quality of the work, is the problem in the tool—or between the keyboard and the floor? Give me a moment while Inkscape launches.

### PAPER CLIP

The American television show _The X_-_Files_ had, in the opening of the third season, a plot that turned on possession of stolen data encrypted in Navajo and written to digital tape cassette. There was only one cassette. The cassette passed from hand to hand, from bad guys to good guys and back.

Prior episodes referred to the content of the tape as documentation of government cooperation with aliens but by episode 2 of season 3 we have forgotten what the data says. We are more concerned with who has it.

The data represents power over the Conspiracy. Its existence signifies power.

The data was decrypted by Albert, a Navajo. The agent overseeing the X-Files instructed Albert and twenty other Navajo to memorize the data. Multiple living copies rendered the power of the single copies moot. 

Though three episodes were devoted to this fugitive recording, the Conspiracy was not exposed. _The X_-_Files_ played for six more seasons, and returns to television for a miniseries later this year. You can watch copies of _The X_-_Files_ on Netflix or other online streaming services.

