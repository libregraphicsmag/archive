
**802.11** Standard specification for the way wireless communication (WiFi) between computers should be processed.

**CAPTCHA** “Completely Automated Public Turing test to tell Computers and Humans Apart” is a clunky acronym for the familiar images which show sequences of letters and/or numbers to be typed out, with the purpose to confuse tools which automatically fill out forms, often used by spammers.

**Computer Vision** A research field that encompasses techniques for automatically analyzing and understanding images.

**CSS** A language built to complement HTML and establish the separation between content and style: while HTML sets the document structure and hierarchy, CSS is used to define how every element will look.

**Desktop Publishing (DTP)** Programs built for creating publications and other kinds of printed matter, which often offer powerful visual editing capabilities and complex typography.

**Gaussian blur** A common image effect present in most bitmap editors, intended to reduce noise and image focus. Its name comes from the mathematical operation used as the formula to alter the image pixels.

**IDE** A text editor specialized in writing code, usually including other tools useful for developers. Stands for Integrated Development Environment.

**ImageMagick**A bitmap image editor for the command line.

**Kiwix**An offline web browser, originally developed to provide offline access to Wikipedia.

**HTML** The language for describing web pages, enabling the use of everyday features like links, metadata and other information. Stands for Hypertext Markup Language.

**LAN** Local Area Network, a term used to refer to wired networks of many computers.

**Laplacian** A smoothing operation for 3D objects. Similar to a Gaussian blur for points in 3D space, useful for smoothing surfaces and details.

**Markdown** A lightweight markup language, designed to describe rich text features (styles, lists, links, etc) in plain text files.

**OCR** Stands for Optical Character Recognition. Refers to the algorithms and techniques used by software to discern text from images.

**Open Data** A movement defending transparency and openness  of public records and their availability in open formats.

**Processing** A minimal, designer-oriented programming environment, popular for introducing coding to visually-oriented people.

**Raspberry Pi** A small computer about the size of a pack of cigarettes, famous for its hackability and suitability for DIY hardware projects.

**SIFT** Scale-invariant feature transform, an algorithm used in computer vision to determine shapes and relevant features in images.

**SSID** Service Set Identifier, the technical designation for a wireless network’s visible name.

**STL** A file format for CAD files, geared towards representation of 3D objects. It has become the “de facto” format for 3D printing.

**+rwx** A reference of a common Unix command, chmod; the +rwx argument makes one or more files readable, editable and executable.

**Tesseract** A free software OCR library, known for its recognition accuracy.

**TeX** A typesetting system devised by Donald Knuth, relevant for its lack of a graphical UI and heavy use of specialized markup.

**UI** User Interface. Refers to the set of disciplines and practices related to designing and implementing the elements which allow people to interact with computers, both in hardware (keyboards, touchscreens) and software (buttons, sliders and layouts).

**UX** User Experience. Comprises the spectrum of subjects related to UI, considering them under the lens of psychology, ergonomics and human behaviour.

**Wireless beacon** Any device that can emit and receive wireless signals according to the 802.11 standard. Commonly used to refer to routers, but also applicable to laptops or smartphones that can be configured as a WiFi access point.

