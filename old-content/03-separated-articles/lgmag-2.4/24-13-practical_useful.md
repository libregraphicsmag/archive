# Practical # & Useful

There’s an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well. This issue, we highlight some tools for capturing the world around you.


### WayBack Machine

The Internet Archive maintains this tremendously useful resource, which allows anyone to step back in time to see previous versions of a web site.

https://archive.org/web


### Textfiles.com

A comprehensive repository of ASCII art and all sorts of text files found in the wild in the 80's and 90's during the golden age of the BBS.

http://textfiles.com


### Hybrid Publishing Resources

Part of the Digital Publishing Toolkit, this wiki gathers many hacks, shortcuts and advice for creating publications using contemporary practices for digital and print outputs.

https://gitlab.com/DigitalPublishingToolkit/Hybrid-Publishing-Resources/wikis/home


### sK1 Palette Collection

A collection of color compilations, ranging from Android UI to browser-safe colors, released to the public domain by the sK1 Project team.

http://sk1project.org/palettes.php


### LOLcommits

Get a snapshot of yourself at every time you commit code, in true LOLcat style. “Git blame has never been so much fun!”

https://github.com/mroth/lolcommits


### OpenStreetMap Traces

A repository of GPS tracks and subjective paths, open for uploading your own.

https://www.openstreetmap.org/traces


### Setting up a hidden volume

Part of the Hide Your Data book from Floss Manuals, this is a quick guide to easily set up encrypted partitions and keep your hard drive safe from prying eyes.

https://flossmanuals.net/hide-your-data-workbook/setting-up-a-hidden-volume

