# Search By Image

## Sebastian Schmieg

Search by Image is a series of algorithmic and experimental videos analyzing Google’s image search function of the same name.

Search by Image (Recursively, Transparent PNG, #1) begins with an empty image. This image—a transparent PNG—served as the starting point for an image search whose result acted as the basis of yet another search. This recursive process was repeated 2951 times and then compiled into a video.

The process was made physical through a series of 3 Artist Blankets and one artist book (edition of 250) developed with Thomas Spallek and Florian Kuhlmann, using the same method to gather and montage image sequences.

http://sebastianschmieg.com/searchbyimage

