Editorial Team

Ana Isabel Carvalho ana@manufacturaindependente.org

ginger coons   ginger@adaptstudio.ca

Ricardo Lafuente ricardo@manufacturaindependente.org

Copy editor

Margaret Burnett

Publisher

ginger coons

Community Board

Dave Crossland, Louis Desjardins, Aymeric Mansoux, Alexandre Prokoudine, Femke Snelting

Contributors

Davide della Casa, Constant, Dave Crossland, Denis Jacquerye, Julian Oliver, Nikki Pugh, Pierros Papadeas, Antonio Roberts, Eric Schrijver, Susan Spencer, Danja Vasiliev, Vinzenz Vietzke.



Printed in Porto by Cromotema (www.cromotema.pt) on recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 



Contact

Write to us at enquiries@libregraphicsmag.com

Our repositories with all source material for the magazine can be found at www.gitorious.org/libregraphicsmag



**Localisation/Internationalization**

ISSUE 2.1, February 2013ISSN: 1925-1416www.libregraphicsmag.com





Images under a CC Attribution Share-Alike licensePhoto of ginger coons by herself. Photo of Ricardo Lafuente by Ana Carvalho. Photo of Ana Carvalho by Luís Camanho. Photo of Antonio Roberts by Emily Davies. Photo of Eric Schrijver by himself.Notebook Mozilla Festival: photos by Flickr user paul_clarke. Image names, from top left to bottom right: Mozfest_10Nov_011, Mozfest_11Nov_048, Mozfest_11Nov_052, Mozfest_11Nov_010, Mozfest_10Nov_134, Mozfest_11Nov_004, Mozfest_11Nov_069.“Crafting type” photos by Rob & Lauren Lim (http://robnlauren.com). These images can be found at http://www.flickr.com/photos/workturn/sets/72157631445394770.All the sketches from “Sketching Creative Code“ can be found at http://www.flickr.com/photos/11701517@N00/sets/72157631514318200. *Sketches names and authors, by order of appearance: Page 16: Suffragette by Sophie McDonald (source code: http://www.sketchpatch.net/view/9Zwh66llS8u). Page 17, from the top to bottom: Stretchy Points Play v2 by DARYL.Gamma and charles_m_dietrich (source code: http://www.sketchpatch.net/view/Lqr5gWHLlg0). Tapestry 4 by DARYL.Gamma (source code: http://www.sketchpatch.net/view/yEeWLKQDCrM). Peaks v3 by Kim Asendorf and DARYL.Gamma (source code: http://www.sketchpatch.net/view/ZC83eYHWq1r); Knots Weave v3 by DARYL.Gamma (source code:http://www.sketchpatch.net/view/oOLMrflXAK8). * Page 18, from the top left to the bottom right: Kind Of Pollock by DARYL_Gamma and eiichiishii1 (source code: http://www.sketchpatch.net/view/GeIursTDPjj). Cushion by DARYL.Gamma and conroy (source code: http://www.sketchpatch.net/view/L8arQDcCGYZ). 80's mania by Sophie McDonald (source code: http://www.sketchpatch.net/view/naotkeywUPi). Where Is She Going by Sophie McDonald and DARYL.Gamma (source code: http://www.sketchpatch.net/view/auPpktqHG9o). *Page 19: Magma Jumping Peaks v2 by Kim Asendorf, DARYL.Gamma (source code: http://www.sketchpatch.net/view/N6fOWgdKacJ).“Crafting type” photos by Rob & Lauren Lim (http://robnlauren.com). These images can be found at http://www.flickr.com/photos/workturn/sets/72157631445394770.Credits for the photos of the piece “La Langue Schaerbeekoise/De Schaarbeekse Taal.” Photos from pages 30, 31, 33 34 and 35 by Pablo Castilla (with the exception of the first top photo in page 34). All other photos are by Constant.All images in the “Showcase” section can be attributed to the creators mentioned therein. All are licensed CC BY-SA with the exception of “La Langue Schaerbeekoise/De Schaarbeekse Taal” which is under the Free Art License.

Images under other licensesIllustrations from the “Picture Feature” are in the Public Domain and can be found on WikiMedia Commons.Denis Jacquerye's photo by Michael Murtaugh (http://gallery3.constantvzw.org/index.php/LGRU-Friday-Shared-Vocabularies/P2245659) under the Free Art License.

GeneralAdvertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

