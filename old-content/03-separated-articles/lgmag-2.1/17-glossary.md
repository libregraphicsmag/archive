
BLENDER: A powerful 3D animation application for GNU/Linux, Mac OS X and Microsoft Windows.

BUG: A technical error in either software or hardware. Apocryphally believed to have been popularized by insects causing faults in early analog computers.

CMS: Stands for Content Management System. Software installed on a server in order to provide a simple framework for editing web pages. WordPress and Drupal are examples of content management systems. 

COMMAND LINE: A text-only user interface, allowing users to input commands without the intervention of cursors and graphical user interfaces.

CREATIVE CODING: Programming not for explicitly functional purposes, but for artistic or creative expression.

CREATIVE COMMONS: A suite of licenses designed to allow creators and users of works flexibility beyond that offered in traditional copyright.

DOMAIN EXTENSION/TOP LEVEL DOMAIN: A component in every domain name. Common TLDs such as ".com" or ".net" are available to anyone, while regional TLDs such as ".ca" or ".pt" may only be available to residents of the countries they represent.

F/LOSS: Stands for Free/Libre Open Source Software. Software which has a viewable, modifiable source. It can be modified and redistributed.

FIREFOX OS: An operating system intended for use on mobile phones. Developed by Mozilla.

FONTFORGE: A F/LOSS font editor for GNU/Linux, Mac OS X and Microsoft Windows.

FREE: As in freedom, or often, that which is or is of Free Software.

FREE SOFTWARE: A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.

FRONT END/BACK END: A distinction in web development between code which runs on the computer of the person viewing a given site (front end) and on the server which hosts the site (back end).

GIMP: A raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows. 

GLITCH ART: A form of art which attempts to produce or leverage bugs or mistakes in software and hardware.

GNU/LINUX: A group of operating systems which are built on the Linux kernel and components from the GNU project, among others, which are widely distributed and freely modifiable.

INKSCAPE: A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

JAVA: A programming language and platform developed by Sun Microsystems, intended for nearly universal compatibility with a variety of devices. 

JAVASCRIPT: A scripting language commonly used on websites. 

JQUERY: A popular library used to write and integrate JavaScript efficiently.

KRITA: A drawing application supporting both vector and raster images. Available for GNU/Linux, FreeBSD and Microsoft Windows.

LIBGEOS/GEOMETRY ENGINE OPEN SOURCE (GEOS): A library for rendering geometry. Available for GNU/Linux and Windows.

LIBRE: A less ambiguous adaptation of the word Free. Implies liberty of use, modification and distribution.

LIBREOFFICE: A F/LOSS office suite incorporating standard productivity tools such as a word processor, spreadsheets and slideshow builder, among others. Available for GNU/Linux, Mac OS X and Windows.

METEOR: A JavaScript-based platform for the development of web applications.

MOZILLA: A non-profit organization best known as the developer of the Firefox web browser. 

OPEN DATA: A concept (and associated movement) promoting the public availability and sharing of data. Often associated with efforts to make government data public.

OPEN HARDWARE: Hardware which follows the same principles as F/LOSS, including publicly available, freely licensed schematics.

OPEN SOURCE: See Free/Libre Open Source Software

OPEN STANDARDS: A standard which is available for viewing and implementation by any party, often at no monetary cost.

OPENSTREETMAP: A permissively licensed world map project developed by a community of contributors.

OPENTYPE: A vector-based font format intended to offer more advanced features than its predecessor, TrueType.

OPEN WEB: A concept based around the combination of open standards and open licenses. Follows the ideal that development on the web should follow best practices of accessible and modifiable code and content.

POPCORN.JS: An interactive web framework which allows increased interactivity with video content.

PROCESSING: A programming language and development environment predominantly used for visually-oriented or media-rich projects. Available for GNU/Linux, Mac OS X and Microsoft Windows.

PROGRAMMING LANGUAGE: An artificial language with a restricted syntax, used as an intermediary between computers and human programmers. 

PROPRIETARY: A piece of software or other work which does not make available its source, which is not allowed or intended to be modified or redistributed without permission.

PUBLIC DOMAIN: The legal status of a creative work for which the copyright (or other rights restriction) has expired. A work in the public domain can be used by anyone, for any purpose, without restriction. Licenses such as the Creative Commons CC0 license emulate public domain.

PURE DATA: A visual programming environment designed for the production of interactive multimedia and audio works. Available for GNU/Linux, Max OS X, Microsoft Windows, iOS and Android.

RGBA: A colour space commonly used on digital displays. 

SCALABLE VECTOR GRAPHICS (SVG): A standard for vector graphics, developed by the W3C.

SCRIBUS: A desktop publishing application for GNU/Linux, Mac OS X and Windows.

SERVER: A computer hosting data which is accessed remotely. 

TRUETYPE: A common vector-based font format.

UNICODE: A standard used for the encoding of characters. The term is often used to refer to the set of characters defined by the standard.

VERSION CONTROL: A means of managing changes (and allowing reversion) to a commonly held body of work, most often a software project. 

W3C: The organization responsible for setting web standards, such as HTML5 and SVG.

