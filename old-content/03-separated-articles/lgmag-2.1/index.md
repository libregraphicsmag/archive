
Editor's Letter: Localisation and internationalization, ginger coons

Production Colophon: Pruning Branches, Manufactura Independente

Notebook: Mozilla Festival 2012

New releases

The transnational glitch, Antonio Roberts

A journey through form fields, Eric Schrijver

Dispatch: Sketching Creative code, Davide della Casa

Small & Useful

Dispatch: Crafting Type: Type design workshops, around the world, Dave Crossland

First time: Styling maps like the web, for the web, Pierros Papadeas

Speaking across borders, ginger coons

Showcase

     3 Bridges, Nikki Pugh

     De Schaarbeekse Taal, Clémentine Delahaut, Peter Westenberg, An Mertens (Constant)

     Newstweek, Julian Oliver and Danja Vasiliev

Hacking clothing: An interview with Susan Spencer by Natalie Maciw

Localizing type, Denis Jacquerye

Resources/Glossary

Call for submissions 2.2

