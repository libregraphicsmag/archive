# Best of SVG

## Wayfinding and warnings from Wikimedia Commons

Warning signs, street signs, all kinds of signs: they blend into our environments, letting us know what we need to know with minimal fuss. Rather, that's what they do when well designed. When badly designed, they confuse and jar us. 

This time around, Best of svg has collected some of the finest examples of signage Wikimedia Commons has to offer. From warnings of lasers and potential explosions, to incredibly pleasing no passing signs, there's a nice assortment on offer.

We've found that warning and traffic signs are one of the strong points of the Wikimedia Commons collection of svg graphics. Signs and heraldry. But that's a collection for another day. If you don't yet know about Wikimedia Commons, it's well worth checking out. Not only do its graphics feature in the Wikipedia articles we know and love, but it has a pretty nice collection of other media, all under permissive licenses, for your appreciation and re-use. Find it at commons.wikimedia.org. 

—the editors

--------------

We at Libre Graphics magazine have a thing for open standards. We like their transparency and their interoperability. We like that, with a well documented standard, everyone has an equal chance to play nicely together. 

That's why we like SVG so much. It's a well developed, well supported standard brought to us by the World Wide Web Consortium (W3C). It's available for implementation by anyone developing software. It shows up in modern browsers, fine vector graphics editors and any number of other places. 

One thing that's missing, though, is you: the designer, the artist, the illustrator. So put down that .ai file and check out SVG.
