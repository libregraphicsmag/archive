# Index

4 Masthead

6 Editor's Letter

7 Production Colophon

9 New Releases

10 Upcoming Events

12 Copyleft Business, Dave Crossland

14 The heritage of our pixels, Eric Schrijver

18 Coding Pictures, Ricardo Lafuente

22 Setting a book with Scribus, Pierre Marchand

24 Best of svg

26 Desktop, Pierros Papadeas

28 Interview with Oxygen's Nuno Pinheiro

35 Showcase

   36 Allison Moore, Papercut

   38 Antonio Roberts, What Revolution?

40 Making your workflow work for you, Seth Kenlon

43 On being a Unicorn: the case for user-involvement in Free/Libre Open Source Software Libre Graphics Meeting Special

44 Talking about our tools Libre Graphics Meeting Special

46 AdaptableGimp: user interfaces for users, ginger coons

51 Resource List

55 Glossary 1.2

