Production Colophon

Ana Carvalho & Ricardo Lafuente



Software

In the process of bringing this first issue into being, our work was made easier by the excellent F/LOSS design tools available.

Scribus 1.3.8 was used to typeset and arrange all the magazine content into a final layout for print.

Inkscape 0.48 was the tool of choice to create the magazine cover, as well as the "Have your say", calendar and UpStage ads.

GIMP 2.7.2 helped us to retouch and edit columnist photos and most of the bitmap illustrations, as well as converting some PDF files into bitmap for inclusion in the layout.

Phatch 0.2.7 saved us from the tedious work of converting images to grayscale for printing.

Pdftk 1.41, a tool for merging several PDFs into one (among many, many other uses), helped us streamline our workflow and produce the layout using smaller Scribus files instead of a big, heavy one.

Git 1.7.0.4 was the version control system that we used to share work between ourselves, as well as providing it for anyone interested in taking a peek into the process. Gitorious was our service of choice when it came to publishing the magazine repository. We also used gitg and git-cola to provide a graphical interface for the Git repository.

A spreadsheet for sorting through submissions was created in OpenOffice.org Calc.

Finally, all plain-text editing was done inside Gedit and vim.




Fonts

Linux Libertine body text 

Linux Biolinum footnotes and captions

PropCourier Sans titles

Linux Libertine and Linux Biolinum are two beautiful and well-crafted typefaces by the Libertine Open Fonts Project.

PropCourier Sans is the official custom typeface for Libre Graphics Magazine. It is a friendly fork of Open Source Publishing's NotCourier Sans, a monospaced typeface which sports the statement "We are not here to be polite." PropCourier Sans is the proportional version of NotCourier; it is an ongoing effort that will be worked on and updated on each issue of Libre Graphics Magazine, following the "Release early, release often" principle. You'll find some inaccuracies and inconsistencies in the current version, but we'd rather just use what we can have now instead of going for a finished and polished typeface -- whatever "finished and polished" might mean. We decided that we would prefer being honest than being right, hence PropCourier's tagline: "We are not here to be correct."



Finds

If you flip over to page 20, you'll find a text illustration based on a photo of Pierre Marchand. This effect was achieved with a Python script written by Alexandre Truppel who is 14 years old, and is attending tenth grade of school in Portugal.

Alex popped up in Hacklaviva (a hackerspace in Porto, Portugal) during a Python meet-up. It didn't take long for everyone to be amazed by his coding-fu, complete with his own full-fledged GUIs. When we asked Alex if we could use his programs to create illustrations for the magazine, we got a positive response, along with some heartening remarks: "I never thought my programs could be used for anything else besides random fun. I did the program as a challenge to learn more Python and to work with images. Then I used it to make some gifts for my family."

Ana Carvalho and Ricardo Lafuente make up Manufactura Independente, a design research studio based in Porto, Portugal. Their design practice orbits around the principles of free software and free culture.

http://manufacturaindependente.org

