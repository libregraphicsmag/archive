Camouflage and Mimicry Illuminated Drawing



Statement

When I was a little kid and heard adults mention "illuminated manuscripts", I visualized an enormous dark archive, reminiscent of cathedrals and the basement vaults of libraries, full of rich images that were softly glowing. I was disappointed to discover that it just meant books with weird little pictures in the margins, however intriguing those books might be. Still, decades later, when someone mentions illuminated manuscripts, the first image that comes to mind is my childhood vision and I have to consciously remember that it's illustrated texts, always with a sense of disappointment.

Flight and Pursuit is the adult creation of my childhood vision. I have combined work from my adult artistic practice and interests with the fairy tales, myths and dreams of childhood to create the imagery. It is an adult narrative of my flight from and pursuit of technology; my love/hate relationship with computers, motorcycles, microwaves, compound bows, my hearing aids, clocks...the entire paraphenalia of technological apparatus. Combined with pre-Internet childhood imagery, it becomes an expression of my flight through, and pursuit of, time and memory.

--------------

42 x 18". Watercolor, aquarelle pencil and pastel on paper stretched over what more or less amounts to a fancy lightbox. From a series of illuminated drawings I call Flight and Pursuit.

--------------

Laura C. Hewitt

--------------

Warfly 1



There is an Asian story that butterflies carry the souls of the dead to heaven. Early Native Americans believed that part of the human soul was captured in photographs. Combining these two ideas, I used media war images to create butterflies after having a dream in which human created patterns became so pervasive that butterflies started mimicking them. Could war images be turned into something beautiful? Dangerous thought. This is one of many butterflies I have designed using computer graphics then hand painting and drawing on the computer print. The butterfly's pattern is a digitally enhanced war image.





--------------

12 x 14". Watercolor and computer print of war image.

--------------
