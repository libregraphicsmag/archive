Visual literacy:knowing through images

Eric Schrijver

--------------

The experience of our own time is mediated through images, but we tend to represent the past in a verbal-discursive way1. That means the model of the history book, with its emphasis on words and stories. In its attempts at establishing a reliable reputation, a resource like Wikipedia exhibits an extremely conservative take on representing knowledge2: an image that goes along with an article is never more than an illustration in the literal sense of the word.

This why all the images we know of child labour show unhappy children: these pictures have been selected to fit to our story of the condemnation and subsequent abolition of child labour.

But when browsing through contemporary image archives, you will find most images show smiling children. This makes perfect sense: a photographer's visit is a special and exciting occasion.

It's the shock we get when confronted with these images that suddenly makes it possible for us to relate to our past. By freeing the image from its iconic role, we can stop seeing what's depicted as nothing but logical steps in a larger story. We can identify, both with the children who are depicted and with the photographer taking the picture.

In the pictures taken for the American National Child Labor Comittee, most depicted children look happy.



Our archives house many photographs of Hitler, and he smiles in most of them.



When you see his picture on the back of a book, Mark Twain is a long dead writer. When you see the whole series, you see he was a super star.3

Grotesk typefaces like Helvetica did not start out with the ‘neutrality’ imparted on them by the Swiss design school—they were whimsical display fonts.

Forms we employ for ironic effect did not start out that way.

All these are examples of how ‘reading’ the images from the past can show us how our own perception and norms have changed since then, allowing us to better understand both past and present.




1. Wikipedia, the deeply conservative and traditional encyclopedia,All The Modern Things, 2008 http://brianna.modernthings.org/article/147/wikipedia-the-deeply-conservative-and-traditional-encyclopedia



2. Interactieve presentatie handschriftenMuseum Meermanno-Westreenianum, 2003 http://collecties.meermanno.nl/handschriften/.

--------------

3. Inspired by spreads from Fantastic Man http://www.fantasticmanmagazine.com

