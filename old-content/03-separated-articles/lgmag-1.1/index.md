# Index

4 Masthead

6 Editor's Letter

7 Production Colophon

8 Notebook

11 New Releases

12 Freed Fonts, Strong Web Dave Crossland

14 This is the first day of my life Eric Schrijver

16 F/LOSS in the classroom Ludivine Loiseau

18 The unicorn tutorial ginger coons

20 Pierre Marchand talks Fontmatrix, responsiveness and user engagement

25 Showcase

    26 Laura C. Hewitt

    32 Pete Meadows

    36 John LeMasney

41 Applying F/LOSS as a final user and not dying in the attempt Lila Pagola

47 Visual literacy: knowing through images Eric Schrijver

54 Interview with Ben Laenen of DejaVu

59 Resource List

62 Glossary and resources
