
GIMP 2.7.5

http://www.gimpusers.com/downloads

Call it a beta of the next gimp 2.8. This devel version of gimp gives a taste of what's coming next, namely faster performance, improved tablet support and a lot of bug fixes.

--------------

Blender 2.62

http://www.blender.org/download/get-blender

The latest version of Blender includes motion tracking, an improved interface for the game engine and a number of new uv editing tools, among other additions.

--------------

VLC 2.0 Twoflower

http://www.videolan.org/vlc/releases/2.0.0.html

A major new release from the ever-popular video player. Now with faster decoding, an enlarged list of formats (which includes hd) and higher quality subtitles.

--------------

Ghostscript 9.05

http://www.ghostscript.com

With improvements to icc colour rendering and font replacement, the newest version of Ghostscript has a number of design-sensitive additions.

--------------

Gnome 3.4

http://gnome.org

The popular desktop environment has come out with its first new release in six months, incorporating new menus and search functionality, as well as a more polished look.


