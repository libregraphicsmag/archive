The graphic side of 3D printing

There's a little revolution taking place in basements, garages and hackerspaces. 3d printers—machines which turn digital meshes into physical objects—are getting cheaper, better and more popular. We've collected samples of some strikingly graphic 3d prints, designed with f/loss tools.

All of the projects featured in this showcase are available from Thingiverse, under permissive licenses, for printing and modification.

http://thingiverse.com


Airplane cookie cutters (Inkscape), user: emmett on Thingiverse: http://www.thingiverse.com/thing:11098

DNA Playset (OpenSCAD), Emmett Lalish: http://www.thingiverse.com/thing:17343

I heart lightning (Inkscape & OpenSCAD), Amy Hurst: http://www.thingiverse.com/thing:17495

Lake and mountain topography (Blender), Luke Chilson: http://www.thingiverse.com/thing:7207
