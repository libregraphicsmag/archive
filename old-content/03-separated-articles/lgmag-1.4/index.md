
Masthead

Editor's Letter

Production Colophon

New Releases

Upcoming Events

Moral rights and the sil Open Font License Dave Crossland

Will these hands never be dirty? Eric Schrijver

Notebook

Small & useful

The finished and unfinished business of OpenLab esev Nelson Gonçalves and Maria Figueiredo

Best of svg

The Interview: Natanael Gama talks techno-fonts and the benefits of Libre Dave Crossland

Showcase

    3D Printing

    Baltan Cutter Eric de Haas

    Paper.js Jonathan Puckey

Folds, impositions and gores: an interview with Tom Lechner Femke Snelting, Pierre Marchand and Ludivine Loiseau

ColorHug: filling the f/loss colour calibration void Richard Hughes

Resurrecting the noble plotter Manufactura Independente

Resource List

Glossary 1.4

