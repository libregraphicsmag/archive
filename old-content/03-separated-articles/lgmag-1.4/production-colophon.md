Branching out:journey notes

Ana Carvalho & Ricardo Lafuente


We now re-grab the reins after last issue's experiment with collaboration. It was a thrill to work with and feature the contributions of so many great practitioners; it was also a chance to go through and become an impromptu version control system, merging the various contributions into one coherent whole.

Version control is a theme that keeps popping up when discussing free and open creative projects, and it has, for good reason, been one of the main threads of discussion at the Libre Graphics Research Unit*. Not only does it provide useful metaphors for us to (re-)think our practice, but it is an effective practical tool for the management of complex editorial projects such as a magazine (though it is becoming unwieldy to manage a repository that takes up more than 8 gigabytes).

And now that we wrap up the first year-run of production with the release of the fourth issue, it is now time to sit down and wonder where we can go from here. This publication made evident that there is an emergent kind of artistic practice based on free culture that lacks proper stages where it can show itself, and the crossings between software tools, design theory and real-world practice remain underexplored. 

There is still a lot to be said, and not just through words. The layout decisions and general graphic and artistic direction of Libre Graphics magazine are also a reflection of our unanswered questions and tentative answers. As an example, we've avoided exuberant experimentalism when conceiving the magazine's identity and graphic principles; this isn't because we're shy to experiment, but because we decided we'd be careful. Balancing reader expectations towards predictability and consistency with the designer's will to push the boundaries of graphic language is a frequent dilemma, and one that keeps popping up when we sit down to lay out a new issue: how far can we go without risking clarity and seriousness? And in what ways does our subject matter demand a new artistic approach? Our way to look for answers is to go on through slow, but steady steps.

We get closer to a satisfactory answer by taking risks and sometimes failing. We took note of our perceived failures (some of them pointed out by our readership), and are getting ready to sit down and, again, take a new risk in defining an improved identity for Libre Graphics magazine to be introduced in issue 5—the first issue of volume 2. We hope you're enjoying the journey, and we hope we can count on your curiosity and critical feedback since, in a good way, we're all lost together trying to figure out all the subtle ways that libre graphics culture is amplifying our collective possibilities and potential.



* From the lgru web site: “The Libre Graphics Research Unit is a traveling lab where new ideas for creative tools are developed. The Research Unit is an initiative of four European media-labs actively engaged in Free/Libre and Open Source Software and Free Culture. This cross-disciplinary project involves artists, designers and programmers and is developed in dialogue with the Libre Graphics community.” (http://lgru.net)


