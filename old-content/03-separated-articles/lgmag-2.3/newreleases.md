# New Releases

### Beautiful Open

A gallery of gorgeous F/LOSS project websites, just to show it can be done.

http://beautifulopen.com


### copyleft.org

Describing itself as “a collaborative project to create and disseminate useful information, tutorial material, and new policy ideas,” copyleft.org aggregates existing and new resources on copyleft licensing.

http://copyleft.org


### Flat/Even

A library for creating PDF layouts using Python code. It can be used as a code-based tool, or along its GUI companion, Even.

http://xxyxyz.org/flat
http://xxyxyz.org/even


### Glyphr Studio

A web-based font editor with a modern interface for rapid tweaking, with exciting features like dual-screen editing and SVG import.

http://glyphrstudio.com


### Birdfont

The up and coming FontForge alternative is about to release its 2.0 version, with a beta package already available for those too eager to wait.

http://birdfont.org/betaversion.php


### Prototypo

A parameter-based font generator, currently under development. While the hosted version is, at present, only available to those who supported the crowdfunding campaign, source is available.

http://www.prototypo.io


### Scri.ch

An elegantly minimalistic online sketch pad. Scri.ch presents you with a totally blank page, waiting for a drawing, and allows you to save your work for later reference and distribution.

http://scri.ch


### Opentype.js

Parses fonts and glyphs, going so far as to show contour parameters. From displaying fonts and their component parts to inspecting metadata, Opentype.js offers a compelling solution for online font display and inspection.

http://nodebox.github.io/opentype.js


### Brick

A collection of high-quality web fonts, for your hyperlinking pleasure.

http://brick.im


### ttfautohint 1.2

The latest version of ttfautohint offers minor bug fixes and some emergent features, which promise to be elaborated on further in future releases.

http://www.freetype.org/ttfautohint


### Plain Pattern

Harnesses the awesome power of SVG to generate tiles for seamless patterns.

http://www.kennethcachia.com/plain-pattern


### The Missing Scarf

Not strictly a new release, because it appeared on the film festival circuit in 2013, but the award-winning animated short, made with Blender, has recently been made available for all to watch.

http://vimeo.com/107395294
http://themissingscarf.com

