# Pirate Party identity and PPPoster

## Samuel Rivers-Moore

### Pirate Party Identity

The Pirate Party is a young political party founded in Sweden in 2006, gently seeping all over the world.Post-digital culture is part of its DNA and its program mainly focuses on human rights and fundamental freedoms issues, as well as on contemporary copyright problematics. Surfing on a general exasperation towards conventional political parties, it presents itself as an alternative to Left/Right divisions, as well as a way to "upgrade" democracy for a more horizontal political system.The Pirate Party acts as a bridge between, on the one hand, some more or less radical and structured political movements (Anonymous, WikiLeaks, GNU, etc.) and on the other hand, an institutional way of influencing the democratic process. In most countries, its visual identity is quite conventional and its communication is sometimes clumsy, as is the case in France.

Since the Pirate Party depicts itself as something different, my project aims to experiment with unconventional methods to design its visual identity. I also aim to imagine some envelopes that fit its ideas in a faithful way, rather than in a simplistic or watered down one. This project is a pirate project in the sense that it hasn't been commissioned by the PP, nor has it been conceived to be used by the party, to be functional or cosmetic. It simply tries to show what could be the visual identity of a political party if this party had an unconventional, self-critical approach to communication, sincerely saying “this is what we are.”

The importance of being recognizable in the visual landscape doesn't mean shapes have to stay rigid. Inspired by water, flags and CAPTCHAs, I developed an undulating logo using Pure Data. Its shape is always changing, influenced by random deformations and twists. It plays with the oxymoron expressed by the party's name, opposing the rigidity of institutional structures to the more dynamic, moving and undefined aspects of activism. By the same token, the fonts used are, on the one hand, a custom version of Terminal Grotesque, a rough and geometric pixel font drawn by Raphaël Bastide; and on the other hand, the more classical Linux Libertine designed by Philip H. Poll and already used for Wikipedia's logo. Both are F/LOSS fonts.


### PPPoster

PPPoster is a small proto-app coded with Processing. It allows the design of simple and strong text-based posters in a very short time without having layout skills. The app works like a minimal text editor. It follows only one strict and direct formal rule: each line of text automatically adjust itself to fit the width of the poster. This strong constraint is an invitation to play. The original idea was to experiment around a tool that could be used by anybody. I pushed the idea a bit further with a program using the same principles to automatically generate posters using tweets containing keywords linked to the Pirate Party. Automatically converting tweets into official and branded posters is a way to invite the Pirate Party to cede control and to give some spontaneity to its own political communication. It is a way to make the party accept and assume its own criticisms. It is also a way to make the party say “anybody can speak in our name” and “these criticisms are part of us.” This project does not pretend to be realistic. Instead, it is very experimental and prospective and aims only to be an inspiration for people who would like to use visual communication in an unconventional manner. 

--------------


Samuel Rivers-Moore

