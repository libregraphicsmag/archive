

Editorial Team

Ana Isabel Carvalho ana@manufacturaindependente.org

ginger coons   ginger@adaptstudio.ca

Ricardo Lafuente ricardo@manufacturaindependente.org

Copy editor

Margaret Burnett

Publisher

ginger coons

Community Board

Dave Crossland, Louis Desjardins, Aymeric Mansoux, Alexandre Prokoudine, Femke Snelting

ContributorsAntonio Roberts, Eric Schrijver



Printed in Porto by Cromotema (www.cromotema.pt) on recycled paper. 

Licensed under a Creative Commons Attribution-Share Alike license (CC-BY-SA). All content should be attributed to its individual author. All content without a stated author can be credited to Libre Graphics Magazine. 



Contact

Write to us at enquiries@libregraphicsmag.com

Our repositories with all source material for the magazine can be found at ww.gitorious.org/libregraphicsmag



**Type issue**ISSUE 2.3, June 2014ISSN: 1925-1416www.libregraphicsmag.com

--------------

**Type Issue**

ISSUE 2.3, June 2014ISSN: 1925-1416WWW.LIBREGRAPHICSMAG.COM

--------------

IMAGES UNDER A CC ATTRIBUTION SHARE-ALIKE LICENSEPhoto of ginger coons by herself.Photo of Ricardo Lafuente by Ana Isabel Carvalho.Photo of Ana Isabel Carvalho by Luís Camanho.Photo of Antonio Roberts by Emily Davies.Photo of Eric Schrijver by himself.All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.



IMAGES UNDER A CC ATTRIBUTION LICENSE



IMAGES UNDER OTHER LICENSES



GENERAL

Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.

