# Building Cyrillic fonts together

## Alexei Vanyashin

_Learn Cyrillic_ is a resource I created to teach type designers across the world to master Cyrillic. The idea came to mind when I realized that there is lack of information on Cyrillic type—unless you can read Russian. When I studied type design in Moscow we were taught by our instructors that only natives can design proper Cyrillic forms. I wanted to challenge that.

In 2011 I ran a Cyrillic workshop for alumni of the Royal Academy of Art in The Hague along with fellow Russian designers Irina Smirnova and Gayaneh Bagdasaryan. I realized that some of the students' works were remarkably accurate, and that anyone with proper training could design as good a Cyrillic script as a native. Shortly after, I launched learncyrillic.tumblr.com and published a quick guide on Cyrillic.

I received emails from designers around the world seeking advice. During my consultations, there was always a dilemma between choosing correct Cyrillic forms and fitting in with the spirit of the typeface. I listened carefully to what designers said and adjusted my methodology accordingly. I learned to be less involved, to stand back and focus on providing more references and hints rather than direct solutions. It’s about guiding, not steering.

By summer 2013 I was consulting on seven Open Fonts with Cyrillic for the Google Fonts library. It was one of my best experiences. The benefit of working with a F/LOSS methodology was the back-and-forth collaboration, involving direct drawing of shapes by both parties. In general it took between three to eight iterations to agree on appropriate Cyrillic forms with the author. When consulting on proprietary fonts I usually don’t have this luxury and freedom of expression. Everything is tied to a deadline.

Here is a preview of fonts with Cyrillic support that are already published or will appear soon:

* Exo by Natanael Gama
* Lora Cyrillic by Olga Karpushina, Cyrillic extension by Alexei Vanyashin
* Arvo Cyrillic by Anton Koovit (unreleased)
* Kaffeesatz Cyrillic by Sol Matas (unreleased)
* Raleway Cyrillic by Pablo Impallari (unreleased)
* Oswald Cyrillic by Vernon Adams (unreleased)
* Merriweather Cyrillic by Eben Sorkin (unreleased)
* Bitter Cyrillic by Huerta Tipográfica (unreleased)

Another thing I realized was that a script has many dialects rather than one norm. A designer from Serbia has a different view on the shapes and proportions of letters compared to his Bulgarian and Russian colleagues—which one is correct? I believe there isn't one correct form. All the different styles are necessary to express national identity. Today, Russian type designers are very intolerant to the “international” Cyrillic style, perhaps in the same fashion as chauvinists when they hear a foreign accent. I feel this is wrong, and should be resolved by educating and providing an opportunity to fine-tune with the peculiarities of our script.

