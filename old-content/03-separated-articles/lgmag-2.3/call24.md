# Call for submissions: Capture

Data capture sounds like a thoroughly dispassionate topic. We collect information from peripherals attached to computers, turning keystrokes into characters, turning clicks into actions, collecting video, audio and images of varying quality and fidelity. Capture in this sense is a young word, devised in the latter half of the twentieth century. For the four hundred years previous, the word suggested something with far higher stakes, something more passionate and visceral. To capture was to seize, to take, like the capture of a criminal or of a treasure trove. Computation has rendered capture routine and safe. 

But capture is neither simply an act of forcible collection nor of technical routine. The sense of capture we would like to approach in this issue is gentler, more evocative. Issue 2.4 of _Libre Graphics_ magazine, the last in volume 2, looks at capture as the act of encompassing, emulating and encapsulating difficult things, subtle qualities. Routinely, we capture with keyboards, mice, cameras, audio recorders, scanners, browsing histories, keyloggers. We might capture a fleeting expression in a photo, or a personal history in an audio recording. Our methods of data capture, though they may seem commonplace at first glance, offer opportunities to catch moments. 

We're looking for work, both visual and textual, exploring the concept of capture, as it relates to or is done with F/LOSS art and design. All kinds of capture, metaphorical or literal, are welcome. Whether it's a treatise on the politics of photo capture in public places, a series of photos taken using novel F/LOSS methods, documentation of a homebrew 3D scanner, any riff on the idea of capture is invited. We encourage submissions for articles, showcases, interviews and anything else you might suggest. Proposals for submissions (no need to send us the completed work right away) can be sent to submissions@libregraphicsmag.com. The deadline for submissions is May 11, 2015.

Capture is the fourth and final issue in volume two of _Libre Graphics_ magazine. _Libre Graphics_ magazine is a print publication devoted to showcasing and promoting work created with Free/Libre and Open Source Software. We accept work about or including artistic practices which integrate Free, Libre and Open software, standards, culture, methods and licenses. 



