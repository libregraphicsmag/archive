# Type etc.

## Manuel Schmalstieg / Greyscale Press

In this issue, I've tried to collect a range of voices from the Libre type community, with their various approaches as designers, curators, afficianados and hackers. Libre type design is evolving quickly, and I hope that the new and upcoming generation of type tools—generative, parametric, running in your browser and elsewhere—will empower many more (non-)designers to get their hands dirty.

Working with Libre and freely licenced type designs opens up many doors for original, enthusiastic and imaginative (re)/(ab)use. I remember the moment when I was told how NotCourier, a derivative of Courier, was “forked” in a couple of hours. Ludivine Loiseau, a member of OSP, the forking foundry, said “we simply cut off the serifs, and there was that completely new font.”

A similar revelation occurred to me while I was attending a FontForge workshop, in a lovely village in the south of France, and realized that with just a couple of commands, I could produce the missing medium weight for this magazine’s title font, PropCourier Sans.

Indeed, fonts—sorry, typefaces!—are an odd thing: on one hand, they can take years, lifetimes, to be refined and polished into perfection (fortunately, the Libre software community has come up with tools such as ttfautohint and Kernagic, that alleviate the most time-consuming parts). On the other, when granted the freedom to transform and to adapt, radically original work can emerge from old designs in a matter of hours, minutes, split-seconds.

While software has a relatively short history, with current systems and languages reaching back to the 1970s (the origins of UNIX), type design is a much older field, as the evolution of typography stretches over a few millennia. Today’s modern typefaces (such as Linux Biolinum, used in this magazine) are based on centuries-old rules and canons. Breaking from those tradition means challenging our perception of legibility (the 0xA000 font showcased in this issue is a perfect example). To consider our work to be embedded in such a slow historical process is a refreshing idea, especially on the backdrop of a speed-obsessed digital culture favouring short release cycles, coding sprints, and technological obsolescence.

But even if our fonts are following old rules, the tools that we use for designing type are evolving, in rather amazing ways. Metapolator is the perfect example of a type design tool that opens up uncharted perspectives, while at the same time incorporating the concepts of Metafont, a command-line font software invented by Donald Knuth 40 years ago.

Julien Deswaef’s contribution to this issue also tackles this paradoxical relation to time: on-screen type rendering technologies and hacks that were considered cutting-edge 5 years ago are already reminding us of artefacts of a forgotten dark age. 

In that context, I see this issue as a snapshot, a moment in the fast and slow evolutions of type design and tools, that will be probably superseded in the forthcoming months by new breakthroughs and discoveries.

So please take your time, and enjoy this issue of _Libre Graphics_ magazine.

