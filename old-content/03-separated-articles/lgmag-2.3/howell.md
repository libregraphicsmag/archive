# The Screenless Office

## Brendan Howell

The Screenless Office is an artistic operating system for working with media, which eschews the use of a raster-based display. The goal of the project is not to produce techniques that can be justified in terms of speed and efficiency. Instead, the Office describes an alternative mode of everyday life with networked computation which might be calmer, more embodied, and personal. The system is constructed using Free/Libre/Open hard- and software components for print, databases, web-scraping, and tangible interaction. 

Some of the experimental components include a printed daily newspaper, a receipt printer that spits out tweets and fortune cookies, a document camera, a hybrid note card database and a “smart” Rolodex contact organizer. The core Office Manager and prototype Bureau modules will be released soon. Anyone experienced with Python and templating systems should find it relatively easy to extend, hack or customize their own Screenless Office.

http://wintermute.org/brendan/?e=261

