# Best of type collections

We all love a good collection of type specimens. There's something pleasing about looking through pages of carefully selected typefaces, admiring the sweep of a lower case “y” or imagining where you could possibly use that absurd but compelling novelty font. That joy of discovery is the common thread tying together this “Best of.”


### Goodgle Fonts

Spawning from Frank Adebiaye's desire to provide a selection of good samples from Google Fonts, Forthcome's Goodgle Fonts is an early effort to single out those “good” fonts to be found in Google's typeface repository.

http://www.forthcome.fr/work/goodgle_fonts


### Use/Modify

Raphael Bastide's growing showcase, Use/Modify, is a particularly well-designed repository of “beautiful, classy, punk, professional, incomplete, weird typefaces” from many sources. The common thread—that they allow their unrestricted use and modification—is the principle that defines the project's apt title.

http://usemodify.com


### Hand-Picked Tales from Aesop’s Fables with Hand-Picked Type from Google Fonts

Phoebe E's project, Hand-Picked Tales from Aesop’s Fables with Hand-Picked Type from Google Fonts, is a one-page feature of several well-considered combinations of typefaces. An important concern when typesetting in print or on the web, different fonts are tastefully combined into an eclectic showcase of distinct and elegant layouts.

http://femmebot.github.io/google-type


### Delubrum

Stephen G. Hartke has maintained a comprehensive list of existing Open fonts which used to live in Geocities, but which now have a home at delubrum.org. While not as designer-oriented as the previous examples, this resource covers several typefaces from the F/LOSS world, along with detailed notes on authorship, license, glyph coverage, styles and TeX support.

http://delubrum.org
