Dispatch

# Mothership HackerMoms: Forging Open Source community for mothers

## Aya de Leon and Sho-Sho Smith

--------------

For most urbanites, becoming a mother is like moving to another country, with its own language, culture and politics. For women artists, motherhood can be another planet. The pressure is extreme: We go from having one consuming passion that is unpaid/underpaid (making art) to having two underpaid passions (making art and people). Our own needs for community and creative stimulation can go unmet while we respond to the massive needs of young children. It’s easy for mothers—and often expected in our culture—to disappear into their children. Motherhood is a hidden life, and mothers are a demographic hidden in the open. But there’s a certain kind of highly creative woman who won’t accept invisibility. Enter Mothership HackerMoms.

Though the free market offers “options” to help us out—hiring childcare, paying for classes—there’s really no service in America dedicated to improving the lives of mothers. HackerMoms aspires to be the mythical village that raises the child and takes care of the mother, a village that  mothers themselves had to create. The HackerMoms mission is twofold: To give mothers of every gender the time and space to explore DIY craft and design, hacker/maker culture, entrepreneurship, and all manner of creative expression – with childcare; and to teach children the creative process through Hacker Sprouts, our educational childcare program. With onsite childcare and hands-on community workshops for adults and kids, HackerMoms creates families that build together.

We need a parenthood commons. We need open source activities for families – and not just park playgrounds. (I never manage to make my own art in a park). HackerMoms unites a community of mothers—and by extension, our children and partners, too—around our deepest passions of creative work and children. We've also become a unique business incubator that supports entrepreneurship and growing income from creative endeavors.

We are betting that a happy mother—that is, a fulfilled woman learning and using her talents—is good for herself, her family, the community and the world. At just a year old in April, the HackerMoms model is still young, but it’s working. We’ve had requests from moms of every gender—dads, non-moms, LGBT moms also factor in the HackerMoms membership—who would like to see more HackerMoms spaces around the world. Our biggest success to date? Making motherhood visible. HackerMoms have hacked a hackerspace to suit mothers.

--------------

Hackerspace: 
3288 Adeline StreetBerkeley, 
CA 94703

http://mothership.hackermoms.org
info@hackermoms.org
facebook.com/MothershipHackerMoms & twitter.com/hackermoms

--------------

Aya de Leon is a performer, blogger and novelist.

Sho-Sho Smith is a writer and co-founder of Mothership HackerMoms.
