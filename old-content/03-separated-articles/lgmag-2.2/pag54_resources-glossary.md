# Resources/Glossary

**Arduino**A popular open hardware platform for rapid electronic prototyping.

**Blender**A powerful 3D animation application for GNU/Linux, Mac OS X and Microsoft Windows.

**copyleft**A style of licensing in which those redistributing the work are required to do so under its original (or a compatible) license.

**Creative Commons**A suite of licenses designed to allow creators and users of works flexibility beyond that offered in traditional copyright.

**distro/distribution**A specific configuration of GNU/Linux, often designed with a particular purpose in mind.

**F**/LOSSStands for Free/Libre Open Source Software. Software which has a viewable, modifiable source. It can be modified and redistributed.

**Fedora**A popular distribution of GNU/Linux, produced by Red Hat, Inc.

**Free**As in freedom, or often, that which is or is of Free Software.

**Free Art License**A copyleft license designed for creative works.

**Flickr Commons**A repository of photographic images, sourced from institutions around the world, with no known copyright restrictions.

**Free Software**A term describing software which is made available under licenses permitting users to not only run it, but to examine its code, redistribute it and modify it.

**Free Software Foundation**An organization dedicated to the promotion of Free Software. Well-known for its development of the GNU General Public License.

GIMPA raster based image editor for GNU/Linux, Mac OS X and Microsoft Windows. 

GNU **General Public License** (GPL)A license originally intended for use with software, but now used for other applications. Made famous the principle of Copyleft, requiring those using GPL licensed work to license derivatives similarly.

GNU**/Linux**A group of operating systems which are built on the Linux kernel and components from the GNU project, among others, which are widely distributed and freely modifiable.

**Inkscape**A vector graphics editor for GNU/Linux, Mac OS X and Microsoft Windows.

KDEA community project which produces various F/LOSS applications, best known as a popular desktop environment for GNU/Linux.

**Libre**A less ambiguous adaptation of the word Free. Implies liberty of use, modification and distribution.

**Open Hardware**Hardware which follows the same principles as F/LOSS, including publicly available, freely licensed schematics.

**Open Source**See Free/Libre Open Source Software

**Scribus**A desktop publishing application for GNU/Linux, Mac OS X and Windows.

**They**Most commonly known as a collective pronoun, it is also used as a gender-neutral singular pronoun. 
