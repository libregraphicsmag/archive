Showcase

# Lelacoders

## Spideralex

Lelacoders is a cyberfeminist research project about women's contribution to computer science, Free Software and hacker cultures. Those contributions have been little-studied and seldom made visible. Lelacoders’ research has sought out women developers and hackers in order to better understand their motivations, practices, and technological perspectives. The research questions why women are under-represented in computer science, studies which practices and initiatives have been successful in overcoming barriers, and analyzes the experiences and subjectivities of many programmers who have chosen to use Free Software for their techno-political practices. The project aims at developing a documentary with Free Software that will be released on the internet using a free license.


https://n-1.cc/g/donestech+lelacoders
http://www.studioxx.org/en/res/alexandra-hach%C3%A9-lelacoders
http://vimeo.com/channels/535358
