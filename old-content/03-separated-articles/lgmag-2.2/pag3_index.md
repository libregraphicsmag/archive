# Index

5 Editor's Letter: Gender ≠ women, ginger coons

6 Production Colophon: Making the switch, Manufactura Independente

8 Notebook: LibrePlanet 2013

10 New Releases

12 Ticking the other box, Antonio Roberts

14 Rewriting hacker culture, Eric Schrijver

16 Consensus is sexy? The gender of collaboration, Eleanor Greenhalgh

18 Dispatch: Mothership HackerMoms: Forging Open Source community for mothers

20 Dispatch: Interactivos?'13, Tools for a Read-Write World

26 Showcase

  28  Knitic, Varvara Guljajeva and Mar Canet

  32  Zone, Ele Carpenter

  36  Lelacoders, Spideralex

38 Small & Useful

41 Best of People icons

42 The Empowermentors Collective — an interview

47 Feature: Modelling, Femke Snelting

54 Resources/Glossary
