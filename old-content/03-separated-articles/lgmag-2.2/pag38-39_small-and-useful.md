# Small # & Useful

There's an adage in the software world: programs should do one thing very well. In that spirit, we offer you a round-up of small and useful programs and resources which do one thing particularly well. This issue, we focus on some key concepts for understanding gender issues.


## The Geek Feminism Wiki

A clearinghouse of concepts and discussions crucial to understanding and promoting issues of equality and representation. What makes it crucial is its understanding of F/LOSS and geek culture, framing its critiques and suggestions in ways that make concrete sense.

_http://geekfeminism.wikia.com/wiki/Geek_Feminism_Wiki_


### Some key concepts:


## Intersectionality

The idea that different kinds of opression and marginalization can't be treated individually, but must be considered and acted upon as a whole.

_http://geekfeminism.wikia.com/wiki/Intersectionality_


## Privilege & privilege checklists

The idea that a group, though they may not be intentionally hostile to others, may have a set of unknown or unnoticed advantages over other groups. Privilege checklists are a way of making such advantages visible.

_http://geekfeminism.wikia.com/wiki/Privilege __http://geekfeminism.wikia.com/wiki/Privilege_checklist_


## Bechdel test

The famous test of woman-friendliness in media. This article also includes variations of the test which focus on other issues.

_http://geekfeminism.wikia.com/wiki/Bechdel_test_
