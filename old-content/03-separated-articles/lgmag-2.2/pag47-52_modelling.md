Feature

# Modelling

## Femke Snelting

Flashy cars and beautiful women are the outlet for the creativity of many digital artists. Femke Snelting tells a story in three parts about the roles these models play in f/loss art.

--------------

This article contains images which are not licensed under our normal CC-BY-SA or compatible image policy. They are used here either with permission, or under Canada's Fair Dealing doctrine.

--------------

### Cars

Just before the release of Inkscape version 0.45, core team member Bulia Byak asked for help. He needed Inkscape artists to showcase recent additions such as clipping, masking and especially blur. In a message to both the developer and user mailing lists he wrote:

_One thing I would like to request in particular: A car. It would be very nice to have a complex, detailed, photorealistic __svg__ image of a shiny impressive car._

→ http://inkscape.13.x6.nabble.com/Artists-needed-td2868775.html

_An image of a broken light bulb would probably be sufficiently complex and shiny to showcase the photorealism that Inkscape is capable of, but Bulia listed several arguments to explain why he felt car images would be desirable. First of all, “cars are sexy (for many people, anyway).”_

→ http://inkscape.13.x6.nabble.com/Artists-needed-td2868775.html

A first encounter with Libre Graphics tools will most likely involve an automobile. In galleries, tutorials and screen shots, cars prominently and consistently figure as the subject of choice. Mixed in with other interesting topics such as romantic portraits, baby pictures, an art catalogue, fantasy characters and wild-life scenes, cars appear to be indispensable for demonstrating what Free, Libre and Open Source graphics software is capable of.As a female feminist without a driving licence, how do I address the disproportionate amount of cars in the context of this issue on gendering F/LOSS? I myself might prefer public transport, but other women love cars and probably dream of something more exciting than driving the practical family sedan.

The typical car appearing in software screen shots is definitely not in the _reasonably priced_ category. Scribus features a Renault Roadster; Blender an Opel GT, a gleaming Audi G8, a BMW M5, a Mercedes G and a Lamborghini Gallardo. Inkscape includes yet another Lamborghini Gallardo, a Ferrari plus an unidentified sports car that looks like a vintage Porsche.

Bulia Byak continued his request by explaining that cars would be easy to draw because “you don't need to be much of an artist for that, you just need patience, Inkscape skills, and a good photo to start from.” He then pointed out that Xara Xtreme, a competing Open Source vector editor, successfully used “amazing” car images for its promotion.

Michael Grosberg and Konstantin Rotkevich ignored the insult and each responded with a skilfully rendered image. Two luxury cars could now be packaged and distributed as sample files with Inkscape 0.45.

_The Gaussian Blur filter support in Inkscape 0.45 made possible some extremely photorealistic art. This Lamborghini Gallardo was created by Michael Grosberg based on a photo and uses blurs extensively for soft shadows and halos around bright reflections._

→ http://inkscape.org/screenshots/?lang=es&version=0.45

Those bright reflections are not just innocent dream images. Glamorous cars sell the idea that exclusivity, advanced technology and speed are linked to excitement, respect and success. For a community energized by sharing, mixing and exchange, to identify with such over-engineered proprietary status symbols is bordering on perverse. It means negating everything that makes Free, Libre and Open Source software desirable.

### Girls

Surprising both the official jury  of the _Blending Life Challenge_1 and members of blenderartists.org, newcomer Nyxia aka Angela Guenette came out first in the category “photo-realistic human.” The prize included one year of premium access to a database of royalty free photo references offered by sponsor 3d.sk. Winning the contest meant above all that the Blender community noticed her skills and talent.

_I admit that Nyxia's is my favorite, based purely on beauty. (Looking like __a dream date)_

→http://blenderartists.org/forum/showthread.php?144103-blending-life-category-a-voting-closed

_My girlfriend walked in the office while I was reading this post. Her: “Oh, she's pretty.” M__e: “That's actually a model.”_

→http://blenderartists.org/forum/showthread.php?142776-Blending-Life-Category-A-Isabella

At around the same time, Blender foundation chairman Ton Roosendaal started preparing for a third Open Movie with a core team of script writers and concept artists. From the beginning of the project, their mind was made up: The follow-up of _Big Buck Bunny_ would be a real film and the main character a warrior girl.2

Only five skilled artists could join the core team at the Blender Institute in Amsterdam to help realize the project. Out of hundreds of applications, Angela Guenette was selected to be one of them. Under the inspired leadership of director Colin Levy and art director David Revoy, she would be responsible for modelling the main character, Sintel. Angela was the only woman on the animation team.

I go through many blog posts, interviews, comment threads and a documentary. The Sintel team has kept the Blender community up to date with endlessly detailed accounts of the sometimes frustrating but always fascinating production process. Unfortunately, that did not include an account of Angela's experience. Did she agree that the main character was transforming into “yet another anorexic anime doll”3 as one blenderartists.org member commented? How did she cope, all those months immersed in adolescent banter and casual sexism if it already gets on my nerves after a few days of reading and watching?

_One Point about the Charakter [sic]. Maybe this is just some more personal liking. But I think at this Point her Proportions are not female enough._

→ Comment thread on “Sintel Model, 2nd Stage, with .blend”http://sintel.org/news/sintel-model-2nd-stage-with-blend

_Guys, if you think the new Sintel is more realistic, warrior-like or even sexy, here's my advice: Stop downloading anime, turn off the computer, get out of the basement and meet some real females. You're in for a big surprise._

→ Comment thread on “Sintel Sighting” http://blenderartists.org/forum/showthread.php?171730-Sintel-sightings

After a year of relentless work, the project was finally finished. Much more than just a technical demo, Sintel had grown into an epic short story featuring a young heroine with a sympathetic dark streak. Both the movie and its protagonist were an immediate success.

In keeping with the ethics of an Open Movie, all files used to generate the film were made publicly available for anyone to inspect, test and learn from. A “lite” version assured that the character was also accessible to amateur 3D animators.

Soon after the completion of Sintel, The Blender Institute contracted Angela to develop a tutorial DVD on character modelling. She created a sturdy girl made to wear arms and armour, standing firm on her two feet in knee-high boots. Meet Blenderella: Yet another fearless warrior to try your hand at.

The Blenderella training DVD includes photographic reference images that can be used with special permission from 3d.sk: Twenty five neutrally lit shots of a reasonably-proportioned girl with manicured nails and blemish-free complexion; full length frontal, back and side views plus several details of her boots, arms and face.

Some Blender fans recognized ANETA000.JPG right away as Czech pornstar Aneta or Anetta Keys, otherwise known as Aneta Steele, Katrin, Adriana, Denise, Sunny, JeeTee or Cindy Sweet.

_If you want to have her tits that high on her thorax, I suggest a B cup rather than D, it just doesn't look proportional, even with a major uplift corset/vest._

→ Comment thread on “Blenderella” http://blenderartists.org/forum/showthread.php?220600-Blenderella/page3

In a short amount of time, Blenderella/Aneta became the benchmark template for anyone wanting to learn character modelling. In numerous threads on blenderartists.org and other sites, aspiring CG artists report on their progress and exchange detailed advice. In this collaborative process of creating virtual women, Blenderella's anatomic proportions are stretched in rather predictable ways.

### Models

Hanging on to stereotypical representations of desire like cars and girls is boring and limits the playground of F/LOSS to the constraints of proprietary, misogynistic values. A more ambitious approach would be to ask for images that experiment with diverse realities, bending the rules of both gender and software.

Sample images, screen shots and tutorials provide a glimpse of what could be achieved and set the scene for prospective users. While demonstrating what is technically possible, they literally provide a window into the world of a program, and it matters what kind of world is captured.

F/LOSS is playing its part in constructing alternative imaginaries of how people might relate to technology. The potential of these shared and participative software projects lies in negotiating new visions and utopias based on the powerful idea that we not only consume but also construct our coded environment.

--------------

1. Inkscape 0.48 with Ferrari by Gilles Pinard

2. Scribus 1.2 cvs with Renault Roadster by unknown illustrator 

3. Blender 2.5 with Opel GT by Rogério Perdiz

4. Screenshot of Xara Xtreme, version unknown. Viper car image courtesy of José de Jesús Campoy Arce

5. Lamborghini Gallardo by Michael Grosberg

6. Photorealistic car by Konstantin Rotkevich

7. Isabella by Nyxia a.k.a. Angela Guenette (2008, all rights reserved) (left page)

8. Female Warrior Character concept art by David Revoy (2009, CC-by SA) (left page)

9. Blender 2.5 modeling test by Angela Guenette (2009, CC-by SA) (right page)

10. Sintel Stage 2 model, Durian movie project (2009, CC-by SA)  (right page)

11. Sintel, Blender Foundation (2010, CC-by SA) (right page)

12. Object linking, scalingand texture exercisewith Sintel-lite by Becsta(2012, all rights reserved)  (right page)

13. Blenderella, character modelling in Blender 2.5, DVD cover 

14. Screen shots from instruction videos included in Blenderella, character modelling in Blender 2.5, Blender Institute (2010, CC-by SA) 

14a. Preparing reference images in Gimp 

14b. Modelling the torso 

15. Blenderella/Savannah exercise file by tfrank (2011, all rights reserved)

16. Vampirella character based on Blenderella by Esemkay (2011, all rights reserved)


--------------

References:

1.→http://blenderartists.org/forum/showthread.php?144103-blending-life-category-a-voting-closed

2.→ “So let’s add a female/girl as main character!“ From the project targets listed on the Sintel About page http://www.sintel.org/about

3.→ Comment thread on "Sintel Sightings" http://blenderartists.org/forum/showthread.php?171730-Sintel-sightings


--------------

> For a community energized by sharing, mixing and exchange, to identify with such over-engineered proprietary status symbols is bordering on perverse.

> “Guys, if you think the new Sintel is more realistic, warrior-like or even sexy, here's my advice: Stop downloading anime, turn off the computer, get out of the basement and meet some real females. You're in for a big surprise.”


