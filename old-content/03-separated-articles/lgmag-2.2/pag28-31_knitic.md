Showcase

# Knitic

## Varvara Guljajeva and Mar Canet

Knitic is an Open Source knitting machine, controlling a Brother electronic knitting machine via Arduino. To be more precise, Knitic is the new “brain” of a knitting machine. From the electronic part of the original machine we use only the end-of-line sensors, encoder and 16 solenoids. With Knitic, one can knit as long a pattern as desired, as well as modify the pattern on the fly. 

Why are we developing Knitic? Because we feel the knitting machine, as the first automatic domestic fabrication tool, has been totally overlooked in the age of digital fabrication.

http://knitic.com http://varvarag.info http://mcanet.info
