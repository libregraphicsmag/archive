Production Colophon

# Making the switch

## Manufactura Independente

Making the switch is one of the main issues in the Libre Graphics community. 

The “professionals use Adobe” meme is still very prevalent, to the point that the Adobe way of doing things has become, to many, the one way to do serious design work. This myth has historically been emphasized by the prominent presence of Adobe in the graphics software arena.

The magazine you’re currently reading is, we hope, evidence to the contrary. While the process of reframing your mindset for a new tool is rarely straightforward, making the switch to F/LOSS design tools is not only an attitude but a refreshing new view on digital design work, moving away from the Adobe way of doing things.

Specialized GNU/Linux distributions like _Ubuntu Studio_ or the _Fedora Design Suite_ try to fill this gap. However, our impression is that these are closer to the audiovisual and web side of design, and do not devote as much attention to print—and we understand that choice. But it means that there’s still work to be done to show how F/LOSS tools can be a viable alternative to the Adobe suite for print designers.

We find that one of the reasons people around us have trouble switching is that the F/LOSS tool ecosystem is hardly the neat, shrink-wrapped experience that Adobe’s suite provides—and while we think that’s a plus for Free Software tools, it lacks the convenience that people crave. It is true that the scattered nature of the Free tool world makes it desirable, if not necessary, to have some hand-holding through the switching process. It is also true that convenience is the main enemy of principle.

So how can we reduce the effort in migrating to Free Software tools? 

1. Give people easy access to Windows or Mac versions of F/LOSS design tools. Requiring an operating system switch is too drastic a change to ask as a first step (naturally, the goal is to make it clear that it’s a worthy change!).

2. Bring together software packages and their documentation, so that one does not need to hop between sites and software versions.

3. Provide a selection of resources that can be used immediately in those tools: brushes, presets, typefaces, ready-made configurations and examples.

These goals fueled the _Libre Graphics Kit_, a project we’re starting that tries to address these issues. It takes the shape of a physical access point, directly inspired by the “USB dead drops” explored by, among others, media artist Aram Bartholl. Our dead drops are read-only USB devices, accessible in public places, which contain a set of tools and resources that we’ve selected for people who want to try something different.

Providing a physical location to access these goes a long way further than telling people “go to site X and download version Y.” Picking locations such as art schools, hackerspaces and libraries can, we believe, work towards familiarizing people with F/LOSS workflows for print design.

We’re still in our first steps with this project, so we’d certainly appreciate whatever feedback you might have about the idea, especially if you’d be interested in placing a dead drop in your campus, office or local coffee house! We’ll be bringing a dead drop with us to our stand at FOSDEM, so be sure to drop by and see how it works.

--------------

Ana Isabel Carvalho and Ricardo Lafuente make up Manufactura Independente,a design research studio based in Porto, Portugal. Their design practice orbits around the principles of Free Software and Free Culture.

http://manufacturaindependente.org 

