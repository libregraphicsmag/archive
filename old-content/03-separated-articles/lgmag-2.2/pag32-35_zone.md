Showcase

# Zone

## Ele Carpenter

The Embroidered Digital Commons is a collectively stitched version of the Raqs Media Collective’s 2003 text “A Concise lexicon of/for the Digital Commons,” facilitated by Ele Carpenter. The term “Zone” featured here was facilitated by ArtYarn in collaboration with MADLAB for “Analogue is the New Digital,” an exhibition curated by Simon Blackmore and Andrea Zapp, AND Festival, Manchester, 2010. Supported by Arts Council England and Manchester Metropolitan UniversityMU/MIRIAD. 

The Zone Embroiderers are: Charlotte Bacci, Katie Brandon, Jo Burton, Ele Carpenter, Jane Deschner, Dawn Elwell, Rachael Elwell, Jan Willem de Fockert, Sally Fort, Sarah Al Haddad, Lulu Hankin, Alex Hodby, Adamandia Kapsalis, Liz Kenney, Heather Kerr, Marianne Laimer, Silke Lambers, Judy Lambert, Maggie Lister, Marie Pattison, Steph Peters, Carrie Reast, Jen Southern, Carol Taylor, Laura Ward, Rebecca Aimee Lanyon Willmott, Katie.

http://open-source-embroidery.org.uk/EDC.htm
