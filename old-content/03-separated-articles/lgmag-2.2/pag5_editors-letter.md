Editor's letter

# Gender ≠ women

## ginger coons

In this issue, we're talking about gender. We're talking about the place of gender in F/LOSS communities, in the way we do our work, in the way we construct ourselves as artists, designers and developers. Some of the discussion about gender revolves around what it means to be a woman in F/LOSS, or in the wider world of technology. That's an important topic right now, one which a lot of people are talking about. To us, that's one subtopic of the larger gender discussion. In this issue, we take a moment to step beyond the women-in-tech talk and have that larger gender discussion.

Art and gender have a long history together. From ancient Greek sculptures of ideal men to Renaissance nudes, visions of what it is to be a man or woman have been with us for about as long as we’ve been representing our world. Design, often viewed as the commercial edge of art, has had a hand in shaping our attitudes towards gender. In the service of advertising, graphic design has given us visions of emancipated women smoking cigarettes, underwear-peddling men with spectacular abdominal muscles, and smiling families eating soup together. Design and art show us images of men, women and children: ourselves as we should and shouldn’t be.

In the world of Free/Libre Open Source Software, and in the larger world of technology, debate rages over the under-representation of women and the frat house attitude occasionally adopted by developers. The conventional family lives of female tech executives are held up as positive examples of progress in the battle for gender equity. Conversely, pop-cultural representations of male developers are evolving, from socially awkward, pocket-protectored nerds to cosmopolitan geek chic. Both images mask the diversity of styles and gender presentations found in the world of F/LOSS and the larger tech ecology. Those images also mask important discussions about bigger issues: is it okay to construct such a strict dichotomy between “man” and “woman”as concepts; how much is our work still divided along traditional gender lines; is it actually enough to get more women involved in F/LOSS generally, or do we need to push for specific kinds of involvement; do we stop at women, or do we push for a more inclusive understanding of representation?

This issue looks at some of the thornier aspects of gender in F/LOSS art and design. In discussing gendered work, the push for greater and greater inclusion in our communities, and representations of gender in our artistic practices, among others, we hope to add and amplify voices in the discussion.

--------------

ginger coons is a member of the Libre Graphics Editorial team.
