Interview

# The Empowermentors Collective—an interview

Kẏra is the founder of the Empowermentors Collective, a new initiative for multiply-marginalized women and queers of colour interested in Free Software and Free Culture. Kẏra was interviewed by ginger coons during Libre Planet 2013 in Cambridge, Massachusetts.

--------------

**ginger coons ****In starting the Empowermentors initiative, what are you aiming to do?**

Kẏra First and foremost, it's a space. It's a space for intersectionally-marginalized people of colour. So by intersectionally-marginalized, that's some lingo that a lot of people don't understand, but basically, people who hold another marginalized identity, whether they're women or gender queer or trans or a person with disabilities, any of these things. As long as they're also a person of colour. They're welcome to join the group. At its core, I started it as a space for that. Anything that comes out of it is subject to change, but it's for those people in Free Software and Free Culture communities. Things that I personally wanted to work on through it, which if other people don't want to, that'll change, was to serve similar purposes to Linux Chicks or GeekFeminism in documenting these issues and experiences for people. As well as to, instead of focusing on being assimilated and normalized in the movement, actually to create a movement that fights for itself and to approach the issues. We're not trying to just make the broader Free Culture/Free Software movement something that people of colour will feel welcome in. We're trying to introduce a new way of approaching the issues entirely. Fundamentally. So you can't do that just by calling people out. So the name “Empowermentors,” as it implies, is about empowerment, which is what Free Software is supposed to be about, but also mentorship and reaching out to people who are also not normative. Instead of just putting those folks in positions that are speaking positions. People have a community that they can reach out to and join and other people. And feel safe and validated. 

**In terms of this focus on intersectionality and people who are multiply marginalized, some people would say that you're drastically cutting down...**

Who we can reach out to? It's such a small niche? 

**Yeah. At the moment there aren't a lot of people who are interested in Free Software and Free Culture who would fit into...**

We're not trying to be big. This is for us. This is not for the broader movement to feel better. This is providing a space for the people that need it. If you don't need it, that's really great for you, but other people do. And this speaks to all the people who, when the group was announced, were like “Oh, I'm a white man, but I feel a sort of kinship with all these people and I wish I could be included.” The rest of the community already includes you. You have your space. It really says something when people don't recognize that, when the tables are turned, there's an imbalance there. 

**I guess what I'm getting at here is this question of: All right. Makes sense. Draw your boundaries. You exist to serve a specific constituency. Sensible. But a lot of queer organizations do “and allies.” What's your stance on...**

“and allies”

**Yeah. Because it's a contentious thing. **

Right now, it seems like the allies outnumber us. Not only in numbers, but in the space that individuals take up. Which is a big problem and really upsetting to have happen. So I don't know how that will go. I don't know how it'll turn out. It'll probably always be an ongoing discussion. But it is an issue, definitely. **About the meritocratic ideals ****of Free Software, which do so much work to make invisible every other issue by saying “oh no, look, ****we give people a chance.” ****Our judgements are based on this abstract technical thing that we say we can quantify, not based on their ability to fit into a community. How do you feel about ****the meritocratic blind?**

I think it's a huge problem. It's a homogeneous community that somehow manages to ignore the fact that most of the people who voluntarily come forward and are able to not only reach the community to get involved but to stay there once they've gotten there are mostly men. So we therefore have to attribute these differences to inherent differences in people that have nothing to do with the way the Free Software community behaves, or who's representing it or any of those issues. What I assume most people think is that decentralization comes first and then after that, diversity and these other problems will just trickle down out of existence. And [they] don't realize that there are other problems that don't depend on centralization and that, in fact, decentralization and networks can be far more oppressive than top-down organizations because of the fact that they're so culturally ingrained in whatever community it is that holds the values that keep people out. 

**More women are being explicitly included in f/loss communities, ****but we can probably take it for granted that almost every woman who fetches up at any kind of public software thing or hacker thing or f/loss thing will at some point be harassed or insulted in some way...**

There's that external push [to include women] that's popular right now. And the people who are making these decisions are still the same people, making a space or making an effort to include women. But that discussion hasn't really started about race. Or other lines. Sexuality or non-binary genders. We're only including these different identities that already agree with our message. I think that closed spaces, specifically, are really important to not compromise on any Free Software ideals, but to approach Free Software ideals from a completely different place. And that's really important for the greater Free Software community because then they can form coalitions on advocating for user freedom and software freedom and also learn from the mistakes that they've had in the past. And I think there's something that we can't achieve simply by trying to reach out more or include other people more because you're only assimilating and you're not changing yourself at all. You're changing yourself superficially. 

I would love for Empowermentors to provide a space specifically for intersectionally-marginalized people of colour because I think that issues of gender and sexuality and class are also important but I think that race is a really not often talked about issue in the Free Culture, Free Software community. I'm hoping that Empowermentors can help people be intentional about their politics. That's definitely the huge thing for me. I guess considering not only the behaviour within the community, but software itself and considering the ways that software can potentially be racist. Like how might software exclude certain people without having explicit rules to do so.

The way that Free Software is framed for most people is individual user empowerment, the fact that we talk about Free Software and not user freedom. Software Freedom kind of implies that the thing that needs to be free is the software, not the user. We're taking the ideals and removing them from bodies and applying them to objects. And so a lot of the emphasis on advocacy tends to be on how we can have the most powerful players take our side and that's often businesses and that's often people who don't want to recognize these issues. And it's often not the people who are working towards social justice but just the people who are working to commoditize free software and turn it into something profitable, which in the end will always undermine the goals of user empowerment.

**So just in terms of putting this in the context of the larger discussion of the inclusion of minorities in Free Software: What do you want to see from Empowermentors? ****Do you want to see more people ****of multiple marginalizations ****in Free Software? Do you want ****to see better support for the people who are there?**

Probably. That's probably one thing that is not a goal at all. But I would see as a natural thing that would happen, alongside the things that I would like us to do. There's no “we've won.” There's no “we did it.” There's no “all intersectionally-marginalized people are using Free Software” or something. There's nothing like that. At its core, it's functioning as a space for people. So the desires and issues of the people it serves are always going to be what matters. For me, I would really like to see us create or write and produce materials that explain Free Software and Free Culture from our perspective. And in doing such, form coalitions with other maybe anti-racist or anti-sexist organizations about issues of media ownership and software ownership and all those things. So that's something I'm interested in. I would love to start making tools by and for us. One of the activities I added to the wiki was to document and work on bugs in Free Software projects in general. And I know that sounds like it's doing that changing the broader Free Software thing, and maybe it is, but software is still infinitely reproducible and has an impact. And one of the bugs that I saw was in this game called _0 __a.d_. which is like a _Civilization_ clone. And there are characters in the game that, if you click on them, their information thing is that they're women and one of their main properties is to make the men work harder, just by their nearby existence. So you can see how things need to change. And calling out how software embeds the views of the people that make it. And how different ideals are encoded into software. 

**If the property of that little woman character in that game is to make men work harder, is that something that we can legitimately say we need to fix? Can we say that that game can't have that property?**

Should they have the freedom to make that game the way they want to? Because it's for them? What I'm hearing is this “oh, but that's like reverse sexism” or “oh, that's reverse racism” or “everyone should have their space.” Actually, no. We can call out the people who are dominant, who have all the space. 



**But people are going to say to you “why are you stepping on my software? You can make your own.” Once you get your inclusive software, why can't they have their non-inclusive software?**

And we'll respond to that when they say it. We'll try to. But our answer, hopefully, will not be catered towards appeasing them but towards helping other people like us who might understand the problem and maybe join us. Our goal isn't to appeal to the people in power and be like “hey, make us legitimate.” That concern is definitely going to come up again and again and again. And it sucks and it's going to be annoying. But I don't think it's a show stopper, because it's always been a problem. There are lots of groups that keep on keeping on.

http://empowermentors.org

--------------

> The way that Free Software is framed for most people is individual user empowerment, the fact that we talk about Free Software and not user freedom. Software Freedom kind of implies that the thing that needs to be free is the software, not the user. We're taking the ideals and removing them from bodies and applying them to objects.

> [O]ne of the bugs that I saw was in this game called 0 A.D. which is like a Civilization clone. And there are characters in the game that, if you click on them, their information thing is that they're women and one of their main properties is to make the men work harder, just by their nearby existence.
