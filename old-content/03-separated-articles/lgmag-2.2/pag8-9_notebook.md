# LibrePlanet 2013

--------------

## Notebook

--------------

### Where

LibrePlanet 2013, Cambridge, Massachusetts


### What

A two day conference, organized by the Free Software Foundation, exploring the diverse meanings of software freedom, as they apply to different people and contexts.


### Most wanted

A 3D printer from Lulzbot was on show, printing out the MediaGoblin mascot and characters from Blender Foundation movies. Based in the popular RepRap community, Lulzbot has produced the first 3D printer to receive the Free Software Foundation's “Respects Your Freedom” hardware certification.


### Best point

Leslie Hawthorn, channeling her years of experience as a community manager, offered an opening talk on negotiation theory. One of the key takeaways? There's a difference between being honest and being mean. 


### Best dinner

The day before the conference began, more than twenty people gathered for the Women in Free Software dinner. Coming from a diverse collection of projects, locales and roles, those present at the dinner offered proof that women in F/LOSS are both many and active.
