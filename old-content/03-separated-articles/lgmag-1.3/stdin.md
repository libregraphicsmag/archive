<stdin>

Alexandre Leray, Stéphanie Vilayphiou



There seems to a be a tendency nowadays for collaboration and cross-disciplinarity, and graphic designers are not at rest. Many designers — including ourselves — are increasivelly working together with artists, thinkers or engineers, blurring the separation between the disciplines. Why? Could it be a way to escape from the division of labour? A way to escape from the thinking of graphic design as a service industry and to start thinking of design as an embedded process? Our intuition is that graphic designers don't want to be the last element of the production line anymore.

Coming from a classical visual design education we became more and more interested in digital culture and networked media. Now, we mix a visual approach with programming to create designs for print and non-print outputs. We proudly claim the two hats of designer and programmer because for us programming is also design. Moreover, we think programs are cultural items, at least as much as they are functional. F/loss carries this thought, but using it doesn't necessarily imply free culture. This is why we focus our personal researches on collective platforms based on f/loss. Sharing is not only about giving, it's also about getting back, it's about starting a discussion.



http://stdin.fr

--------------

Rémy Jacquier

Catalogue of French artist Rémy Jacquier.  software   Indesign 

fonts   CourierSans

content   © Rémy Jacquier   © ADERA

--------------

Éditions B42   http://editions-b42.com/

Website of Éditions B42 publishing house.

software   git   python   django   HTML

                 jquery   mysql

fonts   Verdana   Hermès Sans

content   © Éditions B42

--------------

Brainch   http://brainch.stdin.fr/

Collective writing application.

software   git   python   git-python   

                 django   jquery   sqlite

fonts   Liberation Sans

content   Free Art License

source code   GNU AGPL

http://code.dyne.org/?r=brainch

--------------

cataloged   http://cataloged.cc/

Online portfolio, also generating a printed one, of graphic designers Coline Sunier and Charles Mazé.

with   Coline Sunier   Charles Mazé

software   mercurial   python   django

                  jquery   mysql   reportlab   

fonts   Inconsolata

content   ?

--------------

Constant

Flyer for Constant, association for art and media in Brussels.

software   Scribus

fonts  NotCourierSans Constant Archive

content   Free Art Licence

--------------

Datateb

http://datateb.alexandreleray.com/

Art piece mocking the software industry through recycle bins. 

software   AppleScript   python   django

fonts   Verdana

content   Free Art License

source code   GNU GPL   http://github.com/aleray/datateb/

--------------

else if   http://else-if.net/

Collections of critical texts on graphic design and digital media. 

software   git   python   django   jquery

                 sqlite   close-commenting

fonts   Helvetica

content   free licences   public domain   

               fair use

source code   GNU AGPL

--------------

ERBA Valence 2009—2011

Information leaflet for Valence school of Fine Art and Design. 

software   Indesign   GraphViz

fonts   Nobel

content   ?

--------------

Google Will Feed Itself

http://googlewillfeeditself.blogspot.com/

A Google Blogspot fed with its own Google ads. 

software   BlogSpot   GoogleAdsense

content   ?

--------------

Libre Graphics Meeting 2010

http://libregraphicsmeeting/2010/

Website for LGM 2010 in Brussels.

with   Alessandro Rimoldi   OSP

software   Inkscape   HTML   anwiki

fonts   Cantarell

content   ?

--------------

ISSUE MAGAZINE 

http://www.issue-magazine.net/

Online critical magazine on graphic and media design. 

software   python   HTML   jquery

fonts   Verdana 

content   CC-BY-SA-NC   fair use

--------------

Disappearance

A word can only live once.

with   Marijke Schalken

software   python  mplayer

source code   Free Art Licence

http://git.constantvzw.org/

--------------

Blind Carbon Copy    http://bcc.stdin.fr/

Experimental design hacks to circumvent “Intellectual Property”.

software   python   BeautifulSoup  nltk  

                 Indesign   javascript   

fonts   New Caledonia   Vogue

content   all wrongs reversed

source code   GNU GPL

--------------

acsr   http://acsr.be/

Atelier de Création Sonore Radiophonique sound repository. 

with   OSP (Ludi)   Jérôme Degive

software   Gimp   Inkscape   WordPress

fonts   Univers Else

content © right-owners

--------------

please computer | make me design

Workshop on fun commandline poster generation. 

with   OSP (Ludi, Ivan)

software   git   GNU coreutils   enscript podofoimpose   

fonts   from OSP foundry

content   Free Art Licence

source code   GNU GPL

http://git.constantvzw.org/

--------------

Schaarbeekse Taal

Flyers for events organized by the Schaarbeekse Taal project.

with   OSP (Ludi)

software   git   Gimp   Inkscape

fonts   Limousine   Alfabet III

content   © right owners

--------------

Le Chant des Particules

Website of a movie about LHC-CERN.

with   deValence

software   git   python   django   HTML

                 jquery

fonts   ChicaGogoRGB

content   ?

--------------

2008

--------------

2009

--------------

2010

--------------

2011

--------------

Curating as Environ-mentalism

http://environmentalism.stdin.fr/

Online interface to comment chunks of Elke van Campenhout's essay.

software   git   couchdb   HTML   jquery

fonts   Ume Mincho

content  All wrongs reversed

source code   GNU GPL
