Resource list 1.3

--------------

GIMP

A raster based image editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Inkscape

A vector graphics editor for gnu/Linux, Mac os x and Microsoft Windows.

--------------

Toonloop

A f/loss stop motion animation program.



Scribus

A desktop publishing program for gnu/Linux, Mac os x and Microsoft Windows. 

--------------
