# Cover process

## Thibaut Hofer & Morgan Fortems sum up the process that ended up with the cover.

As co-workers, we needed to gather ideas in a straightforward way, beyond just notes and emails. Pickpic was good for this, because it allowed us to upload pictures while chatting. We met this way on April 21 and 27, then again on May 3. Here are some excerpts, according to the tracks we followed, from conceptual to technical questions.

10:09 mib_x9vil1 we could do this way: visual research - discussion - first draft - discussion - second draft - adjustments.

10:11 loredana ah monsters with many eyes! cool!

10:13 thibaut Because on the one hand, we have multiple subjects, and on the other we have one entity with somehow multiple personalities

10:17 thibaut A blob, says morgan

10:20 thibaut This is kind of horrible but collaboration could be a “blob” in the sense that when collaborating, 1 and 1 are 3, not 2

10:41 mib_x9vil1 another track might be, on a more general level, the idea of multiple things being something else when all together...

15:20 lori let's divide the mitosis example in two concepts: mitosis and multiple visualization

15:22 mib_f3yalx yes, that's what I was still thinking about... maybe eyes could work as a module for the mitosis pattern...

15:28 mm collaboration is a more complex and free process than a simple addition

15:30 lori could we say we restricted to: eyes, mitosis, blob?

16:01 mib_f3yalx randomly constructed/complex/organic pattern.

16:03 mib_f3yalx I would say that the "module" of the pattern could be either geometric or organic

10:02 morgan maybe, but the final result is more consensual than the two others

10:03 thibaut I definitely love the eyes made with Python, Lori too, both of you too. Does anyone has something esle to say?

10:14 sprog yes... and also you can play a bit with the script if you feel like... changing stuff is quite easy if you don't want to change the shape

10:16 thibaut I think the proportions are good. From far, it really looks like a cell pattern, and close, it appears that the idea of multi viz is pregnant

10:26 thibaut Then, do we need type on our cover?

10:27 sprog do we? :)

--------------

April 21

Keywords: Creation process, many eyes, blob, multiple visualiza-tion, 1 and 1 are 3, copy-paste

--------------

April 27

Keywords: Cell mitosis, many eyes, pattern, Bremen musicians

--------------

may 3

Keywords: Cell pattern, many eyes, relevancy > consensus, multiple visualization, script, type
