--------------

New releases

--------------

Pinpoint 0.1.2

http://live.gnome.org/Pinpoint

A tool for simply creating beautiful presentations.

--------------

Mediagoblin

http://mediagoblin.org

A newly-announced, in-progress decentralised media hosting platform. Alpha release currently slated for October 2011.

--------------

Vips 7.24

http://www.vips.ecs.soton.ac.uk

A library and interface for quickly and efficiently manipulating large images.

--------------

Open Font Library and Open Clip Art Library

http://www.openfontlibrary.org • http://www.openclipart.org

Relaunched, with lots of improved functionality.


LightTwist lt-align

http://vision3d.iro.umontreal.ca/en/blog/2011/05/11/easy-multi-projector-desktop

The debut of convenient and handy tool for turning multiple projectors into one giant projector, an accessible addition to the larger LightTwist project.

--------------

Toonloop 2.0.0

http://toonloop.com

Fun and powerful stop motion animation software. Excellent for performance.

--------------

Linux Mint 11

http://www.linuxmint.com

The latest from the second most popular distribution of gnu/Linux. Version 11 makes software management and installation of extras easier than ever.

--------------

Fedora 15

http://fedoraproject.org

Version 15 makes Fedora (our favourite) the first distribution of gnu/Linux to include gnome 3 as its default desktop environment.

--------------

GNOME 3

http://gnome3.org

An attractive, new take on that old favourite: the gnome desktop environment.
