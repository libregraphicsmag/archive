Masthead

Editor's Letter 1

Editor's Letter 2

Production Colophon

New Releases

Upcoming Events

Dave Crossland went to Argentina Dave Crossland

The voice of the shell—in collaboration with my computer Eric Schrijver

In practise RestructWeb

Breaking into f/loss Morgan Fortems

Notebook

Isn't Open Clip Art Library handy?

Showcase

    FF3300

    Lafkon Bash scripts for generative posters

    Makemake Open source recipe for organic logos

    Parcodiyellowstone Digital Dump and Pickpic

    Stdin

Libre Graphics magazine issue 1.4: The Physical, the Digital and the Designer

Managing artist communities: the casefor Ubuntu Artists Martin Owens

SparkleShare: pleasantly invisible version control ginger coons

Parallel School—an interview Thibaut Hofer
