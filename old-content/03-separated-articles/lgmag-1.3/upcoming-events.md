We're very pleased to present a calendar of upcoming events which encompass all things graphic design, media art and F/LOSS. Given that there are few events which tackle all three subjects, we aim to offer you events where you can be the agent of change: the F/LOSS designer at a traditional design event, or maybe the designer at a predominantly software developer event. 

--------------

16 SEP

--------------

Brand New Conference

San Francisco, United States

http://www.underconsideration.com/brandnewconference

--------------

17-25

SEP

--------------

The London Design Festival

London, United Kingdom

http://www.londondesignfestival.com

--------------

24-25 

SEP

--------------

PyCon UK 2011

Coventry, United Kingdom

http://pyconuk.org

--------------

1-18

OCT

--------------

Phoenix Design Week

Phoenix, United States

http://www.phxdw.com

--------------

13-16

OCT

--------------

Pivot: AIGA Design Conference 2011

Phoenix, United States

http://designconference2011.aiga.org

--------------

13-23

OCT

--------------

Design Philadelphia

Philadelphia, United States

http://www.designphiladelphia.org

--------------

15-16

OCT

--------------

West Coast HackMeet

San Francisco, United States

http://hackmeet.org

--------------

17-20

OCT

--------------

SVG Open

Cambridge, Massachusetts

http://www.svgopen.org/2011

--------------

19-21 OCT

--------------

LatinoWare 

Foz do Iguaçu, Brazil

http://www.latinoware.org

--------------

19-22 OCT

--------------

Access 2011 

Vancouver, Canada

http://access2011.library.ubc.ca

--------------

20-22 OCT

--------------

Typo London 2011

London, United Kingdom

http://www.typolondon.com

--------------

31 OCT

4 NOV

--------------

Ubuntu Developer Summit

Orlando, United States

http://uds.ubuntu.com

--------------

2-3 NOV

--------------

DesignThinkers 2011

Toronto, Canada

http://www.designthinkers.com

--------------

4-6 NOV

--------------

Mozilla Festival 2011

London, United Kingdom

http://www.svgopen.org/2011

--------------











--------------

ATypI 2011

Reykjavik, Iceland

http://www.atypi.org/2011-reykjavik

--------------
